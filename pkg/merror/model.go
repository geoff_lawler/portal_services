package merror

import (
	"errors"

	merr "gitlab.com/mergetb/mcc/pkg/errors"
)

var (
	ErrMxCompileError    = errors.New("MX failed to compile")
	ErrReticulationError = errors.New("Reticulation Error")
	ErrCheckerError      = errors.New("Checker Error")
)

func MxCompileError(errStack string) error {
	me := NewMergeError(ErrMxCompileError)
	me.Detail = errStack
	me.Type = "https://mergetb.org/errors/mx-failed-to-compile"

	return me
}

func ModelReticulationError(err error) error {

	me := NewMergeError(ErrReticulationError)
	me.Detail = err.Error()

	if errors.Is(err, merr.ErrNoAddressOnEndpoint) {
		me.Type = "https://mergetb.org/errors/routes-need-addresses"
		me.Err = err
	} else if errors.Is(err, merr.ErrDuplicateSubnets) {
		me.Type = "https://mergetb.org/errors/duplicate-subnets-in-addressing"
		me.Err = err
	} else if errors.Is(err, merr.ErrMultipleLinkSubnets) {
		me.Type = "https://mergetb.org/errors/multiple-link-subnets-in-addressing"
		me.Err = err
	}

	return me
}

func ModelCheckerError(err error) error {
	me := NewMergeError(ErrCheckerError)
	me.Detail = err.Error()

	if errors.Is(err, merr.ErrNoAddressOnEndpoint) {
		me.Type = "https://mergetb.org/errors/routes-need-addresses"
		me.Err = err
	} else if errors.Is(err, merr.ErrDuplicateSubnets) {
		me.Type = "https://mergetb.org/errors/duplicate-subnets-in-addressing"
		me.Err = err
	} else if errors.Is(err, merr.ErrMultipleLinkSubnets) {
		me.Type = "https://mergetb.org/errors/multiple-link-subnets-in-addressing"
		me.Err = err
	}

	return me
}
