package merror

import (
	"errors"
	"strings"
)

var (
	// Just wrap kratos errors in this so thye can be parsed as merge errors
	ErrIdentityError = errors.New("Identity Error")
)

func IdentityError(messages []string) *MergeError {
	e := NewMergeError(ErrIdentityError)
	e.Detail = strings.Join(messages, "\n")
	return e
}
