package merror

import (
	"encoding/json"
	"errors"
	"fmt"

	epb "google.golang.org/genproto/googleapis/rpc/errdetails"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	log "github.com/sirupsen/logrus"
)

// ToString "translates" the error to a pretty printed string suitable
// for dumping to stdout/stderr.
func (me *MergeError) String() string {
	return me.stringwithsep("\n")
}

//
// Error conversions: from error, from/to GRPC, to API (models.Error)
//
/*
func (me *MergeError) ModelError() *models.Error {

	return &models.Error{
		Type:      me.Type,
		Title:     me.Title,
		Detail:    me.Detail,
		Instance:  me.Instance,
		Evidence:  me.Evidence,
		Timestamp: me.Timestamp,
	}
}
*/

// ToMergeError - Given an arbitrary error, convert to MergeError
func ToMergeError(err error) *MergeError {
	if err != nil {
		err = fmt.Errorf("%w", err)
	}
	me := NewMergeError(err)
	me.Detail = "Uncategorized"
	me.Type = fmt.Sprintf("https://mergetb.org/errors/uncategorized")
	return me
}

// FromModelError - convert from Merge API error to MergeError
func FromModelError(e []byte) (*MergeError, error) {

	me := &MergeError{}
	err := json.Unmarshal(e, me)
	if err != nil {
		return nil, err
	}
	me.Err = rebuildError(me.Title)

	return me, nil
}

func ToGRPCError(e error) error {

	var me *MergeError
	if !errors.As(e, &me) {
		log.Infof("NON MERGE ERROR in ToGRPCError: %v", e)
		gerr, ok := status.FromError(e)
		if ok {
			if e == nil {
				// Hmm.
				return ToGRPCError(UncategorizedError("nil error", nil))
			}
			return ToGRPCError(UncategorizedError("Unknown", e))
		}
		return ToGRPCError(UncategorizedError(gerr.Message(), e))
	}

	code := codes.Unknown

	switch me.Err {
	case nil:
		code = codes.Unknown
	case ErrBadRequest:
		code = codes.InvalidArgument // GTL Wrong code.
	case ErrUnauthorized:
		code = codes.Unauthenticated
	case ErrForbidden:
		code = codes.PermissionDenied
	case ErrUserInactive:
		code = codes.FailedPrecondition
	case ErrAlreadyExists:
		code = codes.AlreadyExists
	case ErrInternal:
		code = codes.Internal
	case ErrNotFound:
		code = codes.NotFound
	case ErrMxCompileError:
		code = codes.InvalidArgument
	case ErrReticulationError:
		code = codes.InvalidArgument
	case ErrCheckerError:
		code = codes.InvalidArgument
	case ErrEmptyExperiment:
		code = codes.FailedPrecondition
	case ErrEmptyTestbed:
		code = codes.FailedPrecondition
	case ErrRlzBadXir:
		code = codes.InvalidArgument
	case ErrRlzInternal:
		code = codes.Internal
	case ErrFailedToRealize:
		code = codes.FailedPrecondition
	case ErrRealizeInProgress:
		code = codes.FailedPrecondition
	case ErrMaterializeInProgress:
		code = codes.FailedPrecondition
	case ErrBadRealizeAction:
		code = codes.InvalidArgument
	case ErrNoProjectResources:
		code = codes.FailedPrecondition
	}

	st := status.New(code, me.Title)

	errStr := ""
	if me.Err != nil {
		errStr = me.Err.Error()
	}

	detSt, err := st.WithDetails(&epb.BadRequest{
		FieldViolations: []*epb.BadRequest_FieldViolation{
			{Field: "Type", Description: me.Type},
			{Field: "Title", Description: me.Title},
			{Field: "Detail", Description: me.Detail},
			{Field: "Instance", Description: me.Instance},
			{Field: "Evidence", Description: me.Evidence},
			{Field: "Timestamp", Description: me.Timestamp},
			{Field: "Err", Description: errStr},
		},
	})

	if err != nil {
		return st.Err()
	}

	return detSt.Err()
}

func FromGRPCError(e error) *MergeError {

	me := &MergeError{}
	st := status.Convert(e)
	deets := st.Details()
	if len(deets) > 0 {
		if deet, ok := st.Details()[0].(*epb.BadRequest); ok {
			me.fromStatusDetails(deet.GetFieldViolations())
			return me
		}
	}
	return ToMergeError(e)
}

func (me *MergeError) fromStatusDetails(fields []*epb.BadRequest_FieldViolation) bool {

	for _, f := range fields {

		switch f.Field {
		case "Type":
			me.Type = f.Description
		case "Title":
			me.Title = f.Description
		case "Detail":
			me.Detail = f.Description
		case "Instance":
			me.Instance = f.Description
		case "Evidence":
			me.Evidence = f.Description
		case "Timestamp":
			me.Timestamp = f.Description
		case "Err":
			me.Err = rebuildError(f.Description)
		}
	}
	return true
}

func rebuildError(title string) error {

	// This is pretty awkward.
	// *every* error we support must be listed here.

	switch title {
	// Common Errors
	case ErrBadRequest.Error():
		return ErrBadRequest
	case ErrUnauthorized.Error():
		return ErrUnauthorized
	case ErrForbidden.Error():
		return ErrForbidden
	case ErrNotFound.Error():
		return ErrNotFound
	case ErrAlreadyExists.Error():
		return ErrAlreadyExists
	case ErrInternal.Error():
		return ErrInternal

	// Custom errors.
	// -----------------------------------------
	// Realization
	case ErrEmptyExperiment.Error():
		return ErrEmptyExperiment
	case ErrEmptyTestbed.Error():
		return ErrEmptyTestbed
	case ErrFailedToRealize.Error():
		return ErrFailedToRealize

	case ErrBadRealizeAction.Error():
		return ErrBadRealizeAction
	case ErrRlzBadXir.Error():
		return ErrRlzBadXir
	case ErrRlzInternal.Error():
		return ErrRlzInternal

	case ErrNoProjectResources.Error():
		return ErrNoProjectResources

	// Model
	case ErrMxCompileError.Error():
		return ErrMxCompileError

	case ErrReticulationError.Error():
		return ErrReticulationError

	case ErrIdentityError.Error():
		return ErrIdentityError

	// Default is an unknown or uncategorized error.
	default:
		return errors.New(title)
	}
}
