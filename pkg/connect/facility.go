package connect

import (
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"strings"

	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"

	facility "gitlab.com/mergetb/api/facility/v1/go"
	"gitlab.com/mergetb/portal/services/pkg/storage"
)

func facilityGrpcConn(name string) (*grpc.ClientConn, error) {

	f := storage.NewFacility(name)
	err := f.Read()
	if err != nil {
		return nil, fmt.Errorf("facility read: %+v", err)
	}

	tlsConf := &tls.Config{InsecureSkipVerify: false}

	if f.Certificate != "" { // we have a cert, so use it.
		log.Debugf("Loading cert for %s", f.Name)
		capool := x509.NewCertPool()
		if ok := capool.AppendCertsFromPEM([]byte(f.Certificate)); !ok {
			return nil, fmt.Errorf("Unable to load %s CA cert", f.Name)
		}
		tlsConf.RootCAs = capool

	} else {
		log.Warnf("No facility cert found for %s. Using system CA certs", f.Name)
	}

	creds := credentials.NewTLS(tlsConf)

	addr := fmt.Sprintf("%s:6001", f.Address)
	log.Debugf("dialing %s", addr)

	conn, err := grpc.Dial(addr, grpc.WithTransportCredentials(creds))
	if err != nil {
		log.WithFields(log.Fields{
			"addr": name,
			"err":  err,
		}).Error("bad connect")
		return nil, fmt.Errorf("facility dial: %s %v", f.Address, err)
	}

	return conn, nil
}

func FacilityClient(name string, f func(facility.FacilityClient) error) error {

	conn, err := facilityGrpcConn(name)
	if err != nil {
		return fmt.Errorf("facility connect: %s", err)
	}
	defer conn.Close()

	cli := facility.NewFacilityClient(conn)
	return f(cli)
}


func FacilityWGClient(ep string, f func(facility.WireguardClient) error) error {

	// the ep passed here is the endpoint of the wiregaurd interface. We strip off the
	// port and replace it with the port of the facility apiserver so as to talk to it.
	name := strings.Split(ep, ":")[0]

	conn, err := facilityGrpcConn(name)
	if err != nil {
		return fmt.Errorf("facility connect: %s", err)
	}
	defer conn.Close()

	cli := facility.NewWireguardClient(conn)
	return f(cli)
}
