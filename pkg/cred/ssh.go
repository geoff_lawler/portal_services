package cred

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"

	"golang.org/x/crypto/ssh"

	log "github.com/sirupsen/logrus"
)

// GenerateSSHKeyPair - generate a public private RSA keypair
// suitable for use with SSH. Return values are encoded and ready
// to write to files in ~/.ssh.
func GenerateSSHKeyPair(id string) (string, string, error) {

	log.Infof("[%s] generating keys", id)

	privKey, err := rsa.GenerateKey(rand.Reader, 4096)
	if err != nil {
		log.Errorf("rss gen key: %+v", err)
		return "", "", err
	}

	err = privKey.Validate()
	if err != nil {
		log.Errorf("priv key validate: %+v", err)
		return "", "", err
	}

	pubKey, err := ssh.NewPublicKey(privKey.Public())
	if err != nil {
		log.Errorf("new pub key: %+v", err)
		return "", "", err
	}

	pub := ssh.MarshalAuthorizedKey(pubKey)

	priv := pem.EncodeToMemory(
		&pem.Block{
			Type:    "RSA PRIVATE KEY",
			Headers: nil,
			Bytes:   x509.MarshalPKCS1PrivateKey(privKey),
		},
	)
	return string(pub), string(priv), nil
}
