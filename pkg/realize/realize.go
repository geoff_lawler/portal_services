package realize

import (
	"fmt"
	"strings"

	"github.com/gogo/status"
	// "github.com/gostaticanalysis/nilerr"
	log "github.com/sirupsen/logrus"

	portal "gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/portal/services/internal"
	"gitlab.com/mergetb/portal/services/pkg/realize/sfe"
	"gitlab.com/mergetb/portal/services/pkg/storage"
	xir "gitlab.com/mergetb/xir/v0.3/go"
	ts "google.golang.org/protobuf/types/known/timestamppb"

	"google.golang.org/grpc/codes"
)

type RealizeParameters struct {
	Mzid    string
	Hash    string
	Creator string
	Vid     uint32
}

func Realize(
	tbroot *xir.Device,
	tbx, xp *xir.Topology,
	a *portal.AllocationTable,
	resourcePool *portal.Pool,
	params RealizeParameters,
	exparams *xir.ExperimentParameters,
) (*portal.Realization, portal.Diagnostics, error) {

	mz, err := internal.MzidFromString(params.Mzid)
	if err != nil {
		return nil, nil, err
	}

	// If the user has specified the network emulation backend to use in the experiment model, pull it out
	emu := exparams.GetEmulation().GetValue()

	embedding, diagnostics, err := sfe.Embed(
		tbroot,
		tbx,
		xp,
		a,
		resourcePool,
		params.Mzid,
		emu,
	)

	// Don't kill on error
	// We won't be able to materialize it anyways (there's a check for it),
	// but it's useful debugging information to see how far it got before it died.

	if err != nil {
		diagnostics = append(diagnostics, sfe.DiagnosticErrorf("embedding error: %+v", err))
	}

	/* XXX
	ie, err := ConfigureInfranet(tbx, embedding, params)
	if err != nil {
		log.Error(err)
		return nil, nil, err
	}
	*/

	var rlz *portal.Realization
	var err2 error

	// don't translate and allocate vsets on error
	if !diagnostics.Error() {
		rlz, err2 = EmbeddingToRz(embedding, true)

		if err2 != nil {
			diagnostics = append(diagnostics, sfe.DiagnosticErrorf("embedding to rz error, maplinkids = true: %+v", err2))
			rlz = nil
		}
	}

	// this will happen if diagnostics had an error or
	// if there was an error calling EmbeddingToRz(embedding, true)
	if diagnostics.Error() && rlz == nil {
		rlz, err2 = EmbeddingToRz(embedding, false)

		if err2 != nil {
			diagnostics = append(diagnostics, sfe.DiagnosticErrorf("embedding to rz error, maplinkids = false: %+v", err2))
			return nil, diagnostics, err2
		}
	}

	rlz.Id = mz.Rid
	rlz.Eid = mz.Eid
	rlz.Pid = mz.Pid
	rlz.Xhash = params.Hash
	rlz.Creator = params.Creator
	rlz.Created = ts.Now()

	if diagnostics.Error() {
		return rlz, diagnostics, fmt.Errorf(diagnostics.ToString())
	}

	if !diagnostics.Error() && err == nil {
		rlz.Complete = true
	}

	return rlz, diagnostics, err
}

func Relinquish(rlz *portal.Realization) error {

	var lzs []*portal.LinkRealization

	for _, x := range rlz.Links {
		lzs = append(lzs, x)
	}

	return freeLinkIds(lzs)
}

func EmbeddingToRz(embedding *sfe.Embedding, maplinkids bool) (*portal.Realization, error) {

	// map proxy link segment identifiers calculated during embedding into VIDs and VNIs
	// that are actually available on the underlying hosts and switches
	if maplinkids {
		err := mapLinkIds(embedding.Nodes, embedding.Links)
		if err != nil {
			return nil, err
		}
	}

	rlz := &portal.Realization{
		Nodes:    make(map[string]*portal.NodeRealization),
		Links:    make(map[string]*portal.LinkRealization),
		Infranet: new(portal.InfranetEmbedding),
	}

	// build the Link Emulation struct
	if embedding.LinkEmulation != nil {
		emu := &portal.LinkEmulationParams{Backend: embedding.LinkEmulation.Backend}
		for _, lnk := range embedding.LinkEmulation.Links {
			for _, iface := range lnk.Interfaces {
				// get the final vtep name from the endpoint on the emu server
				lnk.Link.Interfaces = append(lnk.Link.Interfaces, iface.GetName())
			}
			emu.Links = append(emu.Links, lnk.Link)
		}

		rlz.LinkEmulation = emu
	}

	for node, embedding := range embedding.Nodes {
		rlz.Nodes[node] = &portal.NodeRealization{
			Node:         embedding.Node,
			Resource:     embedding.Resource.Resource(),
			Facility:     embedding.Resource.Data.(*xir.Resource).Facility,
			Kind:         embedding.Kind,
			InfranetAddr: embedding.InfranetAddr,
			VmAlloc:      embedding.VmAlloc,
		}
	}
	for link, lxpa := range embedding.Links {
		lbl := link.Label()
		if lbl == "" {
			lbl = lxpa.Link.Id
		}
		rlz.Links[lbl] = lxpa
	}

	rlz.Infranet.InfrapodServer = embedding.InfrapodServer
	rlz.Infranet.InfrapodSite = embedding.InfrapodSite

	return rlz, nil

}

func GetResources() ([]*portal.Resource, error) {

	// final result
	result := []*portal.Resource{}

	// maintain a mapping of mzid to realizationresult to prevent looking up the same realization
	// more than once
	rlzMap := make(map[string]*portal.RealizationResult)

	net, err := BuildResourceInternet()
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	aTable, err := ReadTable()
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	for _, dev := range net.Devices {

		res := dev.Resource()
		rAlloc := []*portal.Allocation{}

		if res.HasAllocMode(xir.AllocMode_NoAlloc) {
			continue
		}

		if !res.HasRole(xir.Role_TbNode) && !res.HasRole(xir.Role_Hypervisor) {
			continue
		}

		if ral, ok := aTable.Resource[dev.Id()]; ok {

			// We need to determine whether each allocation is for a virtual
			// machine or bare-metal node. This can be done in several ways:
			//
			// 1) Use the 'Virtual' flag in xir.ResourceAllocation embedded in
			// the allocation table. If 'Virtual' is true, then the allocation
			// is for a VM. If it is false, it is still possible the allocation
			// is for a VM, as the realization may have predated the existence
			// of the 'Virtual' flag in the ResourceAllocation structure definition
			//
			// 2) If there are more than 1 realizations on the node, then we
			// know the allocation is virtual
			//
			// 3) Finally, if there is exactly 1 allocation, we need to query
			// the RealizationResult. This could obviously be done up front for
			// all nodes, but the lookup is expensive and so should only be
			// done when necessary
			var rlzRes *portal.RealizationResult
			var nodeRlz *portal.NodeRealization

			for _, aAlloc := range ral.Value {
				tkns := strings.Split(aAlloc.Mzid, ".")
				rid := tkns[0]
				eid := tkns[1]
				pid := tkns[2]

				alloc := &portal.Allocation{
					Rid: rid,
					Eid: eid,
					Pid: pid,
					Fid: res.Facility,
					// dig userid out of the realization?
					// organizationid if/when that is defined?
					Node: aAlloc.Node,
				}

				// marked virtual, or more than one rlz per node
				if aAlloc.Virtual || len(ral.Value) > 1 {
					// check and warn if the allocation table is not marked Virtual; this is ok and expected on portals
					// where some user realizations predate the update in which the flag was introduced
					if !aAlloc.Virtual {
						log.Warningf("found virtual node %s.%s that is _not_ marked Virtual in the allocation table",
							aAlloc.Node,
							aAlloc.Mzid,
						)
					}
					alloc.Virt = true
					goto account
				}

				// need to look at the realization; have we queried it yet?
				if _, ok = rlzMap[aAlloc.Mzid]; !ok {
					rlzRes, err = storage.ReadRealizationResult(pid, eid, rid)
					if err != nil {
						log.Errorf("mzid %s in allocation table, but cannot read realization: %v",
							aAlloc.Mzid,
							err,
						)
						continue
					}

					if rlzRes.Diagnostics.Error() {
						log.Errorf("mzid %s in allocation table, but realization had errors: %s",
							aAlloc.Mzid,
							rlzRes.Diagnostics,
						)
						continue
					}

					rlzMap[aAlloc.Mzid] = rlzRes
				}

				rlzRes = rlzMap[aAlloc.Mzid]
				if nodeRlz, ok = rlzRes.Realization.Nodes[aAlloc.Node]; !ok {
					log.Errorf("no NodeRealization for node %s in mzid %s", aAlloc.Node, aAlloc.Mzid)
					continue
				}

				alloc.Virt = (nodeRlz.Kind == portal.NodeRealization_VirtualMachine)

				// check and warn if the allocation table is not marked Virtual; this is ok and expected on portals
				// where some user realizations predate the update in which the flag was introduced
				if alloc.Virt && !aAlloc.Virtual {
					log.Warningf("found virtual node %s.%s that is _not_ marked Virtual in the allocation table",
						aAlloc.Node,
						aAlloc.Mzid,
					)
				}

			account:
				if alloc.Virt {
					alloc.CoresUsed = aAlloc.Procs.TotalCores()
					alloc.MemoryUsed = aAlloc.Memory.TotalCapacity()
				} else {
					// the full resource
					alloc.CoresUsed = res.Cores()
					alloc.MemoryUsed = res.Mem()
				}

				rAlloc = append(rAlloc, alloc)
			}
		}

		result = append(result, &portal.Resource{
			Resource:  res,
			Allocated: rAlloc,
		})
	}

	return result, nil
}

func SiteList(rz *portal.Realization) []string {

	var result []string

	add := func(x string) {
		for _, y := range result {
			if x == y {
				return
			}
		}
		result = append(result, x)
	}

	for _, x := range rz.Nodes {
		add(x.Facility)
	}

	for _, x := range rz.Phyos {
		add(x.Facility)
	}

	for _, x := range rz.Sensors {
		add(x.Facility)
	}

	for _, x := range rz.Actuators {
		add(x.Facility)
	}

	for _, x := range rz.Bonds {
		add(x.Facility)
	}

	return result
}

/* XXX
func ConfigureInfranet(
	tbx *xir.Topology,
	embedding *sfe.Embedding,
	params RealizeParameters,
) (*portal.InfranetEmbedding, error) {

	var vid uint32
	var vrf string
	var err error

	vid = params.Vid

	if vid == 0 {
		vid, err = getInfranetVid(params.Mzid)
		if err != nil {
			return nil, err
		}
	}
	vrf = fmt.Sprintf("vrf%d", vid)

	irt := sfe.BuildInfraRoutingTable(tbx)

	iec := NewInfranetEmbeddingContext(
		vrf,
		vid,
		tbx,
		embedding,
		irt,
	)

	//return iec.mapInfranet(vrf, vid, tbx, embedding, irt)
	err = iec.mapInfranet()
	if err != nil {
		return nil, err
	}

	_, err = iec.commit()
	if err != nil {
		return nil, fmt.Errorf("commit infranet embedding: %v", err)
	}

	return iec.infranet, err

}

//TODO do this hop by hop, the current global vid implies a limit of 3096 total
//	   experiments
func getInfranetVid(mzid string) (uint32, error) {

	vlist := storage.NewVlist("infranet")
	err := storage.ReadVlist(vlist)
	if err != nil {
		return 0, fmt.Errorf("failed to read infranet vlist: %v", err)
	}

	var vid uint32

	for i := uint32(100); i < 3096; i++ {

		avail := true
		for _, v := range vlist.Values {
			if i == v {
				avail = false
				break
			}
		}
		if avail {
			vid = i
			break
		}

	}

	if vid == 0 {
		return 0, fmt.Errorf("no infranet VIDs available")
	}

	vlist.Values = append(vlist.Values, vid)
	err = storage.WriteVlist(vlist)
	if err != nil {
		return 0, fmt.Errorf("failed to write infranet vlist: %v", err)
	}

	return vid, nil

}

func getLeafUplink(leaf *xir.Device) (*xir.Interface, error) {

	//TODO
	return nil, nil

}

func EmbedSys(tbx *xir.Topology) (*portal.Realization, error) {

	if tbx == nil {
		return nil, fmt.Errorf("testbed model cannot be nil")
	}

	e := sfe.NewEmbedding()
	for _, r := range tbx.Devices {
		if !r.Resource().HasRole(xir.Role_TbNode) {
			continue
		}
		e.Nodes[r.Id()] = sfe.NodeEmbedding{
			Node:     &xir.Node{Id: r.Id()},
			Resource: r,
			Kind:     portal.NodeRealization_BareMetal,
		}

	}

	rlz, err := EmbeddingToRz(e, false)
	if err != nil {
		return nil, err
	}

	ic, err := ConfigureInfranet(
		tbx,
		e,
		RealizeParameters{
			Mzid: "harbor.system.marstb",
			Vid:  3,
		},
	)
	if err != nil {
		log.Error(err)
		return nil, err
	}
	rlz.Infranet = ic

	return rlz, nil

}

*/
