// +build dontbuild

//XXX REMOVE THIS FILE XXX
//XXX REMOVE THIS FILE XXX
//XXX REMOVE THIS FILE XXX
//XXX REMOVE THIS FILE XXX
//XXX REMOVE THIS FILE XXX
//XXX REMOVE THIS FILE XXX
//XXX REMOVE THIS FILE XXX
//XXX REMOVE THIS FILE XXX

package sfe

import (
	"fmt"

	log "github.com/sirupsen/logrus"
	rz "gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/xir/v0.3/go"
)

type Pathfinder struct {
	Start, Finish *End
	Path          *Path

	mode PathMode
	hop  int
}

type PathMode int

const (
	VlanAccess PathMode = iota
	VlanTrunk
	BgpPeer
	BgpTransitToStem
)

func NewPathfinder(start, finish *End, rt RoutingTable) *Pathfinder {

	p := &Pathfinder{
		Start:  start,
		Finish: finish,
		Path: &Path{
			Hops: CollectHops(
				rt.Paths(start.Interface, TPA(finish.Interface.Port().TPA)),
			),
		},
	}

	p.Init()

	return p

}

func (p *Pathfinder) Init() {

	switch p.Start.Endpoint.Interface.(type) {

	case *rz.Endpoint_Phy:
		p.mode = VlanAccess

	case *rz.Endpoint_Vlan:
		p.mode = VlanTrunk

	case *rz.Endpoint_Vtep:
		p.mode = BgpPeer

	}

	p.hop = 1

}

func (p *Pathfinder) Run() error {

	//TODO handle multipath
	hops := p.Path.Hops[0]

	// we've reached the end of the recursive traversal
	if p.hop >= len(hops) {
		return nil
	}

	ifx := hops[p.hop]

	switch p.mode {
	case VlanAccess:

		// Downlink
		vid := uint32(47) //TODO
		p.Path.Waypoints = append(p.Path.Waypoints, &rz.Waypoint{
			Host: ifx.Device.Id(),
			Bridge: &rz.BridgeMember{
				Bridge:   "bridge", //assumes cumulus semantics
				Untagged: []uint32{vid},
				Pvid:     vid,
			},
			Interface: &rz.Waypoint_Access{
				&rz.VLANAccessPort{
					Port: ifx.PortName(),
					Vid:  vid,
				},
			},
		})

		p.hop++

		ifx := hops[p.hop] //TODO check bounds

		// check if next-next hop is node accessible via this switch, then all
		// we need to do is plumb an access port

		if p.hop+1 < len(hops) &&
			hops[p.hop+1].Device.Resource().HasRole(xir.Role_TbNode) &&
			p.Finish.Endpoint.GetPhy() != nil {

			p.Path.Waypoints = append(p.Path.Waypoints, &rz.Waypoint{
				Host: ifx.Device.Id(),
				Bridge: &rz.BridgeMember{
					Bridge:   "bridge", //assumes cumulus semantics
					Untagged: []uint32{vid},
					Pvid:     vid,
				},
				Interface: &rz.Waypoint_Access{
					&rz.VLANAccessPort{
						Port: ifx.PortName(),
						Vid:  vid,
					},
				},
			})

			return nil

		}

		if ifx.Device.Resource().HasRole(xir.Role_Stem) {

			//evpn := ifx.Device.Resource().GetEVPN()

			// we've reached an evpn capable device transition the VLAN into VXLAN

			vni := uint32(1701) //TODO
			p.Path.Waypoints = append(p.Path.Waypoints, &rz.Waypoint{
				Host: ifx.Device.Id(),
				Bridge: &rz.BridgeMember{
					Bridge: "bridge",
					Tagged: []uint32{vid},
				},
				Interface: &rz.Waypoint_Vtep{
					&rz.Vtep{
						Name: fmt.Sprintf("vtep%d", vni),
						Vni:  vni,
					},
				},
			})

			p.mode = BgpTransitToStem
			p.hop++

		} else {

			p.Path.Waypoints = append(p.Path.Waypoints, &rz.Waypoint{
				Host: ifx.Device.Id(),
				Bridge: &rz.BridgeMember{
					Bridge: "bridge",
					Tagged: []uint32{vid},
				},
				Interface: &rz.Waypoint_Trunk{
					&rz.VLANTrunkPort{
						Port: ifx.PortName(),
						Vids: []uint32{vid},
					},
				},
			})

			p.mode = VlanTrunk
			p.hop++

		}

	case VlanTrunk:

		vid := uint32(47) //TODO
		p.Path.Waypoints = append(p.Path.Waypoints, &rz.Waypoint{
			Host: ifx.Device.Id(),
			Bridge: &rz.BridgeMember{
				Bridge:   "bridge", //assumes cumulus semantics
				Untagged: []uint32{vid},
				Pvid:     vid,
			},
			Interface: &rz.Waypoint_Trunk{
				&rz.VLANTrunkPort{
					Port: ifx.PortName(),
					Vids: []uint32{vid},
				},
			},
		})

		p.hop++

		if p.hop >= len(hops) {
			log.Infof("terminating vlan trunk at host endpoint")
			return nil
		}

		ifx := hops[p.hop] //TODO check bounds

		// check if next-next hop is node accessible via this switch, then all
		// we need to do is plumb an access port

		if p.hop+1 < len(hops) &&
			hops[p.hop+1].Device.Resource().HasRole(xir.Role_TbNode) &&
			p.Finish.Endpoint.GetPhy() != nil {

			p.Path.Waypoints = append(p.Path.Waypoints, &rz.Waypoint{
				Host: ifx.Device.Id(),
				Bridge: &rz.BridgeMember{
					Bridge:   "bridge", //assumes cumulus semantics
					Untagged: []uint32{vid},
					Pvid:     vid,
				},
				Interface: &rz.Waypoint_Access{
					&rz.VLANAccessPort{
						Port: ifx.PortName(),
						Vid:  vid,
					},
				},
			})

			return nil

		} else {
			if ifx.Device.Resource().HasRole(xir.Role_Stem) {

				//evpn := ifx.Device.Resource().GetEVPN()

				// we've reached an evpn capable device transition the VLAN into VXLAN

				vni := uint32(1701) //TODO
				p.Path.Waypoints = append(p.Path.Waypoints, &rz.Waypoint{
					Host: ifx.Device.Id(),
					Bridge: &rz.BridgeMember{
						Bridge: "bridge",
						Tagged: []uint32{vid},
					},
					Interface: &rz.Waypoint_Vtep{
						&rz.Vtep{
							Name: fmt.Sprintf("vtep%d", vni),
							Vni:  vni,
						},
					},
				})

				p.mode = BgpTransitToStem
				p.hop++

			} else {

				p.Path.Waypoints = append(p.Path.Waypoints, &rz.Waypoint{
					Host: ifx.Device.Id(),
					Bridge: &rz.BridgeMember{
						Bridge: "bridge",
						Tagged: []uint32{vid},
					},
					Interface: &rz.Waypoint_Trunk{
						&rz.VLANTrunkPort{
							Port: ifx.PortName(),
							Vids: []uint32{vid},
						},
					},
				})

			}

		}

		p.hop++

	case BgpPeer:

		bgp := ifx.Device.Resource().GetBGP()
		if bgp == nil {
			return fmt.Errorf("BGP peering required but device has no BGP config")
		}

		var xpBGP *xir.BGPRouterConfig
		for _, x := range bgp {
			if x.Name == "xfabric" {
				xpBGP = x
			}
		}

		if xpBGP == nil {
			return fmt.Errorf("BGP peering required but device has no xp BGP config")
		}

		p.Path.Waypoints = append(p.Path.Waypoints, &rz.Waypoint{
			Host: ifx.Device.Id(),
			Interface: &rz.Waypoint_BgpPeer{
				&rz.BGPPeer{
					PeerGroup: "xfabric", //TODO hardcode
					LocalAsn:  xpBGP.ASN,
				},
			},
		})

		p.hop++

		// done, we've reached the routed edge of the plumbing
		return nil

	case BgpTransitToStem:

		ifx := hops[p.hop]

		if ifx.Device.Resource().HasRole(xir.Role_Stem) {

			vid := uint32(47)   // TODO
			vni := uint32(1701) //TODO

			p.Path.Waypoints = append(p.Path.Waypoints, &rz.Waypoint{
				Host: ifx.Device.Id(),
				Bridge: &rz.BridgeMember{
					Bridge: "bridge",
					Tagged: []uint32{vid},
				},
				Interface: &rz.Waypoint_Vtep{
					&rz.Vtep{
						Name: fmt.Sprintf("vtep%d", vni),
						Vni:  vni,
					},
				},
			})

			p.hop++

			p.Path.Waypoints = append(p.Path.Waypoints, &rz.Waypoint{
				Host: ifx.Device.Id(),
				Bridge: &rz.BridgeMember{
					Bridge: "bridge",
					Tagged: []uint32{vid},
				},
				Interface: &rz.Waypoint_Trunk{
					&rz.VLANTrunkPort{
						Port: ifx.PortName(),
						Vids: []uint32{vid},
					},
				},
			})

			p.mode = VlanTrunk

		}

		p.hop++
	}

	// recurse to next hop
	return p.Run()

}
