package sfe

import (
	"fmt"
	"strings"

	log "github.com/sirupsen/logrus"

	rz "gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/xir/v0.3/go"
)

type PathFinder struct {
	Path *Path

	src, dst     *End
	i            int
	mode         pfMode
	lsi          uint64
	finished     bool
	tbx          *xir.Topology
	vteps        map[string]struct{}
	vrf          string
	placed_vteps int
	diagnostics  rz.Diagnostics
}

type End struct {
	Interface *xir.Interface
	Endpoint  *rz.Endpoint
}

type Path struct {
	Hops      [][]*xir.Interface
	Waypoints []*rz.Waypoint
}

type Direction byte

const (
	Egress Direction = iota
	Ingress
)

func (pf *PathFinder) Direction() Direction {
	return Direction(pf.i % 2)
}

func (pf *PathFinder) vid() uint32 {

	//TODO overflow
	return uint32(pf.lsi)

}

func (pf *PathFinder) clearVid() {

	//TODO
	//pf.lsi = 0

}

func (pf *PathFinder) vni() uint32 {

	/*
		if pf._vni == 0 {
			//TODO get vid
			pf._vni = 47
		}
		return pf._vni
	*/
	//TODO overflow
	return uint32(pf.lsi)

}

func (pf *PathFinder) transition(m pfMode) {

	pf.mode = m

}

func (pf *PathFinder) transitionFinished() {

	pf.finished = true
	pf.mode = &pfFinished{pf}
}

func (pf *PathFinder) current() *xir.Interface {

	//TODO multipath
	if pf.i >= len(pf.Path.Hops[0]) {
		return nil
	}
	return pf.Path.Hops[0][pf.i]

}

func (pf *PathFinder) next() *xir.Interface {

	//TODO multipath
	if pf.i+1 >= len(pf.Path.Hops[0]) {
		return nil
	}
	return pf.Path.Hops[0][pf.i+1]

}

func (pf *PathFinder) prev() *xir.Interface {

	//TODO multipath
	if pf.i == 0 {
		return nil
	}
	return pf.Path.Hops[0][pf.i-1]

}

func (pf *PathFinder) isDestination(ifx *xir.Interface) bool {
	return pf.dst.Interface == ifx
}

func findStems(start, finish *End, hops [][]*xir.Interface) (map[string]struct{}, rz.Diagnostics) {

	var ds rz.Diagnostics

	found_vteps := 0
	vteps := make(map[string]struct{}, 2)

	// from front to end
	if start.Endpoint.GetVtep() != nil {
		found_vteps++
	} else {

		//TODO multipath
		for _, ifx := range hops[0] {
			if ifx.Device.Resource().HasRole(xir.Role_Stem) {
				vteps[ifx.Device.Id()] = struct{}{}
				found_vteps++
				break
			}
		}
	}

	// from end to front
	if finish.Endpoint.GetVtep() != nil {
		found_vteps++
	} else {

		//TODO multipath
		for i := range hops[0] {
			ifx := hops[0][len(hops[0])-1-i]

			if ifx.Device.Resource().HasRole(xir.Role_Stem) {

				// If we found the same one as before, we know the following:
				//   - both endpoints are not vteps
				//   - there's only 1 possible location to place a vtep
				// we can't have a path with only 1 vtep in the middle, so we remove it instead

				if _, ok := vteps[ifx.Device.Id()]; ok {
					delete(vteps, ifx.Device.Id())
					found_vteps--
				} else {
					vteps[ifx.Device.Id()] = struct{}{}
					found_vteps++
				}

				break
			}
		}
	}

	// ds = append(ds, DiagnosticDebugf("pathfinder: %d vteps: %+v", found_vteps, vteps))

	if found_vteps%2 == 1 {
		ds = append(ds, DiagnosticErrorf("pathfinder: one of the endpoints is a vtep, but there is no waypoint device capable of a vtep"))
	}

	return vteps, ds
}

func NewPathFinder(
	start, finish *End,
	rt RoutingTable,
	lsi uint64,
	tbx *xir.Topology,
	vrf string,
) (*PathFinder, rz.Diagnostics) {

	var ds rz.Diagnostics

	wp, d := rt.ShortestPath(start.Interface, TPA(finish.Interface.Port().TPA))
	ds = append(ds, d...)
	if d.Error() {
		return nil, ds
	}

	hops := CollectHops(wp)

	vteps, d := findStems(start, finish, hops)
	ds = append(ds, d...)
	if d.Error() {
		return nil, ds
	}

	log.Debugf("\n\nstems: %+v", vteps)

	/*DEBUG
	wp.Dump()
	fmt.Println("")
	*/

	return &PathFinder{
		src: start,
		dst: finish,
		Path: &Path{
			Hops: hops,
		},
		lsi: lsi,
		//_vni:   vni,
		//_vid:   vid,
		tbx:          tbx,
		vteps:        vteps,
		vrf:          vrf,
		placed_vteps: 0,
	}, ds

}

func (pf *PathFinder) Run() rz.Diagnostics {

	pf.mode = &pfInit{pf}

	// first node node is a special case, only egress

	log.Debugf("\n  >>> %s\n", pf.current().Device.Id())

	pf.mode.init()
	if pf.diagnostics.Error() {
		return pf.diagnostics
	}

	log.Debug("    egress(): ")

	pf.mode.egress()
	if pf.diagnostics.Error() {
		return pf.diagnostics
	}

	pf.i++

	for pf.i <= len(pf.Path.Hops[0]) &&
		pf.mode != nil &&
		!pf.diagnostics.Error() &&
		!pf.finished {

		if pf.current() != nil {
			log.Debugf("\n  >>> %s\n", pf.current().Device.Id())
		} else {
			log.Debugf("\n  >>> nil!\n")
		}

		pf.mode.init()
		if pf.diagnostics.Error() {
			return pf.diagnostics
		}

		log.Debug("    ingress(): ")

		pf.mode.ingress()
		if pf.diagnostics.Error() {
			return pf.diagnostics
		}

		pf.i++

		log.Debug("    egress(): ")

		pf.mode.egress()
		if pf.diagnostics.Error() {
			return pf.diagnostics
		}

		pf.i++

	}

	// This is in the case there's a silent bug or error,
	// which means that something went off during the run
	if !pf.finished {
		pf.diagnostics = append(pf.diagnostics, DiagnosticErrorf("pathfinder: pathfinder didn't finish"))
	}

	return pf.diagnostics

}

func (pf *PathFinder) access() {

	log.Debugf("access()\n")

	ifx := pf.current()
	if ifx == nil {
		pf.diagnostics = append(pf.diagnostics, DiagnosticErrorf("nil access ifx"))
		return
	}

	// we generally don't have MACs for switch ports, so Debug here rather than Warn
	mac := ""
	bond := ifx.Port().GetBond()
	if bond == nil {
		mac = ifx.Port().GetMac()
		if mac == "" {
			log.Debugf("ifx has no MAC address: %+v", ifx)
		}
	}

	wp := &rz.Waypoint{
		Host: ifx.Device.Id(),
		Bridge: &rz.BridgeMember{
			Bridge:   "bridge", //assumes cumulus semantics
			Untagged: []uint32{pf.vid()},
			Pvid:     pf.vid(),
		},
		Interface: &rz.Waypoint_Access{
			&rz.VLANAccessPort{
				Port: &rz.PhysicalInterface{
					Name: ifx.PortName(),
					Mac:  mac,
				},
				Vid: pf.vid(),
			},
		},
		Mtu: JumboMtu,
	}

	pf.Path.Waypoints = append(pf.Path.Waypoints, wp)

	log.Debugf("      WP >> %+v\n", wp)

	if pf.Direction() == Egress {

		if pf.isDestination(pf.next()) {
			pf.transitionFinished()
			return
		}

		switch pf.mode.(type) {
		case *pfAccess:
			return
		default:
			pf.transition(&pfAccess{pf})
			return
		}

	}

	return

}

func (pf *PathFinder) trunk() {

	log.Debugf("trunk()\n")

	ifx := pf.current()
	if ifx == nil {
		pf.diagnostics = append(pf.diagnostics, DiagnosticErrorf("nil trunk ifx"))
		return
	}

	// we generally don't have MACs for switch ports, so Debug here rather than Warn
	mac := ""
	bond := ifx.Port().GetBond()
	if bond == nil {
		mac = ifx.Port().GetMac()
		if mac == "" {
			log.Debugf("ifx has no MAC address: %+v", ifx)
		}
	}

	wp := &rz.Waypoint{
		Host: ifx.Device.Id(),
		Bridge: &rz.BridgeMember{
			Bridge: "bridge",
			Tagged: []uint32{pf.vid()},
		},
		Interface: &rz.Waypoint_Trunk{
			&rz.VLANTrunkPort{
				Port: &rz.PhysicalInterface{
					Name: ifx.PortName(),
					Mac:  mac,
				},
				Vids: []uint32{pf.vid()},
			},
		},
		Mtu: JumboMtu,
	}

	pf.Path.Waypoints = append(pf.Path.Waypoints, wp)

	log.Debugf("      WP >> %+v\n", wp)

	if pf.Direction() == Egress {

		if pf.isDestination(pf.next()) {
			pf.transitionFinished()
			return
		}

		switch pf.mode.(type) {
		case *pfTrunk:
			return
		default:
			pf.transition(&pfTrunk{pf})
			return
		}

	}

	return

}

func (pf *PathFinder) canVtep(ifx *xir.Interface) bool {
	_, found := pf.vteps[ifx.Device.Id()]

	return found && (ifx.Device.Resource().HasRole(xir.Role_Stem))
}

func (pf *PathFinder) vtep() {

	log.Debugf("vtep()\n")

	// current node
	ifx := pf.current()

	if ifx == nil {
		pf.diagnostics = append(pf.diagnostics, DiagnosticErrorf("nil vtep ifx"))
		return
	}

	d := pf.tbx.Device(ifx.Device.Id())
	if d == nil {
		pf.diagnostics = append(pf.diagnostics, DiagnosticErrorf("%s not found in model", ifx.Device.Id()))
		return
	}
	r := d.Resource()
	if r == nil {
		pf.diagnostics = append(pf.diagnostics, DiagnosticErrorf("%s is not a resource", ifx.Device.Id()))
		return
	}
	bgp := bgpConfigByVrf(r, pf.vrf)
	if bgp == nil {
		pf.diagnostics = append(pf.diagnostics, DiagnosticErrorf(
			"cannot place vtep on %s, no bgp config", ifx.Device.Id()))

		return
	}
	tunnelip := bgpTunnelIP(bgp)
	if tunnelip == "" {
		pf.diagnostics = append(pf.diagnostics, DiagnosticErrorf(
			"cannot place vtep on %s, no tunnelip found", ifx.Device.Id()))

		return
	}

	if !pf.canVtep(ifx) {
		pf.diagnostics = append(pf.diagnostics, DiagnosticErrorf(
			"cannot place a vtep on %s, not allowed to place vtep here", ifx.Device.Id()))

		return
	}

	// find the next node
	var next *xir.Interface

	if pf.Direction() == Egress {
		next = pf.next()
	} else {
		next = pf.prev()
	}

	if next == nil {
		pf.diagnostics = append(pf.diagnostics, DiagnosticErrorf("vtep to nowhere at %s", ifx.Device.Id()))
		return
	}

	pf.placed_vteps++

	vtep_wp := &rz.Waypoint{
		Host: ifx.Device.Id(),
		Bridge: &rz.BridgeMember{
			Bridge:   "bridge",
			Untagged: []uint32{pf.vid()},
			Pvid:     pf.vid(),
		},
		Interface: &rz.Waypoint_Vtep{
			&rz.Vtep{
				Name: fmt.Sprintf("vtep%d", pf.vni()),
				Vni:  pf.vni(),

				//TODO what about multi-router configs?
				// TODO what about multi-endpoint configs?
				TunnelIp: tunnelip,
				//ServiceIp: serviceAddress, XXX
			},
		},
		Mtu: DefaultMtu,
	}

	// The order of the next two waypoints does not matter for
	// either realization or materialization.

	// But it's nice to add them in order so when hand verifying paths,
	// they're listed in order.
	if pf.Direction() == Egress {
		pf.Path.Waypoints = append(pf.Path.Waypoints, vtep_wp)
		pf.peer()
	} else {
		pf.peer()
		pf.Path.Waypoints = append(pf.Path.Waypoints, vtep_wp)
	}

	log.Debugf("      WP >> %+v\n", vtep_wp)

	if pf.Direction() == Egress {

		pf.clearVid()

		switch pf.mode.(type) {
		case *pfTransit:
			return
		default:
			pf.transition(&pfTransit{pf})
			return
		}

	}

	return

}

func (pf *PathFinder) peer() {

	log.Debugf("peer()\n")

	// this node
	ifx := pf.current()
	if ifx == nil {
		pf.diagnostics = append(pf.diagnostics, DiagnosticErrorf("nil vtep ifx"))
		return
	}
	d := pf.tbx.Device(ifx.Device.Id())
	if d == nil {
		pf.diagnostics = append(pf.diagnostics, DiagnosticErrorf("%s not found in model", ifx.Device.Id()))
		return
	}
	r := d.Resource()
	if r == nil {
		pf.diagnostics = append(pf.diagnostics, DiagnosticErrorf("%s is not a resource", ifx.Device.Id()))
		return
	}
	bgp := bgpConfigByVrf(r, pf.vrf)
	if bgp == nil {
		pf.diagnostics = append(pf.diagnostics, DiagnosticErrorf(
			"cannot place peer config on %s, no bgp config", ifx.Device.Id()))

		return
	}
	tunnelip := bgpTunnelIP(bgp)
	if tunnelip == "" {
		pf.diagnostics = append(pf.diagnostics, DiagnosticErrorf(
			"cannot place peer config on %s, no tunnelip found", ifx.Device.Id()))

		return
	}

	// we generally don't have MACs for switch ports, so Debug here rather than Warn
	mac := ""
	bond := ifx.Port().GetBond()
	if bond == nil {
		mac = ifx.Port().GetMac()
		if mac == "" {
			log.Debugf("ifx has no MAC address: %+v", ifx)
		}
	}

	// next node
	rasn := uint32(0)

	var next *xir.Interface

	if pf.Direction() == Egress {
		next = pf.next()
	} else {
		next = pf.prev()
	}

	if next != nil {
		dn := pf.tbx.Device(next.Device.Id())
		if dn != nil {
			rn := dn.Resource()
			if rn != nil {
				bgpn := bgpConfigByVrf(rn, pf.vrf)
				if bgpn != nil {
					rasn = bgpn.ASN
				}
			}
		}
	}

	wp := &rz.Waypoint{
		Host: ifx.Device.Id(),
		Interface: &rz.Waypoint_BgpPeer{
			&rz.BGPPeer{
				Interface: &rz.PhysicalInterface{
					Name: ifx.PortName(),
					Mac:  mac,
				},
				LocalAsn:  bgp.ASN,
				RemoteAsn: rasn,
				Network:   tunnelip,
			},
		},
		Mtu: JumboMtu,
	}

	log.Debugf("      WP >> %+v\n", wp)

	pf.Path.Waypoints = append(pf.Path.Waypoints, wp)

	if pf.Direction() == Ingress && pf.isDestination(ifx) {
		pf.transitionFinished()
		return
	}

	pf.transition(&pfTransit{pf})

	return

}

type pfMode interface {
	init()
	ingress()
	egress()
}

// Init ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

type pfInit struct{ *PathFinder }

func (pf *PathFinder) init() { return }
func (pf *PathFinder) ingress() {
	log.Debugf("init()\n")
	return
}
func (pf *PathFinder) egress() {
	log.Debugf("init()\n")

	if pf.isDestination(pf.current()) {
		pf.transitionFinished()
		return
	}

	switch pf.src.Endpoint.Interface.(type) {

	case *rz.Endpoint_Phy:
		pf.transition(&pfAccess{pf})

	case *rz.Endpoint_Vlan:
		pf.transition(&pfTrunk{pf})

	case *rz.Endpoint_Vtep:
		// Unlike the other inits, we need to construct
		// a peer on the first endpoint, then transition to transit
		pf.placed_vteps++
		pf.peer()
	}

	return
}

// Finished ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

type pfFinished struct{ *PathFinder }

func (pf *pfFinished) init() { pf.finished = true }
func (pf *pfFinished) ingress() {
	log.Debugf("finished()\n")

	pf.finished = true
	return
}
func (pf *pfFinished) egress() {
	log.Debugf("finished()\n")

	pf.finished = true
	return
}

// Access ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

type pfAccess struct {
	*PathFinder
}

func (pf *pfAccess) init() { return }

func (pf *pfAccess) ingress() {

	log.Debugf("pfAccess(): ")

	pf.access()

}

func (pf *pfAccess) egress() {

	log.Debugf("pfAccess(): ")

	egress := pf.PathFinder.current()
	if egress == nil {
		return
	}

	next := pf.next()
	if next == nil {
		pf.diagnostics = append(pf.diagnostics, DiagnosticErrorf("early access path termination at %s", egress.Device.Id()))
		return
	}

	switch {

	// if the next device is the destination,
	// then just continue the access path down to the device
	case pf.isDestination(next):
		switch pf.dst.Endpoint.Interface.(type) {

		case *rz.Endpoint_Phy:
			pf.access()
			return

		case *rz.Endpoint_Vlan:
			pf.trunk()
			return

		case *rz.Endpoint_Vtep:
			pf.vtep()
			return

		}

	// we're in transit if the next device is NOT a node
	// try placing a vtep (if we're able)
	case pf.canVtep(egress):
		pf.vtep()
		return

	// otherwise trunk to the next device
	default:
		pf.trunk()
		return

	}

	return

}

// Trunk ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

type pfTrunk struct{ *PathFinder }

func (pf *pfTrunk) init() { return }

func (pf *pfTrunk) ingress() {

	log.Debugf("pfTrunk(): ")

	pf.trunk()

}

func (pf *pfTrunk) egress() {

	log.Debugf("pfTrunk(): ")

	egress := pf.PathFinder.current()
	if egress == nil {
		return
	}

	next := pf.next()
	if next == nil {
		pf.diagnostics = append(pf.diagnostics, DiagnosticErrorf("early trunk path termination at %s", egress.Device.Id()))
		return
	}

	switch {

	case pf.isDestination(next):
		switch pf.dst.Endpoint.Interface.(type) {

		case *rz.Endpoint_Phy:
			pf.access()
			return

		case *rz.Endpoint_Vlan:
			pf.trunk()
			return

		case *rz.Endpoint_Vtep:
			pf.vtep()
			return
		}

	// we're in transit if the next device is NOT a node
	// try placing a vtep (if we're able)
	case pf.canVtep(egress):
		pf.vtep()

	// otherwise trunk to the next device
	default:
		pf.trunk()

	}

	return

}

// Transit ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

type pfTransit struct{ *PathFinder }

func (pf *pfTransit) init() { return }
func (pf *pfTransit) ingress() {

	ifx := pf.PathFinder.current()
	if ifx == nil {
		return
	}

	log.Debugf("pfTransit(): ")

	switch {

	case pf.canVtep(ifx):
		pf.vtep()

	default:
		pf.peer()

	}

	return

}

func (pf *pfTransit) egress() {

	ifx := pf.PathFinder.current()
	if ifx == nil {
		return
	}

	log.Debugf("pfTransit(): ")

	switch pf.placed_vteps % 2 {

	// We've placed pairs of VTEPs, so we're in layer 2 routing now
	case 0:
		next := pf.next()
		if next == nil {
			pf.diagnostics = append(pf.diagnostics, DiagnosticErrorf("early transit path termination at %s", ifx.Device.Id()))
			return
		}

		switch {

		case pf.isDestination(next):
			switch pf.dst.Endpoint.Interface.(type) {

			case *rz.Endpoint_Phy:
				pf.access()
				return

			case *rz.Endpoint_Vlan:
				pf.trunk()
				return

			case *rz.Endpoint_Vtep:
				pf.vtep()
				return
			}

		default:
			pf.trunk()
			return

		}

	// We're between VTEPs, so we're in layer 3 routing now
	case 1:
		pf.peer()
		return

	}

	return

}

// Util =======================================================================

// XXX
func bgpConfigByVrf(r *xir.Resource, vrf string) *xir.BGPRouterConfig {

	bgps := r.GetBGP()
	if bgps == nil {
		return nil
	}

	for _, bgp := range bgps {
		if bgp.Vrf == vrf {
			return bgp
		}
	}

	return nil

}

func bgpConfig(r *xir.Resource) *xir.BGPRouterConfig {

	bgps := r.GetBGP()
	if len(bgps) == 0 {
		return nil
	}

	if len(bgps) > 1 {
		log.Warnf("resource %s has more than one BGP config, using first one", r.Id)
	}

	return bgps[0]
}

func bgpTunnelIP(bgp *xir.BGPRouterConfig) string {

	if bgp != nil {
		for _, ifx := range bgp.Interfaces {
			parts := strings.Split(ifx.Address, "/")
			if len(parts) == 2 {
				return parts[0]
			}
		}
	}

	return ""

}
