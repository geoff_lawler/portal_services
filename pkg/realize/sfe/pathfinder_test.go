package sfe

import (
	"encoding/json"
	"fmt"
	"math/rand"
	"testing"

	portal "gitlab.com/mergetb/api/portal/v1/go"
	xir "gitlab.com/mergetb/xir/v0.3/go"
)

/*
	xleaf0:[x0, x1], xleaf1:[x2, x3], xleaf2:[x4, x5], xleaf3:[x6, x7] are each on the same switch
	So, Local is on the same switch

	[xleaf0, xleaf1] has the stem role, but [xleaf2, xleaf3] do not

	So, TwoStems is between xleaf0 and xleaf1
	OneStem is between xleaf0 and xleaf2, with the VTEP being on xleaf0
	ZeroStems is between xleaf2 and xleaf3

	testTwoWaypoints tests both directions (A -> B) and (B -> A) and checks if they're
	both as we've expected, so symmetrical (local, 0, and 2) stems are tested by permuations,
	while 1 stem is tested by combination
*/

func initMixedFacilityPath() (*xir.Topology, RoutingTable, error) {
	tbx, err := loadFacility("facilities/mixed.json")

	if err != nil {
		return nil, nil, err
	}

	AssignTPAsInfraNet(0x004D000000000000, tbx)
	AssignTPAsXpNet(0x004E000000000000, tbx)

	rt := BuildXpRoutingTable(tbx)
	return tbx, rt, nil
}

/*
	Sanity Checking
*/

func TestDifferentSegments(t *testing.T) {
	expected := `{"Endpoints":[{"Host":"x0","Ifx":"eth2","Net":"Physical","Ext":""},{"Host":"x1","Ifx":"eth2","Net":"Vlan","Ext":""}],"Waypoints":[{"Host":"xplf0","Ifx":"swp3","Net":"Access","Ext":""},{"Host":"xplf0","Ifx":"swp4","Net":"Trunk","Ext":""}]}`

	testTwoWaypoints(t, Failure, expected, XpnetMode,
		DeviceSelection{
			"x0", AccessMode, portal.NodeRealization_BareMetal,
		},
		DeviceSelection{
			"x1", AccessMode, portal.NodeRealization_BareMetal,
		},
	)
}

/*
	Local
*/

func TestLocalAccessToAccess(t *testing.T) {
	expected := `{"Endpoints":[{"Host":"x0","Ifx":"eth2","Net":"Physical","Ext":""},{"Host":"x1","Ifx":"eth2","Net":"Physical","Ext":""}],"Waypoints":[{"Host":"xplf1","Ifx":"swp4","Net":"Access","Ext":""},{"Host":"xplf1","Ifx":"swp6","Net":"Access","Ext":""}]}`

	testTwoWaypoints(t, Success, expected, XpnetMode,
		DeviceSelection{
			"x0", AccessMode, portal.NodeRealization_BareMetal,
		},
		DeviceSelection{
			"x1", AccessMode, portal.NodeRealization_BareMetal,
		},
	)
}

func TestLocalAccessToTrunk(t *testing.T) {
	expected := `{"Endpoints":[{"Host":"x0","Ifx":"eth2","Net":"Physical","Ext":""},{"Host":"x1","Ifx":"eth2","Net":"Vlan","Ext":""}],"Waypoints":[{"Host":"xplf1","Ifx":"swp4","Net":"Access","Ext":""},{"Host":"xplf1","Ifx":"swp6","Net":"Trunk","Ext":""}]}`

	testTwoWaypoints(t, Success, expected, XpnetMode,
		DeviceSelection{
			"x0", AccessMode, portal.NodeRealization_BareMetal,
		},
		DeviceSelection{
			"x1", TrunkMode, portal.NodeRealization_BareMetal,
		},
	)
}

func TestLocalAccessToVtep(t *testing.T) {
	expected := `{"Endpoints":[{"Host":"x0","Ifx":"eth2","Net":"Physical","Ext":""},{"Host":"x1","Ifx":"eth2","Net":"Vtep","Ext":""}],"Waypoints":[{"Host":"xplf1","Ifx":"swp4","Net":"Access","Ext":""},{"Host":"xplf1","Ifx":"","Net":"Vtep","Ext":""},{"Host":"xplf1","Ifx":"swp6","Net":"Peer","Ext":""},{"Host":"x1","Ifx":"eth2","Net":"Peer","Ext":""}]}`

	testTwoWaypoints(t, Success, expected, XpnetMode,
		DeviceSelection{
			"x0", AccessMode, portal.NodeRealization_BareMetal,
		},
		DeviceSelection{
			"x1", VtepMode, portal.NodeRealization_BareMetal,
		},
	)
}

func TestLocalTrunkToTrunk(t *testing.T) {
	expected := `{"Endpoints":[{"Host":"x0","Ifx":"eth2","Net":"Vlan","Ext":""},{"Host":"x1","Ifx":"eth2","Net":"Vlan","Ext":""}],"Waypoints":[{"Host":"xplf1","Ifx":"swp4","Net":"Trunk","Ext":""},{"Host":"xplf1","Ifx":"swp6","Net":"Trunk","Ext":""}]}`

	testTwoWaypoints(t, Success, expected, XpnetMode,
		DeviceSelection{
			"x0", TrunkMode, portal.NodeRealization_BareMetal,
		},
		DeviceSelection{
			"x1", TrunkMode, portal.NodeRealization_BareMetal,
		},
	)
}

func TestLocalTrunkToVtep(t *testing.T) {
	expected := `{"Endpoints":[{"Host":"x0","Ifx":"eth2","Net":"Vlan","Ext":""},{"Host":"x1","Ifx":"eth2","Net":"Vtep","Ext":""}],"Waypoints":[{"Host":"xplf1","Ifx":"swp4","Net":"Trunk","Ext":""},{"Host":"xplf1","Ifx":"","Net":"Vtep","Ext":""},{"Host":"xplf1","Ifx":"swp6","Net":"Peer","Ext":""},{"Host":"x1","Ifx":"eth2","Net":"Peer","Ext":""}]}`

	testTwoWaypoints(t, Success, expected, XpnetMode,
		DeviceSelection{
			"x0", TrunkMode, portal.NodeRealization_BareMetal,
		},
		DeviceSelection{
			"x1", VtepMode, portal.NodeRealization_BareMetal,
		},
	)
}

func TestLocalVtepToVtep(t *testing.T) {
	expected := `{"Endpoints":[{"Host":"x0","Ifx":"eth2","Net":"Vtep","Ext":""},{"Host":"x1","Ifx":"eth2","Net":"Vtep","Ext":""}],"Waypoints":[{"Host":"x0","Ifx":"eth2","Net":"Peer","Ext":""},{"Host":"xplf1","Ifx":"swp4","Net":"Peer","Ext":""},{"Host":"xplf1","Ifx":"swp6","Net":"Peer","Ext":""},{"Host":"x1","Ifx":"eth2","Net":"Peer","Ext":""}]}`

	testTwoWaypoints(t, Success, expected, XpnetMode,
		DeviceSelection{
			"x0", VtepMode, portal.NodeRealization_BareMetal,
		},
		DeviceSelection{
			"x1", VtepMode, portal.NodeRealization_BareMetal,
		},
	)
}

/*
	Two Stems
*/

func TestTwoStemsAccessToAccess(t *testing.T) {
	expected := `{"Endpoints":[{"Host":"x0","Ifx":"eth2","Net":"Physical","Ext":""},{"Host":"x2","Ifx":"eth2","Net":"Physical","Ext":""}],"Waypoints":[{"Host":"xplf1","Ifx":"swp4","Net":"Access","Ext":""},{"Host":"xplf1","Ifx":"","Net":"Vtep","Ext":""},{"Host":"xplf1","Ifx":"swp2","Net":"Peer","Ext":""},{"Host":"xpsp1","Ifx":"swp3","Net":"Peer","Ext":""},{"Host":"xpsp1","Ifx":"swp4","Net":"Peer","Ext":""},{"Host":"xplf2","Ifx":"swp2","Net":"Peer","Ext":""},{"Host":"xplf2","Ifx":"","Net":"Vtep","Ext":""},{"Host":"xplf2","Ifx":"swp4","Net":"Access","Ext":""}]}`

	testTwoWaypoints(t, Success, expected, XpnetMode,
		DeviceSelection{
			"x0", AccessMode, portal.NodeRealization_BareMetal,
		},
		DeviceSelection{
			"x2", AccessMode, portal.NodeRealization_BareMetal,
		},
	)
}

func TestTwoStemsAccessToTrunk(t *testing.T) {
	expected := `{"Endpoints":[{"Host":"x0","Ifx":"eth2","Net":"Physical","Ext":""},{"Host":"x2","Ifx":"eth2","Net":"Vlan","Ext":""}],"Waypoints":[{"Host":"xplf1","Ifx":"swp4","Net":"Access","Ext":""},{"Host":"xplf1","Ifx":"","Net":"Vtep","Ext":""},{"Host":"xplf1","Ifx":"swp2","Net":"Peer","Ext":""},{"Host":"xpsp1","Ifx":"swp3","Net":"Peer","Ext":""},{"Host":"xpsp1","Ifx":"swp4","Net":"Peer","Ext":""},{"Host":"xplf2","Ifx":"swp2","Net":"Peer","Ext":""},{"Host":"xplf2","Ifx":"","Net":"Vtep","Ext":""},{"Host":"xplf2","Ifx":"swp4","Net":"Trunk","Ext":""}]}`

	testTwoWaypoints(t, Success, expected, XpnetMode,
		DeviceSelection{
			"x0", AccessMode, portal.NodeRealization_BareMetal,
		},
		DeviceSelection{
			"x2", TrunkMode, portal.NodeRealization_BareMetal,
		},
	)
}

func TestTwoStemsAccessToVtep(t *testing.T) {
	expected := `{"Endpoints":[{"Host":"x0","Ifx":"eth2","Net":"Physical","Ext":""},{"Host":"x2","Ifx":"eth2","Net":"Vtep","Ext":""}],"Waypoints":[{"Host":"xplf1","Ifx":"swp4","Net":"Access","Ext":""},{"Host":"xplf1","Ifx":"","Net":"Vtep","Ext":""},{"Host":"xplf1","Ifx":"swp2","Net":"Peer","Ext":""},{"Host":"xpsp1","Ifx":"swp3","Net":"Peer","Ext":""},{"Host":"xpsp1","Ifx":"swp4","Net":"Peer","Ext":""},{"Host":"xplf2","Ifx":"swp2","Net":"Peer","Ext":""},{"Host":"xplf2","Ifx":"swp4","Net":"Peer","Ext":""},{"Host":"x2","Ifx":"eth2","Net":"Peer","Ext":""}]}`

	testTwoWaypoints(t, Success, expected, XpnetMode,
		DeviceSelection{
			"x0", AccessMode, portal.NodeRealization_BareMetal,
		},
		DeviceSelection{
			"x2", VtepMode, portal.NodeRealization_BareMetal,
		},
	)
}

func TestTwoStemsTrunkToTrunk(t *testing.T) {
	expected := `{"Endpoints":[{"Host":"x0","Ifx":"eth2","Net":"Vlan","Ext":""},{"Host":"x2","Ifx":"eth2","Net":"Vlan","Ext":""}],"Waypoints":[{"Host":"xplf1","Ifx":"swp4","Net":"Trunk","Ext":""},{"Host":"xplf1","Ifx":"","Net":"Vtep","Ext":""},{"Host":"xplf1","Ifx":"swp2","Net":"Peer","Ext":""},{"Host":"xpsp1","Ifx":"swp3","Net":"Peer","Ext":""},{"Host":"xpsp1","Ifx":"swp4","Net":"Peer","Ext":""},{"Host":"xplf2","Ifx":"swp2","Net":"Peer","Ext":""},{"Host":"xplf2","Ifx":"","Net":"Vtep","Ext":""},{"Host":"xplf2","Ifx":"swp4","Net":"Trunk","Ext":""}]}`

	testTwoWaypoints(t, Success, expected, XpnetMode,
		DeviceSelection{
			"x0", TrunkMode, portal.NodeRealization_BareMetal,
		},
		DeviceSelection{
			"x2", TrunkMode, portal.NodeRealization_BareMetal,
		},
	)
}

func TestTwoStemsTrunkToVtep(t *testing.T) {
	expected := `{"Endpoints":[{"Host":"x0","Ifx":"eth2","Net":"Vlan","Ext":""},{"Host":"x2","Ifx":"eth2","Net":"Vtep","Ext":""}],"Waypoints":[{"Host":"xplf1","Ifx":"swp4","Net":"Trunk","Ext":""},{"Host":"xplf1","Ifx":"","Net":"Vtep","Ext":""},{"Host":"xplf1","Ifx":"swp2","Net":"Peer","Ext":""},{"Host":"xpsp1","Ifx":"swp3","Net":"Peer","Ext":""},{"Host":"xpsp1","Ifx":"swp4","Net":"Peer","Ext":""},{"Host":"xplf2","Ifx":"swp2","Net":"Peer","Ext":""},{"Host":"xplf2","Ifx":"swp4","Net":"Peer","Ext":""},{"Host":"x2","Ifx":"eth2","Net":"Peer","Ext":""}]}`

	testTwoWaypoints(t, Success, expected, XpnetMode,
		DeviceSelection{
			"x0", TrunkMode, portal.NodeRealization_BareMetal,
		},
		DeviceSelection{
			"x2", VtepMode, portal.NodeRealization_BareMetal,
		},
	)
}

func TestTwoStemsVtepToVtep(t *testing.T) {
	expected := `{"Endpoints":[{"Host":"x0","Ifx":"eth2","Net":"Vtep","Ext":""},{"Host":"x2","Ifx":"eth2","Net":"Vtep","Ext":""}],"Waypoints":[{"Host":"x0","Ifx":"eth2","Net":"Peer","Ext":""},{"Host":"xplf1","Ifx":"swp4","Net":"Peer","Ext":""},{"Host":"xplf1","Ifx":"swp2","Net":"Peer","Ext":""},{"Host":"xpsp1","Ifx":"swp3","Net":"Peer","Ext":""},{"Host":"xpsp1","Ifx":"swp4","Net":"Peer","Ext":""},{"Host":"xplf2","Ifx":"swp2","Net":"Peer","Ext":""},{"Host":"xplf2","Ifx":"swp4","Net":"Peer","Ext":""},{"Host":"x2","Ifx":"eth2","Net":"Peer","Ext":""}]}`

	testTwoWaypoints(t, Success, expected, XpnetMode,
		DeviceSelection{
			"x0", VtepMode, portal.NodeRealization_BareMetal,
		},
		DeviceSelection{
			"x2", VtepMode, portal.NodeRealization_BareMetal,
		},
	)
}

/*
	One Stem
*/

func TestOneStemAccessToAccess(t *testing.T) {
	expected := `{"Endpoints":[{"Host":"x0","Ifx":"eth2","Net":"Physical","Ext":""},{"Host":"x4","Ifx":"eth2","Net":"Physical","Ext":""}],"Waypoints":[{"Host":"xplf1","Ifx":"swp4","Net":"Access","Ext":""},{"Host":"xplf1","Ifx":"","Net":"Vtep","Ext":""},{"Host":"xplf1","Ifx":"swp2","Net":"Peer","Ext":""},{"Host":"xpsp1","Ifx":"swp3","Net":"Peer","Ext":""},{"Host":"xpsp1","Ifx":"","Net":"Vtep","Ext":""},{"Host":"xpsp1","Ifx":"swp5","Net":"Trunk","Ext":""},{"Host":"xplf3","Ifx":"swp2","Net":"Trunk","Ext":""},{"Host":"xplf3","Ifx":"swp4","Net":"Access","Ext":""}]}`

	testTwoWaypoints(t, Success, expected, XpnetMode,
		DeviceSelection{
			"x0", AccessMode, portal.NodeRealization_BareMetal,
		},
		DeviceSelection{
			"x4", AccessMode, portal.NodeRealization_BareMetal,
		},
	)
}

func TestOneStemAccessToTrunk(t *testing.T) {
	expected := `{"Endpoints":[{"Host":"x0","Ifx":"eth2","Net":"Physical","Ext":""},{"Host":"x4","Ifx":"eth2","Net":"Vlan","Ext":""}],"Waypoints":[{"Host":"xplf1","Ifx":"swp4","Net":"Access","Ext":""},{"Host":"xplf1","Ifx":"","Net":"Vtep","Ext":""},{"Host":"xplf1","Ifx":"swp2","Net":"Peer","Ext":""},{"Host":"xpsp1","Ifx":"swp3","Net":"Peer","Ext":""},{"Host":"xpsp1","Ifx":"","Net":"Vtep","Ext":""},{"Host":"xpsp1","Ifx":"swp5","Net":"Trunk","Ext":""},{"Host":"xplf3","Ifx":"swp2","Net":"Trunk","Ext":""},{"Host":"xplf3","Ifx":"swp4","Net":"Trunk","Ext":""}]}`

	testTwoWaypoints(t, Success, expected, XpnetMode,
		DeviceSelection{
			"x0", AccessMode, portal.NodeRealization_BareMetal,
		},
		DeviceSelection{
			"x4", TrunkMode, portal.NodeRealization_BareMetal,
		},
	)
}

func TestOneStemTrunkToAccess(t *testing.T) {
	expected := `{"Endpoints":[{"Host":"x0","Ifx":"eth2","Net":"Vlan","Ext":""},{"Host":"x4","Ifx":"eth2","Net":"Physical","Ext":""}],"Waypoints":[{"Host":"xplf1","Ifx":"swp4","Net":"Trunk","Ext":""},{"Host":"xplf1","Ifx":"","Net":"Vtep","Ext":""},{"Host":"xplf1","Ifx":"swp2","Net":"Peer","Ext":""},{"Host":"xpsp1","Ifx":"swp3","Net":"Peer","Ext":""},{"Host":"xpsp1","Ifx":"","Net":"Vtep","Ext":""},{"Host":"xpsp1","Ifx":"swp5","Net":"Trunk","Ext":""},{"Host":"xplf3","Ifx":"swp2","Net":"Trunk","Ext":""},{"Host":"xplf3","Ifx":"swp4","Net":"Access","Ext":""}]}`

	testTwoWaypoints(t, Success, expected, XpnetMode,
		DeviceSelection{
			"x0", TrunkMode, portal.NodeRealization_BareMetal,
		},
		DeviceSelection{
			"x4", AccessMode, portal.NodeRealization_BareMetal,
		},
	)
}

func TestOneStemAccessToVtep(t *testing.T) {
	expected := `{"Endpoints":[{"Host":"x0","Ifx":"eth2","Net":"Physical","Ext":""},{"Host":"x4","Ifx":"eth2","Net":"Vtep","Ext":""}],"Waypoints":[{"Host":"xplf1","Ifx":"swp4","Net":"Access","Ext":""},{"Host":"xplf1","Ifx":"","Net":"Vtep","Ext":""},{"Host":"xplf1","Ifx":"swp2","Net":"Peer","Ext":""},{"Host":"xpsp1","Ifx":"swp3","Net":"Peer","Ext":""},{"Host":"xpsp1","Ifx":"swp5","Net":"Peer","Ext":""},{"Host":"xplf3","Ifx":"swp2","Net":"Peer","Ext":""},{"Host":"xplf3","Ifx":"swp4","Net":"Peer","Ext":""},{"Host":"x4","Ifx":"eth2","Net":"Peer","Ext":""}]}`

	testTwoWaypoints(t, Success, expected, XpnetMode,
		DeviceSelection{
			"x0", AccessMode, portal.NodeRealization_BareMetal,
		},
		DeviceSelection{
			"x4", VtepMode, portal.NodeRealization_BareMetal,
		},
	)
}

func TestOneStemVtepToAccess(t *testing.T) {
	expected := `{"Endpoints":[{"Host":"x0","Ifx":"eth2","Net":"Vtep","Ext":""},{"Host":"x4","Ifx":"eth2","Net":"Physical","Ext":""}],"Waypoints":[{"Host":"x0","Ifx":"eth2","Net":"Peer","Ext":""},{"Host":"xplf1","Ifx":"swp4","Net":"Peer","Ext":""},{"Host":"xplf1","Ifx":"swp2","Net":"Peer","Ext":""},{"Host":"xpsp1","Ifx":"swp3","Net":"Peer","Ext":""},{"Host":"xpsp1","Ifx":"","Net":"Vtep","Ext":""},{"Host":"xpsp1","Ifx":"swp5","Net":"Trunk","Ext":""},{"Host":"xplf3","Ifx":"swp2","Net":"Trunk","Ext":""},{"Host":"xplf3","Ifx":"swp4","Net":"Access","Ext":""}]}`

	testTwoWaypoints(t, Success, expected, XpnetMode,
		DeviceSelection{
			"x0", VtepMode, portal.NodeRealization_BareMetal,
		},
		DeviceSelection{
			"x4", AccessMode, portal.NodeRealization_BareMetal,
		},
	)
}

func TestOneStemTrunkToTrunk(t *testing.T) {
	expected := `{"Endpoints":[{"Host":"x0","Ifx":"eth2","Net":"Vlan","Ext":""},{"Host":"x4","Ifx":"eth2","Net":"Vlan","Ext":""}],"Waypoints":[{"Host":"xplf1","Ifx":"swp4","Net":"Trunk","Ext":""},{"Host":"xplf1","Ifx":"","Net":"Vtep","Ext":""},{"Host":"xplf1","Ifx":"swp2","Net":"Peer","Ext":""},{"Host":"xpsp1","Ifx":"swp3","Net":"Peer","Ext":""},{"Host":"xpsp1","Ifx":"","Net":"Vtep","Ext":""},{"Host":"xpsp1","Ifx":"swp5","Net":"Trunk","Ext":""},{"Host":"xplf3","Ifx":"swp2","Net":"Trunk","Ext":""},{"Host":"xplf3","Ifx":"swp4","Net":"Trunk","Ext":""}]}`

	testTwoWaypoints(t, Success, expected, XpnetMode,
		DeviceSelection{
			"x0", TrunkMode, portal.NodeRealization_BareMetal,
		},
		DeviceSelection{
			"x4", TrunkMode, portal.NodeRealization_BareMetal,
		},
	)
}

func TestOneStemTrunkToVtep(t *testing.T) {
	expected := `{"Endpoints":[{"Host":"x0","Ifx":"eth2","Net":"Vlan","Ext":""},{"Host":"x4","Ifx":"eth2","Net":"Vtep","Ext":""}],"Waypoints":[{"Host":"xplf1","Ifx":"swp4","Net":"Trunk","Ext":""},{"Host":"xplf1","Ifx":"","Net":"Vtep","Ext":""},{"Host":"xplf1","Ifx":"swp2","Net":"Peer","Ext":""},{"Host":"xpsp1","Ifx":"swp3","Net":"Peer","Ext":""},{"Host":"xpsp1","Ifx":"swp5","Net":"Peer","Ext":""},{"Host":"xplf3","Ifx":"swp2","Net":"Peer","Ext":""},{"Host":"xplf3","Ifx":"swp4","Net":"Peer","Ext":""},{"Host":"x4","Ifx":"eth2","Net":"Peer","Ext":""}]}`

	testTwoWaypoints(t, Success, expected, XpnetMode,
		DeviceSelection{
			"x0", TrunkMode, portal.NodeRealization_BareMetal,
		},
		DeviceSelection{
			"x4", VtepMode, portal.NodeRealization_BareMetal,
		},
	)
}

func TestOneStemVtepToTrunk(t *testing.T) {
	expected := `{"Endpoints":[{"Host":"x0","Ifx":"eth2","Net":"Vtep","Ext":""},{"Host":"x4","Ifx":"eth2","Net":"Vlan","Ext":""}],"Waypoints":[{"Host":"x0","Ifx":"eth2","Net":"Peer","Ext":""},{"Host":"xplf1","Ifx":"swp4","Net":"Peer","Ext":""},{"Host":"xplf1","Ifx":"swp2","Net":"Peer","Ext":""},{"Host":"xpsp1","Ifx":"swp3","Net":"Peer","Ext":""},{"Host":"xpsp1","Ifx":"","Net":"Vtep","Ext":""},{"Host":"xpsp1","Ifx":"swp5","Net":"Trunk","Ext":""},{"Host":"xplf3","Ifx":"swp2","Net":"Trunk","Ext":""},{"Host":"xplf3","Ifx":"swp4","Net":"Trunk","Ext":""}]}`

	testTwoWaypoints(t, Success, expected, XpnetMode,
		DeviceSelection{
			"x0", VtepMode, portal.NodeRealization_BareMetal,
		},
		DeviceSelection{
			"x4", TrunkMode, portal.NodeRealization_BareMetal,
		},
	)
}

func TestOneStemVtepToVtep(t *testing.T) {
	expected := `{"Endpoints":[{"Host":"x0","Ifx":"eth2","Net":"Vtep","Ext":""},{"Host":"x4","Ifx":"eth2","Net":"Vtep","Ext":""}],"Waypoints":[{"Host":"x0","Ifx":"eth2","Net":"Peer","Ext":""},{"Host":"xplf1","Ifx":"swp4","Net":"Peer","Ext":""},{"Host":"xplf1","Ifx":"swp2","Net":"Peer","Ext":""},{"Host":"xpsp1","Ifx":"swp3","Net":"Peer","Ext":""},{"Host":"xpsp1","Ifx":"swp5","Net":"Peer","Ext":""},{"Host":"xplf3","Ifx":"swp2","Net":"Peer","Ext":""},{"Host":"xplf3","Ifx":"swp4","Net":"Peer","Ext":""},{"Host":"x4","Ifx":"eth2","Net":"Peer","Ext":""}]}`

	testTwoWaypoints(t, Success, expected, XpnetMode,
		DeviceSelection{
			"x0", VtepMode, portal.NodeRealization_BareMetal,
		},
		DeviceSelection{
			"x4", VtepMode, portal.NodeRealization_BareMetal,
		},
	)
}

/*
	Zero Stems
*/

func TestZeroStemsAccessToAccess(t *testing.T) {
	expected := `{"Endpoints":[{"Host":"x4","Ifx":"eth2","Net":"Physical","Ext":""},{"Host":"x6","Ifx":"eth2","Net":"Physical","Ext":""}],"Waypoints":[{"Host":"xplf3","Ifx":"swp4","Net":"Access","Ext":""},{"Host":"xplf3","Ifx":"swp2","Net":"Trunk","Ext":""},{"Host":"xpsp1","Ifx":"swp5","Net":"Trunk","Ext":""},{"Host":"xpsp1","Ifx":"swp6","Net":"Trunk","Ext":""},{"Host":"xplf4","Ifx":"swp2","Net":"Trunk","Ext":""},{"Host":"xplf4","Ifx":"swp4","Net":"Access","Ext":""}]}`

	testTwoWaypoints(t, Success, expected, XpnetMode,
		DeviceSelection{
			"x4", AccessMode, portal.NodeRealization_BareMetal,
		},
		DeviceSelection{
			"x6", AccessMode, portal.NodeRealization_BareMetal,
		},
	)
}

func TestZeroStemsAccessToTrunk(t *testing.T) {
	expected := `{"Endpoints":[{"Host":"x4","Ifx":"eth2","Net":"Physical","Ext":""},{"Host":"x6","Ifx":"eth2","Net":"Vlan","Ext":""}],"Waypoints":[{"Host":"xplf3","Ifx":"swp4","Net":"Access","Ext":""},{"Host":"xplf3","Ifx":"swp2","Net":"Trunk","Ext":""},{"Host":"xpsp1","Ifx":"swp5","Net":"Trunk","Ext":""},{"Host":"xpsp1","Ifx":"swp6","Net":"Trunk","Ext":""},{"Host":"xplf4","Ifx":"swp2","Net":"Trunk","Ext":""},{"Host":"xplf4","Ifx":"swp4","Net":"Trunk","Ext":""}]}`

	testTwoWaypoints(t, Success, expected, XpnetMode,
		DeviceSelection{
			"x4", AccessMode, portal.NodeRealization_BareMetal,
		},
		DeviceSelection{
			"x6", TrunkMode, portal.NodeRealization_BareMetal,
		},
	)
}

func TestZeroStemsAccessToVtep(t *testing.T) {
	expected := `{"Endpoints":[{"Host":"x4","Ifx":"eth2","Net":"Physical","Ext":""},{"Host":"x6","Ifx":"eth2","Net":"Vtep","Ext":""}],"Waypoints":[{"Host":"xplf3","Ifx":"swp4","Net":"Access","Ext":""},{"Host":"xplf3","Ifx":"swp2","Net":"Trunk","Ext":""},{"Host":"xpsp1","Ifx":"swp5","Net":"Trunk","Ext":""},{"Host":"xpsp1","Ifx":"","Net":"Vtep","Ext":""},{"Host":"xpsp1","Ifx":"swp6","Net":"Peer","Ext":""},{"Host":"xplf4","Ifx":"swp2","Net":"Peer","Ext":""},{"Host":"xplf4","Ifx":"swp4","Net":"Peer","Ext":""},{"Host":"x6","Ifx":"eth2","Net":"Peer","Ext":""}]}`

	testTwoWaypoints(t, Success, expected, XpnetMode,
		DeviceSelection{
			"x4", AccessMode, portal.NodeRealization_BareMetal,
		},
		DeviceSelection{
			"x6", VtepMode, portal.NodeRealization_BareMetal,
		},
	)
}

func TestZeroStemsTrunkToTrunk(t *testing.T) {
	expected := `{"Endpoints":[{"Host":"x4","Ifx":"eth2","Net":"Vlan","Ext":""},{"Host":"x6","Ifx":"eth2","Net":"Vlan","Ext":""}],"Waypoints":[{"Host":"xplf3","Ifx":"swp4","Net":"Trunk","Ext":""},{"Host":"xplf3","Ifx":"swp2","Net":"Trunk","Ext":""},{"Host":"xpsp1","Ifx":"swp5","Net":"Trunk","Ext":""},{"Host":"xpsp1","Ifx":"swp6","Net":"Trunk","Ext":""},{"Host":"xplf4","Ifx":"swp2","Net":"Trunk","Ext":""},{"Host":"xplf4","Ifx":"swp4","Net":"Trunk","Ext":""}]}`

	testTwoWaypoints(t, Success, expected, XpnetMode,
		DeviceSelection{
			"x4", TrunkMode, portal.NodeRealization_BareMetal,
		},
		DeviceSelection{
			"x6", TrunkMode, portal.NodeRealization_BareMetal,
		},
	)
}

func TestZeroStemsTrunkToVtep(t *testing.T) {
	expected := `{"Endpoints":[{"Host":"x4","Ifx":"eth2","Net":"Vlan","Ext":""},{"Host":"x6","Ifx":"eth2","Net":"Vtep","Ext":""}],"Waypoints":[{"Host":"xplf3","Ifx":"swp4","Net":"Trunk","Ext":""},{"Host":"xplf3","Ifx":"swp2","Net":"Trunk","Ext":""},{"Host":"xpsp1","Ifx":"swp5","Net":"Trunk","Ext":""},{"Host":"xpsp1","Ifx":"","Net":"Vtep","Ext":""},{"Host":"xpsp1","Ifx":"swp6","Net":"Peer","Ext":""},{"Host":"xplf4","Ifx":"swp2","Net":"Peer","Ext":""},{"Host":"xplf4","Ifx":"swp4","Net":"Peer","Ext":""},{"Host":"x6","Ifx":"eth2","Net":"Peer","Ext":""}]}`

	testTwoWaypoints(t, Success, expected, XpnetMode,
		DeviceSelection{
			"x4", TrunkMode, portal.NodeRealization_BareMetal,
		},
		DeviceSelection{
			"x6", VtepMode, portal.NodeRealization_BareMetal,
		},
	)
}

func TestZeroStemsVtepToVtep(t *testing.T) {
	expected := `{"Endpoints":[{"Host":"x4","Ifx":"eth2","Net":"Vtep","Ext":""},{"Host":"x6","Ifx":"eth2","Net":"Vtep","Ext":""}],"Waypoints":[{"Host":"x4","Ifx":"eth2","Net":"Peer","Ext":""},{"Host":"xplf3","Ifx":"swp4","Net":"Peer","Ext":""},{"Host":"xplf3","Ifx":"swp2","Net":"Peer","Ext":""},{"Host":"xpsp1","Ifx":"swp5","Net":"Peer","Ext":""},{"Host":"xpsp1","Ifx":"swp6","Net":"Peer","Ext":""},{"Host":"xplf4","Ifx":"swp2","Net":"Peer","Ext":""},{"Host":"xplf4","Ifx":"swp4","Net":"Peer","Ext":""},{"Host":"x6","Ifx":"eth2","Net":"Peer","Ext":""}]}`

	testTwoWaypoints(t, Success, expected, XpnetMode,
		DeviceSelection{
			"x4", VtepMode, portal.NodeRealization_BareMetal,
		},
		DeviceSelection{
			"x6", VtepMode, portal.NodeRealization_BareMetal,
		},
	)
}

func TestVirtualToPhysical(t *testing.T) {
	expected := `{"Endpoints":[{"Host":"x0","Ifx":"eth1","Net":"Physical","Ext":"Virtual"},{"Host":"x2","Ifx":"eth2","Net":"Vlan","Ext":""},{"Host":"x0","Ifx":"eth2","Net":"Vtep","Ext":""}],"Waypoints":[{"Host":"x0","Ifx":"eth1","Net":"Tap","Ext":""},{"Host":"x0","Ifx":"eth2","Net":"Peer","Ext":""},{"Host":"xplf1","Ifx":"swp4","Net":"Peer","Ext":""},{"Host":"xplf1","Ifx":"swp2","Net":"Peer","Ext":""},{"Host":"xpsp1","Ifx":"swp3","Net":"Peer","Ext":""},{"Host":"xpsp1","Ifx":"swp4","Net":"Peer","Ext":""},{"Host":"xplf2","Ifx":"swp2","Net":"Peer","Ext":""},{"Host":"xplf2","Ifx":"","Net":"Vtep","Ext":""},{"Host":"xplf2","Ifx":"swp4","Net":"Trunk","Ext":""}]}`

	testTwoWaypoints(t, Success, expected, XpnetMode,
		DeviceSelection{
			"x0", TrunkMode, portal.NodeRealization_VirtualMachine,
		},
		DeviceSelection{
			"x2", TrunkMode, portal.NodeRealization_BareMetal,
		},
	)
}

func TestVirtualToVirtual(t *testing.T) {
	expected := `{"Endpoints":[{"Host":"x0","Ifx":"eth1","Net":"Physical","Ext":"Virtual"},{"Host":"x2","Ifx":"eth1","Net":"Physical","Ext":"Virtual"},{"Host":"x0","Ifx":"eth2","Net":"Vtep","Ext":""},{"Host":"x2","Ifx":"eth2","Net":"Vtep","Ext":""}],"Waypoints":[{"Host":"x0","Ifx":"eth1","Net":"Tap","Ext":""},{"Host":"x2","Ifx":"eth1","Net":"Tap","Ext":""},{"Host":"x0","Ifx":"eth2","Net":"Peer","Ext":""},{"Host":"xplf1","Ifx":"swp4","Net":"Peer","Ext":""},{"Host":"xplf1","Ifx":"swp2","Net":"Peer","Ext":""},{"Host":"xpsp1","Ifx":"swp3","Net":"Peer","Ext":""},{"Host":"xpsp1","Ifx":"swp4","Net":"Peer","Ext":""},{"Host":"xplf2","Ifx":"swp2","Net":"Peer","Ext":""},{"Host":"xplf2","Ifx":"swp4","Net":"Peer","Ext":""},{"Host":"x2","Ifx":"eth2","Net":"Peer","Ext":""}]}`

	testTwoWaypoints(t, Success, expected, XpnetMode,
		DeviceSelection{
			"x0", TrunkMode, portal.NodeRealization_VirtualMachine,
		},
		DeviceSelection{
			"x2", TrunkMode, portal.NodeRealization_VirtualMachine,
		},
	)
}

func TestVirtualToItself(t *testing.T) {
	expected := `{"Endpoints":[{"Host":"x0","Ifx":"eth1","Net":"Physical","Ext":"Virtual"},{"Host":"x0","Ifx":"eth2","Net":"Physical","Ext":"Virtual"}],"Waypoints":[{"Host":"x0","Ifx":"eth1","Net":"Tap","Ext":""},{"Host":"x0","Ifx":"eth2","Net":"Tap","Ext":""}]}`

	testTwoWaypoints(t, Success, expected, XpnetMode,
		DeviceSelection{
			"x0", TrunkMode, portal.NodeRealization_VirtualMachine,
		},
		DeviceSelection{
			"x0", TrunkMode, portal.NodeRealization_VirtualMachine,
		},
	)
}

/*
	Edge Cases
*/

// It's possible to pass 1 endpoint as a VTEP, but none of the waypoints are capable of a VTEP,
// which the pathfinder isn't capable of constructing a path yet
// This is probably possible if you don't use the shortest path, but a path that goes up to
// a spine, then trunking all the way back down
func TestLocalZeroStemsAccessToVtep(t *testing.T) {
	expected := ``

	testTwoWaypoints(t, Failure, expected, XpnetMode,
		DeviceSelection{
			"x4", AccessMode, portal.NodeRealization_BareMetal,
		},
		DeviceSelection{
			"x6", VtepMode, portal.NodeRealization_BareMetal,
		},
	)
}

func TestPhysicalNodeToItself(t *testing.T) {
	expected := `{"Endpoints":[{"Host":"x0","Ifx":"eth2","Net":"Physical","Ext":""}],"Waypoints":[]}`

	testTwoWaypoints(t, Failure, expected, XpnetMode,
		DeviceSelection{
			"x0", AccessMode, portal.NodeRealization_BareMetal,
		},
		DeviceSelection{
			"x0", VtepMode, portal.NodeRealization_BareMetal,
		},
	)
}

/*
	Helpers
*/

type DeviceSelection struct {
	Node string
	Mode EndpointMode
	Kind portal.NodeRealization_Kind
}

func testTwoWaypoints(t *testing.T, result ExpectedResult, expected string, net NetworkMode, from, to DeviceSelection) {

	// Bookkeeping

	var ds portal.Diagnostics

	name1 := fmt.Sprintf("%s ~> %s", from.Node, to.Node)
	name2 := fmt.Sprintf("%s ~> %s", to.Node, from.Node)
	ex_name := "expected"

	// Make sure to dump diagnostics at the end
	defer func() {
		// Dump diagnostics

		s_ds := ds.ToString()
		if s_ds != "" {
			t.Logf("\n%s\n", s_ds)
		}

		// Report error, based on whether we expected it or not

		if result == Success && ds.Error() {
			t.Error("diagnostics had an unexpected error\n")
		} else if result == Failure && !ds.Error() {
			t.Error("diagnostics had no error, but we expected an error\n")
		}
	}()

	// Unmarshal LinkSegments
	var eseg MiniLinkSegment
	err := json.Unmarshal([]byte(expected), &eseg)
	if err != nil {
		ds = append(ds, DiagnosticErrorf("unmarshal: %+v", err))
	}

	// Build segments in both directions

	s1, ds1 := buildSegment(t, net, from, to)
	s2, ds2 := buildSegment(t, net, to, from)

	ms1 := MiniLinkSegmentFromLinkSegment(s1)
	ms2 := MiniLinkSegmentFromLinkSegment(s2)

	// Check if diagnostics are the same

	if !ds1.Equals(ds2) {
		ds = append(ds, ds1...)
		ds = append(ds, ds2...)
		ds = append(ds, DiagnosticWarningf("The two returned diagnostics are not equal"))
	} else {
		ds = ds1
	}

	if ds.Error() {
		return
	}

	// Check if the segments are the same and as expected

	identical := s1.Equals(s2)
	if !identical {
		ds = append(ds, DiagnosticErrorf("%s, %s: segments differ", name1, name2))
	}

	correct := eseg.Equals(ms1)
	if !correct {
		ds = append(ds, DiagnosticErrorf("%s, %s: segments differ", ex_name, name1))
	}

	if !identical && !eseg.Equals(ms2) {
		ds = append(ds, DiagnosticErrorf("%s, %s: segments differ", ex_name, name2))
	}

	// Dump a segment in json
	// This is used for generating the expected segment in the test cases
	if identical && !correct && result == Success {
		dat, err := json.Marshal(ms1)
		if err != nil {
			ds = append(ds, DiagnosticErrorf("marshal: %+v", err))
		}

		t.Logf("\n`%s`\n", dat)
	}

	// Log the unique segments produced
	var lsi uint64 = 100

	if !identical {
		t.Logf("\n%s:\n%s\n", name1, s1.ToString("  ", lsi))
		t.Logf("\n%s:\n%s\n", name2, s2.ToString("  ", lsi))

	} else {
		if !correct {
			t.Logf("\n%s:\n%s\n", ex_name, eseg.ToString("  ", lsi))
		}

		t.Logf("\n%s:\n%s\n", name1, ms1.ToString("  ", lsi))
	}

}

func buildSegment(t *testing.T, net NetworkMode, from, to DeviceSelection) (*portal.LinkSegment, portal.Diagnostics) {
	var ds portal.Diagnostics

	// Pathfinder currently picks a random shortest route
	// so initialize rand to a consistent value
	rand.Seed(0xDAADCAFE)

	// Bookkeeping

	tbx, rt, err := initMixedFacilityPath()
	if err != nil {
		return nil, append(ds, DiagnosticErrorf("%+v", err))
	}

	e := NewEmbedding()
	seg := new(portal.LinkSegment)

	e.Nodes[from.Node] = &NodeEmbedding{
		Kind:     from.Kind,
		XpnetIdx: 0,
		VmAlloc:  new(xir.ResourceAllocation),
	}
	e.Nodes[to.Node] = &NodeEmbedding{
		Kind:     to.Kind,
		XpnetIdx: 0,
		VmAlloc:  new(xir.ResourceAllocation),
	}

	// Get devices from strings

	fromDev := tbx.Device(from.Node)
	toDev := tbx.Device(to.Node)
	if fromDev == nil {
		ds = append(ds, DiagnosticErrorf("could not find %s", from.Node))
		return seg, ds
	}
	if toDev == nil {
		ds = append(ds, DiagnosticErrorf("could not find %s", to.Node))
		return seg, ds
	}

	// Get interfaces on the devices

	fromIfx, _, d := e.selectXpInterface(fromDev, rt)
	ds = append(ds, d...)
	if d.Error() {
		return seg, ds
	}
	toIfx, _, d := e.selectXpInterface(toDev, rt)
	ds = append(ds, d...)
	if d.Error() {
		return seg, ds
	}

	// Add endpoints from the interfaces

	fromEp, d := e.addSegmentEndpoint(
		net,
		seg,
		from.Node,
		fromIfx,
		from.Mode,
	)
	ds = append(ds, d...)
	if d.Error() {
		return seg, ds
	}

	toEp, d := e.addSegmentEndpoint(
		net,
		seg,
		to.Node,
		toIfx,
		to.Mode,
	)
	ds = append(ds, d...)
	if d.Error() {
		return seg, ds
	}

	// Compute waypoints from endpoints

	d = e.addSegmentWaypoints(
		net,
		seg,
		fromIfx, toIfx,
		fromEp, toEp,
		rt,
		tbx,
	)
	ds = append(ds, d...)
	if d.Error() {
		return seg, ds
	}

	// Clean macs from Virtual Endpoints and Taps
	for i, ep := range seg.Endpoints {
		if ep.Virtual {
			seg.Endpoints[i].Mac = ""
			seg.Endpoints[i].GetPhy().Mac = ""
		}
	}

	for i, wp := range seg.Waypoints {
		if wp.GetTap() != nil {
			seg.Waypoints[i].GetTap().Frontend.Mac = ""
		}
	}

	return seg, ds
}
