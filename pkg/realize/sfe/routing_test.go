package sfe

import (
	"testing"
)

/*
	This test cannot fail; this is used to debug the routing tables.
*/

func testRoutingTable(t *testing.T, filename string) {
	tbx, err := loadFacility(filename)
	if err != nil {
		t.Logf("error loading facility: %+v", err)
		return
	}

	AssignTPAsInfraNet(0x004D000000000000, tbx)
	AssignTPAsXpNet(0x004E000000000000, tbx)

	rt := BuildXpRoutingTable(tbx)
	irt := BuildInfraRoutingTable(tbx)

	t.Logf("\n%s", rt.String(tbx))
	t.Logf("\n%s", irt.String(tbx))
}

func testLH_Routes(t *testing.T) {
	testRoutingTable(t, "facilities/lighthouse.xir")
}
