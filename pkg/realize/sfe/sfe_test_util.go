package sfe

import (
	"encoding/json"
	"fmt"
	"hash/fnv"
	"math/rand"
	"os"
	"path/filepath"
	"reflect"
	"sort"
	"strings"
	"testing"

	"github.com/maruel/natural"
	"google.golang.org/protobuf/encoding/protojson"

	portal "gitlab.com/mergetb/api/portal/v1/go"
	xir "gitlab.com/mergetb/xir/v0.3/go"
)

type ExpectedResult int

const (
	Failure ExpectedResult = iota
	Success
	Any
)

/*
	A MiniNetworkPoint is a mini version of the endpoints and waypoints
	normally found in a LinkSegment.

	The reason for using these instead of the actual endpoints and waypoints is:
		- To reduce the amount of data we need to store for testing and comparisons
		- When a breaking API change occurs, we just have to update the function that translates
			from a endpoint/waypoint to MiniNetworkPoint
*/

type MiniNetPoint struct {
	Host string
	Ifx  string
	Net  string // is something along the lines of vlan/phy/vtep or trunk/access/vtep/peer or so
	Ext  string // extra stuff, like is virtual
}

func (np *MiniNetPoint) Hash() uint32 {
	h := fnv.New32a()

	if np == nil {
		return h.Sum32()
	}

	h.Write([]byte(np.String()))

	return h.Sum32()
}

func (np *MiniNetPoint) Equals(other interface{}) bool {
	x, ok := other.(*MiniNetPoint)

	if !ok {
		return false
	}

	if (np != nil && x == nil) || (np == nil && x != nil) {
		return false
	}

	if np == nil && x == nil {
		return true
	}

	return reflect.DeepEqual(np, x)
}

func (np MiniNetPoint) String() string {
	ifx := ""
	if np.Ifx != "" {
		ifx = "." + np.Ifx
	}

	if np.Ext != "" {
		return fmt.Sprintf("%s @ %s%s (%s)", np.Net, np.Host, ifx, np.Ext)
	}

	return fmt.Sprintf("%s @ %s%s", np.Net, np.Host, ifx)
}

func NetPointFromEndpoint(ep *portal.Endpoint) *MiniNetPoint {

	host := ep.Host
	net := "unknown"
	ifx := "unknown"

	extra := ""
	if ep.Virtual {
		extra = "Virtual"
		host = ep.Node
	}

	switch ep.Interface.(type) {
	case *portal.Endpoint_Phy:
		net = "Physical"
		ifx = ep.GetPhy().GetName()

	case *portal.Endpoint_Vlan:
		net = "Vlan"
		ifx = ep.GetVlan().GetParent().GetName()

	case *portal.Endpoint_Vtep:
		net = "Vtep"
		ifx = ep.GetVtep().GetParent().GetName()
	}

	return &MiniNetPoint{
		Host: host,
		Ifx:  ifx,
		Net:  net,
		Ext:  extra,
	}
}

func NetPointFromWaypoint(wp *portal.Waypoint) *MiniNetPoint {

	net := "unknown"
	ifx := "unknown"
	host := wp.Host

	switch wp.Interface.(type) {
	case *portal.Waypoint_Access:
		net = "Access"
		ifx = wp.GetAccess().GetPort().GetName()

	case *portal.Waypoint_Tap:
		net = "Tap"
		ifx = wp.GetTap().GetFrontend().GetName()
		host = wp.GetTap().GetNode()

	case *portal.Waypoint_Trunk:
		net = "Trunk"
		ifx = wp.GetTrunk().GetPort().GetName()

	case *portal.Waypoint_Vtep:
		net = "Vtep"
		ifx = wp.GetVtep().GetParent().GetName()

	case *portal.Waypoint_BgpPeer:
		net = "Peer"
		ifx = wp.GetBgpPeer().GetInterface().GetName()
	}

	return &MiniNetPoint{
		Host: host,
		Ifx:  ifx,
		Net:  net,
	}
}

type MiniLinkSegment struct {
	Endpoints []*MiniNetPoint
	Waypoints []*MiniNetPoint
}

func (seg *MiniLinkSegment) Equals(other interface{}) bool {
	x, ok := other.(*MiniLinkSegment)

	if !ok {
		return false
	}

	if (seg != nil && x == nil) || (seg == nil && x != nil) {
		return false
	}

	if seg == nil && x == nil {
		return true
	}

	h1 := make([]portal.Hashable, 0, len(seg.Endpoints)+len(seg.Waypoints))
	for _, ep := range seg.Endpoints {
		h1 = append(h1, ep)
	}

	for _, wp := range seg.Waypoints {
		h1 = append(h1, wp)
	}

	h2 := make([]portal.Hashable, 0, len(x.Endpoints)+len(x.Waypoints))
	for _, ep := range x.Endpoints {
		h2 = append(h2, ep)
	}

	for _, wp := range x.Waypoints {
		h2 = append(h2, wp)
	}

	return portal.HashSliceEquals(h1, h2)
}

func (seg *MiniLinkSegment) ToString(indent string, lsi uint64) string {

	if seg == nil {
		return "nil"
	}

	s := indent + "ENDPOINTS:\n"
	for _, ep := range seg.Endpoints {
		s += indent + indent + fmt.Sprintf("[%d] %s\n", lsi, ep.String())
	}
	s += indent + "WAYPOINTS:\n"
	for _, wp := range seg.Waypoints {
		s += indent + indent + fmt.Sprintf("[%d] %s\n", lsi, wp.String())
	}

	return s
}

func MiniLinkSegmentFromLinkSegment(seg *portal.LinkSegment) *MiniLinkSegment {
	if seg == nil {
		return nil
	}

	eps := make([]*MiniNetPoint, len(seg.Endpoints))

	for i, ep := range seg.Endpoints {
		eps[i] = NetPointFromEndpoint(ep)
	}

	wps := make([]*MiniNetPoint, len(seg.Waypoints))

	for i, wp := range seg.Waypoints {
		wps[i] = NetPointFromWaypoint(wp)
	}

	return &MiniLinkSegment{
		Endpoints: eps,
		Waypoints: wps,
	}
}

type MiniLinkRealization struct {
	Segments map[uint64]*MiniLinkSegment
}

func (lrz *MiniLinkRealization) Equals(other *MiniLinkRealization) bool {
	if (lrz != nil && other == nil) || (lrz == nil && other != nil) {
		return false
	}

	if lrz == nil && other == nil {
		return true
	}

	if len(lrz.Segments) != len(other.Segments) {
		return false
	}

	for id, s_seg := range lrz.Segments {
		o_seg, ok := other.Segments[id]

		if !ok || !s_seg.Equals(o_seg) {
			return false
		}
	}

	return true
}

func (lrz *MiniLinkRealization) ToString(indent string) string {

	if lrz == nil {
		return "nil"
	}

	// Sort ids

	ids := make([]uint64, 0, len(lrz.Segments))
	for k := range lrz.Segments {
		ids = append(ids, k)
	}
	sort.Slice(ids,
		func(i, j int) bool {
			return ids[i] < ids[j]
		},
	)

	// Actually print the ids

	s := ""
	for _, lsi := range ids {
		s += lrz.Segments[lsi].ToString(indent, lsi)
	}
	return s

}

/*
	A MiniRealization just contains the Nodes and the Links

	The nodes are stored in a map[string][string], where:
		- The Key is the node name (in the xp model)
		- the Value is the node name (in the testbed model) and if it's physical or virtual

	The Links are stored in a Realization object,
	so we can use protojson to unmarshal them.
	(Regular json doesn't work and protojson doesn't on []LinkSegment)

	We use MiniRealizations instead of a proper realization because
	in a regular realization as to minimize the amount of data we need to
	store for testing and comparisons
*/

type MiniRealization struct {
	Nodes map[string]string
	Links map[string]*MiniLinkRealization
}

func embeddingToMiniRlz(embedding *Embedding) MiniRealization {

	nodes := make(map[string]string, len(embedding.Nodes))

	for node, embedding := range embedding.Nodes {
		nodes[node] = fmt.Sprintf("%s (%s)", embedding.Resource.Id(), embedding.Kind)
	}

	links := make(map[string]*MiniLinkRealization)

	for link, lxpa := range embedding.Links {
		lbl := link.Label()
		if lbl == "" {
			lbl = lxpa.Link.Id
		}

		lrz := &MiniLinkRealization{
			Segments: make(map[uint64]*MiniLinkSegment),
		}

		links[lbl] = lrz

		for id, seg := range lxpa.Segments {
			links[lbl].Segments[id] = MiniLinkSegmentFromLinkSegment(seg)
		}
	}

	miniRlz := MiniRealization{
		Nodes: nodes,
		Links: links,
	}

	return miniRlz
}

func (mr MiniRealization) Equals(other MiniRealization) bool {
	if len(mr.Links) != len(other.Links) {
		return false
	}

	if !reflect.DeepEqual(mr.Nodes, other.Nodes) {
		return false
	}

	for id, s_lrz := range mr.Links {
		o_lrz, ok := other.Links[id]

		if !ok || !s_lrz.Equals(o_lrz) {
			return false
		}
	}

	return true
}

func (mr MiniRealization) ToString() string {
	// Print Nodes

	nodes := make([]string, 0, len(mr.Nodes))
	for n := range mr.Nodes {
		nodes = append(nodes, n)
	}
	sort.Sort(natural.StringSlice(nodes))

	s := fmt.Sprintf("NODES (%d)\n", len(mr.Nodes))

	for _, xnode := range nodes {
		rnode := mr.Nodes[xnode]
		s += fmt.Sprintf("%s -> %s\n", xnode, rnode)
	}

	// Print Links

	links := make([]string, 0, len(mr.Links))
	found_infranet := false
	found_harbornet := false

	for l := range mr.Links {
		switch l {
		case "infranet":
			found_infranet = true
		case "harbor":
			found_harbornet = true
		default:
			links = append(links, l)
		}
	}
	sort.Sort(natural.StringSlice(links))

	if found_infranet {
		links = append(links, "infranet")
	}

	if found_harbornet {
		links = append(links, "harbor")
	}

	s += fmt.Sprintf("LINKS (%d)\n", len(mr.Links))

	for _, xlink := range links {
		s += fmt.Sprintln(xlink)
		s += mr.Links[xlink].ToString("  ")
	}

	s += fmt.Sprintln("")

	return s

}

func testEmbedSys(t *testing.T, tbx *xir.Topology, root *xir.Device, result ExpectedResult, expected string) {

	// Bookkeeping
	var ds portal.Diagnostics

	// Pathfinder currently picks a random shortest route
	// so initialize rand to a consistent value
	rand.Seed(0xDAADCAFE)

	embedding, d, err := EmbedSys(
		root,
		tbx,
	)
	if err != nil {
		ds = append(ds, DiagnosticErrorf("embed error: %+v", err))
	}

	ds = append(ds, d...)

	testEmbedToRlz(t, result, expected, "embed_sys", embedding, ds)
}

func testExperiment(t *testing.T, tbx *xir.Topology, root *xir.Device, result ExpectedResult, expected, filename string) {

	// Bookkeeping
	var ds portal.Diagnostics

	// Pathfinder currently picks a random shortest route
	// so initialize rand to a consistent value
	rand.Seed(0xDAADCAFE)

	// Build embedding

	xp, err := loadExperiment(filename)
	if err != nil {
		t.Logf("Error loading %s: %+v", filename, err)
		return
	}

	a := portal.NewAllocationTable()

	embedding, d, err := Embed(
		root,
		tbx,
		xp,
		a,
		nil,
		"test.test.test",
		xir.Emulation_Netem,
	)
	if err != nil {
		ds = append(ds, DiagnosticErrorf("embed error: %+v", err))
	}

	ds = append(ds, d...)

	testEmbedToRlz(t, result, expected, filepath.Base(filename), embedding, ds)
}

func testEmbedToRlz(t *testing.T, result ExpectedResult, expected, name string, embedding *Embedding, ds portal.Diagnostics) {

	// Make sure to dump diagnostics at the end
	defer func() {
		// Dump diagnostics

		s_ds := ds.ToString()
		if s_ds != "" {
			t.Logf("\n%s\n", s_ds)
		}

		// Report error, based on whether we expected it or not

		if result == Success && ds.Error() {
			t.Error("diagnostics had an unexpected error\n")
		} else if result == Failure && !ds.Error() {
			t.Error("diagnostics had no error, but we expected an error\n")
		}
	}()

	rlz := embeddingToMiniRlz(embedding)

	var expt_rlz MiniRealization
	err := json.Unmarshal([]byte(expected), &expt_rlz)
	if err != nil {
		ds = append(ds, DiagnosticErrorf("unmarshal mini realization: %+v", err))
	}

	// Check if identical
	correct := expt_rlz.Equals(rlz)
	if !correct {
		ds = append(ds, DiagnosticErrorf("expected, %s: embeddings differ", name))
	}

	// Dump the rlz in json
	// This is used for generating the expected rlz in the test cases
	if !correct && result == Success {
		d_nodes, err := json.Marshal(rlz)
		if err != nil {
			ds = append(ds, DiagnosticErrorf("marshal mini realization: %+v", err))
		} else {
			t.Logf("\n`%s`\n", string(d_nodes))
		}
	}

	// Log the unique embeddings produced
	if !correct {
		t.Logf("\n%s:\n%s\n", "expected", expt_rlz.ToString())
	}

	t.Logf("\n%s:\n%s\n", name, rlz.ToString())
}

func findpath(fp string) (string, error) {
	fullpath, err := os.Getwd()
	if err != nil {
		return "", err
	}
	if strings.Contains(filepath.Base(fullpath), "services") {
		fullpath = filepath.Join(fullpath, "/pkg/realize/sfe/")
	}

	if strings.Contains(filepath.Base(fullpath), "sfe") {
		fullpath = filepath.Join(fullpath, "/tests/", fp)
	} else {
		return "", fmt.Errorf("could not find %s", fp)
	}

	return fullpath, nil
}

func loadFacility(path string) (*xir.Topology, error) {
	fullpath, err := findpath(path)
	if err != nil {
		return nil, err
	}

	buf, err := os.ReadFile(fullpath)
	if err != nil {
		return nil, err
	}

	fac := new(xir.Facility)

	if strings.HasSuffix(path, ".json") {
		err = protojson.Unmarshal(buf, fac)
	} else {
		fac, err = xir.FacilityFromB64String(string(buf))
	}

	if err != nil {
		return nil, err
	}

	return fac.Lift(), err
}

func loadExperiment(path string) (*xir.Topology, error) {

	fullpath, err := findpath(path)
	if err != nil {
		return nil, err
	}

	buf, err := os.ReadFile(fullpath)
	if err != nil {
		return nil, err
	}

	net := new(xir.Network)
	if strings.HasSuffix(path, ".json") {
		err = protojson.Unmarshal(buf, net)
	} else {
		net, err = xir.NetworkFromB64String(string(buf))
	}

	if err != nil {
		return nil, err
	}

	return net.Lift(), err
}
