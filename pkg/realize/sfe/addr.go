package sfe

import (
	"fmt"

	"gitlab.com/mergetb/xir/v0.3/go"
)

type TPA uint64

func (t TPA) String() string {

	s := fmt.Sprintf("%016x", uint64(t))
	return fmt.Sprintf("%s:%s:%s:%s",
		s[:4],
		s[4:8],
		s[8:12],
		s[12:16],
	)

}

func (t TPA) GetTestbed() uint16 {
	return uint16(t >> 40)
}
func (t TPA) SetTestbed(x uint16) TPA {
	n := uint64(x)
	n <<= 40
	return TPA(uint64(t)&0x0000ffffffffffff | n)
}

func (t TPA) GetNetwork() uint16 {
	return uint16((t >> 32) & (0x0000ffff))
}
func (t TPA) SetNetwork(x uint16) TPA {
	n := uint64(x)
	n <<= 32
	return TPA(uint64(t)&0xffff0000ffffffff | n)
}

func (t TPA) GetHost() uint16 {
	return uint16((t >> 16) & (0x00000000ffff))
}
func (t TPA) SetHost(x uint16) TPA {
	n := uint64(x)
	n <<= 16
	return TPA(uint64(t)&0xffffffff0000ffff | n)
}

func (t TPA) GetGuest() uint16 {
	return uint16(t & 0x000000000000ffff)
}
func (t TPA) SetGuest(x uint16) TPA {
	n := uint64(x)
	return TPA(uint64(t)&0xffffffffffff0000 | n)
}

func AssignTPAsXpNet(tbAddr TPA, tbx *xir.Topology) (
	map[*xir.Device]TPA, map[*xir.Interface]TPA,
) {

	return AssignAddrs(
		tbAddr,
		tbx,
		[]xir.Role{
			xir.Role_XpSwitch,
			xir.Role_Gateway,
		},
		[]xir.Role{
			xir.Role_TbNode,
			xir.Role_NetworkEmulator,
		},
	)

}

func AssignTPAsInfraNet(tbAddr TPA, tbx *xir.Topology) (
	map[*xir.Device]TPA, map[*xir.Interface]TPA,
) {

	return AssignAddrs(
		tbAddr,
		tbx,
		[]xir.Role{
			xir.Role_InfraSwitch,
			xir.Role_Gateway,
		},
		[]xir.Role{
			xir.Role_TbNode,
			xir.Role_NetworkEmulator,
			xir.Role_StorageServer,
			xir.Role_InfrapodServer,
			xir.Role_InfraServer,
			xir.Role_MinIOHost,
			xir.Role_SledHost,
			xir.Role_RallyHost,
			xir.Role_Hypervisor,
		},
	)

}

func AssignAddrs(
	tbAddr TPA,
	tbx *xir.Topology,
	switchRoles []xir.Role,
	hostRoles []xir.Role,
) (
	map[*xir.Device]TPA, map[*xir.Interface]TPA,
) {

	var switches []*xir.Device
	for _, dev := range tbx.Devices {

		r := dev.Data.(*xir.Resource)
		if r == nil {
			continue
		}
		if r.HasRole(switchRoles...) {
			switches = append(switches, dev)
		}

	}

	lm := make(map[*xir.Device]TPA)
	nm := make(map[*xir.Interface]TPA)

	addr := tbAddr

	for _, sw := range switches {

		addr = addr.SetNetwork(addr.GetNetwork() + 1)
		lm[sw] = addr

		sw.Resource().TPA = uint64(addr)

		var nodes []*xir.Neighbor
		for _, x := range sw.Neighbors() {
			if x.Remote.Device.Resource().HasRole(hostRoles...) {
				nodes = append(nodes, x)
			}
		}

		nodeAddr := addr
		for _, node := range nodes {

			nodeAddr = nodeAddr.SetHost(nodeAddr.GetHost() + 1)
			nm[node.Remote] = nodeAddr
			node.Remote.Port().TPA = uint64(nodeAddr)

		}

	}

	return lm, nm

}
