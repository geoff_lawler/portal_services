from mergexp import *

# Create a netwok topology object.
net = Network('hello-world')

# Create two nodes.
a,b,c = [net.node(name) for name in ['a', 'b', 'c']]

# Create a link connecting the two nodes.
link = net.connect([a,b,c])

# Give IP addresses to the nodes on the link just created.
link[a].socket.addrs = ip4('10.0.0.1/24')
link[b].socket.addrs = ip4('10.0.0.2/24')
link[c].socket.addrs = ip4('10.0.0.3/24')

# Make this file a runnable experiment based on our two node topology.
experiment(net)
