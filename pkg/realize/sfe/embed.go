package sfe

import (
	"fmt"
	"sort"
	_ "strings"
	"time"

	"github.com/maruel/natural"
	log "github.com/sirupsen/logrus"

	portal "gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/portal/services/pkg/storage"
	xir "gitlab.com/mergetb/xir/v0.3/go"
	"gitlab.com/mergetb/xir/v0.3/go/build"
)

const (
	HarborLsi          = 3
	LsiBegin           = 100
	MarinerInfraBridge = "mbr"
	DefaultMtu         = 1500
	JumboMtu           = 9216
)

type NodeEmbedding struct {
	Node         *xir.Node                   // node represents the cyber entity
	Resource     *xir.Device                 // resource represents the physical resource hosting the node
	Kind         portal.NodeRealization_Kind // whether the node is a bare metal or VM entity
	InfranetAddr string                      // the infranet address assigned to the node
	XpnetIdx     uint32                      // Number of xpnet NICs allocated for the node embedding (VMs only)
	VmAlloc      *xir.ResourceAllocation     // XIR representation of the resources allocated for the node (VMs only)
}

type LinkEmulation struct {
	Interfaces []*portal.Vtep // vteps on the emu server
	Link       *portal.LinkEmulation
}

type LinkEmulationParams struct {
	Links   []*LinkEmulation   // the emulated links
	Servers storage.EmuServers // emu server allocations
	Backend xir.Emulation      // emulation type
}

type Embedding struct {
	Nodes          map[string]*NodeEmbedding                   // map xpnode name to the embedding of that node
	Links          map[*xir.Connection]*portal.LinkRealization // map connection to link realization
	Endpoints      map[*xir.Interface][]*portal.Endpoint       // map interface to slice of endpoints on that interface
	lsi            uint64                                      // link segment id counter
	InfrapodServer string                                      // hostname of infrapod server supporting this realization.
	InfrapodSite   string                                      // facility hosting the infrapod server
	VirtualMacs    map[string]bool                             // MACs allocated for VMs in this embedding
	LinkEmulation  *LinkEmulationParams                        // Link emulation parameters
}

// getEmulationServers returns a list of allocatable emulation servers from the topology
func getEmulationServers(tbx *xir.Topology) []string {
	var nodes []string
	for _, r := range tbx.Devices {
		if r.Resource().HasRole(xir.Role_NetworkEmulator) && !r.Resource().HasAllocMode(xir.AllocMode_NoAlloc) {
			nodes = append(nodes, r.Resource().Id)
		}
	}
	return nodes
}

func NewEmbedding() *Embedding {
	return &Embedding{
		Nodes: make(map[string]*NodeEmbedding),
		Links: make(map[*xir.Connection]*portal.LinkRealization),
		// map physical interfaces to endpoint realizations
		Endpoints:   make(map[*xir.Interface][]*portal.Endpoint),
		lsi:         LsiBegin,
		VirtualMacs: make(map[string]bool),
	}
}

func Embed(
	tbroot *xir.Device,
	tbx, xp *xir.Topology,
	a *portal.AllocationTable,
	resourcePool *portal.Pool,
	mzid string,
	emubackend xir.Emulation,
) (
	*Embedding, portal.Diagnostics, error,
) {

	var ds portal.Diagnostics

	if tbroot == nil {
		return nil, ds, fmt.Errorf("root cannot be nil")
	}

	if tbx == nil {
		return nil, ds, fmt.Errorf("testbed model cannot be nil")
	}

	if xp == nil {
		return nil, ds, fmt.Errorf("experiment cannot be nil")
	}

	/* XXX
	 * Assign TPAs for all infranet entities. This can probably be done at commissioning time,
	 * but is here for simplicity for the time being
	 */
	AssignTPAsInfraNet(0x004D000000000000, tbx)

	/* XXX
	 * Assign TPAs for all xpnet entities. This can probably be done at commissioning time,
	 * but is here for simplicity for the time being
	 */
	AssignTPAsXpNet(0x004E000000000000, tbx)

	log.Debugf("%d guests for embedding", len(xp.Devices))

	allhosts := Hosts(tbroot)
	log.Debugf("%d hosts for embedding", len(allhosts))

	// filter by the resource pool allowed to this rlz.
	hosts := []*Host{}
	if resourcePool != nil {
		for _, host := range allhosts {
			for _, facility := range resourcePool.Facilities {
				// May want to make the facility resources into a map for faster lookup.
				for _, resource := range facility.Resources {
					if host.Device.Id() == resource {
						// When the portal supports multiple-facilities, the
						// host list will need to be namespaced per facility as
						// facilities may have the same name for resources.
						hosts = append(hosts, host)
						break
					}
				}
			}
		}
	} else {
		hosts = allhosts
	}

	applyAllocs(hosts, a)

	start := time.Now()
	rt := BuildXpRoutingTable(tbx)
	irt := BuildInfraRoutingTable(tbx)

	log.Debugf("Building routing tables took %v", time.Since(start))

	/*DEBUG
	rt.Dump()
	*/

	ComputeCosts(hosts)
	guests := Guests(xp)

	/*DEBUG
	for _, x := range hosts {
		fmt.Printf("%s\n", x)
	}
	for _, x := range guests {
		fmt.Printf("%s\n", x)
	}
	*/

	metal, virt := Sort(hosts)
	log.Debugf("%d bare metal hosts for embedding", len(metal))
	log.Debugf("%d hypervisor hosts for embedding", len(virt))

	e := NewEmbedding()

	// Clean the segments before we're done
	defer func() {
		CleanSegments(e)
	}()

	d := EmbedNodes(e, a, guests, metal, virt, mzid)
	ds = append(ds, d...)
	if d.Error() {
		return e, ds, nil
	}

	for guest, em := range e.Nodes {
		log.Debugf("%s -> %s (%s)\n", guest, em.Resource.Id(), em.Kind.String())
	}

	// Not all functions have access to the guest -> host mapping,
	// so fill it in at the end
	defer func() {
		ds = FillInHostGuestInfo(ds, e.Nodes)
	}()

	//TODO at this point we should prune the search space to only the testbed
	//facilities in play, otherwise the function below will compute an infranet
	//embedding across all facility gateways, even those not involved in the
	//realization.

	// select an infrapod server to host this materialization
	infraServs := []string{}

	for _, dev := range tbx.Devices {
		r := dev.Resource()
		for _, rs := range r.Roles {
			if rs == xir.Role_InfrapodServer {
				infraServs = append(infraServs, dev.Id())
				e.InfrapodSite = r.Facility // assumes a single facility site
			}
		}
	}

	if len(infraServs) == 0 {
		return e, ds, fmt.Errorf("no infraservers found")
	}

	if len(infraServs) > 1 {
		// Balance the load of infrapods over all the infrapod servers
		var err error

		e.InfrapodServer, err = storage.GetNextInfraserver(e.InfrapodSite, infraServs)
		if err != nil {
			return e, ds, fmt.Errorf("get infraserver: %v", err)
		}
	} else {
		e.InfrapodServer = infraServs[0]
	}

	rvx := tbx.Device(e.InfrapodServer)

	d = EmbedInfranetLinks(InfranetMode, e, tbx, irt, rvx)
	ds = append(ds, d...)
	if d.Error() {
		return e, ds, nil
	}

	err := assignInfranetAddrs(e, "172.30.0.10/16")
	if err != nil {
		return nil, ds, fmt.Errorf("assign infranet addrs: %v", err)
	}

	d = EmbedLinks(e, tbx, xp, rt, a, emubackend)
	ds = append(ds, d...)
	if d.Error() {
		return e, ds, nil
	}

	return e, ds, nil
}

// Perform system static embeddings. This is for creating static virtual
// networks that always exist.
//   - Imaging Network
//   - Mass storage network
func EmbedSys(
	tbroot *xir.Device, //TODO generalize
	tbx *xir.Topology,
) (*Embedding, portal.Diagnostics, error) {

	var ds portal.Diagnostics

	if tbroot == nil {
		return nil, ds, fmt.Errorf("root cannot be nil")
	}

	if tbx == nil {
		return nil, ds, fmt.Errorf("testbed model cannot be nil")
	}

	irt := BuildInfraRoutingTable(tbx)

	e := NewEmbedding()
	e.lsi = HarborLsi

	// Clean the segments before we're done
	defer func() {
		CleanSegments(e)
	}()

	for _, r := range tbx.Devices {
		// TB nodes and hypervisors are part of the system embedding
		if !r.Resource().HasRole(xir.Role_TbNode) && !r.Resource().HasRole(xir.Role_Hypervisor) {
			continue
		}

		// NoAlloc is used to dynamically disable resources
		if r.Resource().HasAllocMode(xir.AllocMode_NoAlloc) {
			continue
		}

		ne := &NodeEmbedding{
			Node:     &xir.Node{Id: r.Id()},
			Resource: r,
			Kind:     portal.NodeRealization_BareMetal,
		}

		// hypervisor image on hypervisor capable nodes
		if r.Resource().HasRole(xir.Role_Hypervisor) {
			ne.Node.Image = &xir.StringConstraint{
				Op:    xir.Operator_EQ,
				Value: "hypervisor",
			}
		}

		e.Nodes[r.Id()] = ne
	}

	d := EmbedHarborLinks(e, tbx, irt, tbroot)
	ds = append(ds, d...)
	if d.Error() {
		return e, ds, nil
	}

	err := assignInfranetAddrs(e, "172.29.0.10/16")
	if err != nil {
		return nil, ds, fmt.Errorf("assign infranet addrs: %v", err)
	}

	return e, ds, nil

}

func EmbedNodes(
	e *Embedding, a *portal.AllocationTable,
	guests []*Guest, metal, virt []*Host,
	mzid string,
) portal.Diagnostics {

	var ds portal.Diagnostics

	for _, g := range guests {

		if g.WantsMetal() {
			ds = append(ds,
				Provision(g, metal, e, a, portal.NodeRealization_BareMetal, mzid)...)
		} else {
			// first try virtual and fall back to bare metal
			if len(virt) > 0 {
				vd := Provision(
					g, virt, e, a, portal.NodeRealization_VirtualMachine, mzid)

				ds = append(ds, vd...)

				if !vd.Error() {
					continue
				}
			}
			ds = append(ds,
				Provision(g, metal, e, a, portal.NodeRealization_BareMetal, mzid)...)
		}

	}

	return ds

}

func Provision(
	g *Guest,
	hosts []*Host,
	e *Embedding,
	a *portal.AllocationTable,
	kind portal.NodeRealization_Kind,
	mzid string,
) portal.Diagnostics {

	var ds portal.Diagnostics

	host_constraint := g.Device.Data.(*xir.Node).GetHost().GetValue()

	for _, h := range hosts {

		if len(h.Guests) > 0 {
			if g.WantsMetal() || h.Guests[0].WantsMetal() {
				continue
			}
		}

		if host_constraint != "" && h.Device.Id() != host_constraint {
			continue
		}

		alc, _ := a.Resource[h.Device.Id()]
		if alc == nil {
			alc = new(portal.ResourceAllocationList)
		}
		newAlloc, diags := ResolveNode(
			h.Device.Data.(*xir.Resource),
			g.Device.Data.(*xir.Node),
			alc.Value,
			mzid,
		)
		if !diags.Error() {

			newAlloc.Virtual = (kind == portal.NodeRealization_VirtualMachine)

			// newAlloc is the xir.ResourceAllocation performed on behalf of
			// the guest

			// In the event of a virtualmachine realization in which either (1)
			// port capacity or (2) link bandwidth constraints were given as
			// part of the experiment model, NIC allocations will be generated
			// as part of newAlloc.NICs. While these resources need to be
			// accounted for in the allocation table below, they must not be
			// placed in the xir.ResourceAllocation that defines the VM
			// hardware spec (NodeEmbedding.VmAlloc)

			var vmAlloc *xir.ResourceAllocation = nil
			if kind == portal.NodeRealization_VirtualMachine {
				// copy in all but the NICs
				vmAlloc = &xir.ResourceAllocation{
					Resource: newAlloc.Resource,
					Facility: newAlloc.Facility,
					Mzid:     newAlloc.Mzid,
					Node:     newAlloc.Node,
					Procs:    newAlloc.Procs,
					Memory:   newAlloc.Memory,
					Disks:    newAlloc.Disks,
					Model:    newAlloc.Model,
					Virtual:  true,
				}
			}

			a.AllocateResource(h.Device.Id(), newAlloc)
			e.Nodes[g.Device.Id()] = &NodeEmbedding{
				Node:     g.Device.Data.(*xir.Node),
				Resource: h.Device,
				Kind:     kind,
				XpnetIdx: 0,
				VmAlloc:  vmAlloc,
			}
			h.Guests = append(h.Guests, g)

			return nil
		}
		ds = append(ds, diags...)

	}

	return append(ds, &portal.Diagnostic{
		Message: "no suitable resource found",
		Level:   portal.DiagnosticLevel_Error,
		Guest:   g.Device.Id(),
		Data: portal.Fields{
			"pool": fmt.Sprintf("%d", len(hosts)),
		},
	})

}

func EmbedLinks(
	e *Embedding,
	tbx, xp *xir.Topology,
	rt RoutingTable,
	a *portal.AllocationTable,
	emubackend xir.Emulation,
) portal.Diagnostics {

	log.Debugf("embedding %d links\n", len(xp.Connections))

	var ds portal.Diagnostics

	for _, x := range xp.Connections {
		d := EmbedLink(e, x, tbx, rt, a, emubackend)
		ds = append(ds, d...)

		if d.Error() {
			ds = append(ds, DiagnosticErrorf("%s link planning failed", x.Label()))
			return ds
		}
	}

	ds = append(ds, CheckLinks(e)...)

	return ds

}

func EmbedInfranetLinks(
	net NetworkMode,
	e *Embedding,
	tbx *xir.Topology,
	rt RoutingTable,
	rvx *xir.Device, // rendezvous point, 'internet' for multisite, gw for single site
) portal.Diagnostics {

	// Set up a link realization object where we'll put all the embedding info.
	// Create a segment that has a path from every node to the rendezvous point,
	// implicitly connects all nodes without doing the whole N^2 thing, here
	// we're doing N*M where M is the number of gateways involved (a few per
	// facility at most).

	var linkName string
	switch net {
	case HarborMode:
		linkName = "harbor"
	case InfranetMode:
		linkName = "infranet"
	}

	seg := singleSegmentLinkRealization(e, linkName)

	eps, ds := computeInfranetEndpoints(net, e, rvx, rt, seg)
	if ds.Error() {
		return ds
	}

	ds = append(ds, computeInfranetPaths(net, e, rvx, rt, seg, eps, tbx)...)

	e.lsi++

	return ds

}

func EmbedHarborLinks(
	e *Embedding,
	tbx *xir.Topology,
	rt RoutingTable,
	rvx *xir.Device, // rendezvous point
) portal.Diagnostics {

	return EmbedInfranetLinks(HarborMode, e, tbx, rt, rvx)

}

func assignInfranetAddrs(e *Embedding, subnet string) error {

	ipc, err := build.NewIPCounter(subnet)
	if err != nil {
		return fmt.Errorf("new ip counter: %v", err)
	}

	keys := make([]string, 0, len(e.Nodes))
	for k := range e.Nodes {
		keys = append(keys, k)
	}
	sort.Sort(natural.StringSlice(keys))

	for _, k := range keys {
		x := e.Nodes[k]
		x.InfranetAddr = ipc.Get().String()
		ipc.Inc()

	}

	return nil

}

func singleSegmentLinkRealization(e *Embedding, linkName string) *portal.LinkSegment {

	seg := new(portal.LinkSegment)
	lnk := &xir.Link{Id: linkName}
	cnx := &xir.Connection{Data: lnk}
	lrz := &portal.LinkRealization{
		Link: lnk,
		Segments: map[uint64]*portal.LinkSegment{
			e.lsi: seg,
		},
	}

	e.Links[cnx] = lrz

	return seg

}

func computeInfranetEndpoints(
	net NetworkMode,
	e *Embedding,
	rvx *xir.Device, //TODO generalize to a set of rendezvous points
	rt RoutingTable,
	seg *portal.LinkSegment,
) (map[*xir.Interface][]*portal.Endpoint, portal.Diagnostics) {

	var ds portal.Diagnostics

	// Associate physical interfaces to list of endpoint implementations (Phy,
	// VLAN, VTEP...)
	eps := make(map[*xir.Interface][]*portal.Endpoint)

	keys := make([]string, 0, len(e.Nodes))
	for k := range e.Nodes {
		keys = append(keys, k)
	}
	sort.Sort(natural.StringSlice(keys))

	for _, k := range keys {
		ne := e.Nodes[k]

		ifx := ne.Resource.Infranet()
		route := rt.GetRandomShortestRouteTo(rvx.Id(), TPA(ifx.Port().TPA))
		if route == nil {
			ds = append(ds, DiagnosticErrorf(
				"No route from %s to %s (%s/%s)",
				rvx.Id(),
				ne.Resource.Id(),
				ifx.PortName(),
				TPA(ifx.Port().TPA),
			))
			return nil, ds
		}

		used := net == InfranetMode
		mode := computeEndpointMode(net, ne.Kind, used)

		ep, d := e.addSegmentEndpoint(
			net,
			seg,
			ne.Node.Id,
			ifx,
			mode,
		)

		ds = append(ds, d...)
		if d.Error() {
			return nil, ds
		}

		eps[ifx] = append(eps[ifx], ep)

	}

	return eps, ds

}

func computeInfranetPaths(
	net NetworkMode,
	e *Embedding,
	rvx *xir.Device, //TODO generalize to a set of rendezvous  points
	rt RoutingTable,
	seg *portal.LinkSegment,
	eps map[*xir.Interface][]*portal.Endpoint,
	tbx *xir.Topology,
) portal.Diagnostics {

	var ds portal.Diagnostics

	for ifxA, epsA := range eps {

		// if we're here we already know at least one route exists
		route := rt.GetRandomShortestRouteTo(rvx.Id(), TPA(ifxA.Port().TPA))
		ifxB := route.Dev

		epB, d := e.addSegmentEndpoint(
			net,
			seg,
			rvx.Id(),
			ifxB,
			VtepMode,
		)

		ds = append(ds, d...)
		if d.Error() {
			return ds
		}

		d = e.addSegmentWaypoints(net, seg, ifxA, ifxB, epsA[0], epB, rt, tbx)
		ds = append(ds, d...)
		if d.Error() {
			return ds
		}

	}

	return ds

}

// TODO somewhat of a bottleneck for large LANs
func EmbedLink(
	e *Embedding,
	x *xir.Connection,
	tbx *xir.Topology,
	rt RoutingTable,
	a *portal.AllocationTable,
	emubackend xir.Emulation,
) portal.Diagnostics {

	emulated := false
	if isEmulatedLink(x.Data.(*xir.Link)) {
		emulated = true
	}

	return embedLink(e, x, tbx, rt, a, emulated, emubackend)

}

func isEmulatedLink(lnk *xir.Link) bool {

	return lnk.Latency != nil || lnk.Capacity != nil || lnk.Loss != nil

}

type InterfaceSelection struct {
	XpNode      *xir.Node
	TbInterface *xir.Interface
	XpKind      portal.NodeRealization_Kind
	AlreadyUsed bool
}

func getLinkTags(lnk *xir.Link) []string {

	if lnk.Properties == nil {
		return []string{}
	}

	// tags are stored as properties
	if v, ok := lnk.Properties.Keyvalues["tags"]; ok {
		return v.Values
	}

	return []string{}
}

func embedLink(
	e *Embedding,
	x *xir.Connection,
	tbx *xir.Topology,
	rt RoutingTable,
	at *portal.AllocationTable,
	emulated bool,
	emubackend xir.Emulation,
) portal.Diagnostics {

	var ds portal.Diagnostics

	// set up a link realization object where we'll put all the embedding info
	seg := new(portal.LinkSegment)
	lrz := &portal.LinkRealization{
		Link: x.Data.(*xir.Link),
		Segments: map[uint64]*portal.LinkSegment{
			e.lsi: seg,
		},
	}
	e.Links[x] = lrz

	// Keep track of what interfaces we select along the way
	var selectedInterfaces []InterfaceSelection

	// Build a list of tb interfaces that will host the link endpoints for the embedding
	for _, a := range x.Edges {

		xpNode := a.Interface.Device.Node()
		ne := e.Nodes[a.Interface.Device.Id()]
		tbResource := ne.Resource

		ifx, used, d := e.selectXpInterface(tbResource, rt)
		ds = append(ds, d...)

		if ifx == nil {
			diag := DiagnosticErrorf("%s: no available interfaces", tbResource.Id())
			diag.Host = tbResource.Id()
			return append(ds, diag)
		}

		if d.Error() {
			return ds
		}

		selectedInterfaces = append(selectedInterfaces, InterfaceSelection{
			XpNode:      xpNode,
			XpKind:      ne.Kind,
			TbInterface: ifx,
			AlreadyUsed: used,
		})
	}

	if emulated {
		if e.LinkEmulation == nil {
			e.LinkEmulation = &LinkEmulationParams{Backend: emubackend}

			var err error
			e.LinkEmulation.Servers, err = storage.GetEmuServers(getEmulationServers(tbx))
			if err != nil {
				return append(ds, DiagnosticErrorf("fetch emu servers: %v", err))
			}
			if len(e.LinkEmulation.Servers) == 0 {
				return append(ds, DiagnosticErrorf("no allocatable emu servers found"))
			}
		}

		params := &portal.LinkEmulation{
			Loss:       lrz.Link.Loss.GetValue(),
			Capacity:   lrz.Link.Capacity.GetValue(),
			Latency:    lrz.Link.Latency.GetValue(),
			Tags:       getLinkTags(lrz.Link),
			Interfaces: []string{},
			Nodes:      []string{},
			Server:     e.LinkEmulation.Servers.Alloc(len(selectedInterfaces)),
		}

		if params.Server == "" {
			return append(ds, DiagnosticErrorf("no emu server allocated"))
		}

		emuDevice := tbx.Device(params.Server)
		if emuDevice == nil {
			return append(ds, DiagnosticErrorf("unable to find emulation server: %s", params.Server))
		}

		emuLink := &LinkEmulation{
			Link: params,
		}
		e.LinkEmulation.Links = append(e.LinkEmulation.Links, emuLink)

		// XXX for now just use the same interface for all segments
		// in the future, we could do something smarter to distribute the load
		// over all xp links to the emu server
		emu_ifx, _, d := e.selectXpInterface(emuDevice, rt)
		ds = append(ds, d...)
		if d.Error() {
			return ds
		}

		for _, ifx := range selectedInterfaces {

			seg := new(portal.LinkSegment)
			lrz.Segments[e.lsi] = seg

			mode := computeEndpointMode(XpnetMode, ifx.XpKind, ifx.AlreadyUsed)

			// add xp node as first endpoint
			ep, d := e.addSegmentEndpoint(
				XpnetMode,
				seg,
				ifx.XpNode.Id,
				ifx.TbInterface,
				mode,
			)

			ds = append(ds, d...)
			if d.Error() {
				return ds
			}

			// save the user's name for this node to aid in moa output
			params.Nodes = append(params.Nodes, ifx.XpNode.Id)

			// add emu server as other endpoint
			var emu_ep *portal.Endpoint
			emu_ep, d = e.addSegmentEndpoint(
				XpnetMode,
				seg,
				emuDevice.Id(),
				emu_ifx,
				VtepMode,
			)
			ds = append(ds, d...)
			if d.Error() {
				return ds
			}

			// Store the VTEP name on the emulation server so Moa can build the click config
			// XXX maybe we shouldn't assume that this is a vtep? eventually it could be a TAP device
			// when doing emulation between two VMs
			vtep := emu_ep.GetVtep()
			if vtep == nil {
				diag := DiagnosticErrorf("unable to get vtep for emulation server")
				diag.Host = emuDevice.Id()
				return append(ds, diag)
			}
			// we can't use the vtep.Name here yet, because we only have the proxy value, which
			// is later mapped into the actual VID.  Just store the Vtep object to retrieve the
			// name later.
			emuLink.Interfaces = append(emuLink.Interfaces, vtep)

			// build path from xp node to emu server
			d = e.addSegmentWaypoints(
				XpnetMode,
				seg,
				ifx.TbInterface,
				emu_ifx,
				ep,
				emu_ep,
				rt,
				tbx,
			)

			ds = append(ds, d...)
			if d.Error() {
				return ds
			}

			e.lsi++
		}

	} else {
		seg := new(portal.LinkSegment)
		lrz.Segments[e.lsi] = seg

		// Associate physical interfaces to list of endpoint implementations (Phy,
		// VLAN, VTEP...)
		eps := make(map[*xir.Interface][]*portal.Endpoint)

		// add endpoints (node interfaces) to the segment
		for _, ifx := range selectedInterfaces {

			mode := computeEndpointMode(XpnetMode, ifx.XpKind, ifx.AlreadyUsed)

			ep, d := e.addSegmentEndpoint(
				XpnetMode,
				seg,
				ifx.XpNode.Id,
				ifx.TbInterface,
				mode,
			)

			ds = append(ds, d...)
			if d.Error() {
				return ds
			}

			eps[ifx.TbInterface] = append(eps[ifx.TbInterface], ep)
		}

		// add waypoints (switch/router hops) to segment
		for ifxA, epsA := range eps {

			for ifxB, epsB := range eps {

				// this ensures that we only do A -> B and skip B -> A
				// this works by only doing the pair (A, B) iff they're in sorted order by some metric
				// we use TPAs because they're ints (so they're quickly orderable) and they're unique
				if TPA(ifxA.Port().TPA) >= TPA(ifxB.Port().TPA) {
					continue
				}

				d := e.addSegmentWaypoints(
					XpnetMode,
					seg,
					ifxA,
					ifxB,
					epsA[0],
					epsB[0],
					rt,
					tbx,
				)
				ds = append(ds, d...)
				if d.Error() {
					return ds
				}
			}
		}

		e.lsi++
	}

	return ds

}

// Select an interface from device 'd' to use for a link embedding in 'e'
//
// If 'd' has multiple interfaces serving as XpLinks, there is a question about
// which is best to use. The algorithm we use currently is to select the least
// used interface, where least means has the fewest other links that have been
// embedded on it.
//
// This function returns the selected interface, along with a boolean indicator
// of whether the interface has already been selected for a separate link in
// this embedding
func (e *Embedding) selectXpInterface(d *xir.Device, rt RoutingTable) (*xir.Interface, bool, portal.Diagnostics) {

	var ds portal.Diagnostics

	// Put the devices in sorted order, for consistency
	sort.SliceStable(d.Interfaces, func(i, j int) bool {
		return TPA(d.Interfaces[i].Port().TPA) < TPA(d.Interfaces[j].Port().TPA)
	})

	// Get the least-used interfaces
	var min_ifxes []*xir.Interface
	min_used := -1

	for _, ifx := range d.Interfaces {

		if ifx.Port().Role != xir.LinkRole_XpLink {
			continue
		}

		if ifx.Edge == nil {
			continue
		}

		if rt.GetRouteUsing(ifx) == nil {
			d := DiagnosticWarningf(
				"skipping %s.%s: it has no routes but has an edge, suggesting a model error",
				ifx.Device.Id(), ifx.PortName(),
			)
			d.Host = ifx.Device.Id()

			ds = append(ds, d)
			continue
		}

		times_used := len(e.Endpoints[ifx])

		if times_used < min_used || min_used == -1 {
			min_used = times_used
			min_ifxes = []*xir.Interface{ifx}
		} else if times_used == min_used {
			min_ifxes = append(min_ifxes, ifx)
		}
	}

	// TODO: Be able to also choose this randomly
	return min_ifxes[0], min_used >= 1, ds
}

type EndpointMode uint8

const (
	TrunkMode EndpointMode = iota
	AccessMode
	VtepMode
)

type NetworkMode uint8

const (
	HarborMode NetworkMode = iota
	InfranetMode
	XpnetMode
)

func vrfByMode(n NetworkMode) string {
	switch n {
	case HarborMode:
		return "ifabric"
	case InfranetMode:
		return "ifabric"
	case XpnetMode:
		return "xfabric"
	default:
		return ""
	}
}

func computeEndpointMode(net NetworkMode, kind portal.NodeRealization_Kind, used bool) EndpointMode {

	switch net {
	case HarborMode:
		return AccessMode

	case InfranetMode:
		// the interface is already used on infranet modes
		used = true
	}

	switch kind {
	case portal.NodeRealization_BareMetal:
		if used {
			return TrunkMode
		} else {
			return AccessMode
		}

	case portal.NodeRealization_VirtualMachine:
		return TrunkMode

	default:
		log.Fatalf("unknown node realization type: %+v", kind)
		return TrunkMode
	}

}

func (e *Embedding) addSegmentEndpoint(
	net NetworkMode,
	seg *portal.LinkSegment,
	node string,
	ifx *xir.Interface,
	ep_mode EndpointMode,
) (*portal.Endpoint, portal.Diagnostics) {

	// determine if we have a node embedding for this node; if not, we assume
	// this is a system resource of some form and thus requires a bare metal endpoint
	var kind portal.NodeRealization_Kind

	ne, ok := e.Nodes[node]
	if !ok {
		kind = portal.NodeRealization_BareMetal
	} else {
		kind = ne.Kind
	}

	switch kind {
	case portal.NodeRealization_BareMetal:
		return e.addBareMetalSegmentEndpoint(net, seg, node, ifx, ep_mode)

	case portal.NodeRealization_VirtualMachine:
		return e.addVirtualEndpoint(net, seg, node, ifx.Device.Id())

	default:
		return nil, portal.Diagnostics{
			&portal.Diagnostic{
				Message: "Internal realization error: unknown node embedding type",
				Host:    ifx.Device.Id(),
				Level:   portal.DiagnosticLevel_Error, //TODO should be error
				Data: portal.Fields{
					"kind": fmt.Sprintf("%+v", kind),
				},
			},
		}
	}

}

func (e *Embedding) addBareMetalSegmentEndpoint(
	net NetworkMode,
	seg *portal.LinkSegment,
	node string,
	ifx *xir.Interface,
	ep_mode EndpointMode,
) (*portal.Endpoint, portal.Diagnostics) {

	var ds portal.Diagnostics
	vrf := vrfByMode(net)

	lsi := e.lsi

	// special case for bonded interfaces: disregard MAC. Otherwise, ports with different MACs
	// that are part of the same bond will be incorrectly interpreted as different interfaces
	mac := ""
	bond := ifx.Port().GetBond()
	if bond == nil {
		mac = ifx.Port().GetMac()
		if mac == "" {
			ds = append(ds, DiagnosticsMacWarning(ifx))
		}
	}

	// check if already exists
	for _, ep := range seg.Endpoints {
		if ep.Host == ifx.Device.Id() &&
			ep.Mac == mac {

			switch ep_mode {
			case AccessMode:
				phy := ep.GetPhy()
				if phy != nil &&
					phy.Name == ifx.PortName() {

					return ep, ds
				}

			case TrunkMode:
				vlan := ep.GetVlan()
				if vlan != nil &&
					vlan.Parent.Name == ifx.PortName() &&
					vlan.Vid == uint32(lsi) {

					return ep, ds
				}

			case VtepMode:
				vtep := ep.GetVtep()
				if vtep != nil &&
					vtep.Parent.Name == ifx.PortName() &&
					vtep.Vni == uint32(lsi) {

					return ep, ds
				}
			}
		}
	}

	switch ep_mode {
	case AccessMode:
		ep := &portal.Endpoint{
			Host: ifx.Device.Id(),
			Node: node,
			Mac:  mac,
			Interface: &portal.Endpoint_Phy{&portal.PhysicalInterface{
				Name: ifx.PortName(),
				Mac:  mac,
			}},
		}

		seg.Endpoints = append(seg.Endpoints, ep)
		e.Endpoints[ifx] = append(e.Endpoints[ifx], ep)

		return ep, ds

	case TrunkMode:
		ep := &portal.Endpoint{
			Host: ifx.Device.Id(),
			Node: node,
			Mac:  mac,
			Interface: &portal.Endpoint_Vlan{&portal.VLANInterface{
				Name: fmt.Sprintf("%s.%d", ifx.PortName(), lsi),
				Vid:  uint32(lsi),
				Parent: &portal.PhysicalInterface{
					Name: ifx.PortName(),
					Mac:  mac,
				},
			}},
		}

		seg.Endpoints = append(seg.Endpoints, ep)
		e.Endpoints[ifx] = append(e.Endpoints[ifx], ep)

		return ep, ds

	case VtepMode:
		bgp := bgpConfigByVrf(ifx.Device.Resource(), vrf)
		if bgp == nil {
			d := DiagnosticErrorf("addSegmentEndpoint: vtep: %s resource has no BGP config", ifx.Device.Id())
			d.Host = ifx.Device.Id()
			ds = append(ds, d)
		}
		tunnelip := bgpTunnelIP(bgp)
		if tunnelip == "" {
			d := DiagnosticErrorf("addSegmentEndpoint: vtep: %s resource has no BGP addresses", ifx.Device.Id())
			d.Host = ifx.Device.Id()
			ds = append(ds, d)
		}

		ep := &portal.Endpoint{
			Host: ifx.Device.Id(),
			Node: node,
			Mac:  mac,
			Interface: &portal.Endpoint_Vtep{&portal.Vtep{
				Name: fmt.Sprintf("vtep%d", lsi),
				Vni:  uint32(lsi),
				Parent: &portal.PhysicalInterface{
					Name: ifx.PortName(),
					Mac:  mac,
				},
				TunnelIp: tunnelip,
			}},
			Mtu: DefaultMtu,
		}

		seg.Endpoints = append(seg.Endpoints, ep)
		e.Endpoints[ifx] = append(e.Endpoints[ifx], ep)

		return ep, ds
	}

	return nil, ds

}

func (e *Embedding) addVMVlanSegmentEndpoint(
	net NetworkMode,
	seg *portal.LinkSegment,
	node string,
	ifx *xir.Interface,
) (*portal.Endpoint, portal.Diagnostics) {

	var ds portal.Diagnostics
	var hypEp *portal.Endpoint = nil

	lsi32 := uint32(e.lsi)

	// check if vlan or vtep already exists for this VNI (multiple VMs on same segment)
	if eps, ok := e.Endpoints[ifx]; ok {
		for _, ep := range eps {
			switch net {
			case XpnetMode:
				vtep := ep.GetVtep()
				if vtep != nil && vtep.Vni == lsi32 {
					hypEp = ep
					break
				}
			default:
				vlan := ep.GetVlan()
				if vlan != nil && vlan.Vid == lsi32 {
					hypEp = ep
					break
				}
			}
		}
	}

	if hypEp != nil {
		return hypEp, ds
	}

	// special case for bonded interfaces: disregard MAC
	mac := ""
	bond := ifx.Port().GetBond()
	if bond == nil {
		mac = ifx.Port().GetMac()
		if mac == "" {
			ds = append(ds, DiagnosticsMacWarning(ifx))
		}
	}

	hypEp = &portal.Endpoint{
		Host: ifx.Device.Id(),
		Node: node,
		Mac:  mac,
	}

	if net == XpnetMode {
		vrf := vrfByMode(net)

		bgp := bgpConfigByVrf(ifx.Device.Resource(), vrf)
		if bgp == nil {
			d := DiagnosticErrorf("addVMVlanSegmentEndpoint: vtep: %s resource has no BGP config", ifx.Device.Id())
			d.Host = ifx.Device.Id()
			ds = append(ds, d)
		}
		tunnelip := bgpTunnelIP(bgp)
		if tunnelip == "" {
			d := DiagnosticErrorf("addVMVlanSegmentEndpoint: vtep: %s resource has no BGP addresses", ifx.Device.Id())
			d.Host = ifx.Device.Id()
			ds = append(ds, d)
		}

		hypEp.Bridge = &portal.BridgeMember{
			Bridge: fmt.Sprintf("mbr%d", lsi32), // placeholder, to be replaced when VNI is allocated
		}
		hypEp.Interface = &portal.Endpoint_Vtep{&portal.Vtep{
			Name: fmt.Sprintf("vtep%d", lsi32),
			Vni:  lsi32,
			Parent: &portal.PhysicalInterface{
				Name: ifx.PortName(),
				Mac:  mac,
			},
			TunnelIp: tunnelip,
		}}

	} else {
		hypEp.Bridge = &portal.BridgeMember{
			Bridge:   MarinerInfraBridge,
			Untagged: []uint32{lsi32},
			Pvid:     lsi32,
		}
		hypEp.Interface = &portal.Endpoint_Vlan{&portal.VLANInterface{
			Name: fmt.Sprintf("%s.%d", ifx.PortName(), lsi32),
			Vid:  lsi32,
			Parent: &portal.PhysicalInterface{
				Name: ifx.PortName(),
				Mac:  mac,
			},
		}}
	}

	seg.Endpoints = append(seg.Endpoints, hypEp)
	e.Endpoints[ifx] = append(e.Endpoints[ifx], hypEp)

	return hypEp, ds

}

func (e *Embedding) generateMac(
	node string,
	host string,
) (string, portal.Diagnostics) {

	var ds portal.Diagnostics
	var mac string
	var err error

	for {
		mac, err = generateUnicastMacAddr()
		if err != nil {
			ds = append(ds, &portal.Diagnostic{
				Message: fmt.Sprintf("Failed to generate unicast mac address: %v", err),
				Host:    host,
				Guest:   node,
				Level:   portal.DiagnosticLevel_Error,
			})
			return "", ds
		}

		if _, ok := e.VirtualMacs[mac]; !ok {
			break
		}

		ds = append(ds, &portal.Diagnostic{
			Message: "Duplicate MAC address generated (regenerating)",
			Host:    host,
			Guest:   node,
			Level:   portal.DiagnosticLevel_Error,
			Data: portal.Fields{
				"MAC address": mac,
			},
		})
	}

	e.VirtualMacs[mac] = true
	return mac, ds

}

func (e *Embedding) addVirtualEndpoint(
	net NetworkMode,
	seg *portal.LinkSegment,
	node string,
	host string,
) (*portal.Endpoint, portal.Diagnostics) {

	var ds portal.Diagnostics
	var mac, name string
	var lsi32, idx uint32

	lsi32 = uint32(e.lsi)

	// grab the node embedding structure
	ne := e.Nodes[node]

	switch net {
	case InfranetMode:
		idx = 0
		name = "eth0"
	case XpnetMode:
		idx = ne.XpnetIdx + 1
		name = fmt.Sprintf("eth%d", idx)
		ne.XpnetIdx++
	}

	// generate MAC for VM endpoint, ensuring uniqueness in this embedding
	mac, d := e.generateMac(node, host)
	ds = append(ds, d...)
	if d.Error() {
		return nil, ds
	}

	// add VM virtual endpoint
	ep := &portal.Endpoint{
		Host: host,
		Node: node,
		Mac:  mac,
		Interface: &portal.Endpoint_Phy{&portal.PhysicalInterface{
			Name: name,
			Mac:  mac,
		}},
		Virtual: true,
	}

	seg.Endpoints = append(seg.Endpoints, ep)

	var brname string
	var vni uint32
	var vid uint32
	var untagged []uint32

	switch net {
	case XpnetMode:
		brname = fmt.Sprintf("mbr%d", lsi32)
		vni = lsi32
	default:
		brname = MarinerInfraBridge
		vid = lsi32
		untagged = append(untagged, lsi32)
	}

	// add waypoint through a TAP device
	wp := &portal.Waypoint{
		Host: host,
		Bridge: &portal.BridgeMember{
			Bridge:   brname,
			Untagged: untagged,
			Pvid:     vid,
		},
		Interface: &portal.Waypoint_Tap{&portal.VLANTapPort{
			Node: node,
			Vid:  vid,
			Vni:  vni,
			Frontend: &portal.PhysicalInterface{
				Name: name,
				Mac:  mac,
			},
		}},
	}

	seg.Waypoints = append(seg.Waypoints, wp)

	// track the NIC in the NodeEmbedding's ResourceAllocation structure
	if ne.VmAlloc.NICs == nil {
		ne.VmAlloc.NICs = &xir.NICsAllocation{
			Alloc: make(map[uint32]*xir.NICAllocation),
		}
	}

	ne.VmAlloc.NICs.Allocate(
		idx,
		0,
		&xir.PortAllocation{
			Name: name,
			Mac:  mac,
			Bridge: &xir.BridgeMember{
				Bridge: brname,
				Vid:    vid,
			},
		},
	)

	return ep, ds
}

func (e *Embedding) addSegmentWaypoints(
	net NetworkMode,
	seg *portal.LinkSegment,
	fromIfx, toIfx *xir.Interface,
	fromEp, toEp *portal.Endpoint,
	rt RoutingTable,
	tbx *xir.Topology,
) portal.Diagnostics {

	lsi := e.lsi
	vrf := vrfByMode(net)

	var ds portal.Diagnostics
	var d portal.Diagnostics

	if fromEp.Virtual && toEp.Virtual && fromEp.Node == toEp.Node {
		return ds
	}

	if fromEp.Virtual {
		fromEp, d = e.addVMVlanSegmentEndpoint(net, seg, fromEp.Node, fromIfx)
		ds = append(ds, d...)
	}

	if toEp.Virtual {
		toEp, d = e.addVMVlanSegmentEndpoint(net, seg, toEp.Node, toIfx)
		ds = append(ds, d...)
	}

	/*DEBUG
	wp := rt.Paths(fromIfx, TPA(toIfx.Port().TPA))
	for _, ifx := range CollectHops(wp)
		log.Printf("WAYPOINT... %s:%s",
			ifx.Device.Id(),
			ifx.PortName(),
		)
	}
	*/

	pf, d := NewPathFinder(
		&End{Interface: fromIfx, Endpoint: fromEp},
		&End{Interface: toIfx, Endpoint: toEp},
		rt,
		lsi,
		tbx,
		vrf,
	)
	ds = append(ds, d...)
	if d.Error() {
		return ds
	}

	log.Debugf("\n===================================\nAdd Waypoints: %s:%s ~~~> %s:%s\n\n", fromIfx.Device.Id(), fromIfx.PortName(), toIfx.Device.Id(), toIfx.PortName())

	wp, d := rt.ShortestPath(fromIfx, TPA(toIfx.Port().TPA))
	ds = append(ds, d...)
	if d.Error() {
		return ds
	}

	//fmt.Printf("Waypoints:\n")
	//wp.JSONDump()
	//fmt.Println("\n")

	for _, ifxs := range CollectHops(wp) {
		for _, ifx := range ifxs {
			log.Debugf("%s:%s\n",
				ifx.Device.Id(),
				ifx.PortName(),
			)
		}
	}

	//fmt.Println()

	d = pf.Run()
	ds = append(ds, d...)
	if d.Error() {
		log.Debugf("  !Pathfinder error!\n  ------------------\n")
		ds = append(ds, DiagnosticErrorf(
			"%s.%s~%s.%s: pathfinder error",
			fromIfx.Device.Id(), fromIfx.PortName(), toIfx.Device.Id(), toIfx.PortName(),
		))
	}

	//fmt.Println()

	for _, w := range pf.Path.Waypoints {
		log.Debugf("  WAYPOINT+++ %+v\n", w)
	}

	/*
		for _, hs := range pf.Path.Hops {
			var hops []string
			for _, h := range hs {
				hops = append(hops, fmt.Sprintf("%s:%s",
					h.Device.Id(),
					h.PortName(),
				))
			}
			log.Debugf("  HOPS+++ %+v\n", hops)
		}*/

	seg.Waypoints = append(seg.Waypoints, pf.Path.Waypoints...)

	return ds
}

func CleanSegments(e *Embedding) {

	for _, link := range e.Links {
		for _, seg := range link.Segments {
			// Waypoints
			h := make([]portal.Hashable, len(seg.Waypoints))
			for i := range h {
				h[i] = seg.Waypoints[i]
			}

			h, _ = portal.RemoveDuplicates(h, nil)

			seg.Waypoints = make([]*portal.Waypoint, len(h))
			for i := range h {
				val, _ := h[i].(*portal.Waypoint)
				seg.Waypoints[i] = val
			}
		}
	}

}

func CheckLinks(e *Embedding) portal.Diagnostics {

	var ds portal.Diagnostics

	/* TODO
	// requested bandwidth
	rbw := make(map[*xir.Cable]uint64)

	for _, le := range e.Links {

		if le.Link.Capacity != nil {

			cap := le.Link.Capacity.Value

			if !isEmulatedLink(le.Link) {
				cap /= uint64(len(le.Link.Endpoints) - 1)
			}

			for _, segment := range le.Segments {
				for _, cable := range segment {
					rbw[cable] += uint64(cap)
				}
			}

		}

	}

	fmt.Println("\nbandwidth table")
	fmt.Println("===============")
	for l, b := range rbw {

		fmt.Printf("%s: %s\n", l.Id, FormatUnits(humanize.Bytes(b)))

	}
	fmt.Println("")

	for cable, bw := range rbw {

		for _, end := range cable.Ends {
			var capacity uint64
			for _, connector := range end.Connectors {
				capacity += connector.Capacity
			}
			if bw > capacity {
				ds = append(ds, &portal.Diagnostic{
					Message: "Bandwidth oversubscription",
					Host:    cable.Id,
					//Level:   Error,
					Level: portal.DiagnosticLevel_Warning, //TODO should be error
					Data: portal.Fields{
						"requested": FormatUnits(humanize.Bytes(bw)),
						"available": FormatUnits(humanize.Bytes(capacity)),
					},
				})
			}
		}

	}
	*/

	return ds

}
