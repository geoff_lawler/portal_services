package sfe

import (
	"fmt"
	"sort"
	"strings"

	"github.com/maruel/natural"
	log "github.com/sirupsen/logrus"

	rz "gitlab.com/mergetb/api/portal/v1/go"
	xir "gitlab.com/mergetb/xir/v0.3/go"
)

func WithGuest(ds rz.Diagnostics, g *Guest) rz.Diagnostics {

	for i := range ds {
		ds[i].Guest = g.Device.Id()
	}

	return ds
}

func WithHost(ds rz.Diagnostics, h *Host) rz.Diagnostics {

	for i := range ds {
		ds[i].Host = h.Device.Id()
	}

	return ds
}

func FillInHostGuestInfo(ds rz.Diagnostics, nodes map[string]*NodeEmbedding) rz.Diagnostics {

	// make a reverse map
	host_map := make(map[string][]string)

	for guest, em := range nodes {
		host := em.Resource.Id()

		host_map[host] = append(host_map[host], guest)
	}

	for _, guests := range host_map {
		sort.Sort(natural.StringSlice(guests))
	}

	for _, d := range ds {

		// fill in host
		if d.Host == "" && d.Guest != "" {
			em, found := nodes[d.Guest]

			if found {
				d.Host = em.Resource.Id()
			} else {
				log.Warnf("couldn't find %s in embedding", d.Guest)
			}
		}

		// fill in guest
		if d.Guest == "" && d.Host != "" {
			guests, found := host_map[d.Host]

			if found {
				d.Guest = strings.Join(guests, ",")
			} else {
				log.Warnf("couldn't find %s in embedding", d.Host)
			}
		}
	}

	return ds
}

func Oversubscribed(component string) *rz.Diagnostic {

	return &rz.Diagnostic{
		Message: "Component oversubscribed",
		Level:   rz.DiagnosticLevel_Error,
		Data: rz.Fields{
			"component": component,
		},
	}

}

func DiagnosticDebugf(format string, a ...interface{}) *rz.Diagnostic {

	return &rz.Diagnostic{
		Message: fmt.Sprintf(format, a...),
		Level:   rz.DiagnosticLevel_Debug,
	}
}

func DiagnosticWarningf(format string, a ...interface{}) *rz.Diagnostic {

	return &rz.Diagnostic{
		Message: fmt.Sprintf(format, a...),
		Level:   rz.DiagnosticLevel_Warning,
	}
}

func DiagnosticErrorf(format string, a ...interface{}) *rz.Diagnostic {

	return &rz.Diagnostic{
		Message: fmt.Sprintf(format, a...),
		Level:   rz.DiagnosticLevel_Error,
	}
}

func DiagnosticsMacWarning(ifx *xir.Interface) *rz.Diagnostic {

	if ifx.Port().GetMac() == "" {
		return &rz.Diagnostic{
			Message: fmt.Sprintf("%s.%s has no mac address", ifx.Device.Id(), ifx.PortName()),
			Level:   rz.DiagnosticLevel_Warning,
			Host:    ifx.Device.Id(),
		}
	}

	return nil

}
