package realize

import (
	"fmt"
	_ "strings"

	log "github.com/sirupsen/logrus"

	portal "gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/portal/services/pkg/realize/sfe"
	"gitlab.com/mergetb/portal/services/pkg/storage"
	xir "gitlab.com/mergetb/xir/v0.3/go"
)

const (
	VniTable = "global-vnis"
)

/*
 * Map the proxy IDs computed during embedding onto IDs that are actually
 * available on the underlying switches and hosts
 */
func mapLinkIds(nodes map[string]*sfe.NodeEmbedding, links map[*xir.Connection]*portal.LinkRealization) error {
	var lzs []*portal.LinkRealization
	for _, x := range links {
		// XXX what is this?
		if x.Link.Id == "bobsled" {
			continue
		}
		lzs = append(lzs, x)
	}

	// compute the VIDs and VNIs for each link segment
	vss, err := computeVlanSets(lzs)
	if err != nil {
		return err
	}

	return assignVlanIds(nodes, vss)
}

func freeLinkIds(links []*portal.LinkRealization) error {
	var lzs []*portal.LinkRealization
	for _, x := range links {
		if x.Link.Id == "bobsled" {
			continue
		}
		lzs = append(lzs, x)
	}

	vss, err := computeVlanSets(lzs)
	if err != nil {
		return err
	}

	return relinquishVlanIds(vss)
}

type VlanSegment struct {
	Vid uint32
	Vni uint32

	Hosts []string

	Vlans   []*portal.Endpoint
	EpVteps []*portal.Endpoint

	Access  []*portal.Waypoint
	Taps    []*portal.Waypoint
	Trunks  []*portal.Waypoint
	WpVteps []*portal.Waypoint
}

func NewVlanSegment() *VlanSegment {
	return &VlanSegment{
		Vid: 0,
		Vni: 0,
	}
}

func (vs *VlanSegment) addHost(host string) {
	for _, x := range vs.Hosts {
		if x == host {
			return
		}
	}
	vs.Hosts = append(vs.Hosts, host)
}

func (vs *VlanSegment) addVni(vni uint32) error {
	if vs.Vni == 0 {
		vs.Vni = vni
	}

	if vs.Vni != vni {
		return fmt.Errorf("invalid VlanSegment: multiple VNIs detected")
	}

	return nil
}

func (vs *VlanSegment) addVid(vid uint32) error {
	if vs.Vid == 0 {
		vs.Vid = vid
	}

	if vs.Vid != vid {
		return fmt.Errorf("invalid VlanSegment: multiple VIDs detected")
	}

	return nil
}

func (vs *VlanSegment) addVids(vids []uint32) error {
	if len(vids) > 1 {
		return fmt.Errorf("cannot add more than 1 VID to VlanSegment")
	}

	vid := vids[0]
	if vs.Vid == 0 {
		vs.Vid = vid
	}

	if vs.Vid != vid {
		return fmt.Errorf("invalid VlanSegment: multiple VIDs detected")
	}

	return nil
}

func (vs *VlanSegment) addEndpoint(ep *portal.Endpoint) error {
	var err error

	vl := ep.GetVlan()
	if vl != nil {
		vs.Vlans = append(vs.Vlans, ep)
		vs.addHost(ep.Host)

		err = vs.addVid(vl.Vid)
		if err != nil {
			return err
		}
	}

	vt := ep.GetVtep()
	if vt != nil {
		vs.EpVteps = append(vs.EpVteps, ep)

		// track VID if a VLAN access port is being bridged here
		if ep.Bridge != nil && len(ep.Bridge.Untagged) > 0 {
			vs.addHost(ep.Host)

			err = vs.addVids(ep.Bridge.Untagged)
			if err != nil {
				return err
			}
		}

		// track VNI
		err = vs.addVni(vt.Vni)
		if err != nil {
			return err
		}
	}

	return nil
}

func (vs *VlanSegment) addWaypoint(wp *portal.Waypoint) error {
	var err error

	a := wp.GetAccess()
	if a != nil {
		vs.Access = append(vs.Access, wp)
		vs.addHost(wp.Host)

		err = vs.addVids(wp.Bridge.Untagged)
		if err != nil {
			return err
		}

		err = vs.addVid(a.Vid)
		if err != nil {
			return err
		}
	}

	p := wp.GetTap()
	if p != nil {
		vs.Taps = append(vs.Taps, wp)
		vs.addHost(wp.Host)

		if p.Vid > 0 {
			err = vs.addVids(wp.Bridge.Untagged)
			if err != nil {
				return err
			}

			err = vs.addVid(p.Vid)
			if err != nil {
				return err
			}
		}

		if p.Vni > 0 {
			err = vs.addVni(p.Vni)
			if err != nil {
				return err
			}
		}
	}

	t := wp.GetTrunk()
	if t != nil {
		vs.Trunks = append(vs.Trunks, wp)
		vs.addHost(wp.Host)

		err = vs.addVids(wp.Bridge.Tagged)
		if err != nil {
			return err
		}

		err = vs.addVids(t.Vids)
		if err != nil {
			return err
		}
	}

	v := wp.GetVtep()
	if v != nil {
		vs.WpVteps = append(vs.WpVteps, wp)

		// track VID if a VLAN access port is being bridged here
		if wp.Bridge != nil && len(wp.Bridge.Untagged) > 0 {
			vs.addHost(wp.Host)

			err = vs.addVids(wp.Bridge.Untagged)
			if err != nil {
				return err
			}
		}

		// track VNI
		err = vs.addVni(v.Vni)
		if err != nil {
			return err
		}
	}

	return nil
}

func computeVlanSets(links []*portal.LinkRealization) ([]*VlanSegment, error) {
	var result []*VlanSegment
	var err error

	for _, lrz := range links {
		for _, seg := range lrz.Segments {
			vs := NewVlanSegment()

			for _, ep := range seg.Endpoints {
				err = vs.addEndpoint(ep)
				if err != nil {
					return nil, err
				}
			}

			for _, wp := range seg.Waypoints {
				err = vs.addWaypoint(wp)
				if err != nil {
					return nil, err
				}
			}

			if len(vs.Hosts) > 0 {
				result = append(result, vs)
			}
		}
	}

	return result, nil
}

// Translate the abstract LSI in the bridge structure to the actual VID allocated
func translateBridgeMember(vs *VlanSegment, bridge *portal.BridgeMember, vid uint32) {
	if bridge == nil {
		return
	}

	// replace the LSI in the bridge vid list with the vid passed to this function
	t := bridge.Tagged[:0]
	for _, v := range bridge.Tagged {
		if v == vs.Vid {
			t = append(t, vid)
		} else {
			t = append(t, v)
		}
	}
	bridge.Tagged = t

	u := bridge.Untagged[:0]
	for _, v := range bridge.Untagged {
		if v == vs.Vid {
			u = append(u, vid)
		} else {
			u = append(u, v)
		}
	}
	bridge.Untagged = u

	if bridge.Pvid == vs.Vid {
		bridge.Pvid = vid
	}
}

func allocateIds(nodes map[string]*sfe.NodeEmbedding, vat *storage.VlanAllocationTable, vs *VlanSegment) error {
	var vid uint32 = 0
	var vni uint32 = 0
	var err error

	// Allocate a VID for this set of hosts, if needed (fully virtual experiments will not need VLANs)
	if vs.Vid != 0 {
		vid, err = vat.AllocateVid()
		if err != nil {
			return err
		}
	}

	// If needed, grab a VNI
	if vs.Vni != 0 {
		vni, err = vat.AllocateVni()
		if err != nil {
			vat.FreeVid(vid)
			return err
		}
	}

	for _, x := range vs.Vlans {
		v := x.GetVlan()

		// <device>.<vid> can outstrip the maximum interface name length
		// because <vid> reuse is not possible across different NICs on the same node, removing the
		// <device> prefix will reduce the name length without losing uniqueness
		// https://gitlab.com/mergetb/portal/services/-/issues/267

		v.Name = fmt.Sprintf("vlan%d", vid)
		//device := strings.Split(v.GetName(), ".")[0] // assumes <device>.<lsi> naming scheme
		//v.Name = fmt.Sprintf("%s.%d", device, vid)

		v.Vid = vid
		translateBridgeMember(vs, x.Bridge, vid)
	}

	for _, x := range vs.Access {
		a := x.GetAccess()
		a.Vid = vid
		translateBridgeMember(vs, x.Bridge, vid)
	}

	for _, x := range vs.Taps {
		p := x.GetTap()
		if p.Vni > 0 {
			p.Vni = vni
			x.Bridge.Bridge = fmt.Sprintf("mbr%d", vni) // regenerate VXLAN connector bridge name
		} else {
			p.Vid = vid
			translateBridgeMember(vs, x.Bridge, vid)
		}

		// update VM config for this NIC. This is what Mariner looks at to configure networking
		ne, ok := nodes[p.Node]
		if ok {
			for _, a := range ne.VmAlloc.NICs.GetAlloc() {
				for _, port := range a.GetAlloc() {
					if port.Name == p.Frontend.Name {
						port.Bridge.Bridge = x.Bridge.Bridge
						port.Bridge.Vid = p.Vid
						break
					}
				}
			}
		} else {
			log.WithFields(log.Fields{"node": p.Node}).Error("node missing from node embedding map")
		}
	}

	for _, x := range vs.Trunks {
		t := x.GetTrunk()
		t.Vids = []uint32{vid}
		translateBridgeMember(vs, x.Bridge, vid)
	}

	for _, x := range vs.EpVteps {
		v := x.GetVtep()
		br := fmt.Sprintf("mbr%d", vs.Vni) // save placeholder name for use below
		v.Vni = vni
		v.Name = fmt.Sprintf("vtep%d", v.Vni)

		if x.Bridge != nil {
			// on hypervisors, update the bridge name
			if br == x.Bridge.Bridge {
				x.Bridge.Bridge = fmt.Sprintf("mbr%d", vni) // regenerate VXLAN connector bridge name
			}

			// if a final VID was assigned, do translation
			if vid != 0 {
				translateBridgeMember(vs, x.Bridge, vid)
			}
		}
	}

	for _, x := range vs.WpVteps {
		v := x.GetVtep()
		v.Vni = vni
		v.Name = fmt.Sprintf("vtep%d", v.Vni)
		translateBridgeMember(vs, x.Bridge, vid)
	}

	vs.Vid = vid
	vs.Vni = vni

	return nil
}

func freeIds(vat *storage.VlanAllocationTable, vs *VlanSegment) error {
	if vs.Vid != 0 {
		err := vat.FreeVid(vs.Vid)
		if err != nil {
			return fmt.Errorf("failed to free VID %d: %v", vs.Vid, err)
		}
	}

	if vs.Vni != 0 {
		err := vat.FreeVni(vs.Vni)
		if err != nil {
			return fmt.Errorf("failed to free VNI %d: %v", vs.Vni, err)
		}
	}

	return nil
}

func assignVlanIds(nodes map[string]*sfe.NodeEmbedding, vss []*VlanSegment) error {
	var err error
	var m = make(map[*storage.VlanAllocationTable]*VlanSegment)

	err = nil
	for _, vs := range vss {
		// VIDs/VNIs below 100 have specific purposes and should not be translated
		if vs.Vid >= 100 || vs.Vni >= 100 {
			vat := storage.NewVlanAllocationTable(vs.Hosts, VniTable)

			err = allocateIds(nodes, vat, vs)
			if err != nil {
				break
			}

			m[vat] = vs
		}
	}

	if err != nil {
		for vat, vs := range m {
			freeIds(vat, vs)
		}
	}

	return err
}

func relinquishVlanIds(vss []*VlanSegment) error {
	for _, vs := range vss {
		// VIDs/VNIs below 100 have specific purposes and should not be translated
		if vs.Vid >= 100 || vs.Vni >= 100 {
			vat := storage.NewVlanAllocationTable(vs.Hosts, VniTable)
			err := freeIds(vat, vs)
			if err != nil {
				log.Errorf("failed to free VLAN IDs: % v", err)
			}
		}
	}

	return nil
}
