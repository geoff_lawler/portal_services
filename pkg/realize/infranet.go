package realize

/* XXX
import (
	"encoding/binary"
	"fmt"
	"net"

	log "github.com/sirupsen/logrus"

	"gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/portal/services/pkg/realize/sfe"
	"gitlab.com/mergetb/portal/services/pkg/storage"
	"gitlab.com/mergetb/xir/v0.3/go"
)

// TODO
//   - ipv6

const (
	ServiceVRFVID  = 10
	ServiceVRFName = "svc"

	BorderVRFVID  = 11
	BorderVRFName = "wan"
)

type InfranetEmbeddingContext struct {
	vrf       string
	vid       uint32
	tbx       *xir.Topology
	embedding *sfe.Embedding
	rt        sfe.RoutingTable
	bpAlloc   []*storage.BlockPoolAllocations
	infranet  *portal.InfranetEmbedding
}

func NewInfranetEmbeddingContext(
	vrf string,
	vid uint32,
	tbx *xir.Topology,
	embedding *sfe.Embedding,
	rt sfe.RoutingTable,
) *InfranetEmbeddingContext {

	return &InfranetEmbeddingContext{
		vrf:       vrf,
		vid:       vid,
		tbx:       tbx,
		embedding: embedding,
		rt:        rt,
		infranet: &portal.InfranetEmbedding{
			InfrapodConfigs: make(map[string]*portal.InfrapodConfig),
			SwitchConfigs:   make(map[string]*portal.InfranetSwitchConfig),
		},
	}

}

func (iec *InfranetEmbeddingContext) commit() (*storage.Rollback, error) {

	//XXX hack for mars testing
	if storage.EtcdClient != nil {
		return storage.WriteBlockPoolAllocations(iec.bpAlloc)
	}

	return nil, nil

}

func (iec *InfranetEmbeddingContext) mapInfranet() error {

	configs := iec.infranet.SwitchConfigs

	nls, nhs, err := nodeFirstHops(iec.embedding)
	if err != nil {
		return err
	}

	// TODO assuming resource internet has been pruned to only facilities of
	// interest

	sls, shs, err := svcFirstHops(iec.tbx)
	if err != nil {
		return err
	}

	bls, bhs, err := borderFirstHops(iec.tbx)
	if err != nil {
		return err
	}

	ils, ihs, err := infrapodFirstHops(iec.tbx)
	if err != nil {
		return err
	}

	for i, ifx := range nhs {
		log.Debugf("node first hop: %s.%s from %s (%s)",
			ifx.Device.Id(), ifx.PortName(),
			nls[i].Device.Id(),
			nls[i].Device.Resource().InfranetAddr,
		)
		err := iec.addFirstHop(iec.vrf, "tenant", iec.vid, ifx, nls[i])
		if err != nil {
			return err
		}
	}

	for i, ifx := range shs {
		log.Debugf("service first hop: %s.%s from %s (%s)",
			ifx.Device.Id(), ifx.PortName(),
			sls[i].Device.Id(),
			sls[i].Device.Resource().InfranetAddr,
		)
		err := iec.addFirstHop(ServiceVRFName, "service", ServiceVRFVID, ifx, sls[i])
		if err != nil {
			return err
		}
	}

	for i, ifx := range ihs {
		log.Debugf("infrapod first hop: %s.%s from %s (%s)",
			ifx.Device.Id(), ifx.PortName(),
			ils[i].Device.Id(),
			ils[i].Device.Resource().InfranetAddr,
		)
		err := iec.addInfrapodFirstHop(iec.vrf, iec.vid, ifx, ils[i])
		if err != nil {
			return err
		}
	}

	for i, ifx := range bhs {
		log.Debugf("border first hop: %s.%s from %s (%s)",
			ifx.Device.Id(), ifx.PortName(),
			bls[i].Device.Id(),
			bls[i].Device.Resource().InfranetAddr,
		)
		err := iec.addFirstHop(BorderVRFName, "wan", BorderVRFVID, ifx, bls[i])
		if err != nil {
			return err
		}
	}

	fhs := interfaceSuperset(nhs, ihs, shs)

	core, err := interconnectingCore(iec.rt, fhs)
	if err != nil {
		return err
	}

	for _, ifx := range core {
		log.Debugf("interior hop: %v", ifx.Device.Id())
		err := addInteriorHop(iec.vrf, iec.vid, configs, ifx)
		if err != nil {
			return err
		}

		err = addInteriorHop(ServiceVRFName, ServiceVRFVID, configs, ifx)
		if err != nil {
			return err
		}

		err = addInteriorHop(BorderVRFName, BorderVRFVID, configs, ifx)
		if err != nil {
			return err
		}
	}

	return nil

}

func nodeFirstHops(embedding *sfe.Embedding) ([]*xir.Interface, []*xir.Interface, error) {

	var locals, ifxs []*xir.Interface

	for _, e := range embedding.Nodes {
		local, ifx, err := infranetNextHop(e.Resource)
		if err != nil {
			return nil, nil, err
		}
		ifxs = append(ifxs, ifx)
		locals = append(locals, local)
	}

	return locals, ifxs, nil

}

func svcFirstHops(tbx *xir.Topology) ([]*xir.Interface, []*xir.Interface, error) {

	return serviceFirstHops(
		tbx,
		xir.Role_SledHost,
		xir.Role_RallyHost,
		xir.Role_MinIOHost,
	)

}

func sledFirstHops(tbx *xir.Topology) ([]*xir.Interface, []*xir.Interface, error) {

	return serviceFirstHops(tbx, xir.Role_SledHost)

}

func rallyFirstHops(tbx *xir.Topology) ([]*xir.Interface, []*xir.Interface, error) {

	return serviceFirstHops(tbx, xir.Role_RallyHost)

}

func borderFirstHops(tbx *xir.Topology) ([]*xir.Interface, []*xir.Interface, error) {

	return serviceFirstHops(tbx, xir.Role_BorderGateway)

}

func infrapodFirstHops(tbx *xir.Topology) ([]*xir.Interface, []*xir.Interface, error) {

	return serviceFirstHops(tbx, xir.Role_InfrapodServer)

}

func serviceFirstHops(
	tbx *xir.Topology, role ...xir.Role) ([]*xir.Interface, []*xir.Interface, error) {

	var locals, ifxs []*xir.Interface

	for _, x := range tbx.Devices {
		if x.Resource().HasRole(role...) {
			local, ifx, err := infranetNextHop(x)
			if err != nil {
				return nil, nil, err
			}
			ifxs = append(ifxs, ifx)
			locals = append(locals, local)
		}
	}

	return locals, ifxs, nil

}

func interfaceSuperset(iss ...[]*xir.Interface) []*xir.Interface {

	var ss []*xir.Interface

	for _, is := range iss {
		for _, ifx := range is {
			found := false
			for _, x := range ss {
				if ifx.Device.Id() == x.Device.Id() &&
					ifx.PortName() == x.PortName() {
					found = true
					break
				}
			}
			if !found {
				ss = append(ss, ifx)
			}
		}
	}

	return ss

}

func interconnectingCore(
	rt sfe.RoutingTable,
	ifxs []*xir.Interface,
) ([]*xir.Interface, error) {

	var result []*xir.Interface

	for i, a := range ifxs[:len(ifxs)-1] {

		for _, b := range ifxs[i+1:] {

			if a.Device.Id() == b.Device.Id() {
				continue
			}

			wp := rt.AnyPaths(a, sfe.TPA(b.Device.Resource().TPA))
			hs := sfe.CollectHops(wp)

			if len(hs) == 0 || len(hs[0]) < 3 {
				return nil, fmt.Errorf("no path from %s:%s to %s:%s (%s)",
					a.Device.Id(), a.PortName(),
					b.Device.Id(), b.PortName(),
					sfe.TPA(b.Device.Resource().TPA),
				)
			}

			fmt.Printf("Path from %s.%s to %s.%s\n",
				a.Device.Id(), a.PortName(),
				b.Device.Id(), b.PortName(),
			)

			for _, xs := range hs {
				for _, x := range xs {
					fmt.Printf("%s.%s ", x.Device.Id(), x.PortName())
				}
				fmt.Printf("\n")
			}
			fmt.Printf("end path")

			//TODO multipath
			// note: [1:] means dont include the source interface, we only care
			// about interior interfaces here
			for _, h := range hs[0][1:] {
				skip := false
				for _, x := range result {
					if h == x {
						skip = true
						break
					}
				}
				if !skip {
					result = append(result, h)
				}
			}

		}
	}

	return result, nil

}

func isInfraVrf(name string) bool { return name == "svc" || name == "wan" }

func (iec *InfranetEmbeddingContext) addInfrapodFirstHop(
	vrf string,
	vid uint32,
	ifx *xir.Interface,
	local *xir.Interface,
) error {

	r := ifx.Device.Resource()
	if r == nil {
		return fmt.Errorf("no resource specification for %s", ifx.Device.Id())
	}

	bgp, err := getBgp(r)
	if err != nil {
		return err
	}

	//TODO this is static and should be a part of commission, not here
	for dev, blks := range r.LeafConfig.InfrapodAddressBlocks {

		var addrs []string
		for _, x := range blks.List {
			addrs = append(addrs, fmt.Sprintf("%s/26", x))
		}

		addVifs(r.Id, vrf, vid, bgp, iec.infranet.SwitchConfigs, []*portal.VrfVlanInterface{{
			Name:  fmt.Sprintf("%s.%d", dev, vid),
			Dev:   dev,
			Addrs: addrs,
		}})

		err = iec.allocateInfrapodAddress(ifx.Device, dev, blks, addrs[0], local.PortName(), vid)
		if err != nil {
			return fmt.Errorf("allocate infrapod address for %s: %v", ifx.Device.Id(), err)
		}

	}


	return nil

}

func (iec *InfranetEmbeddingContext) allocateInfrapodAddress(
	d *xir.Device,
	leafdev string,
	blks *xir.AddressList,
	gw string,
	infradev string,
	vid uint32,
) error {

	r := d.Resource()

	_, ipnet, err := net.ParseCIDR(gw)
	if err != nil {
		return fmt.Errorf("parse gw addr '%s': %v", gw, err)
	}

	for _, blk := range blks.List {

		var err error
		bpa := storage.NewBlockPoolAllocations(blk)

		//XXX hack for mars testing
		if storage.EtcdClient != nil {
			bpa, err = storage.FetchBlockPoolAllocations(blk)
			if err != nil {
				return fmt.Errorf(
					"fetch block pool allocations for %s: %+v", blk, err)
			}
		}
		if bpa.GetVersion() == 0 {
			log.Infof("block allocation for %s is empty starting a new one", blk)
			bpa.Allocations = &portal.CountSet{
				Offset: 1,
				Size:   1024,
			}
		}
		log.Infof("block allocation %s at revision %d", blk, bpa.GetVersion())
		var index uint64
		index, *bpa.Allocations, err = bpa.Allocations.Add()
		if err != nil {
			log.Infof("block %s is full, moving to the next one", blk)
			continue
		}
		log.Infof("index is %d", index)
		iec.bpAlloc = append(iec.bpAlloc, bpa)

		addr, err := incip(blk, uint32(index))
		if err != nil {
			return fmt.Errorf("increment block ip: %v", err)
		}

		iec.addInfrapodRoute(
			r.Id, addr,
			fmt.Sprintf("%s.%d", leafdev, iec.vid),
			iec.vrf,
			iec.vid,
		)

		//TODO add the allocated address to the infranet address config of
		//     the infraserver (neighbor)

		for _, x := range d.Neighbors() {
			nr := x.Remote.Device.Resource()
			if x.Local.PortName() == leafdev && nr.HasRole(xir.Role_InfrapodServer) {
				//commission.AddInfranetAddr(nr, iec.vrf, addr)
				iec.infranet.InfrapodConfigs[nr.Id] = &portal.InfrapodConfig{
					Phy:    infradev,
					Addr:   addr,
					Gw:     gw,
					Subnet: ipnet.String(),
					Vid:    vid,
				}
			}
		}

		return nil

	}
	//}

	return fmt.Errorf("infrapod address pool depleted for %s", r.Id)

}

// TODO ipv6
func (iec *InfranetEmbeddingContext) addInfrapodRoute(
	dev, prefix, nexthop, vrf string, vid uint32) {

	m := iec.infranet.SwitchConfigs

	found := false
	for _, r := range m[dev].Neighbors {
		if r.PrefixV4 == prefix+"/32" {
			found = true
			break
		}
	}
	if !found {
		m[dev].Neighbors = append(m[dev].Neighbors,
			&portal.Neighbor{
				PrefixV4: prefix + "/32",
				Nexthop:  nexthop,
				Vrf:      vrf,
			})
	}

}

// TODO
func freeInfrapodAddress(r *xir.Resource) error {

	return fmt.Errorf("not implemented")

}

func addVifs(
	dev, vrf string,
	vid uint32,
	bgp *xir.BGPRouterConfig,
	m map[string]*portal.InfranetSwitchConfig,
	vifs []*portal.VrfVlanInterface,
) error {

	_, ok := m[dev]
	if !ok {
		m[dev] = &portal.InfranetSwitchConfig{
			Asn: bgp.ASN,
			Vrfs: map[string]*portal.VrfConfig{
				vrf: {
					Name:           vrf,
					Vid:            vid,
					VlanInterfaces: vifs,
				},
			},
		}
	} else {
		_, ok := m[dev].Vrfs[vrf]
		if !ok {
			m[dev].Vrfs[vrf] = &portal.VrfConfig{
				Name:           vrf,
				Vid:            vid,
				VlanInterfaces: vifs,
			}
		} else {
			found := false
			for _, v := range vifs {
				for _, x := range m[dev].Vrfs[vrf].VlanInterfaces {
					if x.Name == v.Name {
						found = true
						break
					}
				}
				if !found {
					m[dev].Vrfs[vrf].VlanInterfaces = append(
						m[dev].Vrfs[vrf].VlanInterfaces, v)
				}
			}
		}
	}

	return nil

}

func getBgp(r *xir.Resource) (*xir.BGPRouterConfig, error) {

	bgps := r.GetBGP()
	if len(bgps) == 0 {
		return nil, fmt.Errorf("no bgp config found for %s", r.Id)
	}
	//TODO handle multi-bgp config
	return bgps[0], nil

}

func (iec *InfranetEmbeddingContext) addFirstHop(
	vrf, role string,
	vid uint32,
	ifx *xir.Interface,
	local *xir.Interface,
) error {

	//TODO make vrf names constant and consistent

	vrfkey := "tenant"
	if vrf == "svc" || vrf == "wan" {
		vrfkey = "service"
	}

	swpmode := portal.VrfSwitchPort_Untagged
	if isInfraVrf(vrf) {
		swpmode = portal.VrfSwitchPort_Tagged
	}

	dev := ifx.Device.Id()
	bgps := ifx.Device.Resource().GetBGP()
	if len(bgps) == 0 {
		return fmt.Errorf("first hop resource %s has no bgp config", dev)
	}
	//TODO handle multiple configs?
	bgp := bgps[0]

	r := ifx.Device.Resource()
	if r == nil {
		return fmt.Errorf("no resource specification for %s", ifx.Device.Id())
	}

	var addrs []string
	iaList := r.InfranetAddr[vrfkey]
	if iaList == nil {
		return fmt.Errorf("%s has no infranet addrs for vrf %s", r.Id, vrfkey)
	}
	for _, x := range iaList.List {
		addrs = append(addrs, fmt.Sprintf("%s/26", x))
	}

	vifs := []*portal.VrfVlanInterface{{
		Name:  fmt.Sprintf("vlan%d", vid),
		Dev:   "bridge",
		Addrs: addrs,
	}}

	m := iec.infranet.SwitchConfigs

	_, ok := m[dev]
	if !ok {
		m[dev] = &portal.InfranetSwitchConfig{
			Asn: bgp.ASN,
			Vrfs: map[string]*portal.VrfConfig{
				vrf: {
					Name:           vrf,
					Vid:            vid,
					VlanInterfaces: vifs,
					SwitchPorts: []*portal.VrfSwitchPort{{
						Name: ifx.PortName(),
						Mode: swpmode,
					}},
				},
			},
		}
	} else {
		_, ok := m[dev].Vrfs[vrf]
		if !ok {
			m[dev].Vrfs[vrf] = &portal.VrfConfig{
				Name:           vrf,
				Vid:            vid,
				VlanInterfaces: vifs,
				SwitchPorts: []*portal.VrfSwitchPort{{
					Name: ifx.PortName(),
					Mode: swpmode,
				}},
			}
		} else {
			found := false
			p := ifx.PortName()
			for _, x := range m[dev].Vrfs[vrf].SwitchPorts {
				if x.Name == p {
					found = true
					break
				}
			}
			if !found {
				m[dev].Vrfs[vrf].SwitchPorts = append(
					m[dev].Vrfs[vrf].SwitchPorts,
					&portal.VrfSwitchPort{
						Name: ifx.PortName(),
						Mode: swpmode,
					},
				)
			}
		}
	}

	// TODO ipv6
	for _, x := range local.Device.Resource().InfranetAddr[role].List {
		found := false
		for _, r := range m[dev].Neighbors {
			if r.PrefixV4 == x+"/32" {
				found = true
				break
			}
		}
		if !found {

			var name string

			// in the case of infrapod neighbors there will be no name
			emb, ok := iec.embedding.Nodes[local.Device.Id()]
			if ok {
				name = emb.Node.Id
			}
			m[dev].Neighbors = append(m[dev].Neighbors,
				&portal.Neighbor{
					PrefixV4: x + "/32",
					Nexthop:  fmt.Sprintf("vlan%d", vid),
					Vrf:      vrf,
					Mac:      local.Port().Mac,
					Name:     name,
				})

		}

	}

	return nil

}

func incip(addr string, delta uint32) (string, error) {

	ip := net.ParseIP(addr)
	if ip == nil {
		return "", fmt.Errorf("invalid ip %s", addr)
	}

	x := binary.BigEndian.Uint32(ip.To4()) + delta
	buf := []byte{0, 0, 0, 0}
	binary.BigEndian.PutUint32(buf, x)
	return net.IP(buf).String(), nil

}

func addInteriorHop(
	vrf string,
	vid uint32,
	m map[string]*portal.InfranetSwitchConfig,
	ifx *xir.Interface,
) error {

	dev := ifx.Device.Id()
	bgps := ifx.Device.Resource().GetBGP()
	if len(bgps) == 0 {
		return fmt.Errorf("first hop resource %s has no bgp config", dev)
	}
	//TODO handle multiple configs?
	bgp := bgps[0]

	vlanIfx := fmt.Sprintf("%s.%d", ifx.PortName(), vid)

	_, ok := m[dev]
	if !ok {
		m[dev] = &portal.InfranetSwitchConfig{
			Asn: bgp.ASN,
			Vrfs: map[string]*portal.VrfConfig{
				vrf: {
					Name: vrf,
					Vid:  vid,
					VlanInterfaces: []*portal.VrfVlanInterface{
						{
							Name: vlanIfx,
							Dev:  ifx.PortName(),
						},
					},
					NeighborInterfaces: []string{vlanIfx},
				},
			},
		}
	} else {
		_, ok := m[dev].Vrfs[vrf]
		if !ok {
			m[dev].Vrfs[vrf] = &portal.VrfConfig{
				Name: vrf,
				Vid:  vid,
				VlanInterfaces: []*portal.VrfVlanInterface{
					{
						Name: vlanIfx,
						Dev:  ifx.PortName(),
					},
				},
				NeighborInterfaces: []string{vlanIfx},
			}
		} else {

			m[dev].Vrfs[vrf].VlanInterfaces = append(m[dev].Vrfs[vrf].VlanInterfaces,
				&portal.VrfVlanInterface{
					Name: fmt.Sprintf("%s.%d", ifx.PortName(), vid),
					Dev:  ifx.PortName(),
				})

			found := false
			for _, x := range m[dev].Vrfs[vrf].NeighborInterfaces {
				if x == vlanIfx {
					found = true
					break
				}
			}
			if !found {
				m[dev].Vrfs[vrf].NeighborInterfaces = append(
					m[dev].Vrfs[vrf].NeighborInterfaces, vlanIfx)
			}

		}
	}

	return nil
}

func infranetNextHop(d *xir.Device) (*xir.Interface, *xir.Interface, error) {
	ifx := d.Infranet()
	if ifx == nil {
		return nil, nil, fmt.Errorf("Node %s has no infranet interface", d.Id())
	}
	nbrs := ifx.Neighbors()
	if len(nbrs) == 0 {
		return nil, nil, fmt.Errorf("Node %s, unconnected infranet interface", d.Id())
	}
	return ifx, nbrs[0].Remote, nil
}
*/
