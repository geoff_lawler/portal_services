package storage

import (
	"encoding/json"
	"fmt"
)

type PortalConfig struct {
	Config struct {
		APIEndpoint     string `json:"api_endpoint"`
		GRPCEndpoint    string `json:"grpc_endpoint"`
		SSHJumpEndpoint string `json:"sshjump_endpoint"`
		Ver             int64  `json:"ver"`
		Reconcile       struct {
			HeartbeatIntervalSec    int64 `json:"heartbeat_interval_sec,omitempty"`
			HeartbeatGracePeriodSec int64 `json:"heartbeat_grace_period_sec,omitempty"`
		}

		Institutions []string `json:"institutions"`

		// User categories
		Categories []string `json:"categories"`

		// Types points to list of subtypes.
		EntityTypes map[string][]string `json:"entitytypes"`
	}
	Ver int64
}

func GetPortalConfig() (*PortalConfig, error) {

	// There is only one.
	pc := &PortalConfig{}
	err := pc.Read()
	if err != nil {
		return nil, err
	}

	return pc, nil
}

func (c *PortalConfig) Marshal() ([]byte, error) {
	return json.Marshal(c.Config)
}

func (c *PortalConfig) Unmarshal(b []byte) error {
	return json.Unmarshal(b, &c.Config)
}

func (c *PortalConfig) Bucket() string {
	return ""
}

func (c *PortalConfig) Id() string {
	// hardcoded ID as there is only one config.
	return "config"
}

func (c *PortalConfig) Key() string {
	return "/portal/" + c.Id()
}

func (c *PortalConfig) Value() interface{} {
	return c
}

func (c *PortalConfig) Substrate() Substrate {
	return Etcd
}

func (c *PortalConfig) Read() error {
	_, err := etcdTx(readOps(c)...)
	return err
}

func (c *PortalConfig) GetVersion() int64 {
	return c.Config.Ver
}

func (c *PortalConfig) SetVersion(v int64) {
	c.Config.Ver = v
}

func (c *PortalConfig) Create() (*Rollback, error) {

	r, err := etcdTx(writeOps(c)...)
	if err != nil {
		return nil, fmt.Errorf("portal config create write: %w", err)
	}

	return r, nil
}

func (c *PortalConfig) Update() (*Rollback, error) {

	r, err := etcdTx(writeOps(c)...)
	if err != nil {
		return nil, fmt.Errorf("portal config update write: %w", err)
	}

	return r, nil
}

func (c *PortalConfig) Delete() (*Rollback, error) {

	r, err := etcdTx(delOps(c)...)
	if err != nil {
		return nil, fmt.Errorf("portal config delete: %w", err)
	}

	return r, nil
}
