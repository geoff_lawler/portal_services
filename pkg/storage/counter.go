package storage

import (
	"fmt"

	"gitlab.com/mergetb/portal/services/internal"
)

type Counter struct {
	internal.CountSet
}

func NewCounter(name string, size, offset uint64) *Counter {
	return &Counter{
		CountSet: internal.CountSet{
			Name:   name,
			Size:   size,
			Offset: offset,
		},
	}
}

func uidCounter() *Counter {
	return NewCounter("uid", 1<<32-1, 1000)
}

func gidCounter() *Counter {
	return NewCounter("gid", 1<<32-1, 1000)
}

func oidCounter() *Counter {
	return NewCounter("oid", 1<<32-1, 1000)
}

func (c *Counter) Bucket() string {

	return "counters"

}

func (c *Counter) Id() string {

	return c.Name

}

func (c *Counter) Key() string {

	return fmt.Sprintf("/%s/%s", c.Bucket(), c.Id())

}

func (c *Counter) Value() interface{} {

	return &c.CountSet

}

func (c *Counter) Substrate() Substrate {

	return Etcd

}

func (c *Counter) Read() error {

	_, err := etcdTx(readOps(c)...)
	return err

}

func (c *Counter) Create() (*Rollback, error) {

	return etcdTx(writeOps(c)...)
}

func (c *Counter) Update() (*Rollback, error) {

	return c.Create()

}

func (c *Counter) Delete() (*Rollback, error) {

	return etcdTx(delOps(c)...)

}

func (c *Counter) GetVersion() int64 {

	return c.Ver

}

func (c *Counter) SetVersion(v int64) {

	c.Ver = v

}

func (c *Counter) Next() (uint64, error) {

	var v uint64
	var err error

	v, c.CountSet, err = c.CountSet.Add()
	if err != nil {
		return 0, err
	}

	_, err = c.Update()
	if err != nil {
		return 0, err
	}

	return v, nil
}

func (c *Counter) Free(v uint64) error {

	var err error

	c.CountSet = c.CountSet.Remove(v)

	_, err = c.Update()
	if err != nil {
		return err
	}

	return nil
}
