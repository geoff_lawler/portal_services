package storage

import (
	"context"
	"fmt"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"
	clientv3 "go.etcd.io/etcd/client/v3"
	"google.golang.org/protobuf/proto"

	portal "gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/portal/services/pkg/merror"
	"gitlab.com/mergetb/tech/reconcile"
)

type User struct {
	*portal.User
	UpdateRequest *portal.UpdateUserRequest
}

func NewUser(username string) *User {

	return &User{
		User: &portal.User{
			Username: username,
		},
	}

}

// Keep track of user state wrt current status.
// Used to generate certs, keys, xdc accounts, etc.
type UserStatus struct {
	*portal.UserStatus
}

func NewUserStatus(username string) *UserStatus {

	return &UserStatus{
		UserStatus: &portal.UserStatus{
			Username: username,
		},
	}
}

func ListUsers() ([]*User, error) {

	var result []*User

	kvc := clientv3.NewKV(EtcdClient)
	resp, err := kvc.Get(context.TODO(), "/users/", clientv3.WithPrefix())
	if err != nil {
		return nil, fmt.Errorf("get users: %v", err)
	}

	for _, kv := range resp.Kvs {

		u := &User{User: new(portal.User)}
		err = proto.Unmarshal(kv.Value, u.User)
		if err != nil {
			return nil, fmt.Errorf("malformed user at %s: %v", err, string(kv.Key))
		}
		result = append(result, u)

	}

	return result, nil

}

func UserExists(username string) bool {

	u := NewUser(username)
	err := u.Read()
	if err != nil {
		return false
	}

	if u.GetVersion() == 0 {
		return false
	}

	return true
}

// Storage Object interface implementation ====================================

func (u *User) Bucket() string {

	return "users"

}

func (u *User) GetVersion() int64 {

	return u.Ver

}

func (u *User) SetVersion(v int64) {

	u.Ver = v

}

func (u *User) Id() string {

	return u.User.Username

}

func (u *User) Key() string {

	return fmt.Sprintf("/%s/%s", u.Bucket(), u.Id())

}

func (u *User) Value() interface{} {

	return u.User

}

func (u *User) Substrate() Substrate {

	return Etcd

}

// WriteRegistration - write the user data required for registration only.
// Do not "init" the user by allocating a uid/gid or creating a user personal
// project. This function only write a minimal set of user data that
// is given during account registration. To generate UID/GID and a
// personal project, call user.Create().
func (u *User) WriteRegistration() (*Rollback, error) {

	rollback, err := etcdTx(writeOps(u)...)
	if err != nil {
		return nil, fmt.Errorf("write user commit: %v", err)
	}

	return rollback, nil
}

func (u *User) Create() (*Rollback, error) {

	// lock the uid and gid counters for user creation, these are globally
	// shared objects by all users and projects, so optimistic concurrency works
	// poorly here.

	// local mutex first to ease strain on distributed lock
	uidmtx.Lock()
	defer uidmtx.Unlock()

	gidmtx.Lock()
	defer gidmtx.Unlock()

	lks, err := locks(uidLock, gidLock)
	if err != nil {
		return nil, fmt.Errorf("uid/gid lock: %v", err)
	}
	defer lks.Unlock()

	// fetch the user and group counter objects from storage

	uid, gid := uidCounter(), gidCounter()
	_, err = etcdTx(readOps(u, uid, gid)...)
	if err != nil {
		return nil, fmt.Errorf("read uid/gid counters: %v", err)
	}

	if u.Uid != 0 {
		return nil, fmt.Errorf("User already exists with UID %d", u.Uid)
	}

	// assign the next available UID to the user

	var nextid uint64
	nextid, uid.CountSet, err = uid.Add()
	if err != nil {
		return nil, fmt.Errorf("uid next: %v", err)
	}
	u.Uid = uint32(nextid)

	// Initialize the user's personal project.

	member := &portal.Member{
		Role:  portal.Member_Creator,
		State: portal.Member_Active,
	}

	u.Projects = map[string]*portal.Member{
		u.Id(): member,
	}

	p := &Project{Project: &portal.Project{
		Name: u.Id(),
		Members: map[string]*portal.Member{
			u.Id(): member,
		},
	}}

	// assign the next available GID to the users personal project

	nextid, gid.CountSet, err = gid.Add()
	if err != nil {
		return nil, fmt.Errorf("gid next: %v", err)
	}
	u.Gid = uint32(nextid)
	p.Gid = uint32(nextid) // personal project's GID will be the same as gid of the user

	rollback, err := etcdTx(writeOps(u, p, uid, gid)...)
	if err != nil {
		return nil, fmt.Errorf("create user commit: %v", err)
	}

	return rollback, nil
}

func (u *User) Read() error {

	_, err := etcdTx(readOps(u)...)
	return err

}

func ReadUsers(users []*User) error {

	var objs []ObjectIO
	for _, u := range users {
		objs = append(objs, u)
	}

	_, err := etcdTx(readOps(objs...)...)
	return err
}

func (u *User) Update() (*Rollback, error) {

	// If the update request is nil, nothing to do

	if u.UpdateRequest == nil {
		return nil, nil
	}
	upd := u.UpdateRequest

	// prepare to read users and projects involved in the update

	projects := make(map[string]*Project)
	objs := []ObjectIO{u}

	processProject := func(p string) {
		if projects[p] == nil {
			proj := &Project{Project: &portal.Project{Name: p}}
			projects[p] = proj
			objs = append(objs, proj)
		}
	}

	if upd.Projects != nil {

		for _, p := range upd.Projects.Remove {
			processProject(p)
		}

		if upd.Projects.Set != nil {
			for p := range upd.Projects.Set {
				processProject(p)
			}
		}

	}

	// read in current user and project state

	_, err := etcdTx(readOps(objs...)...)
	if err != nil {
		return nil, fmt.Errorf("user update read: %v", err)
	}

	// perform updates

	// Colloquial name
	if upd.Name != "" {
		u.Name = upd.Name
	}

	// User state
	if upd.State != nil {
		u.State = upd.State.Value
	}

	// AccessMode
	if upd.AccessMode != nil {
		u.AccessMode = upd.AccessMode.Value
	}

	// admin
	if upd.ToggleAdmin == true {
		log.Infof("Toggling %s admin to %t", u.Username, !u.Admin)
		u.Admin = !u.Admin
	}

	// experiment update. add if not there remove if there.
	if len(upd.Experiments) != 0 {
		for _, ue := range upd.Experiments {
			found := false
			for i, e := range u.Experiments {
				if ue == e {
					u.Experiments = append(u.Experiments[:i], u.Experiments[i+1:]...)
					found = true
					break
				}
			}
			if !found {
				u.Experiments = append(u.Experiments, ue)
			}
		}
	}

	// Project membership
	if upd.Projects != nil {
		for _, proj := range upd.Projects.Remove {
			delete(u.Projects, proj)
			delete(projects[proj].Members, u.Id())
		}
		if upd.Projects.Set != nil {
			for proj, membership := range upd.Projects.Set {
				u.Projects[proj] = membership
				projects[proj].Members[u.Id()] = membership
			}
		}
	}

	// set user status to not logged is if state is not active.
	if u.State != portal.UserState_Active {
		us := NewUserStatus(u.Username)
		err = us.Read()
		if err != nil {
			return nil, fmt.Errorf("user status read: %+v", err)
		}

		if us.Loggedin == true {
			us.Loggedin = false
			objs = append(objs, us)
		}
	}

	// write updates
	rollback, err := etcdTx(writeOps(objs...)...)
	if err != nil {
		return nil, fmt.Errorf("user update tx: %v", err)
	}

	return rollback, nil

}

func (u *User) Delete() (*Rollback, error) {

	// initial read to get linked data

	err := u.Read()
	if err != nil {
		return nil, fmt.Errorf("user read: %v", err)
	}

	linkedOps, err := u.DeleteLinkedOps(nil)
	if err != nil {
		return nil, err
	}

	// write updates
	ops := append(delOps(u), linkedOps...)

	rollback, err := etcdTx(ops...)
	if err != nil {
		return nil, fmt.Errorf("user delete tx: %v", err)
	}

	return rollback, nil

}

func (u *User) DeleteLinkedOps(tc *TransactionCache) ([]StorOp, error) {

	var rops, wops, dops []StorOp

	if tc == nil {
		tc = NewTransactionCache()
	}

	log.Infof("gathering delete linked ops for user %s", u.Username)

	// read organizations involved in update
	var oremove, odelete []ObjectIO
	for x, m := range u.Organizations {
		o := tc.NewObj(PTOrg, x)

		// if the user is the creator of the organization, the organization needs to be
		// deleted; otherwise just remove the user from the organization
		if m.Role == portal.Member_Creator {
			odelete = append(odelete, o)
		} else {
			oremove = append(oremove, o)
		}

	}
	oobjs := oremove

	// read projects involved in update
	var premove, pdelete []ObjectIO
	for x, m := range u.Projects {
		p := tc.NewObj(PTProj, x)

		// if the user is the creator of the project, the project needs to be
		// deleted; otherwise just remove the project from the organization
		// delete user's personal project and any other projects the user created
		if m.Role == portal.Member_Creator {
			pdelete = append(pdelete, p)
		} else {
			premove = append(premove, p)
		}

	}
	pobjs := append(premove, pdelete...)

	// read experiments involved in update
	var eremove, edelete []ObjectIO
	for _, ename := range u.Experiments {
		// ugly and error prone. TODO: make better
		tkns := strings.Split(ename, ".")
		if len(tkns) != 2 {
			return nil, fmt.Errorf("bad exp name format: %s", ename)
		}

		e := tc.NewObj(PTExp, tkns[0], tkns[1]).(*Experiment)
		err := e.Read()
		if err != nil {
			return nil, fmt.Errorf("exp %s read: %+v", ename, err)
		}

		// if the user is the creator of the experiment, the experiment needs to be
		// deleted; otherwise just remove the user from the experiment
		if e.Creator == u.Username {
			edelete = append(edelete, e)
		} else {
			eremove = append(eremove, e)
		}
	}
	eobjs := append(eremove, edelete...)

	// read facilities involved in update
	var fremove, fdelete []ObjectIO
	for x, m := range u.Facilities {
		f := tc.NewObj(PTFac, x)

		// if the user is the creator of the facility, the facility needs to be
		// deleted; otherwise just remove the user from the facility
		if m.Role == portal.Member_Creator {
			fdelete = append(fdelete, f)
		} else {
			fremove = append(fremove, f)
		}
	}
	fobjs := fremove

	rops = append(rops, readOps(oobjs...)...)
	rops = append(rops, readOps(pobjs...)...)
	rops = append(rops, readOps(eobjs...)...)
	rops = append(rops, readOps(fobjs...)...)

	_, err := etcdTx(rops...)
	if err != nil {
		return nil, fmt.Errorf("read user data: %v", err)
	}

	// remove user from organizations
	for _, o := range oremove {
		delete(o.Value().(*portal.Organization).Members, u.Id())
	}

	// remove user from projects
	for _, o := range premove {
		delete(o.Value().(*portal.Project).Members, u.Id())
	}

	// remove user from experiment maintainers
	for _, o := range eremove {
		exp := o.(*Experiment)

		mts := []string{}
		for _, m := range exp.Maintainers {
			if m != u.Username {
				mts = append(mts, m)
			}
		}
		exp.Maintainers = mts
	}

	// remove user from facilities
	for _, o := range fremove {
		delete(o.Value().(*portal.Facility).Members, u.Id())
	}

	// remove user xdcs
	var xdelete []ObjectIO
	xdcs, err := u.GetXDCs()
	if err != nil {
		return nil, fmt.Errorf("get user xdcs: %w", err)
	}
	for _, x := range xdcs {
		xdelete = append(xdelete, x)
		ops, err := x.DelLinkedOps()
		if err != nil {
			return nil, fmt.Errorf("del user link xdc ops: %w", err)
		}
		dops = append(dops, ops...)
	}

	// gather delete ops for organizations
	for _, o := range odelete {
		org := o.(*Organization)

		ops, err := org.DeleteLinkedOps(tc)
		if err != nil {
			return nil, fmt.Errorf("del user linked organization ops: %v", err)
		}

		dops = append(dops, ops...)
	}

	// gather delete ops for projects
	for _, o := range pdelete {
		proj := o.(*Project)

		ops, err := proj.DeleteLinkedOps(tc)
		if err != nil {
			return nil, fmt.Errorf("del user linked project ops: %v", err)
		}

		dops = append(dops, ops...)
	}

	// gether delete ops for experiments
	for _, o := range edelete {
		exp := o.(*Experiment)

		ops, err := exp.DeleteLinkedOps(tc)
		if err != nil {
			return nil, fmt.Errorf("del user linked experiment ops: %v", err)
		}

		dops = append(dops, ops...)
	}

	// things to update
	wops = append(wops, writeOps(oremove...)...)
	wops = append(wops, writeOps(premove...)...)
	wops = append(wops, writeOps(eremove...)...)
	wops = append(wops, writeOps(fremove...)...)
	ops := wops

	// things to delete
	ops = append(ops, delOps(odelete...)...)
	ops = append(ops, delOps(pdelete...)...)
	ops = append(ops, delOps(edelete...)...)
	ops = append(ops, delOps(fdelete...)...)
	ops = append(ops, delOps(xdelete...)...)

	// things from dependent DeleteLinkedOps() calls
	ops = append(ops, dops...)

	return ops, nil

}

func (user *User) GetGoal(timeout time.Duration) (*reconcile.TaskForest, error) {

	if user.GetVersion() == 0 {
		err := user.Read()
		if err != nil {
			return nil, err
		}

		if user.GetVersion() == 0 {
			return nil, merror.NotFoundError("user", user.Username)
		}
	}

	tg := reconcile.NewGoal(user.Key(), "User: "+user.Username, "Manage user: "+user.Username)
	tg.AddTaskRecord(FindTaskRecords(user, reconcile.TaskRecord_Exists)...)

	// the following only exist if the user is active:
	// sshkeys, public keys,
	existence := reconcile.TaskRecord_Exists
	if user.State != portal.UserState_Active {
		existence = reconcile.TaskRecord_Optional
	}

	sshkeypair := NewSSHUserKeyPair(user.Username)
	tg.AddTaskRecord(FindTaskRecords(sshkeypair, existence)...)

	pkeys, err := ListUserPublicKeys(user.Username)
	if err != nil {
		return nil, err
	}
	for _, pkey := range pkeys {
		tg.AddTaskRecord(FindTaskRecords(pkey, existence)...)
	}

	// check if user has logged in at least once, so userstatus and sshcert exists
	userstatus := NewUserStatus(user.Username)
	err = userstatus.Read()
	if err != nil {
		return nil, err
	}

	existence = reconcile.TaskRecord_Exists
	if userstatus.GetVersion() == 0 {
		existence = reconcile.TaskRecord_Optional
	}

	tg.AddTaskRecord(FindTaskRecords(userstatus, existence)...)

	sshcert := NewSSHUserCert(user.Username)
	tg.AddTaskRecord(FindTaskRecords(sshcert, existence)...)

	return tg.WatchTaskForest(EtcdClient, timeout)
}

// return a slice of user-created xdcs
func (u *User) GetXDCs() ([]*XDC, error) {
	xdcs := []*XDC{}
	for p := range u.Projects {
		xs, err := ListXDCs(p)
		if err != nil {
			return nil, fmt.Errorf("list xdc internal error: %w", err)
		}
		for _, x := range xs {
			if  x.Creator == u.Name {
				xdcs = append(xdcs, xs...)
			}
		}
	}
	return xdcs, nil
}

// ****************************************************************************
// UserStatus
// ****************************************************************************
func (us *UserStatus) Bucket() string {

	return "userstatus"
}

func (us *UserStatus) GetVersion() int64 {

	return us.Ver
}

func (us *UserStatus) SetVersion(v int64) {

	us.Ver = v
}

func (us *UserStatus) Id() string {

	return us.UserStatus.Username
}

func (us *UserStatus) Key() string {

	return "/" + us.Bucket() + "/" + us.Id()
}

func (us *UserStatus) Value() interface{} {

	return us.UserStatus
}

func (us *UserStatus) Substrate() Substrate {

	return Etcd
}

func (us *UserStatus) Create() (*Rollback, error) {

	return etcdTx(writeOps(us)...)
}

func (us *UserStatus) Read() error {

	_, rb := etcdTx(readOps(us)...)
	return rb
}

func (us *UserStatus) Update() (*Rollback, error) {

	return etcdTx(writeOps(us)...)
}

func (us *UserStatus) Delete() (*Rollback, error) {

	return etcdTx(delOps(us)...)
}

func (us *UserStatus) Unmarshal(b []byte) error {

	err := proto.Unmarshal(b, us.UserStatus)
	if err != nil {
		return fmt.Errorf("malformed user status at %s: %v", err, string(b))
	}

	return nil
}
