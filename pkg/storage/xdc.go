package storage

import (
	"context"
	"fmt"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"
	clientv3 "go.etcd.io/etcd/client/v3"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/proto"

	portal "gitlab.com/mergetb/api/portal/v1/go"

	"gitlab.com/mergetb/portal/services/internal"

	"gitlab.com/mergetb/tech/reconcile"
	sstor "gitlab.com/mergetb/tech/shared/storage/etcd"
)

type XDC struct {
	*portal.XDCStorage

	UpdateRequest *portal.XDCStorage
}

func NewXDC(creator, name, project string) *XDC {

	return &XDC{
		XDCStorage: &portal.XDCStorage{
			Creator: creator,
			Name:    name,
			Project: project,
		},
	}
}

// The canonical way to name an xdc.
func XdcId(name, project string) string {
	return name + "." + project
}

func (x *XDC) XdcId() string {
	return XdcId(x.Name, x.Project)
}

func (x *XDC) Bucket() string {

	return "xdcs"
}

func (x *XDC) Id() string {

	return x.Name + "." + x.Project
}

func (x *XDC) Key() string {

	return "/" + x.Bucket() + "/" + x.XdcId()
}

func (x *XDC) Value() interface{} {

	return x.XDCStorage
}

func (x *XDC) Substrate() Substrate {

	return Etcd
}

func (x *XDC) Read() error {

	_, err := etcdTx(readOps(x)...)
	return err
}

func (x *XDC) GetVersion() int64 {

	return x.Ver
}

func (x *XDC) SetVersion(v int64) {

	x.Ver = v
}

func (x *XDC) Create() (*Rollback, error) {

	log.Debugf("[%s.%s] creating XDC for %s", x.Name, x.Project, x.Creator)

	err := x.Read()
	if err != nil {
		return nil, fmt.Errorf("xdc read: %w", err)
	}

	if x.Ver > 0 {
		return nil, fmt.Errorf("XDC exists")
	}

	if x.Project == "" || x.Name == "" {
		return nil, fmt.Errorf("incomplete creation information")
	}

	p := NewProject(x.Project)
	err = p.Read()
	if err != nil {
		return nil, fmt.Errorf("project read: %w", err)
	}

	if p.Ver == 0 {
		return nil, fmt.Errorf("project %s does not exist", x.Project)
	}

	log.Debugf("writing XDC: %+v", x)

	r, err := etcdTx(writeOps(x)...)
	if err != nil {
		return nil, fmt.Errorf("xdc write: %w", err)
	}

	return r, nil
}

func (x *XDC) Update() (*Rollback, error) {

	log.Infof("XDC update")

	// If there is no update, there is nothing to do.
	if x.UpdateRequest == nil {
		return nil, nil
	}

	err := x.Read()
	if err != nil {
		return nil, fmt.Errorf("xdc read: %w", err)
	}
	if x.Ver == 0 {
		return nil, fmt.Errorf("XDC does not exist")
	}

	upd := x.UpdateRequest

	if upd.Creator != "" {
		c := NewUser(upd.Creator)
		err = c.Read()
		if err != nil {
			return nil, fmt.Errorf("read user %s: %w", upd.Creator, err)
		}

		if c.Ver == 0 {
			return nil, fmt.Errorf("user %s does not exist", upd.Creator)
		}
	}

	if upd.Image != "" {
		x.Image = upd.Image
	}
	if upd.MemLimit != 0 {
		x.MemLimit = upd.MemLimit
	}
	if upd.CpuLimit != 0 {
		x.CpuLimit = upd.CpuLimit
	}

	r, err := etcdTx(writeOps(x)...)
	if err != nil {
		return nil, fmt.Errorf("xdc write: %w", err)
	}

	return r, nil
}

func (x *XDC) SetMaterialization(mtz string) (*Rollback, error) {

	err := x.Read()
	if err != nil {
		return nil, fmt.Errorf("xdc read: %w", err)
	}
	if x.Ver == 0 {
		return nil, fmt.Errorf("XDC does not exist")
	}

	x.Materialization = mtz

	r, err := etcdTx(writeOps(x)...)
	if err != nil {
		return nil, fmt.Errorf("xdc write: %w", err)
	}

	return r, nil
}

func (x *XDC) Delete() (*Rollback, error) {

	err := x.Read()
	if err != nil {
		return nil, fmt.Errorf("xdc read: %w", err)
	}

	if x.Ver == 0 {
		return nil, fmt.Errorf("xdc does not exist")
	}

	dels := []StorOp{}
	dels = append(dels, delOps(x)...)

	ops, err := x.DelLinkedOps()
	if err != nil {
		log.Warnf("Unable to get linked delete ops for xdc %s", x.Name)
	}

	dels = append(dels, ops...)

	r, err := etcdTx(dels...)
	if err != nil {
		return nil, fmt.Errorf("xdc del: %w", err)
	}

	return r, nil
}

// DelLinkedOps ...
func (x *XDC) DelLinkedOps() ([]StorOp, error) {

	if x.Materialization != "" {
		c := NewXdcWgClient(&portal.AttachXDCRequest{Project: x.Project, Xdc: x.Name})
		err := c.Read()
		if err != nil {
			return nil, status.Error(codes.Internal, "XDC attach data read")
		}

		log.Infof("Found attachment %x -> %s. Deleting attachment key.", x.Name, x.Materialization)

		return delOps(c), nil
	}

	return nil, nil
}

func ListXDCs(pid string) ([]*XDC, error) {

	var result []*XDC

	x := &XDC{}
	kvc := clientv3.NewKV(EtcdClient)
	resp, err := kvc.Get(context.TODO(), "/"+x.Bucket(), clientv3.WithPrefix())
	if err != nil {
		return nil, fmt.Errorf("get xdcs: %w", err)
	}

	for _, kv := range resp.Kvs {

		x := new(portal.XDCStorage)
		err = proto.Unmarshal(kv.Value, x)
		if err != nil {
			return nil, fmt.Errorf("malformed xdc data at %s: %w", string(kv.Key), err)
		}

		if x.Project == pid {
			result = append(result, &XDC{XDCStorage: x})
		}
	}

	return result, nil
}

func (x *XDC) ToXDCInfo() *portal.XDCInfo {
	parts := strings.Split(x.Image, "/")
	if len(parts) > 0 {
		x.Image = parts[len(parts)-1]
	}

	url := ""
	fqdn := x.Name + "-" + x.Project

	jc := NewJupyterCfg(x.Name + "-" + x.Project)
	err := jc.Read()
	if err != nil {
		log.Error("no jupyter cfg for this xdc for some reason")
	} else {
		if jc.Domain == "" {
			url = "N/A"
			fqdn = "N/A"
		} else {
			url = jc.Url
			fqdn += "." + jc.Domain
		}
	}

	return &portal.XDCInfo{
		Name:            x.Name + "." + x.Project,
		Url:             url,
		Fqdn:            fqdn,
		Creator:         x.Creator,
		Memlimit:        x.MemLimit,
		Cpulimit:        x.CpuLimit,
		Image:           x.Image,
		Materialization: x.Materialization,
	}
}

func (x *XDC) GetGoal(timeout time.Duration) (*reconcile.TaskForest, error) {
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()

	cache := sstor.NewReadCache(0, nil)
	defer cache.Close()

	// ssh jump keys
	/*
		tg_sshjump := reconcile.NewGoal("ssh-jump", "SSH Jump Pod", "Manage SSH Jump Pod")

		sshjumpkey := NewSSHHostKeyPair("ssh-jump")
		sshjumpcert := NewSSHHostCert("ssh-jump")
		sshinit := NewPodRaw("ssh-jump", "podinit")

		tg_sshjump.AddTaskRecord(FindTaskRecords(sshjumpkey, reconcile.TaskRecord_Exists)...)
		tg_sshjump.AddTaskRecord(FindTaskRecords(sshjumpcert, reconcile.TaskRecord_Exists)...)
		tg_sshjump.AddTaskRecord(FindTaskRecords(sshinit, reconcile.TaskRecord_Exists)...)

		tf_sshjump, err := tg_sshjump.GetTaskForest(EtcdClient, cache)
		if err != nil {
			return nil, err
		}
	*/

	// the actual keys of the XDC

	tg := reconcile.NewGoal(x.Key(), "XDC: "+x.XdcId(), "Manage XDC: "+x.XdcId())

	tg.AddTaskRecord(FindTaskRecords(x, reconcile.TaskRecord_Exists)...)

	sshkey := NewSSHHostKeyPair(x.XdcId())
	sshcert := NewSSHHostCert(x.XdcId())

	tg.AddTaskRecord(FindTaskRecords(sshcert, reconcile.TaskRecord_Exists)...)
	tg.AddTaskRecord(FindTaskRecords(sshkey, reconcile.TaskRecord_Exists)...)

	xdcinit := NewPodRaw(x.XdcId(), "podinit")
	wgattach := NewPodRaw(x.XdcId(), "wgsvc")

	tg.AddTaskRecord(FindTaskRecords(xdcinit, reconcile.TaskRecord_Exists)...)
	tg.AddTaskRecord(FindTaskRecords(wgattach, reconcile.TaskRecord_Exists)...)

	client := NewXdcWgClient(&portal.AttachXDCRequest{Project: x.Project, Xdc: x.Name})
	err := client.Read()
	if err != nil {
		return nil, err
	}

	// is attached, add stuff
	if client.GetVersion() != 0 {
		// figure out mtzs that the XDC is attached to
		tg.AddTaskRecord(FindTaskRecords(client, reconcile.TaskRecord_Exists)...)

		mzid := internal.MzidToString(
			&internal.Mzid{
				Rid: client.Realization,
				Eid: client.Experiment,
				Pid: client.Project,
			},
		)

		wgifs, err := ListWgIfreqs()
		if err != nil {
			return nil, err
		}

		for _, wgif := range wgifs[mzid] {
			tg.AddTaskRecord(FindTaskRecords(wgif, reconcile.TaskRecord_Exists)...)
		}
	}

	tf, err := tg.WatchTaskForestWithOptions(ctx, EtcdClient, cache)
	if err != nil {
		return nil, err
	}

	//tf.PushFront(false, tf_sshjump)

	return tf, nil
}
