package storage

import (
	"context"
	"fmt"
	"sync"

	log "github.com/sirupsen/logrus"
	"go.etcd.io/etcd/client/v3/concurrency"
)

const (
	uidLock = "uid"
	gidLock = "gid"
	oidLock = "oid"
)

// TODO: Deprecate with the new potpourri storage library

// local locks
var (
	uidmtx sync.Mutex
	gidmtx sync.Mutex
	oidmtx sync.Mutex
)

type Lock struct {
	path    string
	mutex   *concurrency.Mutex
	session *concurrency.Session
}

type Locks []*Lock

func lockpath(o Object) string {

	return fmt.Sprintf("/lock/%s", o.Key())

}

func lockobj(o Object) (*Lock, error) {
	return lock(lockpath(o))
}

func lock(path string) (*Lock, error) {

	path = fmt.Sprintf("/lock/%s", path)

	c := EtcdClient

	se, err := concurrency.NewSession(c)
	if err != nil {
		return nil, fmt.Errorf("lock session: %v", err)
	}

	lk := &Lock{
		path:    path,
		session: se,
		mutex:   concurrency.NewMutex(se, path),
	}

	err = lk.mutex.Lock(context.TODO())
	if err != nil {
		se.Close()
		return nil, fmt.Errorf("lock %s: %v", path, err)
	}

	return lk, nil

}

func lockobjs(objects ...Object) (Locks, error) {

	var paths []string
	for _, o := range objects {
		paths = append(paths, lockpath(o))
	}

	return locks(paths...)

}

func locks(paths ...string) (Locks, error) {

	var lks Locks

	for _, p := range paths {
		lk, err := lock(p)
		if err != nil {
			lks.Unlock()
			return nil, err
		}
		lks = append(lks, lk)
	}

	return lks, nil

}

func (lks Locks) Unlock() []error {

	var errs []error

	for _, lk := range lks {

		err := lk.Unlock()
		if err != nil {
			errs = append(errs, err)
		}

	}

	if len(errs) > 0 {
		log.Errorf("Unlock errors: %v", errs)
	}

	return errs

}

func (lk *Lock) Unlock() error {

	defer lk.session.Close()

	err := lk.mutex.Unlock(context.TODO())
	if err != nil {
		log.Warnf("Unlock error: %s: %v", lk.path, err)
		return fmt.Errorf("unlock %s: %v", lk.path, err)
	}

	return nil

}
