package storage

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"strings"
	"time"

	"github.com/gogo/status"
	"github.com/minio/minio-go/v7"
	log "github.com/sirupsen/logrus"
	clientv3 "go.etcd.io/etcd/client/v3"
	"google.golang.org/grpc/codes"
	"google.golang.org/protobuf/proto"
	"google.golang.org/protobuf/types/known/timestamppb"

	portal "gitlab.com/mergetb/api/portal/v1/go"

	"gitlab.com/mergetb/portal/services/pkg/merror"

	"gitlab.com/mergetb/tech/reconcile"
)

const (
	// Default expiration duration for a realization. Can be overridden
	// by the realization request or a realization update via the Merge API.
	defaultRealizationDuration = "1w" // one week.
)

type RealizeRequest struct {
	*portal.RealizeRequest
}

func NewRealizeRequest(name, experiment, project string) *RealizeRequest {

	return &RealizeRequest{
		RealizeRequest: &portal.RealizeRequest{
			Realization: name,
			Project:     project,
			Experiment:  experiment,
		},
	}

}

// RealizeRequest Storage Object interface implementation =====================

func (r *RealizeRequest) Bucket() string {

	return "realizations"

}

func (r *RealizeRequest) Id() string {

	return fmt.Sprintf("%s/%s/%s", r.Project, r.Experiment, r.Realization)

}

func (r *RealizeRequest) Key() string {

	return fmt.Sprintf("/%s/%s", r.Bucket(), r.Id())

}

func (r *RealizeRequest) Value() interface{} {

	return r.RealizeRequest

}

func (r *RealizeRequest) Substrate() Substrate {

	return Etcd

}

func (r *RealizeRequest) Read() error {

	_, err := etcdTx(readOps(r)...)
	return err

}

// ReadRealizeRequests will read the given realization requests
func ReadRealizeRequests(rs []*RealizeRequest) error {

	var objs []ObjectIO
	for _, r := range rs {
		objs = append(objs, r)
	}

	_, err := etcdTx(readOps(objs...)...)
	return err

}

// GetAllRealizations will read all realization requests in the database
// and return the values.
func GetAllRealizationRequests() ([]*RealizeRequest, error) {

	result := []*RealizeRequest{}

	kvc := clientv3.NewKV(EtcdClient)
	resp, err := kvc.Get(context.TODO(), "/realizations/", clientv3.WithPrefix())
	if err != nil {
		return nil, fmt.Errorf("get realization requests from etcd: %w", err)
	}

	for _, kv := range resp.Kvs {
		rr := new(portal.RealizeRequest)
		err := proto.Unmarshal(kv.Value, rr)
		if err != nil {
			return nil, fmt.Errorf("malformed realize request at %s: %w", string(kv.Key), err)
		}
		result = append(result, &RealizeRequest{
			RealizeRequest: rr,
		})
	}

	return result, nil

}

func (r *RealizeRequest) Create() (*Rollback, error) {

	log.Infof("creating rlz %s.%s.%s", r.Realization, r.Experiment, r.Project)

	if r.Project == "" {
		return nil, merror.BadRequestError("project must be specified")
	}
	if r.Experiment == "" {
		return nil, merror.BadRequestError("experiment must be specified")
	}
	if r.Realization == "" {
		return nil, merror.BadRequestError("realization must have a name")
	}

	//TODO just roll with latest commit if absent?
	if r.Revision == "" {
		return nil, merror.BadRequestError("realization must specify a revision")
	}

	p := NewProject(r.Project)
	err := p.Read()
	if err != nil {
		return nil, fmt.Errorf("project read: %v", err)
	}

	if p.Ver == 0 {
		return nil, fmt.Errorf("project %s does not exist", r.Project)
	}

	e := NewExperiment(r.Experiment, r.Project)
	err = e.Read()
	if err != nil {
		return nil, fmt.Errorf("experiment read: %v", err)
	}

	if e.Ver == 0 {
		return nil, fmt.Errorf("experiment %s does not exist", r.Experiment)
	}

	if e.Models == nil {
		e.Models = make(map[string]*portal.ExperimentModel)
	}

	// sanity checks, but these should have already passed
	model, ok := e.Models[r.Revision]
	if !ok {
		return nil, fmt.Errorf("experiment %s.%s does not contain revision %s",
			r.Experiment, r.Project, r.Revision,
		)
	}

	if model.Compiled == false {
		return nil, fmt.Errorf("experiment %s.%s contains revision %s, but it is not compiled",
			r.Experiment, r.Project, r.Revision,
		)
	}

	found := false
	for _, rlz := range model.Realizations {
		if rlz == r.Realization {
			log.Errorf("BUG: experiment already has reference to new realization %s", rlz)
			found = true
		}
	}

	if !found {
		e.Models[r.Revision].Realizations = append(e.Models[r.Revision].Realizations, r.Realization)
	}

	// set the timestamp on the request.
	ts, err := time.Now().MarshalJSON()
	if err != nil {
		return nil, status.Errorf(codes.Internal, err.Error())
	}
	r.Time = string(ts)

	rb, err := etcdTx(writeOps(r, e)...)
	if err != nil {
		return nil, fmt.Errorf("create realization request write commit: %v", err)
	}

	return rb, nil

}

func (r *RealizeRequest) Delete() (*Rollback, error) {

	log.Infof("deleting rlz %s.%s.%s", r.Realization, r.Experiment, r.Project)

	err := r.Read()
	if err != nil {
		return nil, fmt.Errorf("read realization: %v", err)
	}

	e := NewExperiment(r.Experiment, r.Project)
	err = e.Read()
	if err != nil {
		return nil, fmt.Errorf("read experiment: %v", err)
	}

	// This must be done here and not in DeleteLinkedOps as this effects the parent.
	// When deleting this RealizeRequest, we update the parent experiment. When the
	// Experiment (the parent) is deleted, the models are deleted as part of the experiment deletion.
	if e.Models != nil {
		_, ok := e.Models[r.Revision]
		if ok {
			var lst []string
			for _, rlz := range e.Models[r.Revision].Realizations {
				if rlz == r.Realization {
					continue
				}
				lst = append(lst, rlz)
			}
			e.Models[r.Revision].Realizations = lst
		}
	}

	ops := []StorOp{}

	mreq := NewMaterializeRequest(r.Realization, r.Experiment, r.Project)
	err = mreq.Read()
	if err != nil {
		return nil, fmt.Errorf("mtz req read error: %+v", err)
	}

	if mreq.GetVersion() == 0 { // no mtz
		log.Debug("no associated mtz for this rlz")
	} else {
		log.Debug("found associated mtz. deleting.")
		ops = append(ops, delOps(mreq)...)
	}

	ops = append(ops, delOps(r)...)
	ops = append(ops, writeOps(e)...)
	rb, err := etcdTx(ops...)
	if err != nil {
		return nil, fmt.Errorf("delete realization request commit: %v", err)
	}

	return rb, nil

}

func (r *RealizeRequest) GetVersion() int64 {

	return r.Ver

}

func (r *RealizeRequest) SetVersion(v int64) {

	r.Ver = v

}

func (r *RealizeRequest) ToInProgressRealizationResult() (*portal.RealizationResult, error) {
	// the time string is quoted, which needs to be cleaned up for parsing
	time_string := strings.Trim(r.GetTime(), "\"")

	created, err := time.Parse(time.RFC3339, time_string)
	if err != nil {
		return nil, err
	}

	expires, err := ParseReservationDuration(created, r.GetDuration())
	if err != nil {
		return nil, err
	}

	return &portal.RealizationResult{
		Realization: &portal.Realization{
			Complete: false,
			Id:       r.Realization,
			Eid:      r.Experiment,
			Pid:      r.Project,
			Creator:  r.Creator,
			Created:  timestamppb.New(created),
			Expires:  expires,
			Xhash:    r.Revision,
			Infranet: &portal.InfranetEmbedding{
				InfrapodServer: "",
			},
		},
		Diagnostics: &portal.DiagnosticList{
			Value: []*portal.Diagnostic{{
				Level:   portal.DiagnosticLevel_Warning,
				Message: "in progress...",
			}},
		},
	}, nil
}

func ToRealizationSummary(r *portal.Realization) *portal.RealizationSummary {
	if r == nil {
		return nil
	}

	return &portal.RealizationSummary{
		Complete: r.Complete,
		Id:       r.Id,
		Pid:      r.Pid,
		Eid:      r.Eid,
		Xhash:    r.Xhash,
		Creator:  r.Creator,
		Ver:      r.Ver,
		Created:  r.Created,
		Expires:  r.Expires,
		NumNodes: int32(len(r.Nodes)),
		NumLinks: int32(len(r.Links)),
	}
}

func ReadRealizationResult(project, experiment, realization string) (*portal.RealizationResult, error) {

	bucket := fmt.Sprintf("realize-%s-%s-%s", project, experiment, realization)

	// realization
	obj, err := MinIOClient.GetObject(
		context.TODO(),
		bucket,
		"realization",
		minio.GetObjectOptions{},
	)
	if err != nil {

		return nil, err
	}
	defer obj.Close()

	buf, err := io.ReadAll(obj)
	if err != nil {
		// MinIO does not have structured errors. Just do our best until they do.
		if strings.Contains(err.Error(), "does not exist") {
			// if it doesn't exist, but the request does, it's in progress

			// check to see if the realization request exists, and if so, it's in progress
			rq := NewRealizeRequest(realization, experiment, project)
			err := rq.Read()
			if err != nil {
				return nil, err
			}

			// not found at all, so return that
			if rq.GetVersion() == 0 {
				return nil, merror.NotFoundError("realization", fmt.Sprintf("%s.%s.%s", realization, experiment, project))
			}

			// err == nil, so the request was found, so it's in progress
			return nil, merror.RealizeInProgressError(fmt.Sprintf("%s.%s.%s", realization, experiment, project))
		}

		return nil, err
	}

	rz := new(portal.Realization)
	err = proto.Unmarshal(buf, rz)
	if err != nil {
		return nil, err
	}

	// diagnostics
	obj, err = MinIOClient.GetObject(
		context.TODO(),
		bucket,
		"diagnostics",
		minio.GetObjectOptions{},
	)
	if err != nil {
		return nil, fmt.Errorf("minio get diagnostics: %v", err)
	}
	defer obj.Close()

	buf, err = io.ReadAll(obj)
	if err != nil {
		return nil, fmt.Errorf("minio diagnostics read: %v", err)
	}

	dl := new(portal.DiagnosticList)
	err = proto.Unmarshal(buf, dl)
	if err != nil {
		return nil, fmt.Errorf("diagnostics unmarshal: %v", err)
	}

	// unfortunately we did not set the complete field prior to this
	// so, we'll retroactively set it
	if !rz.Complete && len(rz.Nodes) > 0 && !dl.Error() {
		rz.Complete = true
	}

	return &portal.RealizationResult{
		Realization: rz,
		Diagnostics: dl,
	}, nil
}

func WriteRealizationResult(r *portal.RealizationResult) error {

	rlz := r.Realization

	rbuf, err := proto.Marshal(rlz)
	if err != nil {
		return fmt.Errorf("marshal realization: %v", err)
	}

	dbuf, err := proto.Marshal(r.Diagnostics)
	if err != nil {
		return fmt.Errorf("marshal diags: %v", err)
	}

	bucket := fmt.Sprintf("realize-%s-%s-%s", rlz.Pid, rlz.Eid, rlz.Id)

	found, err := MinIOClient.BucketExists(context.TODO(), bucket)
	if err != nil {
		return fmt.Errorf("minio check bucket: %v", err)
	}
	if !found {
		err := MinIOClient.MakeBucket(
			context.TODO(), bucket, minio.MakeBucketOptions{})
		if err != nil {
			return fmt.Errorf("minio make bucket: %v", err)
		}
	}

	_, err = MinIOClient.PutObject(
		context.TODO(),
		bucket,
		"diagnostics",
		bytes.NewReader(dbuf),
		int64(len(dbuf)),
		minio.PutObjectOptions{},
	)
	if err != nil {
		return fmt.Errorf("minio diagnostics put: %v", err)
	}

	_, err = MinIOClient.PutObject(
		context.TODO(),
		bucket,
		"realization",
		bytes.NewReader(rbuf),
		int64(len(rbuf)),
		minio.PutObjectOptions{},
	)
	if err != nil {
		return fmt.Errorf("minio realization put: %v", err)
	}

	return nil
}

func (r *RealizeRequest) DeleteLinkedOps() ([]StorOp, error) {

	rid := fmt.Sprintf("%s.%s.%s", r.Realization, r.Experiment, r.Project)
	log.Infof("gathering delete linked ops for rlz %s", rid)

	ops := []StorOp{}

	mreq := NewMaterializeRequest(r.Realization, r.Experiment, r.Project)
	err := mreq.Read()
	if err != nil {
		return nil, fmt.Errorf("mtz req read error: %+v", err)
	}

	if mreq.GetVersion() == 0 { // no mtz
		log.Debug("no associated mtz for this rlz")
	} else {
		log.Infof("adding del ops for mtz %s", rid)
		ops = append(ops, delOps(mreq)...)

		delMtzOps, err := mreq.DeleteLinkedOps()
		if err != nil {
			return nil, fmt.Errorf("get linked mtz opts: %+v", err)
		}

		ops = append(ops, delMtzOps...)

		// We handle the minio mtz data async'ly. This is not great. The minio ObjectIO interface
		// should be made to handle this correctly via minioTx(). Alas. TODO.
		// materialization reconciler ends up calling this to remove the data.
		// DeleteMaterialization(r.Project, r.Experiment, r.Realization)
	}

	return ops, nil
}

func (r *RealizeRequest) GetGoal(timeout time.Duration) (*reconcile.TaskForest, error) {
	name := fmt.Sprintf("%s.%s.%s", r.Realization, r.Experiment, r.Project)

	tg := reconcile.NewGoal(r.Key(), "Realization: "+name, "Manage Realization: "+name)

	tg.AddTaskRecord(FindTaskRecords(r, reconcile.TaskRecord_Exists)...)

	return tg.WatchTaskForest(EtcdClient, timeout)
}
