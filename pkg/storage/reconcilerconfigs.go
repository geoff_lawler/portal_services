// I'd rather have this somewhere other than storage,
// but storage needs this to compute TaskRecords,
// and this needs storage to get the buckets of storage objects,
// so it naturally falls here

package storage

import (
	"fmt"
	"reflect"
	"strings"
	"time"

	"github.com/sirupsen/logrus"

	"gitlab.com/mergetb/tech/reconcile"

	"gitlab.com/mergetb/portal/services/internal"
)

type ReconcilerManagerConfig struct {
	Manager     string
	Reconcilers []ReconcilerConfig

	LogLevel              reconcile.TaskMessage_Type
	Workers               int
	JustLogErrors         bool
	HealthFrequency       time.Duration
	DisableMessageLogging bool
}

type ReconcilerConfig struct {
	Prefix string
	Name   string
	Desc   string

	EnsureFrequency          time.Duration
	PollCheckFrequency       time.Duration
	PeriodicMessageFrequency time.Duration

	// These are options for how the reconciler should be interpreted
	// when finding task records against all reconcilers
	TaskRecordExistenceOverride bool
	TaskRecordExistence         reconcile.TaskRecord_ExistenceTypes

	// ObjectTypes specifies what ObjectIO types the reconciler reads
	// ObjectTypes is used when finding task records for an object:
	//   - the requested object must be of the same type of something else in this list
	//     for it to be considered a valid task record
	ObjectTypes []ObjectIO
}

func (rc *ReconcilerManagerConfig) ToReconcilerManager(actions ...reconcile.Actions) *reconcile.ReconcilerManager {
	if len(actions) != len(rc.Reconcilers) {
		panic(fmt.Errorf("unexpected actions length: %d vs. %d", len(rc.Reconcilers), len(actions)))
	}

	rs := make([]*reconcile.Reconciler, len(rc.Reconcilers))
	for i, r := range rc.Reconcilers {
		rs[i] = &reconcile.Reconciler{
			Actions: actions[i],

			Name:   r.Name,
			Desc:   r.Desc,
			Prefix: r.Prefix,

			EnsureFrequency:          r.EnsureFrequency,
			PollCheckFrequency:       r.PollCheckFrequency,
			PeriodicMessageFrequency: r.PollCheckFrequency,
		}
	}

	rm := &reconcile.ReconcilerManager{
		Manager:     rc.Manager,
		Reconcilers: rs,

		EtcdCli:                    EtcdClient,
		ServerHeartbeatGracePeriod: HeartbeatGracePeriodFromConfig(),

		LogLevel:              rc.LogLevel,
		Workers:               rc.Workers,
		JustLogErrors:         rc.JustLogErrors,
		HealthFrequency:       rc.HealthFrequency,
		DisableMessageLogging: rc.DisableMessageLogging,
	}

	return rm
}

func (rc *ReconcilerManagerConfig) Run(actions ...reconcile.Actions) error {
	rm := rc.ToReconcilerManager(actions...)

	logrus.Infof("running %s reconciler", rm.Manager)

	return rm.Run()
}

func FindTaskRecords(o ObjectIO, existence reconcile.TaskRecord_ExistenceTypes) (trs []*reconcile.TaskRecord) {
	key := o.Key()

	for _, rc := range AllReconcilerManagerConfigs {
		for _, r := range rc.Reconcilers {
			okType := false
			for _, t := range r.ObjectTypes {
				if reflect.TypeOf(o) == reflect.TypeOf(t) {
					okType = true
					break
				}
			}

			if okType && strings.HasPrefix(key, r.Prefix) {
				ext := existence

				if r.TaskRecordExistenceOverride {
					ext = r.TaskRecordExistence
				}

				trs = append(trs, &reconcile.TaskRecord{
					Task:              key,
					ReconcilerManager: rc.Manager,
					ReconcilerName:    r.Name,
					Existence:         ext,
				})
			}
		}
	}

	return
}

var (
	ReconcilerConfigHarbor = ReconcilerManagerConfig{
		Manager: "commission",
		Reconcilers: []ReconcilerConfig{{
			ObjectTypes: []ObjectIO{&InitHarborRequest{}},

			Prefix: PrefixedBucket(&InitHarborRequest{}),
			Name:   "harbors",
			Desc:   "Manage facility harbors",
		}},
	}

	ReconcilerConfigCredentials = ReconcilerManagerConfig{
		Manager: "credmgr",
		Reconcilers: []ReconcilerConfig{{
			ObjectTypes: []ObjectIO{&User{}},

			Prefix: PrefixedBucket(&User{}),
			Name:   "UserKeys",
			Desc:   "user key management",
		}, {
			ObjectTypes: []ObjectIO{&UserStatus{}},

			Prefix: PrefixedBucket(&UserStatus{}),
			Name:   "UserCerts",
			Desc:   "user cert management",
		}, {
			ObjectTypes: []ObjectIO{&XDC{}},

			Prefix: PrefixedBucket(&XDC{}),
			Name:   "XDCCreds",
			Desc:   "Host credentials management",
		}},
	}

	ReconcilerConfigGit = ReconcilerManagerConfig{
		Manager: "git",
		Reconcilers: []ReconcilerConfig{{
			ObjectTypes: []ObjectIO{&Experiment{}},

			Prefix: PrefixedBucket(&Experiment{}),
			Name:   "git",
			Desc:   "Process experiment repositories",
		}},
	}

	ReconcilerConfigMaterialize = ReconcilerManagerConfig{
		Manager: "mtz",
		Reconcilers: []ReconcilerConfig{{
			ObjectTypes: []ObjectIO{&MaterializeRequest{}},

			Prefix: PrefixedBucket(&MaterializeRequest{}),
			Name:   "mtzs",
			Desc:   "Manage materializations",
		}},
	}

	// EnsureFrequency is later overriten
	ReconcilerConfigMergeFS = ReconcilerManagerConfig{
		Manager: "mergefs",
		Reconcilers: []ReconcilerConfig{{
			ObjectTypes: []ObjectIO{&User{}},

			Prefix:          PrefixedBucket(&User{}),
			Name:            "User",
			Desc:            "MergeFS User Management",
			EnsureFrequency: 60 * time.Second,
		}, {
			ObjectTypes: []ObjectIO{&Project{}},

			Prefix:          PrefixedBucket(&Project{}),
			Name:            "Project",
			Desc:            "MergeFS Project Management",
			EnsureFrequency: 60 * time.Second,
		}, {
			ObjectTypes: []ObjectIO{&Organization{}},

			Prefix:          PrefixedBucket(&Organization{}),
			Name:            "Organization",
			Desc:            "MergeFS Organization Management",
			EnsureFrequency: 60 * time.Second,
		}, {
			ObjectTypes: []ObjectIO{&SSHKeyPair{}, &SSHCert{}},

			Prefix:          "/auth/ssh/", // hardcoded, as this is the union of SSHKeys and Certs
			Name:            "Creds",
			Desc:            "MergeFS Credentials Management",
			EnsureFrequency: 60 * time.Second,
		}, {
			ObjectTypes: []ObjectIO{&XDC{}},

			Prefix:          PrefixedBucket(&XDC{}),
			Name:            "XcdFS",
			Desc:            "MergeFS XDC Management",
			EnsureFrequency: 60 * time.Second,
		}, {
			ObjectTypes: []ObjectIO{&PublicKey{}},

			Prefix:          PrefixedBucket(&PublicKey{}),
			Name:            "PubkeyFS",
			Desc:            "Manage public user keys in $HOME",
			EnsureFrequency: 60 * time.Second,
		}},
	}

	ReconcilerConfigModel = ReconcilerManagerConfig{
		Manager: "model",
		Reconcilers: []ReconcilerConfig{{
			ObjectTypes: []ObjectIO{}, // doesn't operate on any ObjectIO's

			Prefix: PrefixedBucket(&Experiment{}),
			Name:   "model",
			Desc:   "Compile experiments",
		}},
	}

	ReconcilerConfigRealize = ReconcilerManagerConfig{
		Manager: "realize",
		Reconcilers: []ReconcilerConfig{{
			ObjectTypes: []ObjectIO{&RealizeRequest{}},

			Prefix: PrefixedBucket(&RealizeRequest{}),
			Name:   "rlzs",
			Desc:   "Manage realizations",
		}},
	}

	ReconcilerConfigStats = ReconcilerManagerConfig{
		Manager: "stats",

		Reconcilers: []ReconcilerConfig{{
			ObjectTypes: []ObjectIO{&MaterializeRequest{}},

			Name:   "mzreq",
			Prefix: PrefixedBucket(&MaterializeRequest{}),
			Desc:   "watches for materialization requests",

			TaskRecordExistenceOverride: true,
			TaskRecordExistence:         reconcile.TaskRecord_Ignore,
		},
		},
	}

	ReconcilerConfigWireguard = ReconcilerManagerConfig{
		Manager: "wgsvc",
		Reconcilers: []ReconcilerConfig{{
			ObjectTypes: []ObjectIO{&XdcWgClient{}},

			Prefix: PrefixedBucket(&XdcWgClient{}),
			Name:   "XdcWgClient",
			Desc:   "Manage wireguard xdc connections",
		}, {
			ObjectTypes: []ObjectIO{&WgIfRequest{}},

			Prefix: PrefixedBucket(&WgIfRequest{}),
			Name:   "WgIf",
			Desc:   "Manage wireguard interfaces",
		}, {
			ObjectTypes: []ObjectIO{&MaterializeRequest{}},

			Prefix: PrefixedBucket(&MaterializeRequest{}),
			Name:   "WgEnclave",
			Desc:   "Manage wg enclave data",
		}},
	}

	ReconcilerConfigWireguardPodwatch = ReconcilerManagerConfig{
		Manager: "wgsvc",
		Workers: 4,
		Reconcilers: []ReconcilerConfig{{
			ObjectTypes: []ObjectIO{&Pod{}},

			Prefix: PrefixedBucket(&Pod{Path: "wgsvc"}),
			Name:   "WgXDCAttach",
			Desc:   "Manage wg xdc attachment",
		}},
	}

	ReconcilerConfigXDCPutUsers = ReconcilerManagerConfig{
		Manager: "xdc",
		Reconcilers: []ReconcilerConfig{{
			ObjectTypes: []ObjectIO{&User{}},

			Prefix: PrefixedBucket(&User{}), // we want to add users to all jump boxes.
			Name:   "User",
			Desc:   "Manage User access to XDCs",
		}},
	}

	ReconcilerConfigXDCPutPods = ReconcilerManagerConfig{
		Manager: "xdc",
		Workers: 8,
		Reconcilers: []ReconcilerConfig{{
			ObjectTypes: []ObjectIO{&XDC{}},

			Prefix: PrefixedBucket(&XDC{}),
			Name:   "Xdcs",
			Desc:   "Manage XDCs",
		}},
	}

	ReconcilerConfigXDCPodInit = ReconcilerManagerConfig{
		Manager: "xdc",
		Workers: 16,
		Reconcilers: []ReconcilerConfig{{
			ObjectTypes: []ObjectIO{&Pod{}},

			Prefix: PrefixedBucket(&Pod{Path: "podinit"}),
			Name:   "podinit",
			Desc:   "Initialize XDCs",
		}},
	}
)

var AllReconcilerManagerConfigs = []ReconcilerManagerConfig{
	ReconcilerConfigHarbor,
	ReconcilerConfigCredentials,
	ReconcilerConfigGit,
	ReconcilerConfigMaterialize,
	ReconcilerConfigMergeFS,
	ReconcilerConfigModel,
	ReconcilerConfigRealize,
	ReconcilerConfigStats,
	ReconcilerConfigWireguard,
	ReconcilerConfigWireguardPodwatch,
	ReconcilerConfigXDCPutUsers,
	ReconcilerConfigXDCPutPods,
	ReconcilerConfigXDCPodInit,
}

// Return the heartbeat interval from portal config
// Returns 0 if no config present
func HeartbeatIntervalFromConfig() uint64 {
	config, err := GetPortalConfig()
	if err != nil {
		logrus.Errorf("failed to read portal config: %+v", err)
		return 0
	}

	return uint64(config.Config.Reconcile.HeartbeatIntervalSec)
}

// Return the heartbeat grace period from portal config
// Returns -1 if heartbeat monitoring is disabled
// Returns 0 if no config present
func HeartbeatGracePeriodFromConfig() time.Duration {
	// disable by request
	if !internal.MonitorEtcdServer {
		return -1
	}

	config, err := GetPortalConfig()
	if err != nil {
		logrus.Errorf("failed to read portal config: %+v", err)
		return 0
	}

	return time.Duration(config.Config.Reconcile.HeartbeatGracePeriodSec) * time.Second
}
