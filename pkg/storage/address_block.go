package storage

import (
	"fmt"

	"gitlab.com/mergetb/api/portal/v1/go"
)

// BlockPool ==================================================================

// The max size of this data structure should be 1024 address blocks times 15
// characters max per address ~15KB, so should be safe for etcd.
type BlockPool struct {
	*portal.BlockPool
	ver int64
}

func NewBlockPool() *BlockPool {
	return &BlockPool{
		BlockPool: &portal.BlockPool{},
	}
}

// ObjectIO implementation ----------------------------------------------------

func (x *BlockPool) Bucket() string {
	return ""
}

func (x *BlockPool) Id() string {
	return ""
}

func (x *BlockPool) Key() string {

	return fmt.Sprintf("/blockpool")

}

func (x *BlockPool) Value() interface{} {
	return x.BlockPool
}

func (x *BlockPool) Substrate() Substrate {
	return Etcd
}

func (x *BlockPool) GetVersion() int64 {
	return x.ver
}

func (x *BlockPool) SetVersion(v int64) {
	x.ver = v
}

// Storage API functions ------------------------------------------------------

func FetchBlockPool() (*BlockPool, error) {

	bp := NewBlockPool()
	return bp, ReadBlockPool(bp)

}

func ReadBlockPool(b *BlockPool) error {

	_, err := etcdTx(readOps(b)...)
	return err

}

func WriteBlockPool(b *BlockPool) (*Rollback, error) {

	return etcdTx(writeOps(b)...)

}

// BlockPoolAllocations =======================================================

type BlockPoolAllocations struct {
	*portal.BlockPoolAllocations
	ver int64
}

func NewBlockPoolAllocations(id string) *BlockPoolAllocations {
	return &BlockPoolAllocations{
		BlockPoolAllocations: &portal.BlockPoolAllocations{Id: id},
	}
}

// ObjectIO implementation ----------------------------------------------------

func (x *BlockPoolAllocations) Bucket() string {
	return ""
}

func (x *BlockPoolAllocations) Id() string {
	return x.BlockPoolAllocations.Id
}

func (x *BlockPoolAllocations) Key() string {

	return fmt.Sprintf("/bpalloc/%s/", x.Id())

}

func (x *BlockPoolAllocations) Value() interface{} {
	return x.BlockPoolAllocations
}

func (x *BlockPoolAllocations) Substrate() Substrate {
	return Etcd
}

func (x *BlockPoolAllocations) GetVersion() int64 {
	return x.ver
}

func (x *BlockPoolAllocations) SetVersion(v int64) {
	x.ver = v
}

// Storage API functions ------------------------------------------------------

func FetchBlockPoolAllocations(id string) (*BlockPoolAllocations, error) {

	bp := NewBlockPoolAllocations(id)
	return bp, ReadBlockPoolAllocations([]*BlockPoolAllocations{bp})

}

func ReadBlockPoolAllocations(b []*BlockPoolAllocations) error {

	var objs []ObjectIO
	for _, x := range b {
		objs = append(objs, x)
	}

	_, err := etcdTx(readOps(objs...)...)
	return err

}

func WriteBlockPoolAllocations(b []*BlockPoolAllocations) (*Rollback, error) {

	var objs []ObjectIO
	for _, x := range b {
		objs = append(objs, x)
	}

	return etcdTx(writeOps(objs...)...)

}
