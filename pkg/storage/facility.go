package storage

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"time"

	"github.com/minio/minio-go/v7"
	log "github.com/sirupsen/logrus"
	clientv3 "go.etcd.io/etcd/client/v3"
	"google.golang.org/protobuf/proto"

	portal "gitlab.com/mergetb/api/portal/v1/go"
	xir "gitlab.com/mergetb/xir/v0.3/go"

	"gitlab.com/mergetb/tech/reconcile"
)

type Facility struct {
	*portal.Facility
	UpdateRequest *portal.UpdateFacilityRequest
}

func NewFacility(name string) *Facility {

	return &Facility{
		Facility: &portal.Facility{
			Name: name,
		},
	}

}

// Facility Storage Object interface implementation ============================

func (f *Facility) Bucket() string {

	return "facilities"

}

func (f *Facility) Id() string {

	return f.Name

}

func (f *Facility) Key() string {

	return fmt.Sprintf("/%s/%s/", f.Bucket(), f.Id())

}

func (f *Facility) Value() interface{} {

	return f.Facility

}

func (f *Facility) Substrate() Substrate {

	return Etcd

}

func (f *Facility) Read() error {

	_, err := etcdTx(readOps(f)...)
	return err

}

func ListFacilities() ([]*Facility, error) {

	var result []*Facility

	kvc := clientv3.NewKV(EtcdClient)
	resp, err := kvc.Get(context.TODO(), "/facilities", clientv3.WithPrefix())
	if err != nil {
		return nil, fmt.Errorf("get facilities: %v", err)
	}

	for _, kv := range resp.Kvs {

		f := &Facility{Facility: new(portal.Facility)}
		err = proto.Unmarshal(kv.Value, f.Facility)
		if err != nil {
			return nil, fmt.Errorf("malformed facility at %s: %v", err, string(kv.Key))
		}
		result = append(result, f)

	}

	return result, nil

}

func ReadFacilities(fs []*Facility) error {

	var objs []ObjectIO
	for _, p := range fs {
		objs = append(objs, p)
	}

	_, err := etcdTx(readOps(objs...)...)
	return err

}

func (f *Facility) Create() (*Rollback, error) {

	if f.Name == "" {
		return nil, fmt.Errorf("Facility name must be specified")
	}

	if f.Address == "" {
		return nil, fmt.Errorf("Facility address must be specified")
	}

	// TODO ensure creator
	if len(f.Members) == 0 {
		return nil, fmt.Errorf("Facility must have at least one member")
	}

	// read the user objects corresponding to the facility members

	var users []ObjectIO
	for user := range f.Members {
		users = append(users, NewUser(user))
	}
	_, err := etcdTx(readOps(users...)...)
	if err != nil {
		return nil, fmt.Errorf("read facility users: %v", err)
	}

	// ensure each user exists, and if they do add this facility to their
	// facility list

	for _, _user := range users {

		user := _user.Value().(*portal.User)
		if user.Facilities == nil {
			user.Facilities = make(map[string]*portal.Member)
		}
		if user.Ver == 0 {
			return nil, fmt.Errorf("user %s does not exist", user.Username)
		}
		found := user.Facilities[f.Id()]
		if found != nil {
			log.Warnf(
				"BUG: user %s already has reference to new facility %s",
				user.Username,
				f.Name,
			)
		}
		user.Facilities[f.Id()] = f.Members[user.Username]

	}

	ops := append(writeOps(f), writeOps(users...)...)
	rb, err := etcdTx(ops...)
	if err != nil {
		return nil, fmt.Errorf("create facility write commit: %v", err)
	}

	return rb, nil

}

func (f *Facility) Update() (*Rollback, error) {

	if f.UpdateRequest == nil {
		return nil, nil
	}
	u := f.UpdateRequest

	err := f.Read()
	if err != nil {
		return nil, err
	}

	if u.Address != "" {
		f.Address = u.Address
	}

	if u.Description != nil {
		f.Description = u.Description.Value
	}

	if u.AccessMode != nil {
		f.AccessMode = u.AccessMode.Value
	}

	if u.Members != nil {
		for _, member := range u.Members.Remove {
			delete(f.Members, member)
		}
		for user, membership := range u.Members.Set {
			f.Members[user] = membership
		}
	}

	if u.Certificate != "" {
		f.Certificate = u.Certificate
	}

	if u.Cacertificate != "" {
		f.Certificate = u.Cacertificate
	}

	rb, err := etcdTx(writeOps(f)...)
	if err != nil {
		return nil, fmt.Errorf("facility update tx: %v", err)
	}

	return rb, nil

}

func (f *Facility) Delete() (*Rollback, error) {

	err := f.Read()
	if err != nil {
		return nil, err
	}

	var users []ObjectIO

	for user := range f.Members {
		users = append(users, NewUser(user))
	}
	_, err = etcdTx(readOps(users...)...)
	if err != nil {
		return nil, fmt.Errorf("read facility users: %v", err)
	}

	for _, user := range users {
		delete(user.Value().(*portal.User).Facilities, f.Name)
	}

	ops := append(writeOps(users...), delOps(f)...)
	rb, err := etcdTx(ops...)
	if err != nil {
		return nil, fmt.Errorf("facility delete tx: %v", err)
	}

	return rb, nil

}

func (f *Facility) GetVersion() int64 {

	return f.Ver

}

func (f *Facility) SetVersion(v int64) {

	f.Ver = v

}

type InitHarborRequest struct {
	*portal.InitHarborRequest
}

func NewInitHarborRequest(facility string) *InitHarborRequest {

	return &InitHarborRequest{
		InitHarborRequest: &portal.InitHarborRequest{
			Facility: facility,
		},
	}

}

// InitHarborRequest Storage Object interface implementation =====================

func (i *InitHarborRequest) Bucket() string {
	return "harbor"
}

func (i *InitHarborRequest) Id() string {
	return i.Facility
}

func (i *InitHarborRequest) Key() string {

	return fmt.Sprintf("/%s/%s", i.Bucket(), i.Id())
}

func (i *InitHarborRequest) Value() interface{} {

	return i.InitHarborRequest

}

func (i *InitHarborRequest) Substrate() Substrate {

	return Etcd

}

func (i *InitHarborRequest) GetVersion() int64 {

	return i.Ver

}

func (i *InitHarborRequest) SetVersion(v int64) {

	i.Ver = v

}

func (r *InitHarborRequest) Create() (*Rollback, error) {

	if r.Facility == "" {
		return nil, fmt.Errorf("facility must be specified")
	}

	return etcdTx(writeOps(r)...)

}

func (r *InitHarborRequest) Delete() (*Rollback, error) {

	if r.Facility == "" {
		return nil, fmt.Errorf("facility must be specified")
	}

	return etcdTx(delOps(r)...)

}

func ReadFacilityModel(name string) (*xir.Facility, error) {

	bucket := fmt.Sprintf("facility-%s", name)
	obj, err := MinIOClient.GetObject(
		context.TODO(),
		bucket,
		"model",
		minio.GetObjectOptions{},
	)
	if err != nil {
		return nil, fmt.Errorf("failed to get model: %v", err)
	}
	defer obj.Close()

	buf, err := io.ReadAll(obj)
	if err != nil {
		return nil, fmt.Errorf("failed to read model: %v", err)
	}

	model, err := xir.FacilityFromB64String(string(buf))
	if err != nil {
		return nil, fmt.Errorf("failed to parse model: %v", err)
	}

	return model, nil

}

// Model CRUD
func CreateFacilityModel(name string, m *xir.Facility) error {

	// each facility gets its own bucket, right now the model is the only
	// thing in that bucket under facility-<name>/model, in the future there
	// may be more
	bucket := fmt.Sprintf("facility-%s", name)

	found, err := MinIOClient.BucketExists(context.TODO(), bucket)
	if err != nil {
		log.Errorf("minio: bucket exists check: %s %v", bucket, err)
		return fmt.Errorf("model save error")
	}

	if !found {
		err := MinIOClient.MakeBucket(context.TODO(), bucket, minio.MakeBucketOptions{})
		if err != nil {
			log.Errorf("minio: make bucket: %s %v", bucket, err)
			return fmt.Errorf("model save error")
		}
	} else {

		return fmt.Errorf("facility has already been registered")
	}

	buf, err := m.ToB64Buf()
	if err != nil {
		return fmt.Errorf("serialize model error: %v", err)
	}

	_, err = MinIOClient.PutObject(
		context.TODO(),
		bucket,
		"model",
		bytes.NewReader(buf),
		int64(len(buf)),
		minio.PutObjectOptions{},
	)
	if err != nil {
		log.Errorf("minio: put facility model: %s %v", bucket, err)
		return fmt.Errorf("model save error")
	}

	return nil
}

func UpdateFacilityModel(rq *portal.UpdateFacilityRequest) error {

	bucket := fmt.Sprintf("facility-%s", rq.Name)

	found, err := MinIOClient.BucketExists(context.TODO(), bucket)
	if err != nil {
		log.Errorf("minio: bucket exists check: %s %v", bucket, err)
		return fmt.Errorf("model update error")
	}

	if !found {
		return fmt.Errorf("facility %s model does not exist", rq.Name)
	}

	obj, err := MinIOClient.GetObject(
		context.TODO(),
		bucket,
		"model",
		minio.GetObjectOptions{},
	)
	if err != nil {
		return fmt.Errorf("error reading facility model")
	}
	defer obj.Close()

	buf, err := io.ReadAll(obj)
	if err != nil {
		log.Errorf("failed to read facility model for %s: %v", bucket, err)
		return fmt.Errorf("Facility model read failed")
	}

	model, err := xir.FacilityFromB64String(string(buf))
	if err != nil {
		log.Errorf("failed to parse facility model for %s: %v", bucket, err)
		return fmt.Errorf("Facility parse failed")
	}

	err = updateModel(model, rq.Model, rq.ModelPatchStrategy.Strategy)
	if err != nil {
		log.Errorf("failed to update facility model for %s: %v", bucket, err)
		return fmt.Errorf("Facility model update failed: %s", err.Error())
	}

	// write back the model.
	buf, err = model.ToB64Buf()
	if err != nil {
		return fmt.Errorf("serialize model error: %v", err)
	}

	_, err = MinIOClient.PutObject(
		context.TODO(),
		bucket,
		"model",
		bytes.NewReader(buf),
		int64(len(buf)),
		minio.PutObjectOptions{},
	)
	if err != nil {
		log.Errorf("minio: put facility model: %s %v", bucket, err)
		return fmt.Errorf("model save error")
	}

	return nil
}

func updateModel(to, frm *xir.Facility, strat portal.PatchStrategy_Strategy) error {

	// We only support update of node roles and node alloc modes at the moment.
	for _, rf := range frm.Resources {

		resourceFound := false
		for _, rt := range to.Resources {

			if rf.Id == rt.Id { // update this resource.

				resourceFound = true

				// This cries out for go generics. Need to wait until go v1.18 for that though.
				if len(rf.Roles) > 0 {
					switch strat {
					case portal.PatchStrategy_replace:
						rt.Roles = rf.Roles
					case portal.PatchStrategy_remove:
						rt.Roles = []xir.Role{}
					case portal.PatchStrategy_expand:
						// If the "from" role does not exist in "to" list, add it to the "to" list.
						for _, rolef := range rf.Roles {
							if roleIndex(rolef, rt.Roles) == -1 {
								rt.Roles = append(rt.Roles, rolef)
							}
						}
					case portal.PatchStrategy_subtract:
						// If the role in "from" list exists in "to" list delete it from the "to" list.
						for _, rolef := range rf.Roles {
							if i := roleIndex(rolef, rt.Roles); i >= 0 {
								rt.Roles = append(rt.Roles[:i], rt.Roles[i+1:]...)
							}
						}
					}
				}

				if len(rf.Alloc) > 0 {

					switch strat {
					case portal.PatchStrategy_replace:
						rt.Alloc = rf.Alloc
					case portal.PatchStrategy_remove:
						rt.Alloc = []xir.AllocMode{}
					case portal.PatchStrategy_expand:
						// If the "from" mode does not exist in "to" list, add it to the "to" list.
						for _, allocf := range rf.Alloc {
							if modeIndex(allocf, rt.Alloc) == -1 {
								rt.Alloc = append(rt.Alloc, allocf)
							}
						}
					case portal.PatchStrategy_subtract:
						// If the mode in "from" list exists in "to" list delete it from the "to" list.
						for _, allocf := range rf.Alloc {
							if i := modeIndex(allocf, rt.Alloc); i >= 0 {
								rt.Alloc = append(rt.Alloc[:i], rt.Alloc[i+1:]...)
							}
						}
					}
				}
			}
		}

		// we don't yet support add/rm resources themselves, so not finding the resource
		// is an error.
		if resourceFound == false {
			return fmt.Errorf("Resource not found: %s", rf.Id)
		}
	}

	return nil
}

func roleIndex(r xir.Role, roles []xir.Role) int {
	for i, role := range roles {
		if role == r {
			return i
		}
	}
	return -1
}

func modeIndex(m xir.AllocMode, modes []xir.AllocMode) int {
	for i, mode := range modes {
		if mode == m {
			return i
		}
	}
	return -1
}

// Delete model is still in service/apiserver/commission.go

// Status

func (i *InitHarborRequest) GetGoal(timeout time.Duration) (*reconcile.TaskForest, error) {
	tg := reconcile.NewGoal(i.Key(), i.Id(), "Manage harbor: "+i.Facility)
	tg.AddTaskRecord(FindTaskRecords(i, reconcile.TaskRecord_Exists)...)

	return tg.WatchTaskForest(EtcdClient, timeout)
}
