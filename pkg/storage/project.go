package storage

import (
	"context"
	"fmt"
	"time"

	log "github.com/sirupsen/logrus"
	clientv3 "go.etcd.io/etcd/client/v3"
	"google.golang.org/protobuf/proto"

	portal "gitlab.com/mergetb/api/portal/v1/go"

	"gitlab.com/mergetb/tech/reconcile"
)

type Project struct {
	*portal.Project
	UpdateRequest *portal.UpdateProjectRequest
}

func NewProject(name string) *Project {

	return &Project{
		Project: &portal.Project{
			Name: name,
		},
	}

}

//
// GTL TODO: break out members into a watchable etcd key so member
// actions can be watched separately.
//

// Storage Object interface implementation ====================================

func (p *Project) Bucket() string {

	return "projects"

}

func (p *Project) Id() string {

	return p.Name

}

func (p *Project) Key() string {

	return fmt.Sprintf("/%s/%s", p.Bucket(), p.Id())

}

func (p *Project) Value() interface{} {

	return p.Project

}

func (p *Project) Substrate() Substrate {

	return Etcd

}

func (p *Project) Read() error {

	_, err := etcdTx(readOps(p)...)
	return err

}

// Return a list of all projects.
func ListProjects() ([]*Project, error) {

	var result []*Project

	kvc := clientv3.NewKV(EtcdClient)
	resp, err := kvc.Get(context.TODO(), "/projects", clientv3.WithPrefix())
	if err != nil {
		return nil, fmt.Errorf("get projects: %w", err)
	}

	for _, kv := range resp.Kvs {

		p := &Project{Project: new(portal.Project)}
		err = proto.Unmarshal(kv.Value, p.Project)
		if err != nil {
			return nil, fmt.Errorf("malformed project at %s: %w", string(kv.Key), err)
		}
		result = append(result, p)
	}

	return result, nil
}

// Read the project data for the given projects.
func ReadProjects(ps []*Project) error {

	var objs []ObjectIO
	for _, p := range ps {
		objs = append(objs, p)
	}

	_, err := etcdTx(readOps(objs...)...)
	return err

}

func (p *Project) Create() (*Rollback, error) {

	gidmtx.Lock()
	defer gidmtx.Unlock()

	lks, err := locks(gidLock)
	if err != nil {
		return nil, fmt.Errorf("gid lock: %v", err)
	}
	defer lks.Unlock()

	// fetch group counter object from storage

	gid := gidCounter()
	_, err = etcdTx(readOps(p, gid)...)
	if err != nil {
		return nil, fmt.Errorf("read uid/gid counters: %v", err)
	}

	// if the project already exists, there is nothing to do here

	// XXX this is probably not needed as the optimistic lock will catch this
	//     unecessary round trip to etcd
	if p.Ver > 0 {
		return nil, fmt.Errorf("project exists at version %d", p.Ver)
	}

	// read the objects corresponding to project members and organizations
	var users []ObjectIO

	for user := range p.Members {
		users = append(users, NewUser(user))
	}
	ops := readOps(users...)

	var org ObjectIO
	if p.Organization != "" {
		org = NewOrganization(p.Organization)
		ops = append(ops, readOps(org)...)
	}

	_, err = etcdTx(ops...)
	if err != nil {
		return nil, fmt.Errorf("read project users: %v", err)
	}

	// ensure each user exists, and if they do add this project to their project map
	for _, _user := range users {

		user := _user.Value().(*portal.User)
		if user.Projects == nil {
			user.Projects = make(map[string]*portal.Member)
		}
		if user.Ver == 0 {
			return nil, fmt.Errorf("user %s does not exist", user.Username)
		}
		found := user.Projects[p.Name]
		if found != nil {
			log.Errorf(
				"BUG: user %s already has reference to new project %s",
				user.Username,
				p.Name,
			)
		} else {
			user.Projects[p.Name] = p.Members[user.Username]
		}
	}

	// ensure organization exists, and if it does add this project to their project map
	if p.Organization != "" {

		o := org.Value().(*portal.Organization)
		if o.Projects == nil {
			o.Projects = make(map[string]*portal.Member)
		}
		if o.Ver == 0 {
			return nil, fmt.Errorf("organization %s does not exist", o.Name)
		}

		found := o.Projects[p.Name]
		if found != nil {
			log.Errorf(
				"BUG: organization %s already has reference to new project %s",
				o.Name,
				p.Name,
			)
		} else {
			if p.OrgMembership == nil {
				return nil, fmt.Errorf("Project has organization name, but no membership information")
			}

			o.Projects[p.Name] = p.OrgMembership
		}
	}

	// assign the next available GID to the project
	var nextid uint64
	nextid, gid.CountSet, err = gid.Add()
	if err != nil {
		return nil, fmt.Errorf("gid next: %v", err)
	}
	p.Gid = uint32(nextid)

	// write transaction
	ops = writeOps(p, gid)
	ops = append(ops, writeOps(users...)...)
	if p.Organization != "" {
		ops = append(ops, writeOps(org)...)
	}

	rollback, err := etcdTx(ops...)
	if err != nil {
		return nil, fmt.Errorf("create project commit: %v", err)
	}

	return rollback, nil
}

func (p *Project) Update() (*Rollback, error) {

	// If the update request is nil, nothing to do

	if p.UpdateRequest == nil {
		return nil, nil
	}
	upd := p.UpdateRequest

	// prepare to read project, users and organizations involved in the update
	users := make(map[string]*User)
	orgs := make(map[string]*Organization)
	objs := []ObjectIO{p}

	// function to add users involved in update to 'users' map
	processMember := func(m string) error {
		if users[m] == nil {
			user := NewUser(m)
			err := user.Read()
			if err != nil {
				return err
			}
			if user.Ver == 0 {
				return fmt.Errorf("cannot process non-existent user %s", m)
			}
			users[m] = user
			objs = append(objs, user)
		}
		return nil
	}

	processOrganization := func(o string) error {
		if orgs[o] == nil {
			org := NewOrganization(o)
			err := org.Read()
			if err != nil {
				return err
			}
			if org.Ver == 0 {
				return fmt.Errorf("cannot process non-existent organization %s", o)
			}
			orgs[o] = org
			objs = append(objs, org)
		}
		return nil
	}

	if upd.Members != nil {
		for _, m := range upd.Members.Remove {
			err := processMember(m)
			if err != nil {
				return nil, err
			}
		}
		for m := range upd.Members.Set {
			err := processMember(m)
			if err != nil {
				return nil, err
			}
		}
	}

	if upd.Organization != nil {
		for _, o := range upd.Organization.Remove {
			err := processOrganization(o)
			if err != nil {
				return nil, err
			}
		}
		for o := range upd.Organization.Set {
			err := processOrganization(o)
			if err != nil {
				return nil, err
			}
		}
	}

	// read in current state
	_, err := etcdTx(readOps(objs...)...)
	if err != nil {
		return nil, fmt.Errorf("project update read: %v", err)
	}

	// perform updates

	// Description
	if upd.Description != nil {
		p.Description = upd.Description.Value
	}

	// Access mode
	if upd.AccessMode != nil {
		p.AccessMode = upd.AccessMode.Value
	}

	// Members
	if upd.Members != nil {
		for _, member := range upd.Members.Remove {
			delete(p.Members, member)
			delete(users[member].Projects, p.Id())
		}
		for user, membership := range upd.Members.Set {
			if p.Members == nil {
				p.Members = make(map[string]*portal.Member)
			}
			p.Members[user] = membership

			if users[user].Projects == nil {
				users[user].Projects = make(map[string]*portal.Member)
			}
			users[user].Projects[p.Id()] = membership
		}
	}

	// Organizations
	if upd.Organization != nil {
		// should only ever be one of these...
		if len(upd.Organization.Remove) > 1 {
			log.Warnf("More than one org remove entry in project update")
		}
		for _, org := range upd.Organization.Remove {
			if org == p.Organization {
				p.Organization = ""
				p.OrgMembership = nil
			}
			delete(orgs[org].Projects, p.Id())
		}
		// should only ever be one of these...
		if len(upd.Organization.Set) > 1 {
			log.Warnf("More than one org set entry in project update")
		}
		for org, membership := range upd.Organization.Set {
			p.Organization = org
			p.OrgMembership = membership

			if orgs[org].Projects == nil {
				orgs[org].Projects = make(map[string]*portal.Member)
			}
			orgs[org].Projects[p.Id()] = membership
		}
	}

	// write updates
	rollback, err := etcdTx(writeOps(objs...)...)
	if err != nil {
		return nil, fmt.Errorf("project update tx: %v", err)
	}

	return rollback, nil

}

func (p *Project) Delete() (*Rollback, error) {

	log.Infof("deleting project %s. gathering storage operations...", p.Name)

	err := p.Read()
	if err != nil {
		return nil, fmt.Errorf("project read: %v", err)
	}

	userOps, err := p.DeleteLinkedOps(nil)
	if err != nil {
		return nil, err
	}

	log.Infof("adding del op for project %s", p.Name)
	ops := append(delOps(p), userOps...)

	log.Debugf("del project etcTx ops: %s", opsString(ops))

	rb, err := etcdTx(ops...)
	if err != nil {
		return nil, fmt.Errorf("project delete tx: %v", err)
	}

	return rb, nil
}

func (p *Project) GetVersion() int64 {

	return p.Ver

}

func (p *Project) SetVersion(v int64) {

	p.Ver = v

}

// Helpers ====================================================================

func (p *Project) DeleteLinkedOps(tc *TransactionCache) ([]StorOp, error) {

	log.Infof("gathering delete linked ops for project %s", p.Name)

	if tc == nil {
		tc = NewTransactionCache()
	}

	var wrops []ObjectIO
	var delops []StorOp

	for user := range p.Members {
		wrops = append(wrops, tc.NewObj(PTUser, user))
	}

	_, err := tc.Read()
	if err != nil {
		return nil, fmt.Errorf("project member read: %v", err)
	}

	for _, obj := range wrops {

		user := obj.Value().(*portal.User)
		delete(user.Projects, p.Id())
	}

	for _, xp := range p.Experiments {

		// delete the exp and transact all ops related to deleting the exp.
		e := tc.NewObj(PTExp, xp, p.Id()).(*Experiment)
		_, err := tc.Read()
		if err != nil {
			return nil, fmt.Errorf("exp read %s", e.Id())
		}

		log.Infof("adding del op for exp %s", xp)
		delops = append(delops, delOps(e)...)

		ops, err := e.DeleteLinkedOpsUsers(wrops)
		if err != nil {
			return nil, fmt.Errorf("del exp linked ops: %v", err)
		}

		delops = append(delops, ops...)
	}

	// now we're done with users, we can add the org to write ops if needed.
	if p.Organization != "" {
		o := tc.NewObj(PTOrg, p.Organization).(*Organization)
		_, err := tc.Read()
		if err != nil {
			return nil, fmt.Errorf("project's org read %s", err.Error())
		}

		delete(o.Projects, p.Name)

		log.Infof("adding write op for org %s", o.Name)
		wrops = append(wrops, o)
	}

	// remove project from the pool.
	readPool := GetProjectPool(p.Name)
	if readPool != nil {
		pool := tc.NewObj(PTPool, readPool.Name).(*Pool)
		pool.Read()
		for i, name := range pool.Projects {
			if name == p.Name {
				pool.Projects = append(
					pool.Projects[:i],
					pool.Projects[i+1],
				)
				wrops = append(wrops, pool)
				break
			}
		}
	}

	// NOTE: If anything else uses XDCS data, this needs to be converted
	// into TransactionCache code.
	xdcs, err := ListXDCs(p.Name)
	if err != nil {
		return nil, fmt.Errorf("listxdcs: %+v", err)
	}

	for _, xdc := range xdcs {
		log.Infof("adding delop for xdc %s.%s", xdc.Name, xdc.Project)
		delops = append(delops, delOps(xdc)...)
	}

	log.Infof("adding write op for project users (update project list)")
	ops := append(writeOps(wrops...), delops...)
	return ops, nil
}

// Status

func (p *Project) GetGoal(timeout time.Duration) (*reconcile.TaskForest, error) {
	tg := reconcile.NewGoal(p.Key(), "Project: "+p.Id(), "Manage Project: "+p.Id())

	tg.AddTaskRecord(FindTaskRecords(p, reconcile.TaskRecord_Exists)...)

	return tg.WatchTaskForest(EtcdClient, timeout)
}
