package storage

import (
	"context"
	"fmt"

	"google.golang.org/protobuf/proto"
	portal "gitlab.com/mergetb/api/portal/v1/go"
	"go.etcd.io/etcd/client/v3"
)

type SSHJump struct {
	*portal.SSHJump
}

func NewSSHJump(name string) *SSHJump {

	return &SSHJump{
		&portal.SSHJump{
			Name: name,
		},
	}

}

func (j *SSHJump) Bucket() string {

	return "sshjump"
}

func (j *SSHJump) Id() string {

	return j.Name
}

func (j *SSHJump) Key() string {

	return "/" + j.Bucket() + "/" + j.Id()
}

func (j *SSHJump) Value() interface{} {

	return j.SSHJump
}

func (j *SSHJump) Substrate() Substrate {

	return Etcd
}

func (j *SSHJump) Read() error {

	_, err := etcdTx(readOps(j)...)
	return err
}

func (j *SSHJump) GetVersion() int64 {

	return j.Ver
}

func (j *SSHJump) SetVersion(v int64) {

	j.Ver = v
}

func (j *SSHJump) Create() (*Rollback, error) {

	err := j.Read()
	if err != nil {
		return nil, fmt.Errorf("ssh jump read: %w", err)
	}

	if j.Ver > 0 {
		return nil, fmt.Errorf("ssh jump exists")
	}

	if j.Name == "" {
		return nil, fmt.Errorf("incomplete creation information")
	}

	r, err := etcdTx(writeOps(j)...)
	if err != nil {
		return nil, fmt.Errorf("ssh jump write: %w", err)
	}

	return r, nil
}

func (j *SSHJump) Update() (*Rollback, error) {

	r, err := etcdTx(writeOps(j)...)
	if err != nil {
		return nil, fmt.Errorf("ssh jump write: %w", err)
	}

	return r, nil
}

func (j *SSHJump) Delete() (*Rollback, error) {

	err := j.Read()
	if err != nil {
		return nil, fmt.Errorf("ssh jump read: %w", err)
	}

	if j.Ver == 0 {
		return nil, fmt.Errorf("ssh jump does not exist")
	}

	r, err := etcdTx(delOps(j)...)
	if err != nil {
		return nil, fmt.Errorf("ssh jump del: %w", err)
	}

	return r, nil
}

func ListSSHJumps() ([]*SSHJump, error) {

	result := []*SSHJump{}

	j := &SSHJump{}
	kvc := clientv3.NewKV(EtcdClient)
	resp, err := kvc.Get(context.TODO(), "/"+j.Bucket()+"/", clientv3.WithPrefix())
	if err != nil {
		return nil, fmt.Errorf("get ssh jumps: %w", err)
	}

	for _, kv := range resp.Kvs {

		j := &SSHJump{SSHJump: new(portal.SSHJump)}
		err := proto.Unmarshal(kv.Value, j.SSHJump)
		if err != nil {
			return nil, fmt.Errorf("malformed ssh jump: %w", err)
		}

		result = append(result, j)
	}

	return result, nil
}
