package storage

import (
	"fmt"
	"time"

	"gitlab.com/mergetb/portal/services/internal"
	"go.etcd.io/etcd/client/v3"
)

var EtcdClient *clientv3.Client

func InitPortalEtcdClient() error {

	// NOTE: this is an insecure connection. No TLS used.
	connstr := fmt.Sprintf("etcd:%d", 2379)
	cliconf := clientv3.Config{
		Endpoints:            []string{connstr},
		DialKeepAliveTime:    10 * time.Minute,
		DialKeepAliveTimeout: 10 * time.Second,
		DialTimeout:          2 * time.Second,
		MaxCallSendMsgSize:   internal.GRPCMaxMessageSize,
		MaxCallRecvMsgSize:   internal.GRPCMaxMessageSize,
	}

	var err error
	EtcdClient, err = clientv3.New(cliconf)

	return err

}
