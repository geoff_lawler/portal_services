package storage

import (
	"context"
	"fmt"
	"io"
	"sort"
	"strings"
	"time"

	"github.com/minio/minio-go/v7"
	log "github.com/sirupsen/logrus"
	clientv3 "go.etcd.io/etcd/client/v3"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/timestamppb"
	ts "google.golang.org/protobuf/types/known/timestamppb"

	"gitlab.com/mergetb/portal/services/pkg/merror"

	portal "gitlab.com/mergetb/api/portal/v1/go"
	xir "gitlab.com/mergetb/xir/v0.3/go"

	"gitlab.com/mergetb/tech/reconcile"
	sstor "gitlab.com/mergetb/tech/shared/storage/etcd"
)

type Experiment struct {
	*portal.Experiment
	UpdateRequest *portal.UpdateExperimentRequest
}

func NewExperiment(name, project string) *Experiment {

	return &Experiment{
		Experiment: &portal.Experiment{
			Name:    name,
			Project: project,
		},
	}

}

// Experiment Storage Object interface implementation =========================

func (e *Experiment) Bucket() string {

	return "experiments"

}

func (e *Experiment) Id() string {

	return fmt.Sprintf("%s/%s", e.Project, e.Name)

}

func (e *Experiment) Key() string {

	return fmt.Sprintf("/%s/%s", e.Bucket(), e.Id())

}

func (e *Experiment) Value() interface{} {

	return e.Experiment

}

func (e *Experiment) Substrate() Substrate {

	return Etcd

}

func (e *Experiment) Read() error {

	_, err := etcdTx(readOps(e)...)
	return err

}

func ReadExperiments(es []*Experiment) error {

	var objs []ObjectIO
	for _, p := range es {
		objs = append(objs, p)
	}

	_, err := etcdTx(readOps(objs...)...)
	return err

}

func (e *Experiment) Create() (*Rollback, error) {

	// try and read the experiment to see if it already exists
	// XXX this is probably not needed as the optimistic lock will catch this
	//     unecessary round trip to etcd

	err := e.Read()
	if err != nil {
		return nil, fmt.Errorf("experiment read: %v", err)
	}

	if e.Ver > 0 {
		return nil, fmt.Errorf("experiment exists at version %d", e.Ver)
	}

	if e.Project == "" {
		return nil, fmt.Errorf("experiment create: project must be specified")
	}

	if e.Creator == "" {
		return nil, fmt.Errorf("experiment create: creator must be specified")
	}

	p := NewProject(e.Project)
	err = p.Read()
	if err != nil {
		return nil, fmt.Errorf("project read: %v", err)
	}

	if p.Ver == 0 {
		return nil, fmt.Errorf("project %s does not exist", e.Project)
	}

	// Not sure if this is the proper place to enforce this or not.
	if m, ok := p.Members[e.Creator]; ok {
		if m.State != portal.Member_Active {
			return nil, fmt.Errorf("experiment creator is not active")
		}
	} else {
		return nil, fmt.Errorf("experiment creator is not a project member")
	}

	p.Experiments = append(p.Experiments, e.Name)

	u := NewUser(e.Creator)
	err = u.Read()
	if err != nil {
		return nil, fmt.Errorf("user read: %+v", err)
	}

	u.Experiments = append(u.Experiments, e.Name+"."+e.Project)

	rb, err := etcdTx(writeOps(e, u, p)...)
	if err != nil {
		return nil, fmt.Errorf("create experiment commit: %v", err)
	}

	return rb, nil
}

func (e *Experiment) Update() (*Rollback, error) {

	// If the update request is nil, nothing to do

	if e.UpdateRequest == nil {
		return nil, nil
	}
	upd := e.UpdateRequest

	_, err := etcdTx(readOps(e)...)
	if err != nil {
		return nil, fmt.Errorf("experiment update read: %v", err)
	}

	var users []ObjectIO

	// perform updates

	// Description
	if upd.Description != nil {
		e.Description = upd.Description.Value
	}

	// Access mode
	if upd.AccessMode != nil {
		e.AccessMode = upd.AccessMode.Value
	}

	if upd.Creator != "" {

		// user experiment update.
		ename := e.Name + "." + e.Project

		u := NewUser(upd.Creator)
		err = u.Read()
		if err != nil {
			return nil, fmt.Errorf("read user: %v", err)
		}

		if u.Ver == 0 {
			return nil, fmt.Errorf("creator: no such user %s", upd.Creator)
		}

		// TODO confirm it's not there first.
		u.Experiments = append(u.Experiments, ename)
		users = append(users, u)

		u = NewUser(e.Creator)
		err = u.Read()
		if err != nil {
			return nil, fmt.Errorf("read user: %v", err)
		}

		for i, ue := range u.Experiments {
			if ue == ename {
				u.Experiments = append(u.Experiments[:i], u.Experiments[i+1:]...)
			}
		}
		users = append(users, u)

		// update the experiment creator
		e.Creator = upd.Creator
	}

	// sort out maintainers. If the user is in the update list and not in the existing list, add them.
	// If they are in the update list but are in the list, remove them. The maintainers list in
	// the update request acts as a toggle in maintainer membership.
	if len(upd.Maintainers) != 0 {
		for _, um := range upd.Maintainers {

			u := NewUser(um)
			err = u.Read()
			if err != nil {
				return nil, fmt.Errorf("user read %v", err)
			}

			log.Debugf("maintainer user: %+v", u)

			if u.Ver == 0 {
				return nil, fmt.Errorf("maintainer: no such user %s", upd.Creator)
			}

			ename := e.Name + "." + e.Project // user experiments are in eid.pid format

			removed := false
			for i, m := range e.Maintainers {
				if um == m {
					// remove if it is in the list
					e.Maintainers = append(e.Maintainers[:i], e.Maintainers[i+1:]...)

					// remove from users experiment list
					for i, ue := range u.Experiments {
						if ue == ename {
							u.Experiments = append(u.Experiments[:i], u.Experiments[i+1:]...)
						}
					}

					removed = true
					break
				}
			}
			if !removed {
				// add if it was not in the list.
				e.Maintainers = append(e.Maintainers, um)
				u.Experiments = append(u.Experiments, ename)
			}

			users = append(users, u)
		}
	}

	// write updates
	ops := writeOps(e)
	ops = append(ops, writeOps(users...)...)
	rb, err := etcdTx(ops...)
	if err != nil {
		return nil, fmt.Errorf("experiment update tx: %v", err)
	}

	return rb, nil
}

func (e *Experiment) Delete() (*Rollback, error) {

	// initial read to get linked data

	log.Infof("exp %s delete. collecting storage operations.", e.Name)

	err := e.Read()
	if err != nil {
		return nil, fmt.Errorf("experiment read: %v", err)
	}

	log.Info("removing experiment from project experiment list...")
	p := NewProject(e.Project)
	err = p.Read()
	if err != nil {
		return nil, fmt.Errorf("experiment's project read: %v", err)
	}

	var ops []StorOp

	ops = append(ops, delOps(e)...) // delete experiment storage

	for i, exp := range p.Experiments {
		if exp == e.Name {
			log.Infof("adding project write op for %s (updating experiment list)", p.Name)
			p.Experiments = append(p.Experiments[:i], p.Experiments[i+1:]...)
			ops = append(ops, writeOps(p)...) // write out new project storage
			break
		}
	}

	var users []ObjectIO
	for user := range p.Members {
		users = append(users, NewUser(user))
	}

	_, err = etcdTx(readOps(users...)...)
	if err != nil {
		return nil, fmt.Errorf("exp project member read: %v", err)
	}

	ops = append(ops, writeOps(users...)...)

	linkedOps, err := e.DeleteLinkedOpsUsers(users)
	if err != nil {
		return nil, fmt.Errorf("del linked exp ops: %+v", err)
	}

	ops = append(ops, linkedOps...) // transact any linked operations.

	// TODO: delete unhandled revision keys

	rb, err := etcdTx(ops...)
	if err != nil {
		return nil, fmt.Errorf("experiment commit tx: %v", err)
	}

	return rb, nil
}

func (e *Experiment) GetVersion() int64 {

	return e.Ver

}

func (e *Experiment) SetVersion(v int64) {

	e.Ver = v

}

func (e *Experiment) AddCompilation(revision string, comp_err error) error {

	err := e.Read()
	if err != nil {
		return fmt.Errorf("experiment read: %v", err)
	}

	if e.Models == nil {
		e.Models = make(map[string]*portal.ExperimentModel)
	}

	if _, ok := e.Models[revision]; ok {
		return fmt.Errorf("exp %s.%s already tracking revision %s", e.Name, e.Project, revision)
	}

	// add revision with compilation status
	if comp_err == nil {
		e.Models[revision] = &portal.ExperimentModel{
			Compiled: true,
			Msg:      "Succeeded",
		}
	} else {
		m := comp_err.Error()

		if me, ok := comp_err.(*merror.MergeError); ok {
			m = me.Detail
		}

		e.Models[revision] = &portal.ExperimentModel{
			Compiled: false,
			Msg:      m,
		}
	}

	e.Models[revision].CompileTime = ts.Now()

	_, err = etcdTx(writeOps(e)...)
	if err != nil {
		return fmt.Errorf("experiment tx: %v", err)
	}

	return nil

}

func (e *Experiment) ReadExperimentModels() (map[string]*portal.XpNetModel, error) {

	models := make(map[string]*portal.XpNetModel)

	// for each revision, get the compiled model
	for rev, model := range e.Models {
		bucket := fmt.Sprintf("xp-%s-%s", e.Project, e.Name)

		if model.Compiled {
			obj, err := MinIOClient.GetObject(
				context.TODO(),
				bucket,
				rev,
				minio.GetObjectOptions{},
			)
			if err != nil {
				return nil, status.Error(codes.Internal, err.Error())
			}
			defer obj.Close()

			buf, err := io.ReadAll(obj)
			if err != nil {
				return nil, status.Error(codes.Internal, err.Error())
			}

			xpnet, err := xir.NetworkFromB64String(string(buf))
			if err != nil {
				return nil, status.Error(codes.Internal, err.Error())
			}

			models[rev] = &portal.XpNetModel{
				Compiled: true,
				Model:    xpnet,
			}
		} else {
			models[rev] = &portal.XpNetModel{
				Compiled: false,
				Model:    nil,
			}
		}
	}

	return models, nil
}

/*
func (e *Experiment) ReadAliasedExperimentModel(alias string) (*portal.XpNetModel, string, error) {
	m, ok := e.Models[alias]
	if !ok {
		return nil, "", status.Error(codes.NotFound, "alias does not exist")
	}

	return e.ReadExperimentModel(m.Revision)
}
*/

func (e *Experiment) ReadExperimentModel(revision string) (*portal.XpNetModel, string, error) {

	m, ok := e.Models[revision]
	if !ok {
		return nil, "", status.Error(codes.NotFound, "revision does not exist")
	}

	bucket := fmt.Sprintf("xp-%s-%s", e.Project, e.Name)
	obj, err := MinIOClient.GetObject(
		context.TODO(),
		bucket,
		revision+"-model",
		minio.GetObjectOptions{},
	)
	if err != nil {
		return nil, "", status.Error(codes.Internal, err.Error())
	}
	defer obj.Close()

	modelFile, err := io.ReadAll(obj)
	if err != nil {
		return nil, "", status.Error(codes.Internal, err.Error())
	}

	if m.Compiled {

		obj, err := MinIOClient.GetObject(
			context.TODO(),
			bucket,
			revision,
			minio.GetObjectOptions{},
		)
		if err != nil {
			return nil, "", status.Error(codes.Internal, err.Error())
		}
		defer obj.Close()

		buf, err := io.ReadAll(obj)
		if err != nil {
			return nil, "", status.Error(codes.Internal, err.Error())
		}

		xpnet, err := xir.NetworkFromB64String(string(buf))
		if err != nil {
			return nil, "", status.Error(codes.Internal, err.Error())
		}

		return &portal.XpNetModel{Compiled: true, Model: xpnet}, string(modelFile), nil
	}

	return &portal.XpNetModel{Compiled: false, Model: nil}, string(modelFile), nil
}

// Helpers ====================================================================

// TODO this is a bit gross.....
// /experiments/<proj>/<experiment> -> <experiment> <project>
func XpPathSplit(path string) (string, string, error) {

	parts := strings.Split(path, "/")
	if len(parts) != 2 {
		return "", "", fmt.Errorf(
			"BUG: malformed experiment member reference %s, exepcted <project>/<experiment>",
			path,
		)
	}
	return parts[1], parts[0], nil

}

// Get the storage ops that should be transacted when an experiment is deleted.
// The existing writeOp users are passed in and should be modified in place.
func (e *Experiment) DeleteLinkedOpsUsers(projUsers []ObjectIO) ([]StorOp, error) {

	log.Infof("gathering delete linked ops for exp %s.%s", e.Name, e.Project)

	ops := []StorOp{}

	var err error

	for _, rev := range e.Models {
		for _, rlz := range rev.Realizations {

			r := NewRealizeRequest(rlz, e.Name, e.Project)
			err = r.Read()
			if err != nil {
				return nil, fmt.Errorf("experiment realization read: %v", err)
			}

			log.Infof("adding del op for rlz: %s.%s.%s", r.Realization, e.Name, e.Project)
			ops = append(ops, delOps(r)...)

			rlzOps, err := r.DeleteLinkedOps()
			if err != nil {
				return nil, fmt.Errorf("del rlz linked ops: %+v", err)
			}

			ops = append(ops, rlzOps...)
		}
	}

	users := []string{e.Creator}
	users = append(users, e.Maintainers...)

	log.Info("removing experiment from creator and maintainer experiment lists...")
	for _, user := range users {

		var u *User

		for _, pu := range projUsers {
			pUser := pu.Value().(*portal.User)
			if pUser.Username == user { // this is also a project user. act on the existing user.
				u = pu.(*User)
			}
		}

		if u == nil {
			return nil, fmt.Errorf("exp member is not a project member: %s", user)
		}

		for i, ue := range u.Experiments {
			fqen := fmt.Sprintf("%s.%s", e.Name, e.Project)
			if ue == fqen {
				u.Experiments = append(u.Experiments[:i], u.Experiments[i+1:]...)
				break
			}
		}
	}

	return ops, nil
}

// Get the storage ops that should be transacted when an experiment is deleted.
// The existing writeOp users are passed in and should be modified in place.
func (e *Experiment) DeleteLinkedOps(tc *TransactionCache) ([]StorOp, error) {

	if tc == nil {
		tc = NewTransactionCache()
	}

	p := tc.NewObj(PTProj, e.Project).(*Project)
	_, err := tc.Read()
	if err != nil {
		return nil, fmt.Errorf("experiment's project read: %v", err)
	}

	var users []ObjectIO
	for user := range p.Members {
		users = append(users, tc.NewObj(PTUser, user))
	}

	dops, err := e.DeleteLinkedOpsUsers(users)
	if err != nil {
		return nil, fmt.Errorf("experiment delete linked ops users: %v", err)
	}

	return append(writeOps(users...), dops...), nil
}

func (e *Experiment) GetGoal(timeout time.Duration) (*reconcile.TaskForest, map[string]*portal.ExperimentModel, error) {
	var ctx context.Context
	var cancel context.CancelFunc

	if timeout > 0 {
		ctx, cancel = context.WithTimeout(context.Background(), timeout)
		defer cancel()
	}

	cache := sstor.NewReadCache(0, nil)
	defer cache.Close()

	options := sstor.EtcdTransactionOptions{
		Ctx:   ctx,
		Cache: cache,
	}

	var ops sstor.EtcdTransaction
	ops.ReadKeyWithOpt(e.Key()+"/", clientv3.WithPrefix())

	txr, _, err := ops.EtcdTxWithOptions(EtcdClient, options)
	if err != nil {
		return nil, nil, err
	}

	keys, err := (&sstor.GenericEtcdKey{}).ReadAllFromResponse(txr)
	if err != nil {
		return nil, nil, err
	}

	mod_revs := make(map[string]int64)

	records := make([]*reconcile.TaskRecord, 0, len(keys)+1)

	// we manually add task records for models,
	// as we do not have a model objectIO definition (since it's just a string)

	// failsafe of everything in the prefix space, in case it changes later
	records = append(records, &reconcile.TaskRecord{
		Task:              e.Key() + "/",
		ReconcilerManager: ReconcilerConfigModel.Manager,
		ReconcilerName:    ReconcilerConfigModel.Reconcilers[0].Name,
		Existence:         reconcile.TaskRecord_Optional,
		KeyType:           reconcile.TaskRecord_PrefixSpace,
	})

	for k, v := range keys {
		mod_revs[k] = 0

		ge, ok := v.(*sstor.GenericEtcdKey)
		if !ok {
			log.Errorf("bad key somehow: %s, skipping", k)
			continue
		}

		mod_revs[k] = ge.ModRevision

		if strings.HasSuffix(k, "/rev") {
			// not reconciled, but make a note of it anyways
			records = append(records, &reconcile.TaskRecord{
				Task:              k,
				ReconcilerManager: ReconcilerConfigModel.Manager,
				ReconcilerName:    ReconcilerConfigModel.Reconcilers[0].Name,
				Existence:         reconcile.TaskRecord_Optional,
			})
		} else {
			records = append(records, &reconcile.TaskRecord{
				Task:              k,
				ReconcilerManager: ReconcilerConfigModel.Manager,
				ReconcilerName:    ReconcilerConfigModel.Reconcilers[0].Name,
			})
		}
	}

	// put the revisions in ascending order
	sort.Slice(records, func(i, j int) bool {
		return mod_revs[records[i].Task] >= mod_revs[records[j].Task]
	})

	tg := reconcile.NewGoal(e.Key(), "Experiment: "+e.Id(), e.Id())
	tg.AddTaskRecord(FindTaskRecords(e, reconcile.TaskRecord_Exists)...)
	tg.AddTaskRecord(records...)

	var tf *reconcile.TaskForest

	if timeout > 0 {
		tf, err = tg.WatchTaskForestWithOptions(ctx, EtcdClient, cache)
		if err != nil {
			return nil, nil, err
		}
	} else {
		tf, err = tg.GetTaskForest(EtcdClient, cache)
		if err != nil {
			return nil, nil, err
		}
	}

	in_progress := make(map[string]*portal.ExperimentModel)
	for _, ts := range tf.GetStatuses() {
		if ts.CurrentStatus == reconcile.TaskStatus_Success {
			continue
		}

		spl := strings.Split(ts.TaskKey, "/")
		if len(spl) < 2 {
			log.Errorf("bad experiment model key: %s", ts.TaskKey)
		}

		ge := ts.GetPrevAsGenericEtcdKey()
		err := ge.Read(EtcdClient)
		if err != nil {
			log.Errorf("read %s: %+v", ge.EtcdKey, err)
			continue
		}

		revision := string(ge.EtcdValue)

		in_progress[revision] = &portal.ExperimentModel{
			Compiled:    false,
			Msg:         "In progress, compiling...",
			CompileTime: timestamppb.Now(),
		}

	}

	return tf, in_progress, err
}
