package storage

import (
	"fmt"
	"regexp"
	"strconv"
	"time"

	ts "google.golang.org/protobuf/types/known/timestamppb"

	portal "gitlab.com/mergetb/api/portal/v1/go"
)

// Return now + the given duration. The format
// of the duration is what is accepted by
// time.parseDuration() with possibly prepended
// entries for weeks and days. Non integer
// weeks and days are not supported.
//
// Format: NwNd(duration)
//
// Where:
//   - N can be any integer (within reason)
//   - the week or day fields may be there or not
//   - "(duration)" is valid for time.parseDuration().
func parseDuration(start time.Time, d string) (time.Time, error) {

	toDur := func(s string) (time.Duration, error) {
		if len(s) == 0 {
			return time.Duration(0), nil
		}
		parsed, err := strconv.Atoi(s[:len(s)-1])
		if err != nil {
			return 0, fmt.Errorf("Invalid number: %s", err)
		}
		return time.Duration(parsed), nil
	}

	re := regexp.MustCompile(`^(\d+w)?(\d+d)?(.+)?$`)
	found := re.FindStringSubmatch(d)

	val, err := toDur(found[1])
	if err != nil {
		return time.UnixMicro(0), err
	}
	start = start.Add(time.Hour * 24 * 7 * val) // weeks

	val, err = toDur(found[2])
	if err != nil {
		return time.UnixMicro(0), err
	}
	start = start.Add(time.Hour * 24 * val) // days

	if len(found[3]) > 0 {
		therest, err := time.ParseDuration(found[3])
		if err != nil {
			return time.UnixMicro(0), err
		}
		start = start.Add(therest)
	}

	return start, nil
}

func ParseReservationDuration(from time.Time, r *portal.ReservationDuration) (*ts.Timestamp, error) {

	// No duration field at all - give the realization the default duration.
	if r == nil {
		t, err := parseDuration(from, defaultRealizationDuration)
		if err != nil {
			return nil, fmt.Errorf("Bad duration: %s", err)
		}

		return ts.New(t), nil

	}

	// Setup the expiration
	switch r.When {

	case portal.ReservationDuration_never:
		return ts.New(time.UnixMicro(0)), nil // zero => never expire

	case portal.ReservationDuration_given:

		d := defaultRealizationDuration
		if r.Duration != "" {
			d = r.Duration
		}

		t, err := parseDuration(from, d)
		if err != nil {
			return nil, fmt.Errorf("Bad duration: %s", err)
		}

		return ts.New(t), nil

	default:
		return nil, fmt.Errorf("unhandled ReservationDurationCode: %s", r.When)
	}

}
