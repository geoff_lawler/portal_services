package storage

import (
	"context"
	"fmt"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"
	clientv3 "go.etcd.io/etcd/client/v3"
	"google.golang.org/protobuf/proto"

	portal "gitlab.com/mergetb/api/portal/v1/go"

	"gitlab.com/mergetb/tech/reconcile"
)

type Organization struct {
	*portal.Organization
	UpdateRequest *portal.UpdateOrganizationRequest
}

func NewOrganization(name string) *Organization {

	return &Organization{
		Organization: &portal.Organization{
			Name: name,
		},
	}

}

//
// TODO: break out members into a watchable etcd key so member
// actions can be watched separately.
//

// Storage Object interface implementation ====================================

func (o *Organization) Bucket() string {

	return "organizations"

}

func (o *Organization) Id() string {

	return o.Name

}

func (o *Organization) Key() string {

	return fmt.Sprintf("/%s/%s", o.Bucket(), o.Id())

}

func (o *Organization) Value() interface{} {

	return o.Organization

}

func (o *Organization) Substrate() Substrate {

	return Etcd

}

func (o *Organization) Read() error {

	_, err := etcdTx(readOps(o)...)
	return err

}

// Return a list of all organizations.
func ListOrganizations() ([]*Organization, error) {

	var result []*Organization

	kvc := clientv3.NewKV(EtcdClient)
	resp, err := kvc.Get(context.TODO(), "/organizations", clientv3.WithPrefix())
	if err != nil {
		return nil, fmt.Errorf("get organizations: %w", err)
	}

	for _, kv := range resp.Kvs {

		o := &Organization{Organization: new(portal.Organization)}
		err = proto.Unmarshal(kv.Value, o.Organization)
		if err != nil {
			return nil, fmt.Errorf("malformed organization at %s: %w", string(kv.Key), err)
		}
		result = append(result, o)
	}

	return result, nil
}

// Read the organization data for the given organizations.
func ReadOrganizations(orgs []*Organization) error {

	var objs []ObjectIO
	for _, o := range orgs {
		objs = append(objs, o)
	}

	_, err := etcdTx(readOps(objs...)...)
	return err

}

func (o *Organization) Create() (*Rollback, error) {

	oidmtx.Lock()
	defer oidmtx.Unlock()

	lks, err := locks(oidLock)
	if err != nil {
		return nil, fmt.Errorf("oid lock: %v", err)
	}
	defer lks.Unlock()

	// fetch organization counter object from storage

	oid := oidCounter()
	_, err = etcdTx(readOps(o, oid)...)
	if err != nil {
		return nil, fmt.Errorf("read oid counters: %v", err)
	}

	// if the organization already exists, there is nothing to do here

	// XXX this is probably not needed as the optimistic lock will catch this
	//     unnecessary round trip to etcd
	if o.Ver > 0 {
		return nil, fmt.Errorf("organization exists at version %d", o.Ver)
	}

	// read the objects corresponding to organization members and projects
	var users []ObjectIO
	for user := range o.Members {
		users = append(users, NewUser(user))
	}

	var projects []ObjectIO
	for proj := range o.Projects {
		projects = append(projects, NewProject(proj))
	}

	ops := append(readOps(users...), readOps(projects...)...)
	_, err = etcdTx(ops...)
	if err != nil {
		return nil, fmt.Errorf("read organization users and projects: %v", err)
	}

	// ensure each user exists, and if they do add this organization to their organization map
	for _, _user := range users {

		user := _user.Value().(*portal.User)
		if user.Organizations == nil {
			user.Organizations = make(map[string]*portal.Member)
		}
		if user.Ver == 0 {
			return nil, fmt.Errorf("user %s does not exist", user.Username)
		}
		found := user.Organizations[o.Name]
		if found != nil {
			log.Errorf(
				"BUG: user %s already has reference to new organization %s",
				user.Username,
				o.Name,
			)
		} else {
			user.Organizations[o.Name] = o.Members[user.Username]
		}
	}

	// ensure each project exists, and if they do add this organization to their organization map
	for _, _proj := range projects {

		proj := _proj.Value().(*portal.Project)
		if proj.Ver == 0 {
			return nil, fmt.Errorf("project %s does not exist", proj.Name)
		}

		if proj.Organization == o.Name {
			log.Errorf(
				"BUG: project %s already has reference to new organization %s",
				proj.Name,
				o.Name,
			)
		} else {
			proj.Organization = o.Name
			proj.OrgMembership = o.Members[proj.Name]
		}
	}

	// assign the next available OID to the organization
	var nextid uint64
	nextid, oid.CountSet, err = oid.Add()
	if err != nil {
		return nil, fmt.Errorf("oid next: %v", err)
	}
	o.Oid = uint32(nextid)

	// write transaction
	ops = writeOps(o, oid)
	ops = append(ops, writeOps(users...)...)
	ops = append(ops, writeOps(projects...)...)

	rollback, err := etcdTx(ops...)
	if err != nil {
		return nil, fmt.Errorf("create organization commit: %v", err)
	}

	return rollback, nil

}

func (o *Organization) Update() (*Rollback, error) {

	// If the update request is nil, nothing to do

	if o.UpdateRequest == nil {
		return nil, nil
	}
	upd := o.UpdateRequest

	// prepare to read users and projects involved in the update

	users := make(map[string]*User)
	projects := make(map[string]*Project)
	objs := []ObjectIO{o}

	processMember := func(u string) error {
		if users[u] == nil {
			user := NewUser(u)
			err := user.Read()
			if err != nil {
				return err
			}

			// Non users *can* request membership, and can have membership denied
			if user.Ver == 0 {
				// do not create/remove the user, but continue to process them
				return nil
			}

			users[u] = user
			objs = append(objs, user)
		}
		return nil
	}

	processProject := func(m string) error {
		if projects[m] == nil {
			project := NewProject(m)
			err := project.Read()
			if err != nil {
				return err
			}
			if project.Ver == 0 {
				log.Warn("non-project as member: project does not exist")
				return nil
			}
			projects[m] = project
			objs = append(objs, project)
		}
		return nil
	}

	if upd.Members != nil {
		for _, u := range upd.Members.Remove {
			err := processMember(u)
			if err != nil {
				return nil, err
			}
		}
		for u := range upd.Members.Set {
			err := processMember(u)
			if err != nil {
				return nil, err
			}
		}
	}

	if upd.Projects != nil {
		for _, m := range upd.Projects.Remove {
			err := processProject(m)
			if err != nil {
				return nil, err
			}
		}
		for m := range upd.Projects.Set {
			err := processProject(m)
			if err != nil {
				return nil, err
			}
		}
	}

	// read in current user, project, and organization state

	_, err := etcdTx(readOps(objs...)...)
	if err != nil {
		return nil, fmt.Errorf("organization update read: %v", err)
	}

	// perform updates

	// Description
	if upd.Description != nil {
		o.Description = upd.Description.Value
	}

	// Access mode
	if upd.AccessMode != nil {
		o.AccessMode = upd.AccessMode.Value
	}

	// State
	if upd.State != nil {
		o.State = upd.State.Value
	}

	// Members
	if upd.Members != nil {
		for _, member := range upd.Members.Remove {
			delete(o.Members, member)

			// members may not have a User object, in the case of never-inited IDs that requested
			// org membership but were denied
			if _, ok := users[member]; ok {
				delete(users[member].Organizations, o.Id())
			}
		}

		if len(upd.Members.Set) > 0 && o.Members == nil {
			o.Members = make(map[string]*portal.Member)
		}
		for user, membership := range upd.Members.Set {
			o.Members[user] = membership
			if _, ok := users[user]; ok {
				if users[user].Organizations == nil {
					users[user].Organizations = make(map[string]*portal.Member)
				}
				users[user].Organizations[o.Id()] = membership
			}
		}
	}

	// Projects
	if upd.Projects != nil {
		for _, member := range upd.Projects.Remove {
			delete(o.Projects, member)
			if _, ok := projects[member]; ok {
				projects[member].Organization = ""
				projects[member].OrgMembership = nil
			}
		}

		// o.Projects will be nil if this is the first project added.
		if len(upd.Projects.Set) > 0 && o.Projects == nil {
			o.Projects = make(map[string]*portal.Member)
		}
		for proj, membership := range upd.Projects.Set {
			if o.Projects == nil {
				o.Projects = make(map[string]*portal.Member)
			}
			o.Projects[proj] = membership
			if _, ok := projects[proj]; ok {
				projects[proj].Organization = o.Id()
				projects[proj].OrgMembership = membership
			}
		}
	}

	// write updates
	rollback, err := etcdTx(writeOps(objs...)...)
	if err != nil {
		return nil, fmt.Errorf("organization update tx: %v", err)
	}

	return rollback, nil

}

func (o *Organization) Delete() (*Rollback, error) {

	log.Infof("deleting organization %s. gathering storage operations...", o.Name)

	err := o.Read()
	if err != nil {
		return nil, fmt.Errorf("organization read: %v", err)
	}

	userOps, err := o.DeleteLinkedOps(nil)
	if err != nil {
		return nil, err
	}

	log.Infof("adding del op for organization %s", o.Name)
	ops := append(delOps(o), userOps...)

	// for ease of debug
	ostrs := []string{}
	for _, o := range ops {
		ostrs = append(ostrs, fmt.Sprintf("%s", o.String()))
	}
	log.Debugf("del organization etcd Tx ops: %s", strings.Join(ostrs, ", "))

	rb, err := etcdTx(ops...)
	if err != nil {
		return nil, fmt.Errorf("organization delete tx: %v", err)
	}

	return rb, nil

}

func (o *Organization) GetVersion() int64 {

	return o.Ver

}

func (o *Organization) SetVersion(v int64) {

	o.Ver = v

}

// Helpers ====================================================================

func (o *Organization) DeleteLinkedOps(tc *TransactionCache) ([]StorOp, error) {

	log.Infof("gathering delete linked ops for organization %s", o.Name)

	if tc == nil {
		tc = NewTransactionCache()
	}

	var users []ObjectIO
	var projects []ObjectIO
	var delops []StorOp

	for user, member := range o.Members {
		// check for non-user membership request case.
		if member.State == portal.Member_MemberRequested {
			u := tc.NewObj(PTUser, user)
			err := u.Read()
			if err != nil {
				continue
			}
		}

		users = append(users, tc.NewObj(PTUser, user))
	}

	for project := range o.Projects {
		projects = append(projects, tc.NewObj(PTProj, project))
	}

	// remove users/projects in the org
	_, err := tc.Read()
	if err != nil {
		return nil, fmt.Errorf("organization member read: %v", err)
	}

	for _, obj := range users {
		user := obj.Value().(*portal.User)
		delete(user.Organizations, o.Id())
	}

	for _, obj := range projects {
		project := obj.Value().(*portal.Project)
		project.Organization = ""
		project.OrgMembership = nil
	}

	ops := append(writeOps(users...), delops...)
	ops = append(ops, writeOps(projects...)...)

	// remove Org from pool.
	readPool := GetOrganizationPool(o.Name)
	if readPool != nil {
		// slightly awkward as we have to recreate this in the transaction cache.
		pool := tc.NewObj(PTPool, readPool.Name).(*Pool)
		pool.Read()
		for i, org := range pool.Organizations {
			if org == o.Name {
				pool.Organizations = append(
					pool.Organizations[:i],
					pool.Organizations[i+1],
				)
				poolOps := []ObjectIO{pool}
				ops = append(ops, writeOps(poolOps...)...)
				break
			}
		}
	}

	log.Debugf("deletelinkedops for organization %s: %+v", o.Name, ops)
	return ops, nil
}

// Status

func (o *Organization) GetGoal(timeout time.Duration) (*reconcile.TaskForest, error) {
	tg := reconcile.NewGoal(o.Key(), "Organization: "+o.Id(), "Manage Organization: "+o.Id())

	tg.AddTaskRecord(FindTaskRecords(o, reconcile.TaskRecord_Exists)...)

	return tg.WatchTaskForest(EtcdClient, timeout)
}
