package storage

import (
	"context"
	"fmt"

	"go.etcd.io/etcd/client/v3"
	"google.golang.org/protobuf/proto"

	"gitlab.com/mergetb/api/portal/v1/go"
)

type PublicKey struct {
	*portal.PublicKey
	User string // to whom does this pubkey belong? only used for constructing etcd keys
	Ver  int64  // not stored, is that OK?
}

func NewPublicKey(user string, fingerprint string) *PublicKey {
	// these two fields are required for the Read() operation to succeed
	return &PublicKey{
		PublicKey: &portal.PublicKey{
			Fingerprint: fingerprint,
		},
		User: user,
	}
}

// Return a list of all keys for the specified user
func ListUserPublicKeys(user string) ([]*PublicKey, error) {

	var result []*PublicKey

	kvc := clientv3.NewKV(EtcdClient)
	resp, err := kvc.Get(context.TODO(), fmt.Sprintf("/pubkeys/%s", user), clientv3.WithPrefix())
	if err != nil {
		return nil, fmt.Errorf("get pubkeys: %v", err)
	}

	for _, kv := range resp.Kvs {

		u := &PublicKey{
			PublicKey: &portal.PublicKey{},
			User:      user,
		}
		err = proto.Unmarshal(kv.Value, u.PublicKey)
		if err != nil {
			return nil, fmt.Errorf("malformed pubkey at %s: %v", err, string(kv.Key))
		}
		result = append(result, u)

	}

	return result, nil

}

// Storage Object interface implementation ====================================

func (p *PublicKey) Bucket() string {

	return "pubkeys"

}

func (p *PublicKey) GetVersion() int64 {

	return p.Ver

}

func (p *PublicKey) SetVersion(v int64) {

	p.Ver = v

}

func (p *PublicKey) Id() string {

	return p.Fingerprint

}

func (p *PublicKey) Key() string {

	return fmt.Sprintf("/%s/%s/%s", p.Bucket(), p.User, p.Id())

}

func (p *PublicKey) Value() interface{} {

	return p.PublicKey

}

func (p *PublicKey) Substrate() Substrate {

	return Etcd

}

func (p *PublicKey) Create() (*Rollback, error) {

	// if the pubkey already exists, there is nothing to do here

	if p.Ver > 0 {
		return nil, fmt.Errorf("pubkey exists at version %d", p.Ver)
	}

	rollback, err := etcdTx(writeOps(p)...)
	if err != nil {
		return nil, fmt.Errorf("create pubkey commit: %v", err)
	}

	return rollback, nil

}

func (p *PublicKey) Read() error {

	_, err := etcdTx(readOps(p)...)
	return err

}

func (p *PublicKey) Update() (*Rollback, error) {

	// nothing to do because keys can't be modified

	return nil, nil
}

func (p *PublicKey) Delete() (*Rollback, error) {

	rollback, err := etcdTx(delOps(p)...)
	if err != nil {
		return nil, fmt.Errorf("pubkey delete tx: %v", err)
	}

	return rollback, nil

}
