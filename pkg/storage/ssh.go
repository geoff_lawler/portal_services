package storage

import (
	"context"
	"fmt"
	"strings"

	log "github.com/sirupsen/logrus"
	portal "gitlab.com/mergetb/api/portal/v1/go"
	"go.etcd.io/etcd/client/v3"
	"google.golang.org/protobuf/proto"
)

type SSHKeyPair struct {
	*portal.SSHKeyPair
	User   string
	IsHost bool
	Ver    int64
}

func NewSSHUserKeyPair(user string) *SSHKeyPair {

	return &SSHKeyPair{
		SSHKeyPair: &portal.SSHKeyPair{},
		User:       user,
		IsHost:     false,
	}
}

func NewSSHHostKeyPair(host string) *SSHKeyPair {

	return &SSHKeyPair{
		SSHKeyPair: &portal.SSHKeyPair{},
		User:       host,
		IsHost:     true,
	}
}

type SSHCert struct {
	*portal.SSHCert
	User   string
	IsHost bool // if true this is a host cert, else a user cert
	Ver    int64
}

func NewSSHUserCert(username string) *SSHCert {

	return &SSHCert{
		SSHCert: &portal.SSHCert{
			Cert: "",
		},
		User:   username,
		IsHost: false,
		Ver:    0,
	}
}

func NewSSHHostCert(host string) *SSHCert {

	return &SSHCert{
		SSHCert: &portal.SSHCert{
			Cert: "",
		},
		User:   host,
		IsHost: true,
		Ver:    0,
	}
}

// SSH Key Pair Storage Object IO implementation

func (skp *SSHKeyPair) Bucket() string {
	return "auth/ssh/keys"
}

func (skp *SSHKeyPair) Id() string {
	return skp.User
}

func (skp *SSHKeyPair) Key() string {
	return "/" + skp.Bucket() + "/" + skp.Type() + "/" + skp.Id()
}

func (skp *SSHKeyPair) Value() interface{} {
	return skp.SSHKeyPair
}

func (skp *SSHKeyPair) Substrate() Substrate {
	return Etcd
}

func (skp *SSHKeyPair) Type() string {

	if skp.IsHost == true {
		return "host"
	}

	return "user"
}

func (skp *SSHKeyPair) Read() error {
	_, err := etcdTx(readOps(skp)...)
	return err
}

func (skp *SSHKeyPair) GetVersion() int64 {

	return skp.Ver
}

func (skp *SSHKeyPair) SetVersion(ver int64) {

	skp.Ver = ver
}

func (skp *SSHKeyPair) Create() (*Rollback, error) {

	log.Infof("[%s] creating keypair (in db)", skp.User)

	rb, err := etcdTx(writeOps(skp)...)
	if err != nil {
		log.Errorf("create ssh key pair fail: %+v", err)
		return nil, err
	}

	return rb, nil
}

func (skp *SSHKeyPair) Update() (*Rollback, error) {

	log.Infof("ssh key update (aka create) for %s", skp.User)

	return skp.Create()
}

func (skp *SSHKeyPair) Delete() (*Rollback, error) {

	log.Infof("ssh key delete for %s", skp.User)

	rb, err := etcdTx(delOps(skp)...)
	if err != nil {
		return nil, err
	}

	return rb, nil
}

func GetSSHKeys() ([]*SSHKeyPair, error) {

	var res []*SSHKeyPair

	kvc := clientv3.NewKV(EtcdClient)
	resp, err := kvc.Get(context.TODO(), "/auth/ssh/keys", clientv3.WithPrefix())
	if err != nil {
		return nil, fmt.Errorf("read ssh keys: %w", err)
	}

	for _, kv := range resp.Kvs {

		k := new(portal.SSHKeyPair)
		err = proto.Unmarshal(kv.Value, k)
		if err != nil {
			return nil, fmt.Errorf("malformed sshkey: %w", err)
		}

		tkns := strings.Split(string(kv.Key), "/")

		res = append(res, &SSHKeyPair{
			SSHKeyPair: k,
			User:       tkns[5],
			Ver:        k.Ver,
			IsHost:     tkns[4] == "host",
		})
	}

	return res, nil
}

// SSH Cert
func (sc *SSHCert) Bucket() string {
	return "auth/ssh/cert"
}

func (sc *SSHCert) Id() string {
	return sc.User
}

func (sc *SSHCert) Key() string {

	// /auth/ssh/{host,user}/<name>
	return "/" + sc.Bucket() + "/" + sc.Type() + "/" + sc.Id()
}

func (sc *SSHCert) Value() interface{} {
	return sc.SSHCert
}

func (sc *SSHCert) Substrate() Substrate {
	return Etcd
}

func (sc *SSHCert) Read() error {
	_, err := etcdTx(readOps(sc)...)
	return err
}

func (sc *SSHCert) GetVersion() int64 {

	return sc.Ver
}

func (sc *SSHCert) SetVersion(ver int64) {

	sc.Ver = ver
}

func (sc *SSHCert) Type() string {

	if sc.IsHost == true {
		return "host"
	}
	return "user"
}

func (sc *SSHCert) Create() (*Rollback, error) {

	log.Infof("[%s] creating ssh %s cert", sc.Id(), sc.Type())

	rb, err := etcdTx(writeOps(sc)...)
	if err != nil {
		log.Errorf("create ssh cert fail: %+v", err)
		return nil, err
	}

	return rb, nil
}

func (sc *SSHCert) Update() (*Rollback, error) {

	log.Infof("ssh %s cert update (aka create) for %s", sc.Type(), sc.Id())

	return sc.Create()
}

func (sc *SSHCert) Delete() (*Rollback, error) {

	log.Infof("ssh %s cert delete for %s", sc.Type(), sc.Id())

	rb, err := etcdTx(delOps(sc)...)
	if err != nil {
		return nil, err
	}

	return rb, nil
}

func GetSSHCerts() ([]*SSHCert, error) {

	var res []*SSHCert

	kvc := clientv3.NewKV(EtcdClient)
	resp, err := kvc.Get(context.TODO(), "/auth/ssh/cert", clientv3.WithPrefix())
	if err != nil {
		return nil, fmt.Errorf("read ssh certs: %w", err)
	}

	for _, kv := range resp.Kvs {

		c := new(portal.SSHCert)
		err = proto.Unmarshal(kv.Value, c)
		if err != nil {
			return nil, fmt.Errorf("malformed ssh cert: %w", err)
		}

		tkns := strings.Split(string(kv.Key), "/")

		res = append(res, &SSHCert{
			SSHCert: c,
			User:    tkns[5],
			Ver:     c.Ver,
			IsHost:  tkns[4] == "host",
		})
	}

	return res, nil
}
