package storage

import (
	"context"
	"fmt"
	"strings"

	log "github.com/sirupsen/logrus"
	portal "gitlab.com/mergetb/api/portal/v1/go"
	clientv3 "go.etcd.io/etcd/client/v3"
	"google.golang.org/protobuf/proto"
)

// ***************************************************************************
// Wireguard Enclave information.
// ***************************************************************************

var (
	wgEncKey   = "/wgenc/"
	wgIfreqKey = "/wgif/"
)

type WgEnclave struct {
	*portal.WgEnclave

	AddIf *portal.AddWgIfConfigRequest
	DelIf *portal.AddWgIfConfigRequest
}

func NewWgEnclave(id string) *WgEnclave {
	return &WgEnclave{
		WgEnclave: &portal.WgEnclave{
			Enclaveid:  id,
			Gateways:   make(map[string]*portal.WgIfConfig),
			Clients:    make(map[string]*portal.WgIfConfig),
			GatewayIps: make(map[string]string),
			ClientIps:  make(map[string]string),
		},
	}
}

func (e *WgEnclave) Bucket() string {
	return wgEncKey
}

func (e *WgEnclave) Id() string {
	return e.Enclaveid
}

func (e *WgEnclave) Key() string {
	return wgEncKey + e.Id()
}

func (e *WgEnclave) Value() interface{} {
	return e.WgEnclave
}

func (e *WgEnclave) Substrate() Substrate {
	return Etcd
}

func (e *WgEnclave) Read() error {
	_, err := etcdTx(readOps(e)...)
	return err
}

func (e *WgEnclave) GetVersion() int64 {
	return e.Ver
}

func (e *WgEnclave) SetVersion(ver int64) {
	e.Ver = ver
}

func (e *WgEnclave) Create() (*Rollback, error) {
	log.Tracef("Create wg enclave")
	return etcdTx(writeOps(e)...)
}

func (e *WgEnclave) Update() (*Rollback, error) {
	log.Tracef("Update wg enclave")

	// TODO: maps in proto bufs are not created if they have no entries.
	// figure out why and fix it, in the meantime create the maps
	// if they do not exist.
	if e.AddIf != nil && e.AddIf.Config != nil {

		k := e.AddIf.Config.Key

		if e.AddIf.Gateway == true {
			if e.Gateways == nil {
				e.Gateways = make(map[string]*portal.WgIfConfig)
			}
			if _, ok := e.Gateways[k]; !ok {
				log.Tracef("Adding gateway if: %s/%s", e.Enclaveid, k)
				e.Gateways[k] = e.AddIf.Config
			} else {
				log.Tracef("Not adding gw if, key exists: %s/%s", e.Enclaveid, k)
			}
		} else {
			if e.Clients == nil {
				e.Clients = make(map[string]*portal.WgIfConfig)
			}
			if _, ok := e.Clients[k]; !ok {
				log.Tracef("Adding client if: %s/%s", e.Enclaveid, k)
				e.Clients[k] = e.AddIf.Config
			} else {
				log.Tracef("Not adding client if, key exists: %s/%s", e.Enclaveid, k)
			}
		}
	}

	if e.DelIf != nil && e.DelIf.Config != nil {

		k := e.DelIf.Config.Key
		removed := false

		if _, ok := e.Gateways[k]; ok {
			delete(e.Gateways, k)
			log.Tracef("deleted gw if: %s/%s", e.Enclaveid, k)
			removed = true
		}

		if _, ok := e.Clients[k]; ok {
			delete(e.Clients, k)
			log.Tracef("deleted client if: %s/%s", e.Enclaveid, k)
			removed = true
		}

		if !removed {
			log.Warnf("cannot delete key. no such key in enclave: %s/%s", e.Enclaveid, k)
		}
	}

	return etcdTx(writeOps(e)...)
}

func (e *WgEnclave) Delete() (*Rollback, error) {
	log.Tracef("Delete wg enclave")
	return etcdTx(delOps(e)...)
}

func ListWgEnclaves() ([]*WgEnclave, error) {

	result := []*WgEnclave{}

	kvc := clientv3.NewKV(EtcdClient)
	resp, err := kvc.Get(context.TODO(), wgEncKey, clientv3.WithPrefix())
	if err != nil {
		return nil, fmt.Errorf("get wg enclaves: %w", err)
	}

	for _, kv := range resp.Kvs {

		e := new(portal.WgEnclave)
		err = proto.Unmarshal(kv.Value, e)
		if err != nil {
			return nil, fmt.Errorf("malformed enclave at %s: %w", string(kv.Key), err)
		}
		result = append(result, &WgEnclave{
			WgEnclave: e,
		})
	}

	return result, nil
}

// ***************************************************************************
// XDC Specific wireguard client information.
// ***************************************************************************
type XdcWgClient struct {
	*portal.AttachXDCRequest
	Ver int64
}

func NewXdcWgClient(rq *portal.AttachXDCRequest) *XdcWgClient {

	return &XdcWgClient{
		AttachXDCRequest: rq,
		Ver:              0,
	}
}

func (x *XdcWgClient) Bucket() string {
	return "/wgclient/xdc/"
}

func (x *XdcWgClient) Id() string {
	return x.Xdc + "." + x.Project
}

func (x *XdcWgClient) Key() string {
	return "/wgclient/xdc/" + x.Id()
}

func (x *XdcWgClient) Value() interface{} {
	return x
}

func (x *XdcWgClient) Substrate() Substrate {
	return Etcd
}

func (x *XdcWgClient) Read() error {
	_, err := etcdTx(readOps(x)...)
	return err
}

func (x *XdcWgClient) GetVersion() int64 {
	return x.Ver
}

func (x *XdcWgClient) SetVersion(ver int64) {
	x.Ver = ver
}

func (x *XdcWgClient) Create() (*Rollback, error) {
	log.Tracef("Create xdc wg client")
	return etcdTx(writeOps(x)...)
}

func (x *XdcWgClient) Update() (*Rollback, error) {
	log.Tracef("Update xdc wg client")
	return x.Create()
}

func (x *XdcWgClient) Delete() (*Rollback, error) {
	log.Tracef("Delete xdc wg client")
	return etcdTx(delOps(x)...)
}

// ***************************************************************************
// Wireguard interface requests
// ***************************************************************************
type WgIfRequest struct {
	*portal.AddWgIfConfigRequest
	Ver int64
}

func NewWgIfRequest(enclaveid, key string) *WgIfRequest {
	return &WgIfRequest{
		AddWgIfConfigRequest: &portal.AddWgIfConfigRequest{
			Enclaveid: enclaveid,
			Config: &portal.WgIfConfig{
				Key: key,
			},
		},
		Ver: 0,
	}
}

func (r *WgIfRequest) Bucket() string {
	return "/wgif/"
}

func (r *WgIfRequest) Id() string {
	return r.Config.Key // NB: this is the if public key
}

func (r *WgIfRequest) Key() string {
	return "/wgif/" + r.Enclaveid + "/" + r.Id()
}

func (r *WgIfRequest) Value() interface{} {
	return r.AddWgIfConfigRequest
}

func (r *WgIfRequest) Substrate() Substrate {
	return Etcd
}

func (r *WgIfRequest) Read() error {
	_, err := etcdTx(readOps(r)...)
	return err
}

func (r *WgIfRequest) GetVersion() int64 {
	return r.Ver
}

func (r *WgIfRequest) SetVersion(ver int64) {
	r.Ver = ver
}

func (r *WgIfRequest) Create() (*Rollback, error) {
	log.Tracef("Create wg interface")
	return etcdTx(writeOps(r)...)
}

func (r *WgIfRequest) Update() (*Rollback, error) {
	log.Tracef("Update wg interface")
	return r.Create()
}

func (r *WgIfRequest) Delete() (*Rollback, error) {
	log.Tracef("Delete wg interface")

	// This was mistakenly getting a "" key which deleted /wgenc/ aka all the enclaves.
	// So check for that and do not let this happen!
	if r.Id() == "" {
		log.Errorf("Will not delete non existent key")
	}

	return etcdTx(delOps(r)...)
}

func ListWgIfreqs() (map[string][]*WgIfRequest, error) {

	result := make(map[string][]*WgIfRequest)

	kvc := clientv3.NewKV(EtcdClient)
	resp, err := kvc.Get(context.TODO(), wgIfreqKey, clientv3.WithPrefix())
	if err != nil {
		return nil, fmt.Errorf("get wg ifreqs: %w", err)
	}

	for _, kv := range resp.Kvs {

		e := &WgIfRequest{AddWgIfConfigRequest: &portal.AddWgIfConfigRequest{}}
		err = proto.Unmarshal(kv.Value, e)
		if err != nil {
			return nil, fmt.Errorf("malformed ifreq at %s: %w", string(kv.Key), err)
		}

		// split the key into the requisite components
		parts := strings.Split(string(kv.Key), "/")
		if len(parts) < 4 {
			return nil, fmt.Errorf("malformed ifreq key path at %s", string(kv.Key))
		}

		encid := parts[2]
		if _, ok := result[encid]; !ok {
			result[encid] = []*WgIfRequest{}
		}
		result[encid] = append(result[encid], e)
	}

	return result, nil
}

func DeleteEnclaveWgIfreqs(enclave string) error {

	key := fmt.Sprintf("%s/%s", wgIfreqKey, enclave)
	kvc := clientv3.NewKV(EtcdClient)
	_, err := kvc.Delete(context.TODO(), key, clientv3.WithPrefix())
	if err != nil {
		return fmt.Errorf("delete %s wg ifreqs: %w", enclave, err)
	}

	return nil
}
