package storage

import (
	"context"
	"errors"
	"fmt"
	"math"

	log "github.com/sirupsen/logrus"
	portal "gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/portal/services/internal"
	clientv3 "go.etcd.io/etcd/client/v3"
	"google.golang.org/protobuf/proto"
)

// returns the etcd key
func infrapodCountKey(rlz *portal.Realization) string {
	return fmt.Sprintf("/infrapods/%s/%s/%s.%s.%s", rlz.Infranet.InfrapodSite, rlz.Infranet.InfrapodServer, rlz.Id, rlz.Eid, rlz.Pid)
}

// Record the allocation of an infrapod on the selected infraserver
func StoreInfrapodCount(rlz *portal.Realization) error {
	v := &internal.InfrapodCount{Pods: 1} // just count mzs for now
	b, err := proto.Marshal(v)
	if err != nil {
		return fmt.Errorf("unable to marshal infrapod count: %w", err)
	}

	var ops []clientv3.Op
	ops = append(ops, clientv3.OpPut(infrapodCountKey(rlz), string(b)))

	kvc := clientv3.NewKV(EtcdClient)
	resp, err := kvc.Txn(context.Background()).If().Then(ops...).Commit()
	if err != nil {
		return fmt.Errorf("etcd transaction: %w", err)
	}
	if !resp.Succeeded {
		return fmt.Errorf("etcd transaction failed")
	}

	return nil
}

// Remove the allocation of the infrapod from the selected infraserver
func DeleteInfrapodCount(rlz *portal.Realization) error {
	var ops []clientv3.Op
	ops = append(ops, clientv3.OpDelete(infrapodCountKey(rlz)))

	kvc := clientv3.NewKV(EtcdClient)
	resp, err := kvc.Txn(context.Background()).If().Then(ops...).Commit()
	if err != nil {
		return fmt.Errorf("etcd txn: %w", err)
	}
	if !resp.Succeeded {
		return errors.New("etcd transaction failed")
	}

	return nil
}

// Return the infraserver with the fewest infrapods
func GetNextInfraserver(site string, host []string) (string, error) {
	ret := ""
	pods := math.MaxInt32

	kvc := clientv3.NewKV(EtcdClient)

	for _, h := range host {
		k := fmt.Sprintf("/infrapods/%s/%s/", site, h)
		resp, err := kvc.Get(context.TODO(), k, clientv3.WithPrefix())
		if err != nil {
			return "", fmt.Errorf("etcd get: %w", err)
		}
		sum := 0
		for _, kv := range resp.Kvs {

			var cnt internal.InfrapodCount
			err := proto.Unmarshal(kv.Value, &cnt)
			if err != nil {
				return "", fmt.Errorf("unable to unmarshal infrapod count: %w", err)
			}
			sum += int(cnt.GetPods())
		}
		log.Infof("infrapod server %s/%s -> %d pods", site, h, sum)

		if sum < pods {
			pods = sum
			ret = h
		}
	}
	return ret, nil
}
