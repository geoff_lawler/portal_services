package workspace

import (
	"fmt"
	"os"
	"sort"
	"strconv"
	"strings"

	log "github.com/sirupsen/logrus"
)

type Passwd struct {
	File    *SharedFile
	Entries map[string]PasswdEntry
}

type PasswdEntry struct {
	Username string
	Uid      int
	Gid      int
	Shell    string
}

func (pe *PasswdEntry) Equals(other *PasswdEntry) bool {
	return pe.Username == other.Username &&
		pe.Uid == other.Uid &&
		pe.Gid == other.Gid &&
		pe.Shell == other.Shell
}

func OpenPasswd(file string) (*Passwd, error) {

	sf, err := SharedOpen(file)
	if err != nil {
		return nil, err
	}

	passwd := &Passwd{
		File:    sf,
		Entries: make(map[string]PasswdEntry),
	}

	// read passwd file content into Entries member
	err = passwd.read()
	if err != nil {
		passwd.Close()
		return nil, err
	}

	return passwd, nil

}

func (p *Passwd) read() error {

	// if the passwd file exists, read it's existing entries
	if _, err := os.Stat(p.File.Path); !os.IsNotExist(err) {
		src, err := os.ReadFile(p.File.Path)
		if err != nil {
			return fmt.Errorf("could not read %s", err)
		}

		lines := strings.Split(string(src), "\n")

		for _, l := range lines {
			e := readPasswdEntry(l)
			if e == nil {
				continue
			}
			p.Entries[e.Username] = *e
		}
	}

	// ensure root entry
	p.Entries["root"] = PasswdEntry{
		Username: "root",
		Uid:      0,
		Gid:      0,
		Shell:    "/bin/bash",
	}
	// ensure sshd entry
	p.Entries["sshd"] = PasswdEntry{
		Username: "sshd",
		Uid:      1,
		Gid:      0,
		Shell:    "/bin/bash",
	}

	return nil

}

// Contains returns true if the passed entry is in the Passwd file.
func (p *Passwd) Contains(pe *PasswdEntry) bool {
	entry, ok := p.Entries[pe.Username]
	if !ok {
		return false
	}
	return pe.Equals(&entry)
}

// Add a passwd entry to the passwd file.
func (p *Passwd) Add(e *PasswdEntry) error {
	p.Entries[e.Username] = *e
	return nil
}

// Merge - merge the passwd entries in other into p.
func (p *Passwd) Merge(other *Passwd) error {
	// This is a brain dead algorithm. Just look at all and add over and over.
	for _, oe := range other.Entries {
		if !p.Contains(&oe) {
			p.Add(&oe)
		}
	}
	return nil
}

func readPasswdEntry(line string) *PasswdEntry {

	if line == "" {
		return nil
	}

	parts := strings.Split(line, ":")
	if len(parts) < 7 {
		log.WithFields(log.Fields{"entry": line}).Warn("invalid passwd entry")
		return nil
	}
	uid, err := strconv.Atoi(parts[2])
	if err != nil {
		log.WithFields(log.Fields{"entry": line}).Warn("invalid passwd uid")
		return nil
	}

	gid, err := strconv.Atoi(parts[3])
	if err != nil {
		log.WithFields(log.Fields{"entry": line}).Warn("invalid passwd gid")
		return nil
	}

	return &PasswdEntry{
		Username: parts[0],
		Uid:      uid,
		Gid:      gid,
		Shell:    parts[6],
	}

}

func (e *PasswdEntry) Write() string {
	return fmt.Sprintf("%s:x:%d:%d:%s:/home/%s:%s",
		e.Username,
		e.Uid,
		e.Gid,
		e.Username,
		e.Username,
		e.Shell,
	)

}

func (p *Passwd) String() string {

	var entries []PasswdEntry
	for _, e := range p.Entries {
		entries = append(entries, e)
	}

	// sort entries according to uid
	sort.Slice(entries, func(i, j int) bool {
		return entries[i].Uid < entries[j].Uid
	})

	// create file source
	var src string
	for _, e := range entries {
		src += e.Write() + "\n"
	}
	return src
}

func (p *Passwd) Write() error {

	src := p.String()
	return os.WriteFile(p.File.Path, []byte(src), 0644)

}

func (p *Passwd) Close() {
	p.File.Close()
}
