package materialize

import (
	"context"
	"fmt"
	"sync"

	log "github.com/sirupsen/logrus"

	"gitlab.com/mergetb/api/facility/v1/go"
	"gitlab.com/mergetb/api/portal/v1/go"

	"gitlab.com/mergetb/portal/services/internal"
	"gitlab.com/mergetb/portal/services/pkg/connect"
	"gitlab.com/mergetb/portal/services/pkg/cred"
	"gitlab.com/mergetb/portal/services/pkg/realize/sfe"
	"gitlab.com/mergetb/portal/services/pkg/storage"
)

type MzContext struct {

	// Users provisioned to all nodes
	GlobalUsers []*portal.UserInfo

	// Users provisioned to specific nodes keyed by node name
	NodeUsers map[string][]*portal.UserInfo
}

func RzToMz(rz *portal.Realization, mzc *MzContext) (*portal.Materialization, error) {

	mz := &portal.Materialization{
		Rid: rz.Id,
		Eid: rz.Eid,
		Pid: rz.Pid,
		//Infranet: rz.Infranet,
		LinkEmulation: rz.LinkEmulation,
	}

	// XXX remove when multiple infrapod servers are supported
	/*
		for srv, x := range rz.Infranet.InfrapodAddrs {

			mz.Params = &portal.MzParameters{
				InfranetServer: srv,
				InfranetAddr:   x,
			}

			break

		}
	*/

	// Need to pass the VNI for the infrapod server to MARS. MARS uses this
	// to configure the infrapod's networking via CNI.

	// This would need to be fixed if/when we support multiple infrapod servers
	infraVni, err := getInfraVni(rz)
	if err != nil {
		return nil, err
	}

	log.Debugf("found vni %d for mzid %s.%s.%s", infraVni, rz.Id, rz.Eid, rz.Pid)

	resolveExpnet := false

	if infraVni != sfe.HarborLsi {

		// Read exp revision model to extract experimentnetresolve. This is a waste - it should be stashed somewhere.
		exp := storage.NewExperiment(rz.Eid, rz.Pid)
		err = exp.Read()
		if err != nil {
			return nil, fmt.Errorf("unable to read experiment %s.%s", rz.Eid, rz.Pid)
		}

		xpMdl, _, err := exp.ReadExperimentModel(rz.Xhash)
		if err != nil {
			return nil, fmt.Errorf("unable to read model for experiment%s.%s", rz.Eid, rz.Pid)
		}
		resolveExpnet = xpMdl.Model.Parameters.Experimentnetresolution
	}

	mz.Params = &portal.MzParameters{
		InfranetVni:    infraVni,
		InfrapodServer: rz.Infranet.InfrapodServer,
		ResolveExpnet:  resolveExpnet,
	}

	log.Debugf("Exp Net Resolution is %t", mz.Params.ResolveExpnet)

	rz.RemoveDuplicateWaypoints()

	// Load User information from database.
	err = addUserConfigs(mzc, rz)
	if err != nil {
		return nil, err
	}

	// make a map of hosts to infranet VLAN id
	infravids := make(map[string]uint32)

	for _, link := range rz.Links {
		infranet := link.Link.GetId() == "infranet"
		harbor := link.Link.GetId() == "harbor"

		// collect infravids for infranet/harbor links
		if infranet || harbor {

			if len(link.Segments) != 1 {
				return nil, fmt.Errorf("model error: realizations only support 1 infranet/harbor link")
			}

			for _, s := range link.Segments {
				for _, e := range s.Endpoints {
					if harbor {
						infravids[e.Host] = 1
						continue
					}

					switch e.Interface.(type) {
					case *portal.Endpoint_Phy:
						if !e.Virtual {
							return nil, fmt.Errorf("found bare-metal phy infranet link endpoint: %+v", e)
						}
					case *portal.Endpoint_Vlan:
						infravids[e.Host] = e.GetVlan().GetVid()
					case *portal.Endpoint_Vtep:
						log.Debugf("infranet VTEP on host %s", e.Host)
						infravids[e.Host] = 1
					default:
						return nil, fmt.Errorf("model error: infranet link endpoint has interface with bad type: %+v", e)
					}
				}
			}
		}

		// record link in the mz
		mz.Links = append(mz.Links, &portal.Link{
			Realization: link,
		})

	}

	for _, x := range rz.Nodes {
		switch x.Kind {

		case portal.NodeRealization_BareMetal:

			iifx := x.Resource.Infranet()
			if iifx == nil {
				return nil, fmt.Errorf(
					"model error: resource %s has no infranet interface", x.Resource.Id)
			}
			if iifx.Mac == "" {
				return nil, fmt.Errorf(
					"model error: resource %s has no infranet mac", x.Resource.Id)
			}

			osc := x.Resource.OS
			if osc == nil {
				return nil, fmt.Errorf(
					"model error: resource %s has no os config", x.Resource.Id)
			}

			if osc.Rootdev == "" {
				return nil, fmt.Errorf(
					"model error: resource %s has no rootdev", x.Resource.Id)
			}

			if _, ok := infravids[x.Resource.Id]; !ok {
				return nil, fmt.Errorf(
					"model error: resource %s has no VLAN VID", x.Resource.Id)
			}

			metal := &portal.BareMetal{
				Facility:     x.Facility,
				Resource:     x.Resource.Id,
				Model:        x.Node,
				Inframac:     iifx.Mac,
				Infravid:     infravids[x.Resource.Id],
				Infraport:    x.Resource.PortName(iifx),
				InfranetAddr: x.InfranetAddr,
				Rootdev:      osc.Rootdev,
			}
			if mzc != nil {
				for _, u := range mzc.GlobalUsers {
					metal.Users = append(metal.Users, u)
				}
				if mzc.NodeUsers != nil {
					for _, u := range mzc.NodeUsers[x.Node.Id] {
						metal.Users = append(metal.Users, u)
					}
				}
			}
			mz.Metal = append(mz.Metal, metal)

		case portal.NodeRealization_VirtualMachine:
			vm := &portal.VirtualMachine{
				VmAlloc:      x.VmAlloc,
				Inframac:     x.VmAlloc.NICs.Alloc[0].Alloc[0].Mac, // first port on first nic
				InfranetAddr: x.InfranetAddr,
			}

			if mzc != nil {
				for _, u := range mzc.GlobalUsers {
					vm.Users = append(vm.Users, u)
				}
				if mzc.NodeUsers != nil {
					for _, u := range mzc.NodeUsers[x.Node.Id] {
						vm.Users = append(vm.Users, u)
					}
				}
			}
			mz.Vms = append(mz.Vms, vm)

		default:
			log.Warnf("unhandled node kind %v", x.Kind)
		}
	}

	return mz, nil

}

// TODO assumes network emulation is local to facilities nodes live in
func SiteList(mz *portal.Materialization) []string {

	var result []string

	add := func(x string) {
		for _, y := range result {
			if x == y {
				return
			}
		}
		result = append(result, x)
	}

	for _, x := range mz.Metal {
		add(x.Facility)
	}

	for _, x := range mz.Vms {
		add(x.VmAlloc.Facility)
	}

	// TODO CPS

	return result

}

func NodeToSiteMap(mz *portal.Materialization) map[string]string {

	result := make(map[string]string)

	for _, x := range mz.Metal {
		result[x.Model.Id] = x.Facility
	}

	for _, x := range mz.Vms {
		result[x.VmAlloc.Node] = x.VmAlloc.Facility
	}

	// TODO CPS

	return result
}

func Reboot(rq *portal.RebootMaterializationRequest, mz *portal.Materialization) error {

	mzid := internal.MzidToString(&internal.Mzid{
		Rid: rq.Realization,
		Eid: rq.Experiment,
		Pid: rq.Project,
	})

	l := log.WithFields(log.Fields{
		"mzid":      mzid,
		"mode":      rq.Mode.String(),
		"hostnames": rq.Hostnames,
		"all_nodes": rq.AllNodes,
	})

	// build a map from node names to site
	nts := NodeToSiteMap(mz)

	var hostnames []string
	if !rq.AllNodes {
		hostnames = rq.Hostnames
	} else {
		for node := range nts {
			hostnames = append(hostnames, node)
		}
	}

	// iterate through hosts and create reverse mapping from site to list of nodes
	stn := make(map[string][]string)
	for _, node := range hostnames {
		// make sure the nodes requested are in the mtz
		site, ok := nts[node]
		if !ok {
			l.Errorf("%s is not a node in materialization %s", node, mzid)
			continue
		}

		if _, ok := stn[site]; !ok {
			stn[site] = []string{}
		}
		stn[site] = append(stn[site], node)
	}

	for site, nodes := range stn {
		err := connect.FacilityClient(
			site,
			func(cli facility.FacilityClient) error {
				_, err := cli.RebootMaterialization(
					context.TODO(),
					&facility.RebootMaterializationRequest{
						Mode:      rq.Mode,
						Pid:       rq.Project,
						Eid:       rq.Experiment,
						Rid:       rq.Realization,
						Hostnames: nodes,
					},
				)
				return err
			},
		)
		if err != nil {
			err = fmt.Errorf("facility reboot error: %+v", err)
			l.Error(err)
			return err
		}

		l.Infof("node reboot(s) requested @ %s", site)
	}

	return nil
}

// Return the VNI that the infrapod's network interface is part of
func getInfraVni(rz *portal.Realization) (uint32, error) {
	for _, link := range rz.Links {
		if link.Link.GetId() == "infranet" || link.Link.GetId() == "harbor" {
			if len(link.Segments) != 1 {
				return 0, fmt.Errorf("found infranet link with %d segments -- should only be 1", len(link.Segments))
			}
			for _, seg := range link.Segments {
				for _, ep := range seg.Endpoints {
					// Find any VTEP endpoint. They all have the same VNI
					if ep.GetVtep() != nil {
						if vni := ep.GetVtep().GetVni(); vni != 0 {
							return vni, nil
						} else {
							return 0, fmt.Errorf("endpoint vtep didn't have a vni?")
						}
					}
				}
			}
		}
	}

	return 0, fmt.Errorf("unable to determine VNI for infranet")
}

type userInfo struct {
	info *portal.UserInfo
	err  error
}

func addUserConfig(u *storage.User) (*portal.UserInfo, error) {
	// TODO disconnect here. Portal has gui and uid. Foundry
	// expects username and groupnames.

	// key generated by merge for the user.
	pubKey := storage.NewSSHUserKeyPair(u.Username)
	err := pubKey.Read()
	if err != nil {
		return nil, fmt.Errorf("read user pubkey pair: %+v", err)
	}

	if pubKey.GetVersion() == 0 {
		return nil, fmt.Errorf("user pub key not found")
	}

	allPubKeys := []string{pubKey.Public}

	// Keys uploaded by the user
	keys, err := storage.ListUserPublicKeys(u.Username)
	if err != nil {
		return nil, fmt.Errorf("list user pub keys: %+v", err)
	}

	for _, k := range keys {
		allPubKeys = append(allPubKeys, k.PublicKey.Key)
	}

	// generate ephemeral passwordless ssh keys
	ephem_pub, ephem_priv, err := cred.GenerateSSHKeyPair(u.Username)
	if err != nil {
		return nil, fmt.Errorf("can't generate ephemeral passwordless ssh keys for user %s: %s", u.Username, err.Error())
	}

	log.Infof("Generated keypair for GlobalUser: %s - public: %s", u.Username, ephem_pub)
	allPubKeys = append(allPubKeys, ephem_pub)

	return &portal.UserInfo{
		Name:     u.Username,
		Groups:   []string{"sudo", u.Username},
		Pubkeys:  allPubKeys,
		Privkeys: []string{ephem_priv},
	}, nil
}

func addUserConfigs(mzc *MzContext, rz *portal.Realization) error {
	sp := storage.NewProject(rz.Pid) // sp ==> "storage project"
	err := sp.Read()
	if err != nil {
		return fmt.Errorf("err project read: %+v", rz.Pid)
	}

	sus := []*storage.User{} // sus ==> "storage users"
	for username := range sp.Members {
		sus = append(sus, storage.NewUser(username))
	}

	err = storage.ReadUsers(sus)
	if err != nil {
		return fmt.Errorf("err users read: %+v", err)
	}

	var wg sync.WaitGroup
	chanInfo := make(chan userInfo, len(sus))
	wg.Add(len(sus))

	// generate configs in parallel; each can take some time due to ephemeral keypair generation
	for _, u := range sus {
		go func(u *storage.User) {
			info, err := addUserConfig(u)
			chanInfo <- userInfo{
				info: info,
				err:  err,
			}
			wg.Done()
		}(u)
	}

	wg.Wait()

	for _ = range sus {
		info := <-chanInfo
		if info.err != nil {
			log.Error(err)
		} else {
			mzc.GlobalUsers = append(mzc.GlobalUsers, info.info)
		}
	}

	// NOTE: no idea when we would add/use mzc.NodeUsers.
	//  BJK: I believe the idea is red/blue exercises, where each user only has access to a subset
	//  of the mtz nodes

	return nil
}
