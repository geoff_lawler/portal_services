package model

import (
	"context"
	"io"
	"os"
	"os/exec"
	"time"

	log "github.com/sirupsen/logrus"

	"gitlab.com/mergetb/portal/services/pkg/merror"
	"gitlab.com/mergetb/xir/v0.3/go"
)

type PkgHandle struct{}

const dumper = `
import sys
from sys import stdin, stderr, exit
import os
import mergexp
import base64

# grab the well-known module-level global that has the topology.
from mergexp import __xp

# execute the given python model script.
try:
    exec(stdin.read())
except Exception as e:
    print("Exception: {}".format(e), file=stderr)
    exit(10)

# make sure the __xp instance is OK.
if not isinstance(__xp['topo'], mergexp.Network):
    print("bad mergexp network %s"%(type(__xp['topo'])), file=stderr)
    print("netwk %s"%(__xp['topo']), file=stderr)
    exit(20)

topo = __xp['topo']

# base64 encode it so it's predictably printable and readable
print(base64.b64encode(topo.xir().SerializeToString()).decode())
exit(0)
`

const DumpScript = "dumpModelTopology.py"

var DumpScriptDir = "/model/bin"

func CompileToXir(src string) (*xir.Network, error) {

	ds := DumpScriptDir + "/" + DumpScript

	// try to always 'just work' in some form
	_, err := os.Stat(ds)
	if os.IsNotExist(err) {
		ds = "/tmp/" + DumpScript
		err := os.WriteFile(ds, []byte(dumper), 0644)
		if err != nil {
			return nil, err
		}
	}

	// Do not let the command take "too long" and steal resources.
	// Slight protection against DoS attack. 15 seconds is probably not enough
	// to protect against a determined attacker. C'est la vie.
	ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
	defer cancel()

	cmd := exec.CommandContext(ctx, "python3", ds)
	cmd.Env = append(os.Environ())
	log.Debugf("PYTHONPATH=%s", os.Getenv("PYTHONPATH"))
	stdin, err := cmd.StdinPipe()
	if err != nil {
		log.Fatalf("faied to get python stdin pipe: %v", err)
	}

	go func() {
		defer stdin.Close()
		io.WriteString(stdin, src)
	}()

	out, err := cmd.Output()
	if err != nil {
		if exiterr, ok := err.(*exec.ExitError); ok {
			return nil, merror.MxCompileError(string(exiterr.Stderr))
		}
		return nil, merror.UncategorizedError("cmd error", err)
	}

	return xir.NetworkFromB64String(string(out))
}
