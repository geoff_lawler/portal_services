#!/usr/bin/env python3

from pathlib import Path
import sys
from sys import stdin, stderr, exit
import os
import mergexp
import google.protobuf.json_format

# grab the well-known module-level global that has the topology.
from mergexp import __xp   

if __name__ == "__main__":

    if len(sys.argv) >= 2:
        with open(sys.argv[1]) as r:
            text = r.read()
    else:
        text = stdin.read()

    # execute the given python model script.
    try:
        exec(text)
    except Exception as e:
        print("Exception: {}".format(e), file=stderr)
        exit(10)

    # make sure the __xp instance is OK.
    if not isinstance(__xp['topo'], mergexp.Network):
        print("bad mergexp network %s"%(type(__xp['topo'])), file=stderr)
        print("netwk %s"%(__xp['topo']), file=stderr)
        exit(20)

    topo = __xp['topo']

    # save as json
    output = google.protobuf.json_format.MessageToJson(topo.xir())

    if len(sys.argv) >= 2:
        wfile = str(Path(sys.argv[1]).stem) + '.json'
        with open(wfile, 'w') as w:
            w.write(output)
            print(f"wrote successfully to {wfile}")
    else:
        print(output)
    
    exit(0)
