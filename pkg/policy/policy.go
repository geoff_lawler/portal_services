package policy

import (
	"fmt"
	"os"
	"strings"

	log "github.com/sirupsen/logrus"
	portal "gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/portal/services/pkg/identity"
	"gopkg.in/yaml.v2"
)

var (
	policy *Policy = nil

	// Well known identity name which acts as bootstrap/fallback admin.
	PolicyAdmin = "portalops"
)

type Policy struct {
	Project         map[Mode]CrudOperationPolicy
	Experiment      map[Mode]CrudOperationPolicy
	User            map[Mode]CrudOperationPolicy
	Organization    map[Mode]OrganizationOperationPolicy
	Xdc             map[Mode]XdcOperationPolicy
	Realization     map[Mode]CarrOperationPolicy
	Materialization map[Mode]CreateDestroyOperationPolicy
	Facility        map[Mode]CrudcdOperationPolicy
	Pool            map[Mode]PoolOperationPolicy
	Identity        IdentityOperationPolicy
	Portal          PortalOperationPolicy
}

type CrudMap map[Mode]CrudOperationPolicy

type UserPolicy struct {
	Activate []RoleBinding
	Init     []RoleBinding
	CrudMap  `yaml:",inline"`
}

type Mode int

func (m Mode) MarshalYAML() (interface{}, error) {

	switch m {
	case Public:
		return "public", nil
	case Protected:
		return "protected", nil
	case Private:
		return "private", nil
	}

	return nil, fmt.Errorf("invalid mode %d", m)

}

func (m *Mode) UnmarshalYAML(unmarshal func(interface{}) error) error {

	var val string
	err := unmarshal(&val)
	if err != nil {
		return err
	}

	switch val {
	case "public":
		*m = Public
		return nil

	case "protected":
		*m = Protected
		return nil

	case "private":
		*m = Private
		return nil
	}

	return fmt.Errorf("invalid mode %s", val)

}

const (
	Public Mode = iota
	Protected
	Private
)

type Scope string

const (
	PortalScope          Scope = "Portal"
	OrganizationScope    Scope = "Organization"
	ProjectScope         Scope = "Project"
	ExperimentScope      Scope = "Experiment"
	UserScope            Scope = "User"
	RealizationScope     Scope = "Realization"
	MaterializationScope Scope = "Materialization"
	FacilityScope        Scope = "Facility"
	PoolScope            Scope = "Pool"
	XDCScope             Scope = "Xdc"
	AnyScope             Scope = "Any"
)

var Scopes = []Scope{
	PortalScope,
	OrganizationScope,
	ProjectScope,
	ExperimentScope,
	UserScope,
	RealizationScope,
	MaterializationScope,
	FacilityScope,
	PoolScope,
	XDCScope,
	AnyScope,
}

type RoleKind string

const (
	CreatorRole    RoleKind = "Creator"
	MaintainerRole RoleKind = "Maintainer"
	MemberRole     RoleKind = "Member"
	AnyRole        RoleKind = "Any"
)

type Role struct {
	Name     RoleKind
	Includes []Role
}

type RoleBinding struct {
	Scope Scope
	Role  RoleKind
}

func (m RoleBinding) MarshalYAML() (interface{}, error) {

	val := fmt.Sprintf("%s.%s", m.Scope, m.Role)

	return val, nil

}

func (m *RoleBinding) UnmarshalYAML(unmarshal func(interface{}) error) error {

	var val string
	err := unmarshal(&val)
	if err != nil {
		return err
	}

	parts := strings.Split(val, ".")
	if len(parts) != 2 {
		return fmt.Errorf("invalid role binding '%s', format is <scope>.<role>", val)
	}
	m.Scope = Scope(parts[0])
	m.Role = RoleKind(parts[1])

	return nil

}

type PortalOperationPolicy struct {
	Read   []RoleBinding
	Update []RoleBinding
}

type IdentityOperationPolicy struct {
	Read        []RoleBinding
	Register    []RoleBinding
	Unregister  []RoleBinding
	UpdateState []RoleBinding
	Init        []RoleBinding
}

type CrudOperationPolicy struct {
	Create []RoleBinding
	Read   []RoleBinding
	Update []RoleBinding
	Delete []RoleBinding
}

type OrganizationOperationPolicy struct {
	CrudOperationPolicy `yaml:",inline"`
	AddProject          []RoleBinding
	UpdateState         []RoleBinding
	WriteStorage        []RoleBinding
}

type XdcOperationPolicy struct {
	Spawn   []RoleBinding
	Destroy []RoleBinding
	Attach  []RoleBinding
	Detach  []RoleBinding
}

type CarrOperationPolicy struct {
	Create  []RoleBinding
	Accept  []RoleBinding
	Reject  []RoleBinding
	Release []RoleBinding
}

type CrudcdOperationPolicy struct {
	Create       []RoleBinding
	Read         []RoleBinding
	Update       []RoleBinding
	Delete       []RoleBinding
	Commission   []RoleBinding
	Decommission []RoleBinding
}

type PoolOperationPolicy struct {
	Create             []RoleBinding
	Read               []RoleBinding
	UpdateProject      []RoleBinding
	UpdateOrganization []RoleBinding
	UpdateFacility     []RoleBinding
	Delete             []RoleBinding
}

type CreateDestroyOperationPolicy struct {
	Create  []RoleBinding
	Destroy []RoleBinding
}

type CrudOp func(CrudOperationPolicy) []RoleBinding

func CrudCreate(x CrudOperationPolicy) []RoleBinding { return x.Create }
func CrudRead(x CrudOperationPolicy) []RoleBinding   { return x.Read }
func CrudUpdate(x CrudOperationPolicy) []RoleBinding { return x.Update }
func CrudDelete(x CrudOperationPolicy) []RoleBinding { return x.Delete }

type Object interface {
	UserRoles(*portal.User) ([]RoleBinding, error)
}

var (
	policyPath string = "/etc/merge/policy.yml"
)

func SetPolicyPath(p string) {
	policyPath = p
}

func PolicyFile() string {
	return policyPath
}

func GetPolicy() Policy {

	// TODO: replace these panic()s with something saner.
	if policy == nil {
		src, err := os.ReadFile(policyPath)
		if err != nil {
			panic(fmt.Sprintf("could not read policy file: %v", err))
		}
		log.Debug("Loaded policy from " + policyPath)

		p := &Policy{}
		err = yaml.Unmarshal([]byte(src), p)
		if err != nil {
			panic(fmt.Sprintf("could not parse policy file: %v", err))
		}

		err = ValidatePolicy(*p)
		if err != nil {
			panic(fmt.Sprintf("invalid policy: %v", err))
		}

		policy = p
	}

	return *policy

}

func ValidatePolicy(p Policy) error {

	for mode, cop := range p.Project {
		if err := ValidateMode(mode); err != nil {
			return err
		}
		if err := ValidateCrudOperationPolicy(cop); err != nil {
			return err
		}
	}

	return nil

}

func ValidateCrudOperationPolicy(c CrudOperationPolicy) error {

	if err := ValidateRoleBindings(c.Create); err != nil {
		return err
	}
	if err := ValidateRoleBindings(c.Read); err != nil {
		return err
	}
	if err := ValidateRoleBindings(c.Update); err != nil {
		return err
	}
	if err := ValidateRoleBindings(c.Delete); err != nil {
		return err
	}

	return nil

}

func ValidateRoleBindings(bs []RoleBinding) error {

	for _, binding := range bs {
		if err := ValidateBinding(binding); err != nil {
			return err
		}
	}

	return nil

}

func ValidateBinding(b RoleBinding) error {

	valid := false
	for _, s := range Scopes {
		if b.Scope == s {
			valid = true
			break
		}
	}
	if !valid {
		return fmt.Errorf("invalid context %s, must be one of %v", b.Scope, Scopes)
	}

	return nil
}

func ValidateMode(m Mode) error {

	if m < 0 || m > Private {
		return fmt.Errorf(
			"invalid mode %d: must be positive number <= %d", m, Private)
	}

	return nil
}

func Satisfies(provided, required RoleBinding) bool {

	if required.Scope != AnyScope && provided.Scope != required.Scope {
		return false
	}

	switch required.Role {
	case CreatorRole:
		return provided.Role == CreatorRole

	case MaintainerRole:
		return provided.Role == MaintainerRole ||
			provided.Role == CreatorRole

	case MemberRole:
		return provided.Role == MemberRole ||
			provided.Role == MaintainerRole ||
			provided.Role == CreatorRole

	case AnyRole:
		return true
	}

	log.WithFields(log.Fields{"role": required.Role}).Error("invalid role")
	return false
}

// Policy about policy itself
func ReadPolicy(caller *identity.IdentityTraits) error {

	if caller.Admin == true {
		return nil
	}

	return fmt.Errorf("permission denied")
}
