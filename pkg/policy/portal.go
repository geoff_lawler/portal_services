package policy

import (
	log "github.com/sirupsen/logrus"
	portal "gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/portal/services/pkg/identity"
)

// Policy Object interface implementation ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// ProjectObject ...
type PortalObject struct {
	Identity *identity.IdentityTraits
}

// UserRoles for projects
func (p PortalObject) UserRoles(u *portal.User) ([]RoleBinding, error) {

	result := []RoleBinding{}

	if u.Admin {
		result = append(result, RoleBinding{PortalScope, CreatorRole})
	}

	return result, nil
}

func ReadPortal(caller *identity.IdentityTraits) error {

	requirements := GetPolicy().Portal.Read
	log.Debugf("policy req: %v", requirements)
	return Authorize(caller, requirements, PortalObject{Identity: caller})
}

func UpdatePortal(caller *identity.IdentityTraits) error {

	requirements := GetPolicy().Portal.Update
	return Authorize(caller, requirements, PortalObject{Identity: caller})
}
