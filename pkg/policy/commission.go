package policy

import (
	portal "gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/portal/services/pkg/identity"
	"gitlab.com/mergetb/portal/services/pkg/storage"
)

type FacilityObject struct {
	*portal.Facility
}

func (o FacilityObject) UserRoles(u *portal.User) ([]RoleBinding, error) {

	f := storage.NewFacility(o.Name)
	err := f.Read()
	if err != nil {
		return nil, err
	}

	roles := []RoleBinding{}

	if m, ok := f.Members[u.Username]; ok {

		if m.State == portal.Member_Active {

			switch m.Role {
			case portal.Member_Creator:
				roles = append(roles, RoleBinding{FacilityScope, CreatorRole})

			case portal.Member_Maintainer:
				roles = append(roles, RoleBinding{FacilityScope, MaintainerRole})

			case portal.Member_Member:
				roles = append(roles, RoleBinding{FacilityScope, MemberRole})
			}
		}
	}

	if u.Admin {
		roles = append(roles, RoleBinding{FacilityScope, CreatorRole})
	}

	return roles, nil
}

func CreateFacility(caller *identity.IdentityTraits) error {

	// Cannot specify an AccessMode if the thing does not exist yet,
	// so use Public
	reqs := GetPolicy().Facility[Mode(Public)].Create
	return AuthorizeCreate(caller, reqs, FacilityScope)
}

func ReadFacility(caller *identity.IdentityTraits, facility string) error {

	f := storage.NewFacility(facility)
	err := f.Read()
	if err != nil {
		return err
	}

	reqs := GetPolicy().Facility[Mode(f.AccessMode)].Read

	return Authorize(caller, reqs, FacilityObject{f.Facility})
}

func UpdateFacility(caller *identity.IdentityTraits, facility string) error {

	f := storage.NewFacility(facility)
	err := f.Read()
	if err != nil {
		return err
	}

	reqs := GetPolicy().Facility[Mode(f.AccessMode)].Update

	return Authorize(caller, reqs, FacilityObject{f.Facility})
}

func DeleteFacility(caller *identity.IdentityTraits, facility string) error {

	f := storage.NewFacility(facility)
	err := f.Read()
	if err != nil {
		return err
	}

	reqs := GetPolicy().Facility[Mode(f.AccessMode)].Delete
	return Authorize(caller, reqs, FacilityObject{f.Facility})
}
