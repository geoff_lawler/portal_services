package policy

import (
	"context"
	"fmt"
	"os"
	"testing"

	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	portal "gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/portal/services/internal"
	"gitlab.com/mergetb/portal/services/pkg/identity"
	"gitlab.com/mergetb/portal/services/pkg/storage"
	clientv3 "go.etcd.io/etcd/client/v3"
)

func InitLogging() {
	value, ok := os.LookupEnv("LOGLEVEL")
	if ok {
		lvl, err := log.ParseLevel(value)
		if err != nil {
			log.SetLevel(log.InfoLevel)
			log.Errorf("bad LOGLEVEL env var: %s. ignoring", value)
		} else {
			log.Infof("setting log level to %s", value)
			log.SetLevel(lvl)
		}
	}
}

func etcdInit() error {

	endpoint := os.Getenv("TEST_ETCD_ENDPOINT") // may be set be CI or other scripts
	if endpoint == "" {
		endpoint = "localhost:2379"
	}

	cli, err := clientv3.New(clientv3.Config{
		Endpoints: []string{endpoint},
	})
	if err != nil {
		return fmt.Errorf("etcd client: %v", err)
	}

	storage.EtcdClient = cli

	return nil
}

func TestMain(m *testing.M) {

	err := etcdInit()
	if err != nil {
		fmt.Printf("init error: %v", err)
		os.Exit(1)
	}

	internal.InitLogging()

	SetPolicyPath("./policy.yml")

	os.Exit(m.Run())
}

// Organization policy.
func TestOrgUserInitPolicy(t *testing.T) {

	var rbs storage.RbStack
	var rb *storage.Rollback
	a := assert.New(t)
	oid := "theorg"

	defer func() {
		rbs.Unwind()
		checkPristine(t)
	}()

	// different users/ids
	orgcreator := userCreate(a, &rbs, "orgcreator", false)
	orgmaintainer := userCreate(a, &rbs, "orgmaintainer", false)
	admin := userCreate(a, &rbs, "admin", true)
	user := userCreate(a, &rbs, "user", false)
	nonuser := identity.IdentityTraits{Username: "nonuser"}

	org, rb := orgCreate(a, oid, orgcreator.Username, orgmaintainer.Username)
	a.NotNil(org)
	rbs = rbs.Push(rb)

	// Add a non-user as a requested member.
	_, rb = orgAddUserMembers(a, oid, portal.Member_MemberRequested, nonuser.Username)
	rbs = rbs.Push(rb)

	// confirm org creator can init the user.
	err := InitUser(orgcreator, nonuser.Username)
	a.Nil(err, "orgcreator.init.nonuser")

	// confirm org maintainer can init the user.
	err = InitUser(orgmaintainer, nonuser.Username)
	a.Nil(err, "orgmaintainer.init.nonuser")

	// confirm admin init the user.
	err = InitUser(admin, nonuser.Username)
	a.Nil(err, "admin.init.nonuser")

	// confirm user cannot init the user
	err = InitUser(user, nonuser.Username)
	a.NotNil(err, "user.init.nonuser")

	// Add a user to the org so we can test Org admin control
	// over other ID actions.
	existingUser := userCreate(a, &rbs, "existinguser", false)
	_, rb = orgAddUserMembers(a, oid, portal.Member_Active, existingUser.Username)
	rbs = rbs.Push(rb)

	err = DeactivateUser(orgmaintainer, existingUser.Username)
	a.Nil(err)

	err = ActivateUser(orgmaintainer, existingUser.Username)
	a.Nil(err)

	err = UnregisterUser(orgmaintainer, existingUser.Username)
	a.Nil(err)

	err = RegisterUser(orgmaintainer, existingUser.Username)
	a.Nil(err)

	err = ReadIdentities(orgmaintainer)
	a.NotNil(err) // org admins cannot real all IDs.

	err = ReadIdentities(admin)
	a.Nil(err)
}

func TestOrgRead(t *testing.T) {
	// OrgRead is an odd one so it gets its own test.
	var rbs storage.RbStack
	var rb *storage.Rollback
	a := assert.New(t)
	oid := "starfleet"

	defer func() {
		rbs.Unwind()
		checkPristine(t)
	}()

	// different users/ids
	creator := userCreate(a, &rbs, "creator", false)

	// Create project with creator
	_, rb = orgCreate(a, oid, creator.Username)
	rbs = rbs.Push(rb)

	rb = setOrgMode(a, oid, portal.AccessMode_Public)
	rbs = rbs.Push(rb)

	err := ReadOrganization(nil, oid)
	a.Nil(err)
}

func TestOrgCRUD(t *testing.T) {

	var rbs storage.RbStack
	var rb *storage.Rollback
	a := assert.New(t)
	oid := "starfleet"

	defer func() {
		rbs.Unwind()
		checkPristine(t)
	}()

	// different users/ids
	creator := userCreate(a, &rbs, "creator", false)
	maintainer := userCreate(a, &rbs, "maintainer", false)
	member := userCreate(a, &rbs, "member", false)
	admin := userCreate(a, &rbs, "admin", true)
	nonmember := userCreate(a, &rbs, "nonmember", false)

	// Create project with creator and maintainer as members.
	o, rb := orgCreate(a, oid, creator.Username, maintainer.Username)
	rbs = rbs.Push(rb)

	o.UpdateRequest = &portal.UpdateOrganizationRequest{
		Members: &portal.MembershipUpdate{
			Set: map[string]*portal.Member{
				member.Username: {
					Role:  portal.Member_Member,
					State: portal.Member_Active,
				},
			},
		},
	}

	// add member
	rb, err := o.Update()
	a.Nil(err)
	rbs = rbs.Push(rb)

	type args struct {
		Id       *identity.IdentityTraits
		expected bool
	}

	type expTest struct {
		Test func(uid *identity.IdentityTraits, pid string) error
		Desc string
		Args []args
	}

	type modeRun struct {
		mode  portal.AccessMode
		tests []expTest
	}

	// CreateOrganization has a different set of args so make wrappers for it.
	// TODO: update create org to match all other Create funcions.
	createPub := func(user *identity.IdentityTraits, org string) error {
		return CreateOrganization(user, org, portal.AccessMode_Public)
	}
	createProtect := func(user *identity.IdentityTraits, org string) error {
		return CreateOrganization(user, org, portal.AccessMode_Protected)
	}
	createPriv := func(user *identity.IdentityTraits, org string) error {
		return CreateOrganization(user, org, portal.AccessMode_Private)
	}

	// set up tests for all CRUD functions at all access levels.
	modes := []modeRun{
		{
			mode: portal.AccessMode_Public,
			tests: []expTest{
				{
					Test: createPub,
					Desc: "CreateOrganization",
					Args: []args{
						{admin, true},
						{nonmember, true},
						{creator, true},
						{maintainer, true},
						{member, true},
					},
				},
				{
					Test: ReadOrganization,
					Desc: "ReadOrganization",
					Args: []args{
						{creator, true},
						{maintainer, true},
						{member, true},
						{admin, true},
						{nonmember, true},
					},
				},
				{
					Test: UpdateOrganization,
					Desc: "UpdateOrganization",
					Args: []args{
						{creator, true},
						{maintainer, true},
						{member, false},
						{admin, true},
						{nonmember, false},
					},
				},
				{
					Test: DeleteOrganization,
					Desc: "DeleteOrganization",
					Args: []args{
						{creator, true},
						{maintainer, true},
						{member, false},
						{admin, true},
						{nonmember, false},
					},
				},
				{
					Test: ActivateOrganization,
					Desc: "ActivateOrganization",
					Args: []args{
						{creator, false},
						{maintainer, false},
						{member, false},
						{admin, true},
						{nonmember, false},
					},
				},
				{
					Test: DeactivateOrganization,
					Desc: "DeactivateOrganization",
					Args: []args{
						{creator, false},
						{maintainer, false},
						{member, false},
						{admin, true},
						{nonmember, false},
					},
				},
				{
					Test: AddOrganizationProject,
					Desc: "AddOrganizationProject",
					Args: []args{
						{creator, true},
						{maintainer, true},
						{member, true},
						{admin, true},
						{nonmember, false},
					},
				},
			},
		}, {
			mode: portal.AccessMode_Protected,
			tests: []expTest{
				{
					Test: createProtect,
					Desc: "CreateOrganization",
					Args: []args{
						{creator, true},
						{maintainer, true},
						{member, true},
						{admin, true},
						{nonmember, true},
					},
				}, {
					Test: ReadOrganization,
					Desc: "ReadOrganization",
					Args: []args{
						{creator, true},
						{maintainer, true},
						{member, true},
						{admin, true},
						{nonmember, false},
					},
				}, {
					Test: UpdateOrganization,
					Desc: "UpdateOrganization",
					Args: []args{
						{creator, true},
						{maintainer, true},
						{member, false},
						{admin, true},
						{nonmember, false},
					},
				}, {
					Test: DeleteOrganization,
					Desc: "DeleteOrganization",
					Args: []args{
						{creator, true},
						{maintainer, true},
						{member, false},
						{admin, true},
						{nonmember, false},
					},
				},
				{
					Test: ActivateOrganization,
					Desc: "ActivateOrganization",
					Args: []args{
						{creator, false},
						{maintainer, false},
						{member, false},
						{admin, true},
						{nonmember, false},
					},
				},
				{
					Test: DeactivateOrganization,
					Desc: "DeactivateOrganization",
					Args: []args{
						{creator, false},
						{maintainer, false},
						{member, false},
						{admin, true},
						{nonmember, false},
					},
				},
				{
					Test: AddOrganizationProject,
					Desc: "AddOrganizationProject",
					Args: []args{
						{creator, true},
						{maintainer, true},
						{member, true},
						{admin, true},
						{nonmember, false},
					},
				},
			},
		}, {
			mode: portal.AccessMode_Private,
			tests: []expTest{
				{
					Test: createPriv,
					Desc: "CreateOrganization",
					Args: []args{
						{creator, true},
						{maintainer, true},
						{member, true},
						{admin, true},
						{nonmember, true},
					},
				}, {
					Test: ReadOrganization,
					Desc: "ReadOrganization",
					Args: []args{
						{creator, true},
						{maintainer, true},
						{member, false},
						{admin, true},
						{nonmember, false},
					},
				}, {
					Test: UpdateOrganization,
					Desc: "UpdateOrganization",
					Args: []args{
						{creator, true},
						{maintainer, true},
						{member, false},
						{admin, true},
						{nonmember, false},
					},
				}, {
					Test: DeleteOrganization,
					Desc: "DeleteOrganization",
					Args: []args{
						{creator, true},
						{maintainer, true},
						{member, false},
						{admin, true},
						{nonmember, false},
					},
				},
				{
					Test: ActivateOrganization,
					Desc: "ActivateOrganization",
					Args: []args{
						{creator, false},
						{maintainer, false},
						{member, false},
						{admin, true},
						{nonmember, false},
					},
				},
				{
					Test: DeactivateOrganization,
					Desc: "DeactivateOrganization",
					Args: []args{
						{creator, false},
						{maintainer, false},
						{member, false},
						{admin, true},
						{nonmember, false},
					},
				},
				{
					Test: AddOrganizationProject,
					Desc: "AddOrganizationProject",
					Args: []args{
						{creator, true},
						{maintainer, true},
						{member, false},
						{admin, true},
						{nonmember, false},
					},
				},
			},
		},
	}

	for _, m := range modes {
		mstr := m.mode.String()
		t.Logf("Setting mode %s", mstr)
		rb = setOrgMode(a, oid, m.mode)
		rbs = rbs.Push(rb)
		for _, tt := range m.tests {
			for _, as := range tt.Args {
				t.Logf("Test: %s.%s.%s. Expected: %t", mstr, tt.Desc, as.Id.Username, as.expected)
				err := tt.Test(as.Id, oid)
				if as.expected == true {
					a.Nil(err, mstr+"."+tt.Desc+"."+as.Id.Username)
				} else {
					a.NotNil(err, mstr+"."+tt.Desc+"."+as.Id.Username)
				}
			}
		}
	}
}

// test if org maintainers can control project CRUD.
func TestOrgProjectControl(t *testing.T) {

	var rbs storage.RbStack
	var rb *storage.Rollback
	a := assert.New(t)
	oid := "theorg"
	pid := "theproj"

	defer func() {
		rbs.Unwind()
		checkPristine(t)
	}()

	// different users/ids
	orgcreator := userCreate(a, &rbs, "orgcreator", false)
	orgmaintainer := userCreate(a, &rbs, "orgmaintainer", false)
	projcreator := userCreate(a, &rbs, "projcreator", false)

	// create the org
	org, rb := orgCreate(a, oid, orgcreator.Username, orgmaintainer.Username)
	a.NotNil(org)
	rbs = rbs.Push(rb)

	// Create project with creator and maintainer as members.
	_, rb = projectCreate(a, pid, projcreator.Username)
	rbs = rbs.Push(rb)

	// set the project as requested member
	org, rb = orgAddProjMembers(a, oid, portal.Member_EntityRequested, pid)
	rbs = rbs.Push(rb)

	// confirm the orgcreator can confirm the project. This requires update permissions
	// for org and project.
	err := UpdateOrganization(orgcreator, oid)
	a.Nil(err, "org creator update own organization")

	err = UpdateProject(orgcreator, pid)
	a.Nil(err, "org creator update requested project")

	// Now set the project as an active member.
	org.UpdateRequest = &portal.UpdateOrganizationRequest{
		Projects: &portal.MembershipUpdate{
			Set: map[string]*portal.Member{
				pid: {
					Role:  portal.Member_Member,
					State: portal.Member_Active,
				},
			},
		},
	}
	rb, err = org.Update()
	rbs = rbs.Push(rb)
	a.Nil(err, "org update project to active member")

	// reread org to get updates.
	err = org.Read()
	a.Nil(err)

	type args struct {
		Id       *identity.IdentityTraits
		expected bool
	}

	type expTest struct {
		Test func(uid *identity.IdentityTraits, pid string) error
		Desc string
		Args []args
	}

	type modeRun struct {
		mode  portal.AccessMode
		tests []expTest
	}

	// set up tests for all CRUD functions at all access levels.
	modes := []modeRun{
		{
			mode: portal.AccessMode_Public,
			tests: []expTest{
				{
					Test: ReadProject,
					Desc: "ReadProject",
					Args: []args{
						{orgcreator, true},
						{orgmaintainer, true},
						{projcreator, true},
					},
				},
				{
					Test: UpdateProject,
					Desc: "UpdateProject",
					Args: []args{
						{orgcreator, true},
						{orgmaintainer, true},
						{projcreator, true},
					},
				},
				{
					Test: DeleteProject,
					Desc: "DeleteProject",
					Args: []args{
						{orgcreator, true},
						{orgmaintainer, true},
						{projcreator, true},
					},
				},
			},
		},
		{
			mode: portal.AccessMode_Protected,
			tests: []expTest{
				{
					Test: ReadProject,
					Desc: "ReadProject",
					Args: []args{
						{orgcreator, true},
						{orgmaintainer, true},
						{projcreator, true},
					},
				},
				{
					Test: UpdateProject,
					Desc: "UpdateProject",
					Args: []args{
						{orgcreator, true},
						{orgmaintainer, true},
						{projcreator, true},
					},
				},
				{
					Test: DeleteProject,
					Desc: "DeleteProject",
					Args: []args{
						{orgcreator, true},
						{orgmaintainer, true},
						{projcreator, true},
					},
				},
			},
		},
		{
			mode: portal.AccessMode_Private,
			tests: []expTest{
				{
					Test: ReadProject,
					Desc: "ReadProject",
					Args: []args{
						{orgcreator, true},
						{orgmaintainer, true},
						{projcreator, true},
					},
				},
				{
					Test: UpdateProject,
					Desc: "UpdateProject",
					Args: []args{
						{orgcreator, true},
						{orgmaintainer, false},
						{projcreator, true},
					},
				},
				{
					Test: DeleteProject,
					Desc: "DeleteProject",
					Args: []args{
						{orgcreator, true},
						{orgmaintainer, false},
						{projcreator, true},
					},
				},
			},
		},
	}

	for _, m := range modes {

		mstr := m.mode.String()
		t.Logf("Setting mode %s", mstr)
		rb = setProjMode(a, pid, m.mode)
		rbs = rbs.Push(rb)

		for _, tt := range m.tests {
			for _, as := range tt.Args {
				t.Logf("Test: %s.%s.%s. Expected: %t", mstr, tt.Desc, as.Id.Username, as.expected)
				err := tt.Test(as.Id, pid)
				if as.expected == true {
					a.Nil(err, mstr+"."+tt.Desc+"."+as.Id.Username)
				} else {
					a.NotNil(err, mstr+"."+tt.Desc+"."+as.Id.Username)
				}
			}
		}
	}
}

// Project policy.
func TestProjectCRUD(t *testing.T) {

	var rbs storage.RbStack
	var rb *storage.Rollback
	a := assert.New(t)
	pid := "UFP"

	defer func() {
		rbs.Unwind()
		checkPristine(t)
	}()

	// different users/ids
	creator := userCreate(a, &rbs, "creator", false)
	maintainer := userCreate(a, &rbs, "maintainer", false)
	member := userCreate(a, &rbs, "member", false)
	admin := userCreate(a, &rbs, "admin", true)
	nonmember := userCreate(a, &rbs, "nonmember", false)

	// Create project with creator and maintainer as members.
	_, rb = projectCreate(a, pid, creator.Username, maintainer.Username)
	rbs = rbs.Push(rb)

	// and add the member as a project member.
	_, rb = addMembers(a, pid, member.Username)
	rbs = rbs.Push(rb)

	type args struct {
		Id       *identity.IdentityTraits
		expected bool
	}

	type expTest struct {
		Test func(uid *identity.IdentityTraits, pid string) error
		Desc string
		Args []args
	}

	type modeRun struct {
		mode  portal.AccessMode
		tests []expTest
	}

	// set up tests for all CRUD functions at all access levels.
	modes := []modeRun{
		{
			mode: portal.AccessMode_Public,
			tests: []expTest{
				{
					Test: CreateProject,
					Desc: "CreateProject",
					Args: []args{
						{admin, true},
						{nonmember, true},
						{creator, true},
						{maintainer, true},
						{member, true},
					},
				},
				{
					Test: ReadProject,
					Desc: "ReadProject",
					Args: []args{
						{creator, true},
						{maintainer, true},
						{member, true},
						{admin, true},
						{nonmember, true},
					},
				},
				{
					Test: UpdateProject,
					Desc: "UpdateProject",
					Args: []args{
						{creator, true},
						{maintainer, true},
						{member, false},
						{admin, true},
						{nonmember, false},
					},
				},
				{
					Test: DeleteProject,
					Desc: "DeleteProject",
					Args: []args{
						{creator, true},
						{maintainer, false},
						{member, false},
						{admin, true},
						{nonmember, false},
					},
				},
			},
		}, {
			mode: portal.AccessMode_Protected,
			tests: []expTest{
				{
					Test: CreateProject,
					Desc: "CreateProject",
					Args: []args{
						{creator, true},
						{maintainer, true},
						{member, true},
						{admin, true},
						{nonmember, true},
					},
				}, {
					Test: ReadProject,
					Desc: "ReadProject",
					Args: []args{
						{creator, true},
						{maintainer, true},
						{member, true},
						{admin, true},
						{nonmember, false},
					},
				}, {
					Test: UpdateProject,
					Desc: "UpdateProject",
					Args: []args{
						{creator, true},
						{maintainer, true},
						{member, false},
						{admin, true},
						{nonmember, false},
					},
				}, {
					Test: DeleteProject,
					Desc: "DeleteProject",
					Args: []args{
						{creator, true},
						{maintainer, false},
						{member, false},
						{admin, true},
						{nonmember, false},
					},
				},
			},
		}, {
			mode: portal.AccessMode_Private,
			tests: []expTest{
				{
					Test: CreateProject,
					Desc: "CreateProject",
					Args: []args{
						{creator, true},
						{maintainer, true},
						{member, true},
						{admin, true},
						{nonmember, true},
					},
				}, {
					Test: ReadProject,
					Desc: "ReadProject",
					Args: []args{
						{creator, true},
						{maintainer, false},
						{member, false},
						{admin, true},
						{nonmember, false},
					},
				}, {
					Test: UpdateProject,
					Desc: "UpdateProject",
					Args: []args{
						{creator, true},
						{maintainer, false},
						{member, false},
						{admin, true},
						{nonmember, false},
					},
				}, {
					Test: DeleteProject,
					Desc: "DeleteProject",
					Args: []args{
						{creator, true},
						{maintainer, false},
						{member, false},
						{admin, true},
						{nonmember, false},
					},
				},
			},
		},
	}

	for _, m := range modes {
		mstr := m.mode.String()
		t.Logf("Setting mode %s", mstr)
		rb = setProjMode(a, pid, m.mode)
		rbs = rbs.Push(rb)
		for _, tt := range m.tests {
			for _, as := range tt.Args {
				t.Logf("Test: %s.%s.%s. Expected: %t", mstr, tt.Desc, as.Id.Username, as.expected)
				err := tt.Test(as.Id, pid)
				if as.expected == true {
					a.Nil(err, mstr+"."+tt.Desc+"."+as.Id.Username)
				} else {
					a.NotNil(err, mstr+"."+tt.Desc+"."+as.Id.Username)
				}
			}
		}
	}
}

// Experiment policy.
func TestExperimentCRUD(t *testing.T) {

	var rbs storage.RbStack
	var rb *storage.Rollback
	a := assert.New(t)

	eid, pid := "enterprise", "UFP"

	defer func() {
		rbs.Unwind()
		checkPristine(t)
	}()

	// different users/ids
	projcreator := userCreate(a, &rbs, "projcreator", false)
	projmaintainer := userCreate(a, &rbs, "projmaintainer", false)
	projmember := userCreate(a, &rbs, "projmember", false)
	expcreator := userCreate(a, &rbs, "expcreator", false)
	expmaintainer := userCreate(a, &rbs, "expmaintainer", false)
	admin := userCreate(a, &rbs, "admin", true)
	nonmember := userCreate(a, &rbs, "nonmember", false)

	_, rb = projectCreate(a, pid, projcreator.Username, projmaintainer.Username)
	rbs = rbs.Push(rb)

	_, rb = addMembers(a, pid, projmember.Username, expcreator.Username, expmaintainer.Username)
	rbs = rbs.Push(rb)

	e, rb := experimentCreate(a, pid, eid, expcreator.Username, expmaintainer.Username)
	rbs = rbs.Push(rb)
	a.Equal(e.Creator, expcreator.Username)
	a.Contains(e.Maintainers, expmaintainer.Username)

	type args struct {
		Id       *identity.IdentityTraits
		expected bool
	}

	type expTest struct {
		Test func(uid *identity.IdentityTraits, pid, eid string) error
		Desc string
		Args []args
	}

	type modeRun struct {
		mode  portal.AccessMode
		tests []expTest
	}

	// default (public) update
	modes := []modeRun{
		{
			mode: portal.AccessMode_Public,
			tests: []expTest{
				{
					Test: CreateExperiment,
					Desc: "CreateExperiment",
					Args: []args{
						{projcreator, true},
						{projmaintainer, true},
						{projmember, true},
						{expcreator, true},
						{expmaintainer, true},
						{admin, true},
						{nonmember, false},
					},
				},
				{
					Test: ReadExperiment,
					Desc: "ReadExperiment",
					Args: []args{
						{projcreator, true},
						{projmaintainer, true},
						{projmember, true},
						{expcreator, true},
						{expmaintainer, true},
						{admin, true},
						{nonmember, true},
					},
				},
				{
					Test: UpdateExperiment,
					Desc: "UpdateExperiment",
					Args: []args{
						{projcreator, true},
						{projmaintainer, true},
						{projmember, false},
						{expcreator, true},
						{expmaintainer, true},
						{admin, true},
						{nonmember, false},
					},
				},
				{
					Test: DeleteExperiment,
					Desc: "DeleteExperiment",
					Args: []args{
						{projcreator, true},
						{projmaintainer, true},
						{projmember, false},
						{expcreator, true},
						{expmaintainer, false},
						{admin, true},
						{nonmember, false},
					},
				},
			},
		}, {
			mode: portal.AccessMode_Protected,
			tests: []expTest{
				{
					Test: CreateExperiment,
					Desc: "CreateExperiment",
					Args: []args{
						{projcreator, true},
						{projmaintainer, true},
						{projmember, true},
						{expcreator, true},
						{expmaintainer, true},
						{admin, true},
						{nonmember, false},
					},
				}, {
					Test: ReadExperiment,
					Desc: "ReadExperiment",
					Args: []args{
						{projcreator, true},
						{projmaintainer, true},
						{projmember, true},
						{expcreator, true},
						{expmaintainer, true},
						{admin, true},
						{nonmember, false},
					},
				}, {
					Test: UpdateExperiment,
					Desc: "UpdateExperiment",
					Args: []args{
						{projcreator, true},
						{projmaintainer, true},
						{projmember, false},
						{expcreator, true},
						{expmaintainer, true},
						{admin, true},
						{nonmember, false},
					},
				}, {
					Test: DeleteExperiment,
					Desc: "DeleteExperiment",
					Args: []args{
						{projcreator, true},
						{projmaintainer, true},
						{projmember, false},
						{expcreator, true},
						{expmaintainer, false},
						{admin, true},
						{nonmember, false},
					},
				},
			},
		}, {
			mode: portal.AccessMode_Private,
			tests: []expTest{
				{
					Test: CreateExperiment,
					Desc: "CreateExperiment",
					Args: []args{
						{projcreator, true},
						{projmaintainer, true},
						{projmember, true},
						{expcreator, true},
						{expmaintainer, true},
						{admin, true},
						{nonmember, false},
					},
				}, {
					Test: ReadExperiment,
					Desc: "ReadExperiment",
					Args: []args{
						{projcreator, true},
						{projmaintainer, true},
						{projmember, false},
						{expcreator, true},
						{expmaintainer, true},
						{admin, true},
						{nonmember, false},
					},
				}, {
					Test: UpdateExperiment,
					Desc: "UpdateExperiment",
					Args: []args{
						{projcreator, true},
						{projmaintainer, true},
						{projmember, false},
						{expcreator, true},
						{expmaintainer, true},
						{admin, true},
						{nonmember, false},
					},
				}, {
					Test: DeleteExperiment,
					Desc: "DeleteExperiment",
					Args: []args{
						{projcreator, true},
						{projmaintainer, true},
						{projmember, false},
						{expcreator, true},
						{expmaintainer, false},
						{admin, true},
						{nonmember, false},
					},
				},
			},
		},
	}

	for _, m := range modes {
		mstr := m.mode.String()
		t.Logf("Setting mode %s", mstr)
		rb = setExpMode(a, pid, eid, m.mode)
		rbs = rbs.Push(rb)
		for _, tt := range m.tests {
			for _, as := range tt.Args {
				t.Logf("Test: %s.%s.%s. Expected: %t", mstr, tt.Desc, as.Id.Username, as.expected)
				err := tt.Test(as.Id, pid, eid)
				if as.expected == true {
					a.Nil(err, fmt.Sprintf("%s.%s.%s: Allowed", mstr, tt.Desc, as.Id.Username))
				} else {
					a.NotNil(err, fmt.Sprintf("%s.%s.%s: Forbidden", mstr, tt.Desc, as.Id.Username))
				}
			}
		}
	}

}

// XDC policy.
func TestXDCCRUD(t *testing.T) {
	var rbs storage.RbStack
	var rb *storage.Rollback
	a := assert.New(t)
	pid, xid := "UFP", "enterprise"

	defer func() {
		rbs.Unwind()
		checkPristine(t)
	}()

	creator := userCreate(a, &rbs, "creator", false)
	maintainer := userCreate(a, &rbs, "maintainer", false)
	member := userCreate(a, &rbs, "member", false)
	np := userCreate(a, &rbs, "nonmember", false)

	_, rb = projectCreate(a, pid, creator.Username, maintainer.Username)
	rbs = rbs.Push(rb)

	_, rb = addMembers(a, pid, member.Username)
	rbs = rbs.Push(rb)

	// spawn
	req := &portal.CreateXDCRequest{
		Project: pid,
		Xdc:     xid,
	}

	// normal spawn
	err := SpawnXDC(member, req)
	a.Nil(err, member.Username)

	// admin spawn fail
	req.Image = "someimage"
	err = SpawnXDC(member, req)
	a.NotNil(err)

	// admin spawn success
	req.Image = ""
	err = SpawnXDC(member, req)
	a.Nil(err)

	// non project member spawn
	err = SpawnXDC(np, req)
	a.NotNil(err)

	// destroy
	x := storage.NewXDC(member.Username, xid, pid)
	rb, err = x.Create()
	a.Nil(err)
	rbs = rbs.Push(rb)

	err = DestroyXDC(member, pid, xid)
	a.Nil(err)

	err = DestroyXDC(maintainer, pid, xid) // project maintainer should succeed
	a.Nil(err)

	err = DestroyXDC(creator, pid, xid) // project creator should succeed
	a.Nil(err)

	err = DestroyXDC(np, pid, xid) // non proj member should fail
	a.NotNil(err)
}

// //
// // Realization policy.
// //
//
// // TODO
//
// //
// // Materialization policy.
// //
//
// // TODO
//
// //
// // Commission policy.
// //
//
// // TODO
//
// Facility policy.
func TestFacilityCRUD(t *testing.T) {

	var rbs storage.RbStack
	var rb *storage.Rollback
	a := assert.New(t)

	defer func() {
		rbs.Unwind()
		checkPristine(t)
	}()

	admin := &identity.IdentityTraits{Username: PolicyAdmin, Admin: true}
	nonadmin := &identity.IdentityTraits{Username: "nonadmin", Admin: false}

	fc := userCreate(a, &rbs, "facCreat", false)
	fm := userCreate(a, &rbs, "facMaint", false)

	// create the actual facility
	f, rb := facilityCreate(a, "ds9", "dsp.ufp.gov", fc.Username, fm.Username)
	rbs = rbs.Push(rb)

	//
	// Create policy test
	//
	err := CreateFacility(admin)
	a.Nil(err)

	err = CreateFacility(nonadmin)
	a.NotNil(err)

	//
	// Read
	//
	type Test struct {
		F        func(*identity.IdentityTraits, string) error
		id       *identity.IdentityTraits
		expected bool
	}

	tests := []Test{
		{ReadFacility, fc, true},
		{ReadFacility, fm, true},
		{ReadFacility, admin, true},
		{ReadFacility, nonadmin, false},
	}

	for _, tt := range tests {
		err := tt.F(tt.id, f.Name)
		if tt.expected == true {
			a.Nil(err, "readFac."+tt.id.Username)
		} else {
			a.NotNil(err)
		}
	}

	//
	// Update policy test
	//
	tests = []Test{
		{UpdateFacility, fc, true},
		{UpdateFacility, fm, true},
		{UpdateFacility, admin, true},
		{UpdateFacility, nonadmin, false},
	}

	for _, tt := range tests {
		err := tt.F(tt.id, f.Name)
		if tt.expected == true {
			a.Nil(err, "updateFac."+tt.id.Username)
		} else {
			a.NotNil(err, "updateFac."+tt.id.Username)
		}
	}

	//
	// Delete
	//
	tests = []Test{
		{DeleteFacility, fc, true},
		{DeleteFacility, fm, false},
		{DeleteFacility, admin, true},
		{DeleteFacility, nonadmin, false},
	}

	for _, tt := range tests {
		err := tt.F(tt.id, f.Name)
		if tt.expected == true {
			a.Nil(err, "deleteFac."+tt.id.Username)
		} else {
			a.NotNil(err, "deleteFac."+tt.id.Username)
		}
	}

	// TODO: repeat above tests for protected and private
}

// TODO

func userCreate(a *assert.Assertions, rbs *storage.RbStack, name string, admin bool) *identity.IdentityTraits {

	u := storage.NewUser(name)
	u.State = portal.UserState_Active
	u.Admin = admin
	rollback, err := u.Create()
	a.Nil(err)
	*rbs = (*rbs).Push(rollback)

	err = u.Read()
	a.Nil(err)

	id := &identity.IdentityTraits{Username: name}

	return id
}

func projectCreate(a *assert.Assertions, name string, creator string, maintainers ...string,
) (*storage.Project, *storage.Rollback) {

	p := storage.NewProject(name)
	p.Members = map[string]*portal.Member{
		creator: {
			Role:  portal.Member_Creator,
			State: portal.Member_Active,
		},
	}
	for _, m := range maintainers {
		p.Members[m] = &portal.Member{
			Role:  portal.Member_Maintainer,
			State: portal.Member_Active,
		}
	}

	rb, err := p.Create()
	a.Nil(err)

	return p, rb
}

func orgCreate(a *assert.Assertions, name string, creator string, maintainers ...string,
) (*storage.Organization, *storage.Rollback) {

	o := storage.NewOrganization(name)
	o.State = portal.UserState_Active
	o.Members = map[string]*portal.Member{
		creator: {
			Role:  portal.Member_Creator,
			State: portal.Member_Active,
		},
	}
	for _, m := range maintainers {
		o.Members[m] = &portal.Member{
			Role:  portal.Member_Maintainer,
			State: portal.Member_Active,
		}
	}

	rb, err := o.Create()
	a.Nil(err)

	return o, rb
}

func orgAddMembers(a *assert.Assertions, oid string, user bool, state portal.Member_State, members ...string) (*storage.Organization, *storage.Rollback) {

	o := storage.NewOrganization(oid)
	err := o.Read()
	a.Nil(err)

	set := make(map[string]*portal.Member)

	for _, m := range members {
		set[m] = &portal.Member{
			Role:  portal.Member_Member,
			State: state,
		}
	}

	if user {
		o.UpdateRequest = &portal.UpdateOrganizationRequest{
			Members: &portal.MembershipUpdate{
				Set: set,
			},
		}
	} else {
		o.UpdateRequest = &portal.UpdateOrganizationRequest{
			Projects: &portal.MembershipUpdate{
				Set: set,
			},
		}
	}

	rb, err := o.Update()
	a.Nil(err)

	err = o.Read()
	a.Nil(err)

	return o, rb
}

func orgAddUserMembers(a *assert.Assertions, oid string, state portal.Member_State, members ...string) (*storage.Organization, *storage.Rollback) {
	return orgAddMembers(a, oid, true, state, members...)
}

func orgAddProjMembers(a *assert.Assertions, oid string, state portal.Member_State, members ...string) (*storage.Organization, *storage.Rollback) {
	return orgAddMembers(a, oid, false, state, members...)
}

func addMembers(a *assert.Assertions, pid string, members ...string) (*storage.Project, *storage.Rollback) {

	p := storage.NewProject(pid)
	err := p.Read()
	a.Nil(err)

	set := make(map[string]*portal.Member)

	for _, uid := range members {
		set[uid] = &portal.Member{
			Role:  portal.Member_Member,
			State: portal.Member_Active,
		}
	}

	p.UpdateRequest = &portal.UpdateProjectRequest{
		Members: &portal.MembershipUpdate{
			Set: set,
		},
	}

	rb, err := p.Update()
	a.Nil(err)

	err = p.Read()
	a.Nil(err)

	return p, rb
}

func setExpMode(a *assert.Assertions, pid, eid string, mode portal.AccessMode) *storage.Rollback {

	e := storage.NewExperiment(eid, pid)
	err := e.Read()
	a.Nil(err)

	e.UpdateRequest = &portal.UpdateExperimentRequest{
		AccessMode: &portal.AccessModeUpdate{
			Value: mode,
		},
	}

	rb, err := e.Update()
	a.Nil(err)

	err = e.Read()
	a.Nil(err)

	return rb
}

func setProjMode(a *assert.Assertions, pid string, mode portal.AccessMode) *storage.Rollback {

	p := storage.NewProject(pid)
	err := p.Read()
	a.Nil(err)

	p.UpdateRequest = &portal.UpdateProjectRequest{
		AccessMode: &portal.AccessModeUpdate{
			Value: mode,
		},
	}

	rb, err := p.Update()
	a.Nil(err)

	err = p.Read()
	a.Nil(err)

	return rb
}

func setOrgMode(a *assert.Assertions, oid string, mode portal.AccessMode) *storage.Rollback {

	o := storage.NewOrganization(oid)
	err := o.Read()
	a.Nil(err)

	o.UpdateRequest = &portal.UpdateOrganizationRequest{
		AccessMode: &portal.AccessModeUpdate{
			Value: mode,
		},
	}

	rb, err := o.Update()
	a.Nil(err)

	err = o.Read()
	a.Nil(err)

	return rb
}

func experimentCreate(a *assert.Assertions, pid, name string, creator string, maintainers ...string,
) (*storage.Experiment, *storage.Rollback) {

	e := storage.NewExperiment(name, pid)
	e.Creator = creator
	e.Maintainers = append(e.Maintainers, maintainers...)

	rb, err := e.Create()
	a.Nil(err)

	return e, rb
}

func facilityCreate(a *assert.Assertions, name, address, creator string, maintainers ...string,
) (*storage.Facility, *storage.Rollback) {

	f := storage.NewFacility(name)
	f.Address = address
	f.Description = fmt.Sprintf("The %s facility", name)

	f.Members = map[string]*portal.Member{
		creator: {
			Role:  portal.Member_Creator,
			State: portal.Member_Active,
		},
	}
	for _, m := range maintainers {
		f.Members[m] = &portal.Member{
			Role:  portal.Member_Maintainer,
			State: portal.Member_Active,
		}
	}

	rb, err := f.Create()
	a.Nil(err)

	return f, rb
}
func checkPristine(t *testing.T) {

	kvc := clientv3.NewKV(storage.EtcdClient)

	resp, err := kvc.Get(context.TODO(), "", clientv3.WithPrefix())
	assert.Nil(t, err)

	assert.Equal(t, 0, len(resp.Kvs), "should be no keys in pristine state")
	if len(resp.Kvs) > 0 {
		for _, x := range resp.Kvs {
			t.Errorf("key %s should be gone", string(x.Key))
		}
		t.Error("pristine check failed")
	}

}
