package policy

import (
	log "github.com/sirupsen/logrus"
	portal "gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/portal/services/pkg/identity"
	me "gitlab.com/mergetb/portal/services/pkg/merror"
	"gitlab.com/mergetb/portal/services/pkg/storage"
)

// Policy Object interface implementation ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// OrganizationObject ...
type OrganizationObject struct {
	*portal.Organization
}

// UserRoles for organizations
func (o OrganizationObject) UserRoles(u *portal.User) ([]RoleBinding, error) {

	result := []RoleBinding{}

	if o.State == portal.UserState_Active {
		member, ok := o.Members[u.Username]
		if ok {
			switch member.State {

			case portal.Member_Pending:
				// no role bindings for pending members

			case portal.Member_MemberRequested:
				// no role bindings for pending members

			case portal.Member_EntityRequested:
				// no role bindings for pending members

			case portal.Member_Active:
				switch member.Role {

				case portal.Member_Creator:
					result = append(result, RoleBinding{OrganizationScope, CreatorRole})

				case portal.Member_Maintainer:
					result = append(result, RoleBinding{OrganizationScope, MaintainerRole})

				case portal.Member_Member:
					result = append(result, RoleBinding{OrganizationScope, MemberRole})

				}

			}
		}
	}

	if u.Admin {
		result = append(result, RoleBinding{OrganizationScope, CreatorRole})
	}

	return result, nil
}

// Policy API for Organizations ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// CreateOrganization policy
func CreateOrganization(user *identity.IdentityTraits, organization string, mode portal.AccessMode) error {
	requirements := GetPolicy().Organization[Mode(mode)].Create
	return AuthorizeCreate(user, requirements, OrganizationScope)
}

// ReadOrganization policy
//
// Note this method can be called by non-users, so we must handle that case. In
// that case, we treat the user an an AnyRole in the Organization scope.
func ReadOrganization(user *identity.IdentityTraits, organization string) error {

	o := storage.NewOrganization(organization)
	err := o.Read()
	if err != nil {
		return err
	}

	requirements := GetPolicy().Organization[Mode(o.AccessMode)].Read

	if user == nil {
		return AuthorizeAny(requirements, OrganizationScope)
	}

	return Authorize(user, requirements, OrganizationObject{o.Organization})
}

// UpdateOrganization policy
func UpdateOrganization(user *identity.IdentityTraits, organization string) error {

	o := storage.NewOrganization(organization)
	err := o.Read()
	if err != nil {
		return err
	}

	if o.GetVersion() == 0 {
		return me.NotFoundError("organization", organization)
	}

	log.Debugf("Update Org State: %+v", o)

	if o.State != portal.UserState_Active {
		return me.ForbiddenError("organization is not active")
	}

	requirements := GetPolicy().Organization[Mode(o.AccessMode)].Update

	return Authorize(user, requirements, OrganizationObject{o.Organization})
}

// DeleteOrganization policy
func DeleteOrganization(user *identity.IdentityTraits, organization string) error {

	o := storage.NewOrganization(organization)
	err := o.Read()
	if err != nil {
		return err
	}

	requirements := GetPolicy().Organization[Mode(o.AccessMode)].Delete
	return Authorize(user, requirements, OrganizationObject{o.Organization})
}

// AddOrganizationProject policy
func AddOrganizationProject(user *identity.IdentityTraits, organization string) error {

	o := storage.NewOrganization(organization)
	err := o.Read()
	if err != nil {
		return err
	}

	if o.State != portal.UserState_Active {
		return me.ForbiddenError("organization is not active")
	}

	requirements := GetPolicy().Organization[Mode(o.AccessMode)].AddProject
	return Authorize(user, requirements, OrganizationObject{o.Organization})
}

// ActivateOrganization policy
func ActivateOrganization(user *identity.IdentityTraits, organization string) error {

	o := storage.NewOrganization(organization)
	err := o.Read()
	if err != nil {
		return err
	}

	requirements := GetPolicy().Organization[Mode(o.AccessMode)].UpdateState
	return Authorize(user, requirements, OrganizationObject{o.Organization})
}

// DeactivateOrganization policy
func DeactivateOrganization(user *identity.IdentityTraits, organization string) error {

	o := storage.NewOrganization(organization)
	err := o.Read()
	if err != nil {
		return err
	}

	requirements := GetPolicy().Organization[Mode(o.AccessMode)].UpdateState
	return Authorize(user, requirements, OrganizationObject{o.Organization})
}

// WriteOrganizationStoragePolicy policy - given a list of usernames, can the list
// as a whole write to local organization storage?
func WriteOrganizationStoragePolicy(usernames []string, organization string) error {

	o := storage.NewOrganization(organization)
	err := o.Read()
	if err != nil {
		return err
	}

	if o.State != portal.UserState_Active {
		return me.ForbiddenError("organization is not active")
	}

	requirements := GetPolicy().Organization[Mode(o.AccessMode)].WriteStorage
	log.Debugf("Got write storage reqs: %v", requirements)

	orgObj := OrganizationObject{o.Organization}
	log.Debugf("Got write storage org: %v", orgObj)

	// iterate over users and return the most restrictive policy
	for _, u := range usernames {
		uid, err := identity.GetIdForUser(u)
		if err != nil {
			return err
		}

		log.Debugf("Checking auth for %s", uid.Traits.Username)
		err = Authorize(&uid.Traits, requirements, orgObj)
		if err != nil {
			log.Debugf("Auth failed")
			return err
		}

		log.Debugf("Auth succeeded")
	}

	return nil
}
