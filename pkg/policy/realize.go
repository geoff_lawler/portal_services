package policy

import (
	portal "gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/portal/services/pkg/identity"
	"gitlab.com/mergetb/portal/services/pkg/storage"
)

// RealizationObject ...
type RealizationObject struct {
	*portal.Realization
}

// UserRoles ...
func (o RealizationObject) UserRoles(u *portal.User) ([]RoleBinding, error) {

	roles, err := ProjectAndExperimentRoles(u, o.Pid, o.Eid)
	if err != nil {
		return nil, err
	}

	if u.Admin {
		roles = append(roles, RoleBinding{RealizationScope, CreatorRole})
	}

	r := storage.NewRealizeRequest(o.Id, o.Eid, o.Pid)
	err = r.Read()
	if err != nil {
		return nil, err
	}

	if r.Creator == u.Username {
		roles = append(roles, RoleBinding{RealizationScope, CreatorRole})
	}

	return roles, nil
}

func ReadRealizations(caller *identity.IdentityTraits, pid, eid string) error {
	// Reading realizations is the same as Read on the experiment
	// that the realizations are a part of.
	return ReadExperiment(caller, pid, eid)
}

func ReadRealization(caller *identity.IdentityTraits, pid, eid, rid string) error {
	// Reading realizations is the same as Read on the experiment
	// that the realizations are a part of.
	return ReadExperiment(caller, pid, eid)
}

func CreateRealization(caller *identity.IdentityTraits, pid, eid, rid string) error {
	policy := GetPolicy().Realization[Mode(Public)].Create
	return authorizeRealizationPolicy(caller, pid, eid, rid, policy)
}

func DeleteRealization(caller *identity.IdentityTraits, pid, eid, rid string) error {
	policy := GetPolicy().Realization[Mode(Public)].Release
	return authorizeRealizationPolicy(caller, pid, eid, rid, policy)
}

func AcceptRealization(caller *identity.IdentityTraits, pid, eid, rid string) error {
	policy := GetPolicy().Realization[Mode(Public)].Accept
	return authorizeRealizationPolicy(caller, pid, eid, rid, policy)
}

func RejectRealization(caller *identity.IdentityTraits, pid, eid, rid string) error {
	policy := GetPolicy().Realization[Mode(Public)].Reject
	return authorizeRealizationPolicy(caller, pid, eid, rid, policy)
}

func authorizeRealizationPolicy(caller *identity.IdentityTraits, pid, eid, rid string, policy []RoleBinding) error {

	rlz := &portal.Realization{
		Id:  rid,
		Pid: pid,
		Eid: eid,
	}

	return Authorize(caller, policy, RealizationObject{rlz})
}
