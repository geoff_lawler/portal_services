package identity

import (
	"crypto/tls"
	"net/http"
	"net/http/cookiejar"

	ory "github.com/ory/kratos-client-go"
)

var (
	// Well known locations of kratos services within this namespace.
	KratosPubURL   string = "http://kratos-public"
	KratosAdminURL string = "http://kratos-admin"
)

func KratosAdminCli() *ory.APIClient {

	conf := ory.NewConfiguration()
	conf.Servers = []ory.ServerConfiguration{{
		URL: KratosAdminURL,
	}}

	// We are inside k8s so we know who we are talking to here.
	// When run on an appliance with self-signed certs,
	// calls on this client API will fail as they cannot verify the
	// cert. So we tell it to ignore the cert. Woo!
	// TODO: make this a runtime config so you can untoggle it
	// on a non-appliance Portal. This is low priority.
	conf.HTTPClient = &http.Client{
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		},
	}

	return ory.NewAPIClient(conf)
}

func KratosPublicCli() *ory.APIClient {

	conf := ory.NewConfiguration()
	conf.Servers = []ory.ServerConfiguration{{
		URL: KratosPubURL,
	}}

	cj, _ := cookiejar.New(nil)
	conf.HTTPClient = &http.Client{Jar: cj}

	return ory.NewAPIClient(conf)
}
