package identity

func NewIdentity() *Identity {
	return &Identity{
		Id: "",
		Traits: IdentityTraits{
			Email:    "",
			Username: "",
			Admin:    false,
			Traits:   make(map[string]string),
		},
	}
}

type Identity struct {
	Id     string         `json:"id"`
	Traits IdentityTraits `json:"traits"`
}

// These stucts must match the identity schemas given to kratos.
//
// Here is Kratos internally generating this data and passing it around.
// API yaml Identity Traits: https://github.com/ory/kratos-client-go/blob/master/api/openapi.yaml
// && https://github.com/ory/kratos-client-go/blob/master/api/openapi.yaml#L2815.
// This generates: https://github.com/ory/kratos-client-go/blob/master/model_admin_create_identity_body.go#L31
// The type of these Traits are `map[string]interface{}`. This type is set by the Open API generator.
//
// The values that Merge Portal uses are here:
// https://gitlab.com/mergetb/portal/helm/-/blob/main/auth/values.yaml#L152
type IdentityTraits struct {
	Email    string            `json:"email"`
	Username string            `json:"username"`
	Admin    bool              `json:"admin"`
	Traits   map[string]string `json:"traits"`
}

// Make a new Identity
func NetIdentity() *Identity {
	return &Identity{
		Id: "",
		Traits: IdentityTraits{
			Email:    "",
			Username: "",
			Admin:    false,
			Traits:   make(map[string]string),
		},
	}
}

// If we add other schemas, we can put them here.
