package main

import (
	"context"

	log "github.com/sirupsen/logrus"
	xdcd "gitlab.com/mergetb/api/xdcd/v1/go"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func (x *XdcdServer) ClearTunnelData(
	ctx context.Context, req *xdcd.ClearTunnelDataRequest,
) (*xdcd.ClearTunnelDataResponse, error) {

	log.Info("Got clear tunnel data request")

	err := ClearTunnelData()
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	return &xdcd.ClearTunnelDataResponse{}, nil
}

func (x *XdcdServer) ConfigureTunnel(
	ctx context.Context, req *xdcd.ConfigureTunnelRequest,
) (*xdcd.ConfigureTunnelResponse, error) {

	log.Info("Got configure tunnel request")

	err := ConfigureTunnel(req.Nameserver, req.Rid)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	return &xdcd.ConfigureTunnelResponse{}, nil
}

func (x *XdcdServer) AddUsers(
	ctx context.Context, req *xdcd.AddUsersRequest,
) (*xdcd.AddUsersResponse, error) {

	log.Info("add users request")

	err := addUsers(req.Users, req.CommonGroups, req.Sudo, req.Interactive)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	return &xdcd.AddUsersResponse{}, nil
}

func (x *XdcdServer) DeleteUsers(
	ctx context.Context, req *xdcd.DeleteUsersRequest,
) (*xdcd.DeleteUsersResponse, error) {

	log.Info("delete users request")

	err := deleteUsers(req.Users)

	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	return &xdcd.DeleteUsersResponse{}, nil
}
func (x *XdcdServer) InitHost(
	ctx context.Context, req *xdcd.InitHostRequest,
) (*xdcd.InitHostResponse, error) {

	log.Info("init host")

	err := InitHost()
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	return &xdcd.InitHostResponse{}, nil
}
