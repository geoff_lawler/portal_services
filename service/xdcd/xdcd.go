package main

import (
	"errors"
	"fmt"
	"os"
	"os/exec"
	"strings"

	log "github.com/sirupsen/logrus"
)

const DNSMASQ_HELPER = "/usr/local/bin/dnsmasq_helper.sh"

func ClearTunnelData() error {

	if _, err := os.Stat(DNSMASQ_HELPER); errors.Is(err, os.ErrNotExist) {
		// File does not exist, move on. This will happen on ssh-jump containers.
		return nil
	}

	log.Infof("Clearing dnsmasq data")

	domains_re := `infra\.([^ ]*)  *(\1 *)?`
	out, err := exec.Command(DNSMASQ_HELPER, "clear", domains_re).CombinedOutput()
	if err != nil {
		return fmt.Errorf("error clearing dnsmasq configuration: %s, %s", string(out), err)
	}

	return nil
}

func ConfigureTunnel(ns, id string) error {

	if strings.Count(id, ".") != 2 {
		return fmt.Errorf("bad realization id format: %s. Should be rid.eid.pid.", id)
	}

	return updateDnsmasq(ns, id)
}

// setupDnsmasq inserts dnsmasq as an intermediary between the local resolve library and
// and the default resolver.  It makes it easy to add a special resolver for materialization
// internal domains
func setupDnsmasq() error {

	if _, err := os.Stat(DNSMASQ_HELPER); errors.Is(err, os.ErrNotExist) {
		// File does not exist, move on. This will happen on ssh-jump containers.
		return nil
	}

	log.Infof("setup dnsmasq")
	out, err := exec.Command(DNSMASQ_HELPER, "setup").CombinedOutput()
	if err != nil {
		return fmt.Errorf("error updating dnsmasq - setup: %s, %s", string(out), err)
	}
	return nil
}

// updateDnsmasq configure dnsmasq to use the nameserver ip address `ns` to be used only for
// resolving the materialization domain "infra."+`rid`.  Connection to that nameserver is
// currently tunneled and when this tunnel goes down we'll still be able to use the main k8s
// server in case we need to detach/reattach the xdc from inside the xdc.
// Here we also update the /etc/resolv.conf to put our materialization-internal domain first,
// so names like "a" and "b" can be resolved.
func updateDnsmasq(ns, rid string) error {

	if _, err := os.Stat(DNSMASQ_HELPER); errors.Is(err, os.ErrNotExist) {
		// File does not exist, move on. This will happen on ssh-jump containers.
		return nil
	}

	domain := "infra." + rid
	log.Infof("updating dnsmasq: nameserver: %s, domain: %s", ns, domain)
	// using shell script to do all the work
	out, err := exec.Command(DNSMASQ_HELPER, "newserver", ns, domain).CombinedOutput()
	if err != nil {
		return fmt.Errorf("error updating dnsmasq - newserver: %s, %s", string(out), err)
	}

	return nil
}
