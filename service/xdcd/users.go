package main

import (
	"fmt"
	"math/rand"
	"os"
	"os/exec"
	"os/user"
	"strconv"
	"strings"

	log "github.com/sirupsen/logrus"
	portal "gitlab.com/mergetb/api/portal/v1/go"
	xdcd "gitlab.com/mergetb/api/xdcd/v1/go"

	"golang.org/x/sync/errgroup"
)

// addUsers - add users
func addUsers(users []*portal.User, groups []*xdcd.GroupInfo, sudo, interactive bool) error {

	// we use newusers to add users. If it's not there abort.

	if _, err := exec.LookPath("newusers"); err != nil {
		return handleError(fmt.Errorf("required utility newusers not installed - unable to add new users"))
	}

	// Generate file for newusers. Format is the same as /etc/passwd.
	f, err := os.CreateTemp("tmp", "users")
	if err != nil {
		return handleError(fmt.Errorf("creating temp file: %w", err))
	}
	defer os.Remove(f.Name())

	shell := "/bin/bash"
	if !interactive {
		shell = "/sbin/nologin" // seems to be standard for non-interactive users.
	}

	usernames := []string{}

	for _, u := range users {

		if u.Uid == 0 || u.Gid == 0 {
			log.Warnf("addUsers: unsed gid/uid - uname=%s, uid=%d, gid=%d", u.Username, u.Uid, u.Gid)
		}
		// pw_name:pw_passwd:pw_uid:pw_gid:pw_gecos:pw_dir:pw_shell
		line := fmt.Sprintf("%s:%s:%d:%d::/home/%s:%s\n",
			u.Username,
			randomString(16),
			u.Uid,
			u.Gid,
			u.Username,
			shell,
		)

		_, err := f.WriteString(line)
		if err != nil {
			return handleError(fmt.Errorf("error writing user line: %w", err))
		}

		usernames = append(usernames, u.Username)
	}

	log.Infof("Creating users: %s", strings.Join(usernames, ", "))

	// create users.
	cmd := exec.Command("newusers", f.Name())
	o, err := cmd.CombinedOutput()
	if err != nil {
		return handleError(fmt.Errorf("%s: %s", "newusers", o))
	}

	// Now add to sudo group, if requested.
	if sudo {
		err := addToGroup("sudo", users)
		if err != nil {
			return err
		}
	}

	if interactive {
		// setup user mrg config. Do this in a go routine and do not care if it fails.
		// We want to return from this function as quickly as possible. So just
		// attempt the configuration and do not worry about errors.
		users := users
		go configUserClis(users)
	}

	if len(groups) > 0 {
		return setupCommonGroups(users, groups)
	}
	return nil
}

// setupCommonGroups create `groups` shared by `users` and update membership
func setupCommonGroups(users []*portal.User, groups []*xdcd.GroupInfo) error {
	// Create and add shared groups
	groupadd_path, err := exec.LookPath("groupadd")
	if err != nil {
		return handleError(fmt.Errorf("required utility `groupadd` not installed - unable to create shared groups"))
	}

	for _, grp_info := range groups {
		grp, err := user.LookupGroup(grp_info.GroupName)
		if err != nil {
			// assume the group doesn't exist
			log.Infof("creating common group: %s", grp_info.GroupName)
			cmd := exec.Command(groupadd_path, "-g", strconv.Itoa(int(grp_info.Gid)), grp_info.GroupName)
			o, err := cmd.CombinedOutput()
			if err != nil {
				return handleError(fmt.Errorf("groupadd for %s failed: %s", grp_info.GroupName, string(o)))
			}
		} else {
			log.Infof("common group already exists: %s", grp_info.GroupName)
			if grp.Gid != strconv.Itoa(int(grp_info.Gid)) {
				return handleError(fmt.Errorf("group %s already exists, but it's gid is %s, not %d",
					grp_info.GroupName, grp.Gid, grp_info.Gid))
			}
		}
		// add membership
		if err = addToGroup(grp_info.GroupName, users); err != nil {
			return handleError(fmt.Errorf("Unable to add membership to group %s: %w", grp_info.GroupName, err))
		}
	}
	return nil
}

// note no error return
func configUserClis(users []*portal.User) {

	for _, u := range users {
		configUserCli(u.Username)
	}
}

func addToGroup(group string, users []*portal.User) error {

	// The only clean way I've found to add many users to a group
	// is via gpasswd [group] -M user,user,user...user. This however
	// replaces all users. So we must first get the existing list, then
	// append new users to it. It may be easier to just edit /etc/group,
	// but less robust to system changes. Using system tools seems wiser.

	cmd := exec.Command("getent", "group", group)
	o, err := cmd.Output()
	if err != nil {
		return handleError(fmt.Errorf("Unable to get sudo group: %w", err))
	}

	// example output:
	// sudo:x:27:user084,user008,user073,user019,user011,user005,user037\n

	trimmed := strings.Trim(string(o), "\n")
	line := strings.Split(trimmed, ":")
	if len(line) < 3 {
		return handleError(fmt.Errorf("Unable to parse %s group", group))
	}

	// set of users.
	userSet := make(map[string]struct{})

	// add existing, if any. line[3] is the comma separated list of existing users in the group.
	if line[3] != "" {
		for _, u := range strings.Split(line[3], ",") {
			userSet[u] = struct{}{}
		}
	}

	// append new
	for _, u := range users {
		userSet[u.Username] = struct{}{}
	}

	newUsers := []string{}
	for key := range userSet {
		newUsers = append(newUsers, key)
	}

	cmd = exec.Command("gpasswd", group, "-M", strings.Join(newUsers, ","))
	o, err = cmd.CombinedOutput()
	if err != nil {
		// May not want to error out here...
		return handleError(fmt.Errorf("%s: %s: %w", "gpasswd", string(o), err))
	}

	log.Debugf("added to group %s: %s", group, strings.Join(newUsers, ","))

	return nil
}

func deleteUsers(users []*portal.User) error {

	// There is no equivelent to newusers that will delete a bunch of users.
	// So we do each one in a go routine to speed it up and hope there are
	// not 9238029834029348023 users to remove.

	eg := new(errgroup.Group)
	for _, u := range users {

		u := u

		eg.Go(func() error {

			// Note userdel will remove user from sudo if they are there.

			_, err := user.Lookup(u.Username)
			if err != nil {
				log.Debugf("User %s does not exist", u.Username)
			} else {
				cmd := exec.Command("userdel", u.Username)

				o, err := cmd.CombinedOutput()
				if err != nil {
					return handleError(fmt.Errorf("%s: %s", "userdel", o))
				}
			}

			_, err = user.LookupGroup(u.Username)
			if err != nil {
				log.Debugf("Group %s does not exist", u.Username)
			} else {
				cmd := exec.Command("groupdel", u.Username)

				o, err := cmd.CombinedOutput()
				if err != nil {
					return handleError(fmt.Errorf("%s: %s", "groupdel", o))
				}
			}

			return nil
		})
	}

	if err := eg.Wait(); err != nil {
		return err
	}

	return nil
}

func randomString(n int) string {

	var letter = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")

	b := make([]rune, n)
	for i := range b {
		b[i] = letter[rand.Intn(len(letter))]
	}
	return string(b)
}
