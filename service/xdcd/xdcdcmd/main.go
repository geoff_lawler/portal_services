package main

import (
	"context"
	"fmt"
	"strconv"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	portal "gitlab.com/mergetb/api/portal/v1/go"
	xdcd "gitlab.com/mergetb/api/xdcd/v1/go"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

var (
	srv     string
	Version = "undefined"
)

func init() {
	root.PersistentFlags().StringVarP(
		&srv, "server", "s", "0.0.0.0:6000", "Where the server is listening",
	)
}

var root = &cobra.Command{
	Use:   "xdcdcmd",
	Short: "command line interface to xdcd api",
}

func main() {

	ver := &cobra.Command{
		Use:   "version",
		Short: "Show version and exit",
		Args:  cobra.NoArgs,
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Printf("version: %s\n", Version)
		},
	}
	root.AddCommand(ver)

	clear := &cobra.Command{
		Use:   "clear",
		Short: "Clear existing tunnel meta-data",
		Args:  cobra.NoArgs,
		Run: func(cmd *cobra.Command, args []string) {
			clear()
		},
	}
	root.AddCommand(clear)

	confTunnel := &cobra.Command{
		Use:   "configtunnel rlz.exp.project",
		Short: "Configure the XDC to use the tunnel",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			configTunnel(args[0])
		},
	}
	root.AddCommand(confTunnel)

	var sudo, interactive bool
	adduser := &cobra.Command{
		Use:   "adduser <name> <uid> <gid> ... <name> <uid> <gid>",
		Short: "add a user or users to the system",
		Args:  cobra.MinimumNArgs(3),
		Run: func(cmd *cobra.Command, args []string) {
			addUsers(args, sudo, interactive)
		},
	}
	adduser.Flags().BoolVarP(&sudo, "sudo", "", true, "Allow the users to use sudo")
	adduser.Flags().BoolVarP(&interactive, "interactive", "", true, "Give the users an interactive shell")
	root.AddCommand(adduser)

	deluser := &cobra.Command{
		Use:   "deluser <name> ... <name>",
		Short: "remove a user  or users from the system",
		Args:  cobra.MinimumNArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			delUsers(args)
		},
	}
	root.AddCommand(deluser)

	init := &cobra.Command{
		Use:   "init",
		Short: "Configure the node for XDC use",
		Args:  cobra.NoArgs,
		Run: func(cmd *cobra.Command, args []string) {
			initHost()
		},
	}
	root.AddCommand(init)

	root.Execute()
}

func withClient(f func(xdcd.XdcdClient) error) error {

	conn, err := grpc.Dial(srv, grpc.WithTransportCredentials(insecure.NewCredentials()))

	if err != nil {
		return fmt.Errorf("dial: %s", err)
	}
	defer conn.Close()

	cli := xdcd.NewXdcdClient(conn)

	return f(cli)
}

func clear() {

	err := withClient(func(c xdcd.XdcdClient) error {

		_, err := c.ClearTunnelData(
			context.Background(),
			&xdcd.ClearTunnelDataRequest{},
		)
		return err
	})

	if err != nil {
		log.Fatal(err)
	}
}

func configTunnel(rid string) {

	err := withClient(func(c xdcd.XdcdClient) error {

		_, err := c.ConfigureTunnel(
			context.Background(),
			&xdcd.ConfigureTunnelRequest{
				Nameserver: "172.30.0.1", // TODO make configurable
				Rid:        rid,
			},
		)
		return err
	})

	if err != nil {
		log.Fatal(err)
	}
}

func initHost() {

	err := withClient(func(c xdcd.XdcdClient) error {

		_, err := c.InitHost(
			context.Background(),
			&xdcd.InitHostRequest{},
		)
		return err
	})

	if err != nil {
		log.Fatal(err)
	}
}

func addUsers(args []string, sudo, interactive bool) {

	if len(args)%3 != 0 {
		log.Fatal("Args must be of the form 'username uid gid' in triples")
	}

	users := []*portal.User{}

	for i := 0; i < len(args); i += 3 {

		u, err := strconv.Atoi(args[i+1])
		if err != nil {
			log.Fatal(err)
		}

		g, err := strconv.Atoi(args[i+2])
		if err != nil {
			log.Fatal(err)
		}

		users = append(users, &portal.User{
			Username: args[i],
			Uid:      uint32(u),
			Gid:      uint32(g),
		})
	}

	withClient(func(c xdcd.XdcdClient) error {

		_, err := c.AddUsers(
			context.TODO(),
			&xdcd.AddUsersRequest{
				Users:       users,
				Sudo:        sudo,
				Interactive: interactive,
			},
		)

		if err != nil {
			log.Fatal(err)
		}

		return nil
	})
}

func delUsers(args []string) {

	users := []*portal.User{}
	for _, u := range args {
		users = append(users, &portal.User{Username: u})
	}

	withClient(func(c xdcd.XdcdClient) error {

		_, err := c.DeleteUsers(
			context.TODO(),
			&xdcd.DeleteUsersRequest{
				Users: users,
			},
		)

		if err != nil {
			log.Fatal(err)
		}

		return nil
	})
}
