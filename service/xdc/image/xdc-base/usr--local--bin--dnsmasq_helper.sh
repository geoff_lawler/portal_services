#!/bin/bash

# This script is a helper for managing /etc/resolv.conf and dnsmasq.

show_help() {
	cat <<EOF
Helper script for managing /etc/resolv.conf and dnsmasq domain-specific servers

USAGE:

	$0 {setup|clear <domain_re>|newserver <ip> <domain>}

EOF
}

reload_dnsmasq_servers() {
	kill -HUP $(cat /run/dnsmasq/dnsmasq.pid) 2>/dev/null || {
		echo "dnsmasq isn't running" >&2
		return 1
	}
	return 0
}

cmd_setup() {
	local servers=$(grep ^nameserver /etc/resolv.conf | grep -v "nameserver 127.0.0.1" | sed 's/^nameserver */server=/')

	if [ -n "$servers" ]; then
		echo "$servers" >> /etc/dnsmasq.servers
		reload_dnsmasq_servers || return 1
	fi

	# make sure we've switched
	grep -q "^nameserver 127.0.0.1" /etc/resolv.conf ||
		echo "nameserver 127.0.0.1" >> /etc/resolv.conf
	# remove everything other than localhost
	sed '/^nameserver/ {/127.0.0.1/!d}' /etc/resolv.conf >/etc/resolv.conf.new
	if ! cmp -s /etc/resolv.conf /etc/resolv.conf.new; then
		cat /etc/resolv.conf.new >/etc/resolv.conf #xxx mv says file is busy
	fi
	rm /etc/resolv.conf.new
	return 0
}

cmd_clear() {
	local domain_re=$1
	sed -i '/^server=\//d' /etc/dnsmasq.servers
	sed -E "/^search/ { s/$domain_re *//g }" /etc/resolv.conf >/etc/resolv.conf.new &&
		cat /etc/resolv.conf.new > /etc/resolv.conf &&
		rm -f /etc/resolv.conf.new
	reload_dnsmasq_servers
}

cmd_newserver() {
	local serverip=$1 domain=$2
	echo "server=/$domain/$serverip" >> /etc/dnsmasq.servers
	reload_dnsmasq_servers
	# update /etc/resolv.conf
	# add domain, as well as the first sub-domain e.g. domain=infra.a.b.c, subdomain=a.b.c to search
	local subdomain=${domain#*.}
	sed "s/^search /search $domain $subdomain /" /etc/resolv.conf > /etc/resolv.conf.new
	cat /etc/resolv.conf.new > /etc/resolv.conf
	rm -f /etc/resolv.conf.new
}

# MAIN
cmd="$1"
case "$cmd" in
	( setup ) cmd_setup ;;
	( clear )
		[ "$#" == "2" ] || {
			echo "clear takes one argument: domain_regex" >&2
			exit 1
		}
		cmd_clear "$2" ;;
	( newserver )
		[ "$#" == "3" ] || {
			echo "newserver takes two arguments: ip and domain" >&2
			exit 1
		}
		cmd_newserver "$2" "$3" ;;
	( * ) show_help; exit 1 ;;
esac
