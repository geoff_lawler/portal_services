package main

import (
	"context"

	portal "gitlab.com/mergetb/api/portal/v1/go"
	xdcd "gitlab.com/mergetb/api/xdcd/v1/go"
	"gitlab.com/mergetb/portal/services/pkg/storage"
)

func initJump(svc string) error {

	// we note the jump machine in etcd so UserTask can find the ssh-jumps' xdcd instances.
	sj := storage.NewSSHJump(svc)
	err := sj.Read()
	if err != nil {
		return err
	}

	_, err = sj.Update()
	if err != nil {
		return err
	}

	users, err := storage.ListUsers()
	if err != nil {
		return err
	}

	ps := []*portal.User{}
	for _, u := range users {
		if u.State == portal.UserState_Active {
			ps = append(ps, u.User)
		}
	}

	if len(users) > 0 {
		err = withXdcdClient(svc, func(c xdcd.XdcdClient) error {
			_, err := c.AddUsers(
				context.TODO(),
				&xdcd.AddUsersRequest{
					Users:       ps,
					Sudo:        false,
					Interactive: true, // Seems odd, but the user needs a shell to be able to ssh through the machine.
				},
			)
			return err
		})

		if err != nil {
			return err
		}
	}

	err = withXdcdClient(svc, func(c xdcd.XdcdClient) error {
		_, err := c.InitHost(
			context.TODO(),
			&xdcd.InitHostRequest{},
		)
		return err
	})
	if err != nil {
		return err
	}

	return nil
}
