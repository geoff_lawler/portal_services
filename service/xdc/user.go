package main

import (
	"context"
	"regexp"

	log "github.com/sirupsen/logrus"
	portal "gitlab.com/mergetb/api/portal/v1/go"
	xdcd "gitlab.com/mergetb/api/xdcd/v1/go"
	"gitlab.com/mergetb/portal/services/pkg/storage"
	"gitlab.com/mergetb/tech/reconcile"
	"google.golang.org/protobuf/proto"
)

var (
	userBucket = storage.PrefixedBucket(&storage.User{})
	userKey    = regexp.MustCompile("^" + userBucket + nameExp + "$")
)

type UserTask struct {
	User string
}

func (ut *UserTask) Parse(key string) bool {

	tnks := userKey.FindAllStringSubmatch(key, -1)
	if len(tnks) > 0 {
		ut.User = tnks[0][1]
		return true
	}

	return false
}

func (ut *UserTask) Create(value []byte, version int64, td *reconcile.TaskData) *reconcile.TaskMessage {

	// Only thing we do at the moment is add the user to the ssh-jumps

	u := new(portal.User)
	err := proto.Unmarshal(value, u)
	if err != nil {
		return reconcile.TaskMessageError(err)
	}

	js, err := storage.ListSSHJumps()
	if err != nil {
		return reconcile.TaskMessageError(err)
	}

	for _, j := range js {

		// Do not add inactive accounts. Return error on non-active accounts.
		if u.State == portal.UserState_Active {
			err = withXdcdClient(j.Name, func(c xdcd.XdcdClient) error {
				_, err := c.AddUsers(
					context.TODO(),
					&xdcd.AddUsersRequest{
						Users:       []*portal.User{u},
						Sudo:        false,
						Interactive: true, // true as users need a shell to ssh through the jump box
					},
				)
				return err
			})
			if err != nil {
				return reconcile.TaskMessageError(err)
			}

		} else {
			return reconcile.TaskMessageWarningf("Not reconciling user task for inactive user: %s", u.Username)
		}
	}

	return nil
}

func (ut *UserTask) Update(prev, value []byte, version int64, td *reconcile.TaskData) *reconcile.TaskMessage {

	// Only thing we do at the moment is add the user to the ssh-jumps
	if td.Status.IsReconciled(td.TaskValue) {
		log.Info("Ignoring completed user put task")
		return nil
	}

	return ut.Create(value, version, td)
}

func (ut *UserTask) Ensure(prev, value []byte, version int64, td *reconcile.TaskData) *reconcile.TaskMessage {

	// Only thing we do at the moment is add the user to the ssh-jumps
	log.Infof("UserTask: ensure not implemented")

	return reconcile.TaskMessageUndefined()
}

func (ut *UserTask) Delete(value []byte, version int64, td *reconcile.TaskData) *reconcile.TaskMessage {

	u := new(portal.User)
	err := proto.Unmarshal(value, u)
	if err != nil {
		return reconcile.TaskMessageError(err)
	}

	js, err := storage.ListSSHJumps()
	if err != nil {
		return reconcile.TaskMessageError(err)
	}

	for _, j := range js {

		us := []*portal.User{u}
		err = withXdcdClient(j.Name, func(c xdcd.XdcdClient) error {
			_, err := c.DeleteUsers(
				context.TODO(),
				&xdcd.DeleteUsersRequest{Users: us},
			)
			return err
		})
		if err != nil {
			return reconcile.TaskMessageError(err)
		}
	}

	return nil
}
