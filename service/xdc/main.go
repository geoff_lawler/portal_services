package main

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/mergetb/portal/services/internal"
	"gitlab.com/mergetb/portal/services/pkg/storage"
)

func init() {
	internal.InitLogging()
	internal.InitReconciler()
}

var Version = ""
var podManagerName = "initxdcpod"

func main() {

	log.Infof("portal version: %s", Version)
	log.Infof("XDC Domain: %s", xdcDomain)
	log.Infof("XDC Namespace: %s", xdcNs)
	log.Infof("XDC Registry: %s", xdcRegistry)
	log.Infof("default XDC image: %s", defaultImage)

	log.Infof("Starting xdc service")

	err := storage.InitPortalEtcdClient()
	if err != nil {
		log.Fatalf("storage client init: %v", err)
	}

	go runPodWatch()

	runReconciler()
}
