package main

import (
	"context"
	"encoding/json"
	"fmt"
	"net"
	"os"
	"regexp"
	"strings"
	"time"

	"github.com/gofrs/uuid"
	log "github.com/sirupsen/logrus"
	portal "gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/portal/services/pkg/policy"
	"gitlab.com/mergetb/portal/services/pkg/storage"
	apps "k8s.io/api/apps/v1"
	v1 "k8s.io/api/core/v1"
	netv1 "k8s.io/api/networking/v1"
	errors "k8s.io/apimachinery/pkg/api/errors"
	resource "k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	k8intstr "k8s.io/apimachinery/pkg/util/intstr"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
)

var (
	k8c *kubernetes.Clientset

	ports = map[string]int32{
		"ssh":  22,
		"web":  8889,
		"xdcd": 6000,
	}

	xdcDomain   = ""
	xdcNs       = ""
	xdcRegistry = ""
	xdcImage    = ""

	xdcExtras = ""

	hostAliases = []v1.HostAlias{}

	mergefsPVName = ""
	stepcaCMName  = ""
)

// XdcOperator ...
type XdcOperator struct {
	// Where to pull the image from, fully pathed out.
	// e.g.: evilpea.org:4433/glawlerisi/xdc-jupyter:latest
	Image string

	// Limits are the maximum CPu/Memory resources an XDC is allowed. CPU will
	// be limited. Processes that take more memory than allowed will be killed.
	CpuLimit int64
	MemLimit int64

	// Requests are the minimum free resources on a host before an XDC can
	// be placed there. It should be less than the Limits above.
	CpuRequest int64
	MemRequest int64
}

func init() {

	log.Infof("init'ing xdc operator/controller")
	k8init()

	x, ok := os.LookupEnv("XDC_DOMAIN")
	if !ok {
		log.Fatal("XDC_DOMAIN must be defined in the environment")
	}
	xdcDomain = x

	x, ok = os.LookupEnv("XDC_NAMESPACE")
	if !ok {
		log.Fatal("XDC_NAMESPACE must be defined in the environment")
	}
	xdcNs = x

	x, ok = os.LookupEnv("XDC_REGISTRY")
	if !ok {
		log.Fatal("XDC_REGISTRY must be defined in the environment")
	}
	xdcRegistry = x

	x, ok = os.LookupEnv("XDC_IMAGE")
	if !ok {
		log.Info("XDC_IMAGE not found. Constructing it from other environment variables.")
		x = ""
	}
	xdcImage = x

	// Aliases given here will show up in the XDC /etc/hosts file.
	x, ok = os.LookupEnv("HOST_ALIASES")
	if ok {
		// Add the given host aliases.
		// Format is: ip,name,name,name:ip,name,...:ip,name...
		// ex: 192.168.126.10,grpc.mergetb.net,api.mergetb.net:1.2.3.4,foo.example.com
		entries := strings.Split(x, ":")

		for _, entry := range entries {
			tkns := strings.Split(entry, ",")
			hostAliases = append(hostAliases, v1.HostAlias{
				IP:        tkns[0],
				Hostnames: tkns[1:],
			})
		}
	}

	defaultImage = xdcRegistry + "/" + xdcNs + "/xdc-base:latest"
	if xdcImage != "" {
		defaultImage = xdcImage
	}

	x, ok = os.LookupEnv("MERGEFS_PVNAME")
	if !ok {
		log.Fatal("MERGEFS_PVNAME not found. Cannot continue")
		x = ""
	}
	mergefsPVName = x

	x, ok = os.LookupEnv("STEPCA_CMNAME")
	if !ok {
		log.Fatal("STEPCA_CMNAME not found. Cannot continue")
		x = ""
	}
	stepcaCMName = x

	x, ok = os.LookupEnv("XDC_EXTRAS_CM")
	if ok {
		xdcExtras = x
	}
}

func NewXdcOperator(x *portal.XDCStorage) *XdcOperator {

	r := &XdcOperator{
		CpuRequest: 100, // %1 of one cpu.
		MemRequest: 10,  // in MB
	}

	r.Image = x.Image
	r.CpuLimit = int64(x.CpuLimit)
	r.MemLimit = int64(x.MemLimit)

	return r
}

// Spawn ...
func (x *XdcOperator) Spawn(user, name, project string) error {

	l := log.WithFields(log.Fields{
		"proj": project,
		"name": name,
		"user": user,
	})
	l.Debug("spawn")

	// create deployment
	err := x.k8deploy(user, project, name)
	if err != nil {
		l.Errorf("failed to create k8s deployment :%+v", err)

		// Destroy any partially created deployment.
		if destroyErr := Destroy(name, project); destroyErr != nil {
			l.Errorf("destroying failed xdc deployment: %+v", destroyErr)
		}

		return err
	}

	fqdn := fmt.Sprintf("%s-%s.%s", name, project, xdcDomain)

	// wait 5 minutes for the deployment to become active by waiting for the pods ip address
	var podip string
	timeout := time.After(300 * time.Second)
	tick := time.Tick(500 * time.Millisecond)
	for done := false; !done; {
		select {
		case <-timeout:
			log.WithFields(log.Fields{"fqdn": fqdn}).Warn("no pods timeout")
			return fmt.Errorf("no pods spawn error")

		case <-tick:
			pods, err := k8c.CoreV1().Pods(xdcNs).List(
				context.TODO(),
				metav1.ListOptions{
					LabelSelector: fmt.Sprintf("proj=%s, name=%s", project, name),
				},
			)
			if err != nil {
				log.WithFields(log.Fields{"fqdn": fqdn}).Error("failed to list k8s pods")
				return err
			}
			if len(pods.Items) == 0 {
				log.WithFields(log.Fields{"fqdn": fqdn}).Debug("no pods yet")
				continue
			}
			if pods.Items[0].DeletionTimestamp != nil {
				log.WithFields(log.Fields{"fqdn": fqdn}).Debug("matching pod being deleted, skipping")
				continue
			}
			podip = pods.Items[0].Status.PodIP
			if podip != "" {
				done = true
			} else {
				log.WithFields(log.Fields{"fqdn": fqdn}).Warn("no pod ip yet")
			}
		}
	}
	log.WithFields(log.Fields{"fqdn": fqdn, "ip": podip}).Debug("pod ip")

	return nil
}

func createIngress(proj, name string) error {

	iname := name + "-" + proj
	_, err := k8c.NetworkingV1().Ingresses(xdcNs).Get(context.TODO(), iname, metav1.GetOptions{})
	if err == nil {
		log.WithFields(log.Fields{"name": iname}).Debug("ingress already exists")
		return nil
	}

	host := iname + "." + xdcDomain
	pathType := netv1.PathTypePrefix

	_, err = k8c.NetworkingV1().Ingresses(xdcNs).Create(
		context.TODO(),
		&netv1.Ingress{
			ObjectMeta: metav1.ObjectMeta{
				Name:      iname,
				Namespace: xdcNs,
				Labels: map[string]string{
					"app":       "portal",
					"component": "xdc",
				},
			},
			Spec: netv1.IngressSpec{
				TLS: []netv1.IngressTLS{{
					Hosts:      []string{host},
					SecretName: "xdcingresskey", // TODO read this from config or somewhere.
				}},
				Rules: []netv1.IngressRule{{
					Host: host,
					IngressRuleValue: netv1.IngressRuleValue{
						HTTP: &netv1.HTTPIngressRuleValue{
							Paths: []netv1.HTTPIngressPath{{
								Path:     "/jupyter",
								PathType: &pathType,
								Backend: netv1.IngressBackend{
									Service: &netv1.IngressServiceBackend{
										Name: iname,
										Port: netv1.ServiceBackendPort{
											Number: ports["web"],
										},
									},
								},
							}},
						},
					},
				}},
			},
		},
		metav1.CreateOptions{},
	)
	if err != nil {
		log.WithFields(log.Fields{"name": iname}).Debug("ingress create error")
		return err
	}

	return nil
}

func createService(proj, name, svcname string) error {

	// check if service already exists
	_, err := k8c.CoreV1().Services(xdcNs).Get(context.TODO(), svcname, metav1.GetOptions{})
	if err == nil {
		log.WithFields(log.Fields{"name": svcname}).Debug("service already exists")
		// nothing to do
		return nil
	}

	sports := []v1.ServicePort{}
	for k, p := range ports {
		sports = append(sports, v1.ServicePort{
			Name:       k,
			Port:       p,
			TargetPort: k8intstr.FromInt(int(p)),
			Protocol:   v1.ProtocolTCP,
		})
	}

	_, err = k8c.CoreV1().Services(xdcNs).Create(context.TODO(), &v1.Service{
		ObjectMeta: metav1.ObjectMeta{
			Name: svcname,
		},
		Spec: v1.ServiceSpec{
			Selector: map[string]string{
				"proj": proj,
				"name": name,
			},
			Ports: sports,
		},
	}, metav1.CreateOptions{})

	if err != nil {
		return fmt.Errorf("failed to create service: %w", err)
	}

	return nil
}

func deleteIngress(name string) error {

	_, err := k8c.NetworkingV1().Ingresses(xdcNs).Get(context.TODO(), name, metav1.GetOptions{})
	if err != nil {
		log.WithFields(log.Fields{"name": name}).Debug(name + " ingress does not exist")
		return nil
	}

	dp := new(metav1.DeletionPropagation)
	*dp = metav1.DeletePropagationBackground

	err = k8c.NetworkingV1().Ingresses(xdcNs).Delete(
		context.TODO(),
		name,
		metav1.DeleteOptions{
			PropagationPolicy: dp,
		},
	)
	if err != nil {
		log.Warnf("failed to delete xdc ingress: %+v", err)
		return err
	}

	return nil
}

func deleteService(name string) error {

	propagation := new(metav1.DeletionPropagation)
	*propagation = metav1.DeletePropagationBackground

	// check if service already exists
	_, err := k8c.CoreV1().Services(xdcNs).Get(context.TODO(), name, metav1.GetOptions{})
	if err != nil {
		if errors.IsNotFound(err) {
			log.WithFields(log.Fields{"name": name}).Debug("service does not exist")
			// nothing to do
			return nil
		}
		return fmt.Errorf("failed to check for existing service: %w", err)
	}

	err = k8c.CoreV1().Services(xdcNs).Delete(context.TODO(),
		name,
		metav1.DeleteOptions{
			PropagationPolicy: propagation,
		},
	)

	if err != nil {
		return fmt.Errorf("failed to delete service: %w", err)
	}

	return nil
}

func deleteDeployment(proj, name string) error {

	propagation := new(metav1.DeletionPropagation)
	*propagation = metav1.DeletePropagationBackground
	dname := fmt.Sprintf("%s.%s", name, proj)

	_, err := k8c.AppsV1().Deployments(xdcNs).Get(context.TODO(), dname, metav1.GetOptions{})
	if err != nil {
		if errors.IsNotFound(err) {
			//nothing to do here
			return nil
		}
		return fmt.Errorf("failed to query for existing deployment: %w", err)
	}

	err = k8c.AppsV1().Deployments(xdcNs).Delete(context.TODO(),
		dname,
		metav1.DeleteOptions{
			PropagationPolicy: propagation,
		},
	)
	if err != nil {
		return fmt.Errorf("failed to delete deployment: %w", err)
	}

	return nil
}

func getProjectAndMembers(proj string) ([]string, *portal.Project, error) {

	p := storage.NewProject(proj)
	err := p.Read()
	if err != nil {
		return nil, nil, err
	}

	var mids []string
	for mName, m := range p.Members {
		if m.State == portal.Member_Active {
			mids = append(mids, mName)
		}
	}
	return mids, p.Project, nil
}

func (x *XdcOperator) k8deploy(user, proj, name string) error {

	fullname := fmt.Sprintf("%s.%s", name, proj)
	replicas := new(int32)
	*replicas = 1

	// generate the jupyter notebook token and the corresponding config file
	token, err := uuid.NewV4()
	if err != nil {
		return fmt.Errorf("failed to generate jupyter token: %w", err)
	}

	// build the volumes and volume mounts based on project members.
	members, project, err := getProjectAndMembers(proj)
	if err != nil {
		return fmt.Errorf("failed to get project members: %w", err)
	}

	log.Infof("found project members: %s", strings.Join(members, ", "))

	// "standard" VolumeMounts
	volumeMounts := []v1.VolumeMount{{
		Name:      "mergefs-volume",
		MountPath: "/project",
		SubPath:   fmt.Sprintf("project/%s", proj),
	}, { // Mount the mege generated keys and certs.
		Name:      "mergefs-volume",
		MountPath: "/etc/ssh/auth",
		SubPath:   fmt.Sprintf("xdc/%s.%s/auth", name, proj), // inside knowledge should be in a pkg
		ReadOnly:  true,
	}, { // mount step-ca certs so we can auth the CA
		Name:      "step-ca-volume",
		MountPath: "/etc/step-ca/data/certs",
		SubPath:   "certs",
		ReadOnly:  true,
	}}

	// "standard" Volumes
	volumes := []v1.Volume{{
		Name: "mergefs-volume",
		VolumeSource: v1.VolumeSource{
			PersistentVolumeClaim: &v1.PersistentVolumeClaimVolumeSource{
				ClaimName: mergefsPVName,
			},
		},
	}, {
		Name: "step-ca-volume",
		VolumeSource: v1.VolumeSource{
			ConfigMap: &v1.ConfigMapVolumeSource{
				LocalObjectReference: v1.LocalObjectReference{
					Name: stepcaCMName,
				},
			},
		},
	}}

	if project.Organization != "" {
		log.Infof("XDC project is in organization %s. Adding org mount.", project.Organization)

		vm := v1.VolumeMount{
			Name:      "mergefs-volume",
			MountPath: "/organization",
			SubPath:   fmt.Sprintf("organization/%s", project.Organization),
		}

		// Apply policy to the storage mount. Org admins may want to limit
		// who can write to the storage mount.
		usernames := []string{}
		for key := range project.Members {
			usernames = append(usernames, key)
		}

		log.Infof("Checking read/write policy against users: %v", usernames)
		err = policy.WriteOrganizationStoragePolicy(usernames, project.Organization)
		if err != nil {
			log.Infof("Setting org mount to read only")
			vm.ReadOnly = true
		} else {
			log.Infof("Setting org mount to read/write")
			vm.ReadOnly = false
		}

		volumeMounts = append(volumeMounts, vm)
	}

	// add dynamic member volumes and volumeMounts
	for _, m := range members {
		log.Infof("adding %s", m)

		// Volume Names must be DNS-1123 compliant.
		// a DNS-1123 label must consist of lower case alphanumeric characters or '-', and must start and end
		// with an alphanumeric character (e.g. 'my-name',  or '123-abc', regex used for
		// validation is '[a-z0-9]([-a-z0-9]*[a-z0-9])?'),
		vmName := regexp.MustCompile(`[^a-z0-9-]`).ReplaceAllString(m, "-")
		log.Infof("m: %s, vmName: %s", m, vmName)

		volumeMounts = append(volumeMounts, v1.VolumeMount{
			Name:      "mergefs-volume",
			MountPath: fmt.Sprintf("/home/%s", m),
			SubPath:   fmt.Sprintf("user/%s", m),
		})
	}

	// container ports.
	cports := []v1.ContainerPort{}
	for k, p := range ports {
		cports = append(cports, v1.ContainerPort{
			Name:          k,
			ContainerPort: p,
			Protocol:      v1.ProtocolTCP,
		})
	}

	// create service
	svcname := fmt.Sprintf("%s-%s", name, proj)
	log.Infof("Creating service %s", svcname)
	err = createService(proj, name, svcname)
	if err != nil {
		return err
	}
	log.Infof("Created service %s", svcname)

	// Create web ingress
	err = createIngress(proj, name)
	if err != nil {
		return err
	}

	log.Infof("Creating deployment %s", fullname)
	_, err = k8c.AppsV1().Deployments(xdcNs).Get(context.TODO(), fullname, metav1.GetOptions{})
	if err == nil {
		log.WithFields(log.Fields{"name": fullname}).Info("xdc deployment already exists")
		// nothing to do
		return nil
	}

	var grpcServer string
	pc, err := storage.GetPortalConfig()
	if err == nil {
		grpcServer, _, err = net.SplitHostPort(pc.Config.GRPCEndpoint)
	}

	privileged := true

	podTemplateSpec := v1.PodTemplateSpec{
		ObjectMeta: metav1.ObjectMeta{
			Labels: map[string]string{
				"proj":      proj,
				"name":      name,
				"app":       "portal",
				"component": "xdc",
				"instance":  "xdc",
			},
		},
		Spec: v1.PodSpec{
			HostAliases: hostAliases,
			Tolerations: []v1.Toleration{{
				Key:      "dedicated",
				Operator: v1.TolerationOpEqual,
				Value:    "xdc_worker",
				Effect:   v1.TaintEffectNoSchedule,
			}},
			Containers: []v1.Container{{
				Name:            name,
				Image:           x.Image,
				ImagePullPolicy: v1.PullIfNotPresent,
				SecurityContext: &v1.SecurityContext{
					Privileged: &privileged,
				},
				Resources: v1.ResourceRequirements{
					Limits: v1.ResourceList{
						// 2 cpu == 2 cores
						v1.ResourceCPU:    *resource.NewQuantity(x.CpuLimit, resource.DecimalSI),
						v1.ResourceMemory: *resource.NewQuantity(x.MemLimit*1024*1024*1024, resource.BinarySI),
					},
					Requests: v1.ResourceList{
						// 100/1000 = 100m, or 1% of one CPU.
						// and 1MB of memory
						v1.ResourceCPU:    *resource.NewMilliQuantity(x.CpuRequest, resource.DecimalSI),
						v1.ResourceMemory: *resource.NewQuantity(x.MemRequest*1024*1024, resource.BinarySI),
					},
				},
				Ports:        cports,
				VolumeMounts: volumeMounts,
				Env: []v1.EnvVar{
					{Name: "GIT_SSL_NO_VERIFY", Value: "true"},
					{Name: "JUPYTER_TOKEN", Value: token.String()},
					{Name: "MERGE_GRPC_SERVER", Value: grpcServer},
				},
			}},
			Volumes: volumes,
		},
	}

	if xdcExtras != "" {

		log.Infof("Reading extra XDC Pod Spec %s", xdcExtras)

		extraTemplateSpecs, err := readXDCExtraTemplateSpecs()
		if err != nil {
			return fmt.Errorf("Unable to read XDC extras: %v", err)
		}

		if extraTemplateSpecs != nil {
			err = mergePodTemplateSpec(extraTemplateSpecs, &podTemplateSpec)
			if err != nil {
				return fmt.Errorf("Error merging pod spec from %s into XDC deployment: %s", xdcExtras, err)
			}
		}
	}

	// create deployment
	dep, err := k8c.AppsV1().Deployments(xdcNs).Create(context.TODO(), &apps.Deployment{
		TypeMeta: metav1.TypeMeta{
			Kind:       "Deployment",
			APIVersion: "apps/v1",
		},
		ObjectMeta: metav1.ObjectMeta{
			Name: fullname,
		},
		Spec: apps.DeploymentSpec{
			Replicas: replicas,
			Selector: &metav1.LabelSelector{
				MatchLabels: map[string]string{
					"proj":      proj,
					"name":      name,
					"app":       "portal",
					"component": "xdc",
					"instance":  "xdc",
				},
			},
			Template: podTemplateSpec,
		},
	}, metav1.CreateOptions{})

	if err != nil {
		log.Errorf("error deploying: %+v", err)
		return err
	}
	log.WithFields(log.Fields{"name": fullname}).Info("xdc deployed")
	log.Tracef("deployed: %+v", dep)

	// save the token
	host := name + "-" + proj
	jc := storage.NewJupyterCfg(host)
	jc.Token = token.String()
	jc.Url = fmt.Sprintf("https://%s.%s/jupyter/lab?token=%s", host, xdcDomain, jc.Token)
	jc.Domain = xdcDomain

	// always write it
	jc.Ver = -1
	_, err = jc.Create()
	if err != nil {
		return fmt.Errorf("juptyer config write: %w", err)
	}

	return nil
}

func Destroy(project, name string) error {

	log.WithFields(log.Fields{
		"proj": project,
		"name": name,
	}).Debug("destroy")

	err := deleteDeployment(project, name)
	if err != nil {
		log.Errorf("delete deployment %s/%s error: %v", project, name, err)
		return err
	}

	svcname := fmt.Sprintf("%s-%s", name, project)
	err = deleteService(svcname)
	if err != nil {
		log.Errorf("delete service %s error: %v", svcname, err)
		return err
	}

	err = deleteIngress(svcname)
	if err != nil {
		log.Errorf("delete ingress %s error: %v", svcname, err)
		return err
	}

	jc := storage.NewJupyterCfg(name + "-" + project)
	jc.Delete()

	return nil
}

func readXDCExtraTemplateSpecs() (*v1.PodTemplateSpec, error) {

	// XDC extras exist in the given CM in the xdc namespace.
	cm, err := k8c.CoreV1().ConfigMaps(xdcNs).Get(context.TODO(), xdcExtras, metav1.GetOptions{})
	if err != nil {
		log.Warnf("Error getting config map %s", xdcExtras)
		return nil, nil
	}

	// Read the well known filename PodSpec.json for the extras.
	data := cm.Data["PodTemplateSpec.json"]

	pts := &v1.PodTemplateSpec{}
	err = json.Unmarshal([]byte(data), &pts)
	if err != nil {
		return nil, fmt.Errorf("Error unmarshalling xdc extra pod template spec")
	}

	log.Debugf("Read pod template spec: %+v", pts)

	return pts, nil
}

func mergePodTemplateSpec(frm, to *v1.PodTemplateSpec) error {

	log.Infof("Merging extras: %v", frm)

	// Supported:
	// * metadata.annotations
	// * spec.tolerations
	// * spec.volumes
	// * spec.securitycontext.selinuxoptions.level
	// * spec.containers[].volumeMounts

	for k, v := range frm.ObjectMeta.Annotations {
		if _, ok := to.ObjectMeta.Annotations[k]; !ok {
			to.ObjectMeta.Annotations = make(map[string]string)
		}
		to.ObjectMeta.Annotations[k] = v
	}

	// Ideally here we'd check for duplicates. But this is not an ideal world.
	to.Spec.Tolerations = append(to.Spec.Tolerations, frm.Spec.Tolerations...)
	to.Spec.Volumes = append(to.Spec.Volumes, frm.Spec.Volumes...)

	for i := range to.Spec.Containers {
		if len(frm.Spec.Containers) > 0 {
			if len(frm.Spec.Containers[i].VolumeMounts) > 0 {
				to.Spec.Containers[i].VolumeMounts = append(
					to.Spec.Containers[i].VolumeMounts,
					frm.Spec.Containers[i].VolumeMounts...,
				)
			}

			to.Spec.Containers[i].Env = append(to.Spec.Containers[i].Env, frm.Spec.Containers[i].Env...)
		}
	}

	if frm.Spec.SecurityContext != nil {
		if frm.Spec.SecurityContext.SELinuxOptions != nil {
			if frm.Spec.SecurityContext.SELinuxOptions.Level != "" {
				// We created the "to" PodTemplateSpec passed into this funciton,
				// so we know it does not have a podspec level SecurityContext.
				// So creating a new one here is ok.
				to.Spec.SecurityContext = &v1.PodSecurityContext{
					SELinuxOptions: &v1.SELinuxOptions{
						Level: frm.Spec.SecurityContext.SELinuxOptions.Level,
					},
				}
			}
		}
	}

	return nil
}

func k8init() {

	config, err := rest.InClusterConfig()
	if err != nil {
		log.WithFields(log.Fields{
			"err": err,
		}).Fatal("failed to fetch kubeconfig")
	}

	k8c, err = kubernetes.NewForConfig(config)
	if err != nil {
		log.WithFields(log.Fields{
			"err": err,
		}).Fatal("failed to create k8s client")
	}
}
