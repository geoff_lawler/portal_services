package main

import (
	"fmt"
	"regexp"
	"strings"

	log "github.com/sirupsen/logrus"
	v1 "k8s.io/api/core/v1"

	"gitlab.com/mergetb/portal/services/pkg/podwatch"
	"gitlab.com/mergetb/portal/services/pkg/storage"
	"gitlab.com/mergetb/tech/reconcile"
)

var (
	podPath   = "podinit"
	podBucket = storage.PrefixedBucket(&storage.Pod{Path: podPath})
	podKey    = regexp.MustCompile("^" + podBucket + "(.+)$")

	workerCount = 16
)

func init() {
	podwatch.InitPodWatch()
}

func runPodWatch() {

	pw := &podwatch.PodWatcher{
		WorkerPoolSize: workerCount,
		OnPodAdd:       handleCreate,
		OnPodUpdate:    handleUpdate,
		OnPodDelete:    handleDelete,
		Namespace:      xdcNs,
	}

	pw.Watch()
}

func handleCreate(pod *v1.Pod) {

	log.Infof("XDC update pod create fired for %s", pod.Name)

	handlePod(pod, podwatch.Create)
}

func handleUpdate(prevPod, newPod *v1.Pod) {

	log.Tracef("XDC update pod callback fired for %s", newPod.Name)

	handlePod(newPod, podwatch.Update)
}

func handleDelete(pod *v1.Pod) {

	log.Infof("XDC delete pod callback fired for %s", pod.Name)

	handlePod(pod, podwatch.Delete)
}

// bridge this to a reconciler by writing keys
// this is important for task status to be done correctly
func handlePod(pod *v1.Pod, e podwatch.PodEventType) {

	t, err := podwatch.PodType(pod)
	if err != nil {
		log.Errorf("podtype: %v", err)
		return
	}

	if t != podwatch.JUMP && t != podwatch.XDC {
		log.Debugf("Ignoring non merge pod")
		return
	}

	p := storage.NewPod(podPath, pod)
	switch e {
	case podwatch.Create:
		log.Infof("writing task: %s", p.Key())

		// always write
		p.Ver = -1
		_, err = p.Create()
		if err != nil {
			log.Errorf("pod update: %v", err)
		}
	case podwatch.Update:
		log.Debugf("writing task: %s", p.Key())

		// only create if it doesn't exist, do not update
		p.Ver = 0
		_, err = p.Create()
		if err != nil && !strings.Contains(err.Error(), "transaction failed") {
			log.Errorf("pod update: %v", err)
		}
	case podwatch.Delete:
		log.Infof("deleting task: %s", p.Key())

		// always delete
		p.Ver = -1
		_, err = p.Delete()
		if err != nil {
			log.Errorf("pod delete: %v", err)
		}
	}
}

func initPod(pod *v1.Pod) error {

	// distinguish between xdc and jumps, both are watched here.
	t, err := podwatch.PodType(pod)
	if err != nil {
		return fmt.Errorf("podtype: %v", err)
	}

	if t == podwatch.XDC {
		name, proj, err := podwatch.PodName(pod)
		if err != nil {
			return fmt.Errorf("podname: %v", err)
		}
		alog := log.WithField("svc", fmt.Sprintf("%s-%s", name, proj))
		alog.Info("initXDC start")
		err = initXDC(name, proj)
		if err != nil {
			alog.Errorf("initXDC failed: %v", err)
		}
		alog.Info("initXDC complete")
		return err

	} else if t == podwatch.JUMP {
		svc := pod.Labels["svc"]
		alog := log.WithField("svc", svc)
		alog.Info("initJump start")
		err = initJump(svc)
		if err != nil {
			alog.Errorf("initJump failed: %v", err)
		}
		alog.Info("initJump complete")
		return err
	}

	return fmt.Errorf("unknown pod instance")
}

type InitPodWatch struct {
	Name string
}

func (w *InitPodWatch) Parse(k string) bool {
	tkns := podKey.FindAllStringSubmatch(k, -1)
	if len(tkns) > 0 {
		w.Name = tkns[0][1]

		return true
	}

	return false
}

func (w *InitPodWatch) Create(value []byte, version int64, td *reconcile.TaskData) *reconcile.TaskMessage {
	return w.handle(value, td)
}

func (w *InitPodWatch) Update(prev, value []byte, version int64, td *reconcile.TaskData) *reconcile.TaskMessage {
	return w.handle(value, td)
}

func (w *InitPodWatch) Ensure(prev, value []byte, version int64, td *reconcile.TaskData) *reconcile.TaskMessage {
	return w.handle(value, td)
}

func (w *InitPodWatch) Delete(value []byte, version int64, td *reconcile.TaskData) *reconcile.TaskMessage {
	// NOOP
	return nil
}

func (w *InitPodWatch) handle(value []byte, td *reconcile.TaskData) *reconcile.TaskMessage {
	if td.Status.LastStatus == reconcile.TaskStatus_Success {
		return nil
	}

	if value == nil {
		return reconcile.TaskMessageErrorf("cannot handle podwatch request: nil value")
	}

	pod := new(v1.Pod)
	err := pod.Unmarshal(value)
	if err != nil {
		return reconcile.TaskMessageError(err)
	}

	return reconcile.CheckErrorToMessage(initPod(pod))
}
