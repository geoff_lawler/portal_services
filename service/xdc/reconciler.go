package main

import (
	log "github.com/sirupsen/logrus"

	"gitlab.com/mergetb/portal/services/pkg/storage"

	"gitlab.com/mergetb/tech/reconcile"
)

func runReconciler() {

	log.Infof("starting xdc reconciler")

	// Things to watch:
	// * xdc events -
	//   * new/del xdc instances - spawn xdcs
	// * project events
	//   * watch membership and mount/unmount user home dirs?
	// * user events -
	//   * add/delete users to/from jump boxes

	t1 := storage.ReconcilerConfigXDCPutUsers.ToReconcilerManager(
		&UserTask{},
	)

	t2 := storage.ReconcilerConfigXDCPutPods.ToReconcilerManager(
		&XdcTask{},
	)

	t3 := storage.ReconcilerConfigXDCPodInit.ToReconcilerManager(
		&InitPodWatch{},
	)

	reconcile.RunReconcilerManagers(t1, t2, t3)
}
