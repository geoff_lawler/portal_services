package main

import (
	"context"
	"fmt"
	"regexp"

	log "github.com/sirupsen/logrus"
	portal "gitlab.com/mergetb/api/portal/v1/go"
	xdcd "gitlab.com/mergetb/api/xdcd/v1/go"
	"gitlab.com/mergetb/portal/services/pkg/storage"
	"gitlab.com/mergetb/tech/reconcile"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/protobuf/proto"
)

var (
	nameExp   = "([a-zA-Z_]+[a-zA-Z0-9_]+)"
	xdcBucket = storage.PrefixedBucket(&storage.XDC{})
	xdcKey    = regexp.MustCompile(`^` + xdcBucket + nameExp + `\.` + nameExp + `$`)

	defaultMemLimit int32 = 2
	defaultCPULimit int32 = 2

	// set in  opeator init() after env vars are read.
	defaultImage string
)

type XdcTask struct {
	XDC     string
	Project string
}

func (xt *XdcTask) Parse(key string) bool {

	tkns := xdcKey.FindAllStringSubmatch(key, -1)
	if len(tkns) > 0 {
		xt.XDC = tkns[0][1]
		xt.Project = tkns[0][2]

		return true
	}

	return false
}

func (xt *XdcTask) Create(value []byte, version int64, td *reconcile.TaskData) *reconcile.TaskMessage {

	log.Infof("[%s] XDC key put", xt.XDC)

	x := new(portal.XDCStorage)
	err := proto.Unmarshal(value, x)
	if err != nil {
		return reconcile.TaskMessageError(err)
	}

	// is the user overriding the defaults?
	// (Note we only get here if the user is authorized to set these.
	// apiserver will check this for us.
	// We also set the defaults so subsequent XDC list or accesses will
	// know the XDC configuration. apiserver does not know the defaults
	// so cannot write them so it falls to us.
	if x.Image == "" || x.MemLimit == 0 || x.CpuLimit == 0 {

		xStor := storage.NewXDC(x.Creator, x.Name, x.Project)
		err := xStor.Read()
		if err != nil {
			return reconcile.TaskMessageError(err)
		}

		xStor.UpdateRequest = &portal.XDCStorage{}
		update := false

		if x.Image == "" {
			x.Image = defaultImage
			xStor.UpdateRequest.Image = x.Image
			update = true
		}
		if x.MemLimit == 0 {
			x.MemLimit = defaultMemLimit
			xStor.UpdateRequest.MemLimit = x.MemLimit
			update = true
		}
		if x.CpuLimit == 0 {
			x.CpuLimit = defaultCPULimit
			xStor.UpdateRequest.CpuLimit = x.CpuLimit
			update = true
		}
		if update {

			// IS RECONCILED
			_, err := xStor.Update()
			if err != nil {
				return reconcile.TaskMessageError(err)
			}
		}
	}

	log.Infof("spawning new XDC")
	log.Infof("XDC Request: %+v", x.Name)

	xo := NewXdcOperator(x)
	err = xo.Spawn(x.Creator, x.Name, x.Project)

	return reconcile.CheckErrorToMessage(err)
}

func (xt *XdcTask) Update(prev, value []byte, version int64, td *reconcile.TaskData) *reconcile.TaskMessage {

	return xt.Create(value, version, td)
}

func (xt *XdcTask) Ensure(prev, value []byte, version int64, td *reconcile.TaskData) *reconcile.TaskMessage {

	return xt.Create(value, version, td)
}

func (xt *XdcTask) Delete(value []byte, version int64, td *reconcile.TaskData) *reconcile.TaskMessage {

	log.Infof("[%s] XDC key delete", xt.XDC)

	x := new(portal.XDCStorage)
	err := proto.Unmarshal(value, x)
	if err != nil {
		return reconcile.TaskMessageError(err)
	}

	err = Destroy(x.Project, x.Name)
	if err != nil {
		return reconcile.TaskMessageError(err)
	}

	return nil
}

// initXDC: call out to xdcd to create users and groups.
func initXDC(xdcid, pid string) error {
	members, proj, err := getProjectAndMembers(pid)
	if err != nil {
		return fmt.Errorf("failed to get project members: %w", err)
	}

	us := []*portal.User{}
	for _, m := range members {

		u := storage.NewUser(m)
		err = u.Read()
		if err != nil {
			return fmt.Errorf("user read failed on user '%s': %+v", m, err)
		}

		if u.State == portal.UserState_Active {
			us = append(us, u.User)
		}
	}

	if len(us) == 0 {
		return fmt.Errorf("This xdc has no users somehow")
	}

	svc := xdcid + "-" + pid

	err = withXdcdClient(svc, func(c xdcd.XdcdClient) error {
		_, err := c.AddUsers(
			context.TODO(),
			&xdcd.AddUsersRequest{
				Users:       us,
				Sudo:        true,
				Interactive: true,
				CommonGroups: []*xdcd.GroupInfo{
					&xdcd.GroupInfo{GroupName: proj.Name, Gid: proj.Gid},
				},
			},
		)
		return err
	})
	if err != nil {
		return fmt.Errorf("error adding users: %w", err)
	}

	err = withXdcdClient(svc, func(c xdcd.XdcdClient) error {
		_, err := c.InitHost(
			context.TODO(),
			&xdcd.InitHostRequest{},
		)
		return err
	})
	if err != nil {
		return fmt.Errorf("xdc init host: %w", err)
	}

	return nil
}

func withXdcdClient(srv string, f func(xdcd.XdcdClient) error) error {

	conn, err := grpc.Dial(srv+"."+xdcNs+":6000", grpc.WithTransportCredentials(insecure.NewCredentials()))

	if err != nil {
		return fmt.Errorf("dial: %s", err)
	}
	defer conn.Close()

	cli := xdcd.NewXdcdClient(conn)

	return f(cli)
}
