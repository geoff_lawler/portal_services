package main

import (
	"context"
	"fmt"
	"regexp"

	log "github.com/sirupsen/logrus"
	"google.golang.org/protobuf/proto"

	facility "gitlab.com/mergetb/api/facility/v1/go"
	portal "gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/portal/services/pkg/connect"
	"gitlab.com/mergetb/portal/services/pkg/materialize"
	"gitlab.com/mergetb/portal/services/pkg/realize"
	"gitlab.com/mergetb/portal/services/pkg/realize/sfe"
	"gitlab.com/mergetb/portal/services/pkg/storage"
	"gitlab.com/mergetb/tech/reconcile"
	xir "gitlab.com/mergetb/xir/v0.3/go"
)

var (
	nameExp      = "([a-zA-Z_]+[a-zA-Z0-9_]+)"
	harborBucket = storage.PrefixedBucket(&storage.InitHarborRequest{})
	harborKey    = regexp.MustCompile(
		"^" + harborBucket + nameExp + "$",
	)
)

type HarborTask struct {
	Facility string
}

func (ht *HarborTask) Parse(key string) bool {

	parts := harborKey.FindAllStringSubmatch(key, -1)
	if len(parts) > 0 {
		ht.Facility = parts[0][1]
		return true
	}

	return false

}

func findTbRoot(tbx *xir.Facility) (*xir.Resource, error) {
	for _, rsrc := range tbx.Resources {
		if rsrc.HasRole(xir.Role_BorderGateway) {
			return rsrc, nil
		}
	}

	log.Warnf("facility has no BorderGateway -- searching for Gateway ...")

	for _, rsrc := range tbx.Resources {
		if rsrc.HasRole(xir.Role_Gateway) {
			return rsrc, nil
		}
	}

	return nil, fmt.Errorf("facility has no Gateway")
}

func (ht *HarborTask) Create(
	value []byte, version int64, td *reconcile.TaskData) *reconcile.TaskMessage {

	log.Infof("[%s] create", ht.Facility)

	rq := new(portal.InitHarborRequest)
	err := proto.Unmarshal(value, rq)
	if err != nil {
		return reconcile.TaskMessageError(err)
	}

	tbx, err := storage.ReadFacilityModel(rq.Facility)
	if err != nil {
		return reconcile.TaskMessageErrorf("read facility %s: %v", rq.Facility, err)
	}

	tbroot, err := findTbRoot(tbx)
	if err != nil {
		return reconcile.TaskMessageErrorf("find facility %s TbRoot: %v", rq.Facility, err)
	}

	tbt := tbx.Lift()

	sfe.AssignTPAsInfraNet(0x004D000000000000, tbt)

	embedding, diags, err := sfe.EmbedSys(tbt.Device(tbroot.Id), tbt)
	if err != nil {
		return reconcile.TaskMessageErrorf("embed system: %v", err)
	}
	if diags.Error() {
		return reconcile.TaskMessageErrorf("embed system diagnostics: %s", diags.ToString())
	}

	embedding.Dump()

	rz, err := realize.EmbeddingToRz(embedding, false)
	if err != nil {
		return reconcile.TaskMessageErrorf("embedding to rz: %v", err)
	}

	rz.Id = rq.Rid
	rz.Eid = rq.Eid
	rz.Pid = rq.Pid

	mz, err := materialize.RzToMz(rz, nil)
	if err != nil {
		return reconcile.TaskMessageErrorf("rz to mz: %v", err)
	}

	// The harbor always goes on the Infraserver
	infraServer := ""
	for _, r := range tbx.GetResources() {
		if r.HasRole(xir.Role_InfraServer) {
			infraServer = r.GetId()
			break
		}
	}
	if infraServer == "" {
		return reconcile.TaskMessageErrorf("facility %s has no infraserver", rq.Facility)
	}

	mz.Params = &portal.MzParameters{
		InfrapodServer: infraServer,
		InfranetVni:    3,
	}

	err = connect.FacilityClient(
		rq.Facility,
		func(cli facility.FacilityClient) error {
			_, err = cli.Materialize(
				context.TODO(),
				&facility.MaterializeRequest{
					Materialization: mz,
				},
			)
			return err
		},
	)
	if err != nil {
		return reconcile.TaskMessageErrorf("[%s] materialize harbor: %v", rq.Facility, err)
	}

	log.Infof("[%s] harbor materialization requested", rq.Facility)

	return nil

}

func (ht *HarborTask) Update(prev, value []byte, version int64, td *reconcile.TaskData) *reconcile.TaskMessage {

	log.Infof("[%s] update (not implemented)", ht.Facility)

	return nil
}

func (ht *HarborTask) Ensure(prev, value []byte, version int64, td *reconcile.TaskData) *reconcile.TaskMessage {

	log.Infof("[%s] ensure (not implemented)", ht.Facility)

	return reconcile.TaskMessageUndefined()
}

func (ht *HarborTask) Delete(value []byte, version int64, td *reconcile.TaskData) *reconcile.TaskMessage {

	log.Infof("[%s] delete", ht.Facility)

	rq := new(portal.InitHarborRequest)
	err := proto.Unmarshal(value, rq)
	if err != nil {
		return reconcile.TaskMessageError(err)
	}

	err = connect.FacilityClient(
		rq.Facility,
		func(cli facility.FacilityClient) error {
			_, err := cli.Dematerialize(
				context.TODO(),
				&facility.DematerializeRequest{
					Rid: rq.Rid,
					Eid: rq.Eid,
					Pid: rq.Pid,
				},
			)
			return err
		},
	)
	if err != nil {
		return reconcile.TaskMessageErrorf("[%s] dematerialize harbor: %v", rq.Facility, err)
	}

	log.Infof("[%s] harbor dematerialization requested", rq.Facility)

	return nil

}
