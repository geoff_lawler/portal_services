package main

import (
	"bufio"
	"fmt"
	"os"
	"path"
	"regexp"
	"strings"

	log "github.com/sirupsen/logrus"
	portal "gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/portal/services/pkg/storage"
	"gitlab.com/mergetb/tech/reconcile"
	"google.golang.org/protobuf/proto"
)

var (
	credKey = regexp.MustCompile(`^/auth/ssh/(keys|cert)/(user|host)/([a-zA-Z_]+[\-\.a-zA-Z0-9_]+)$`)
	sshConf = `#
# Note: This file is placed by Merge. It changes the default ssh key to use the merge-generated
# key "merge_key". Please only update if you know what you are doing. Thanks.
#
Host *
  IdentityFile ~/.ssh/` + privkeyName + "\n  StrictHostKeyChecking no\n"
)

type CredTask struct {
	User    string
	Type    string // host or user
	Content string // cert or keys
	Key     string
}

func (ct *CredTask) Parse(k string) bool {

	tkns := credKey.FindAllStringSubmatch(k, -1)
	if len(tkns) > 0 {
		ct.Content = tkns[0][1]
		ct.Type = tkns[0][2]
		ct.User = tkns[0][3]
		ct.Key = k
		return true
	}

	return false
}

func (ct *CredTask) Create(value []byte, version int64, td *reconcile.TaskData) *reconcile.TaskMessage {

	log.Debugf("putting cred: %s/%s/%s", ct.User, ct.Type, ct.Content)

	switch ct.Content {

	case "keys":
		err := ct.writeKeys(value, version)
		if err != nil {
			return reconcile.TaskMessageError(err)
		}

	case "cert":
		err := ct.writeCert(value, version)
		if err != nil {
			return reconcile.TaskMessageError(err)
		}
	}

	return nil
}

func (ct *CredTask) Update(prev, value []byte, version int64, td *reconcile.TaskData) *reconcile.TaskMessage {
	// Create is idempotent
	return ct.Create(value, version, td)
}

func (ct *CredTask) Ensure(prev, value []byte, version int64, td *reconcile.TaskData) *reconcile.TaskMessage {
	// Create is idempotent
	log.Debugf("ensuring %s", ct.Key)
	return ct.Create(value, version, td)
}

func (ct *CredTask) Delete(value []byte, version int64, td *reconcile.TaskData) *reconcile.TaskMessage {

	// We most certainly delete the creds.

	switch ct.Content {

	case "keys":
		skp := storage.NewSSHUserKeyPair(ct.User)
		err := skp.Read()
		if err != nil {
			return reconcile.TaskMessageError(err)
		}

		if skp.GetVersion() > 0 {
			skp.Delete()
		}

	case "cert":
		cert := storage.NewSSHUserCert(ct.User)
		err := cert.Read()
		if err != nil {
			return reconcile.TaskMessageError(err)
		}

		if cert.GetVersion() > 0 {
			cert.Delete()
		}
	}

	return nil
}

func (ct *CredTask) writeKeys(value []byte, version int64) error {

	log.Debugf("writing merge user ssh keys for %s", ct.User)

	kp := new(portal.SSHKeyPair)
	err := proto.Unmarshal(value, kp)
	if err != nil {
		return fmt.Errorf("sshkeypair unmarshal: %v", err)
	}

	root := userRoot
	sub := ".ssh"
	uid, gid := 0, 0
	if ct.Type == "host" {
		root = xdcRoot
		sub = "auth"

	} else {
		u := storage.NewUser(ct.User)
		err = u.Read()
		if err != nil {
			return err
		}

		uid = int(u.Uid)
		gid = int(u.Gid)
	}

	d := path.Join(root, ct.User, sub)

	mkDir(d, uint32(uid), uint32(gid), 0700)
	setUserGroup(d, uid, gid)

	// write the keys with our hopefully unique name
	p := path.Join(d, privkeyName)
	err = writeFileOnDiff(p, []byte(kp.Private), 0600)
	if err != nil {
		return fmt.Errorf("[%s] write private key %v", p, err)
	}
	setUserGroup(p, uid, gid)

	p = path.Join(d, pubkeyName)
	err = writeFileOnDiff(p, []byte(kp.Public), 0644)
	if err != nil {
		return fmt.Errorf("[%s] write public key %v", p, err)
	}
	setUserGroup(p, uid, gid)

	if ct.Type == "user" {
		p = path.Join(d, "config")
		err = writeFileOnDiff(p, []byte(sshConf), 0644)
		if err != nil {
			return fmt.Errorf("[%s] write ssh config key %v", p, err)
		}
		setUserGroup(p, uid, gid)

		// authorized_keys
		p = path.Join(d, "authorized_keys")
		if _, err := os.Stat(p); os.IsNotExist(err) {
			log.Infof("Creating authorized_keys")
			err = writeFileOnDiff(p, []byte(kp.Public), 0600)
			if err != nil {
				return fmt.Errorf("writing %s: %s", p, err)
			}

			setUserGroup(p, uid, gid)
		} else {

			// enforce user/group
			setUserGroup(p, uid, gid)

			// append keys if they are not there.
			afd, err := os.Open(p)
			if err != nil {
				return fmt.Errorf("reading %s: %s", p, err)
			}
			defer afd.Close()

			scan := bufio.NewScanner(afd)
			keys := make(map[string]bool)
			for scan.Scan() {
				keys[scan.Text()] = true
			}
			if err := scan.Err(); err != nil {
				return fmt.Errorf("reading keys from %s: %s", p, err)
			}

			k := strings.Trim(kp.Public, "\n")

			if _, ok := keys[k]; !ok {
				log.Infof("Adding public key to %s", p)
				fd, err := os.OpenFile(p, os.O_APPEND|os.O_WRONLY, 0600)
				if err != nil {
					return fmt.Errorf("opening %s to append: %s", p, err)
				}
				if _, err := fd.Write([]byte(kp.Public)); err != nil {
					return fmt.Errorf("appending key to %s: %s", p, err)
				}
			}

		}
	}

	return nil
}

// Write cert to host or user path.
// host: /xdc/<host>/auth/<cert>
// user: /users/<user>/.ssh/<cert>
//
//	also append CA host cert to known_hosts if this is a user cert
func (ct *CredTask) writeCert(value []byte, version int64) error {

	// value is just a string that is the cert.
	user := ct.User
	subdir := "auth"
	root := xdcRoot
	if ct.Type == "user" {
		subdir = ".ssh"
		root = userRoot
	}

	c := new(portal.SSHCert)
	err := proto.Unmarshal(value, c)
	if err != nil {
		log.Errorf("malformed cert for %s", user)
		return err
	}

	dir := path.Join(root, user, subdir)
	if _, err := os.Stat(dir); os.IsNotExist(err) {
		log.Debugf("Skipping write cert for XDC %s. Inactive XDC.", user)
		return nil
	}

	p := path.Join(dir, certName)
	err = writeFileOnDiff(p, []byte(c.Cert), 0660)
	if err != nil {
		log.Errorf("Error writing cert %s: %v", p, err)
		return err
	}

	if ct.Type == "user" {
		u := storage.NewUser(user)
		err = u.Read()
		if err != nil {
			return err
		}

		// chown cert file.
		os.Chown(p, int(u.Uid), int(u.Gid))

		err = writeCaKeyKnownHosts(u, c.CAHostPubKey)
		if err != nil {
			return err
		}
	}

	return nil
}

func writeCaKeyKnownHosts(u *storage.User, pubkey string) error {

	log.Debugf("checking %s known_hosts for CA Host pub key", u.Username)

	line := "@cert-authority * " + pubkey + "\n"

	// There should be a better way to do this. I read the entire known_hosts looking for this line
	// and append if not there.
	p := path.Join(userRoot, u.Username, ".ssh", "known_hosts")

	if _, err := os.Stat(p); os.IsNotExist(err) {
		// just add it
		log.Infof("creating known_hosts")
		os.WriteFile(p, []byte(line), 0600)
		os.Chown(p, int(u.Uid), int(u.Gid))
		return nil
	}

	exists, err := lineExists(p, line)
	if err != nil {
		return err
	}

	if exists == false {

		log.Infof("appending CA pubkey to known_hosts")

		f, err := os.OpenFile(p, os.O_APPEND|os.O_WRONLY, 0600)
		if err != nil {
			return fmt.Errorf("open file %s: %w", p, err)
		}
		defer f.Close()

		_, err = f.WriteString(line)
		if err != nil {
			return fmt.Errorf("append file %s: %w", p, err)
		}
	} else {
		log.Debugf("ca host key is already in known_hosts")
	}

	return nil
}
