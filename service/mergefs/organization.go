package main

import (
	"fmt"
	"path"
	"regexp"

	log "github.com/sirupsen/logrus"
	portal "gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/portal/services/pkg/storage"
	"gitlab.com/mergetb/tech/reconcile"
	"google.golang.org/protobuf/proto"
)

var (
	orgBucket = storage.PrefixedBucket(&storage.Organization{})
	orgKey    = regexp.MustCompile("^" + orgBucket + "([a-zA-Z_]+[a-zA-Z0-9_]+)$")
)

type OrganizationTask struct {
	Name string
}

func (ot *OrganizationTask) Parse(k string) bool {

	tkns := orgKey.FindAllStringSubmatch(k, -1)
	if len(tkns) > 0 {
		ot.Name = tkns[0][1]
		return true
	}

	return false
}

func (ot *OrganizationTask) Create(value []byte, version int64, td *reconcile.TaskData) *reconcile.TaskMessage {

	o := new(portal.Organization)
	err := proto.Unmarshal(value, o)
	if err != nil {
		return reconcile.TaskMessageErrorf("organization unmarshal: %v", err)
	}

	err = mkOrgDir(o)
	if err != nil {
		return reconcile.TaskMessageError(err)
	}

	return nil
}

func (ot *OrganizationTask) Update(prev, value []byte, version int64, td *reconcile.TaskData) *reconcile.TaskMessage {
	return ot.Create(value, version, td)
}

func (ot *OrganizationTask) Ensure(prev, value []byte, version int64, td *reconcile.TaskData) *reconcile.TaskMessage {
	return ot.Create(value, version, td)
}

func (ot *OrganizationTask) Delete(value []byte, version int64, td *reconcile.TaskData) *reconcile.TaskMessage {

	log.Infof("[%s] delete organization", ot.Name)

	o := new(portal.Organization)
	err := proto.Unmarshal(value, o)
	if err != nil {
		return reconcile.TaskMessageErrorf("organization unmarshal: %v", err)
	}

	err = rmDir(path.Join(orgRoot, o.Name))
	if err != nil {
		return reconcile.TaskMessageError(err)
	}

	return nil
}

func mkOrgDir(o *portal.Organization) error {

	var uid uint32 = 0
	c, err := orgCreator(o)
	if err != nil {
		log.Debug("no org creator set, using root UID for org dir")
	} else {
		uid = c.Uid
	}

	err = mkDir(path.Join(orgRoot, o.Name), uid, uid, 0700)
	if err != nil {
		return fmt.Errorf("[%s] mkDir: %v", o.Name, err)
	}

	return nil
}

func orgCreator(o *portal.Organization) (*storage.User, error) {

	for mid, m := range o.Members {
		if m.Role == portal.Member_Creator {

			u := storage.NewUser(mid)
			err := u.Read()
			if err != nil {
				return nil, fmt.Errorf("member/user read: %v", err)
			}
			return u, nil
		}
	}

	return nil, fmt.Errorf("[%s] no organization creator found", o.Name)
}
