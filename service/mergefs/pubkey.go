package main

import (
	"os"
	"path"
	"regexp"
	"strings"

	log "github.com/sirupsen/logrus"
	portal "gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/portal/services/pkg/storage"
	"gitlab.com/mergetb/tech/reconcile"
	"google.golang.org/protobuf/proto"
)

var (
	// fingerprint is bas64 whick includes /
	pubkeyBucket = storage.PrefixedBucket(&storage.PublicKey{})
	pubkeyKey    = regexp.MustCompile("^" + pubkeyBucket + " (" + nameExp + ")/(.+)$")
)

type PubkeyTask struct {
	user string
	fp   string
}

func (pt *PubkeyTask) Parse(k string) bool {

	tkns := pubkeyKey.FindAllStringSubmatch(k, -1)
	if len(tkns) > 0 {
		pt.user = tkns[0][1]
		pt.fp = tkns[0][2]
		return true
	}

	return false
}

func (pt *PubkeyTask) Create(value []byte, version int64, td *reconcile.TaskData) *reconcile.TaskMessage {

	k := new(portal.PublicKey)
	err := proto.Unmarshal(value, k)
	if err != nil {
		return reconcile.TaskMessageErrorf("proto unmarshal: %v", err)
	}

	p := path.Join(userRoot, pt.user, ".ssh", "authorized_keys")

	err = touchFile(pt.user, p, 0600)
	if err != nil {
		return reconcile.TaskMessageError(err)
	}

	// The 'key' in the PublicKey is just the chars from the file itself. So we can just add it
	// to the existing file.
	ok, err := lineExists(p, k.Key)
	if err != nil {
		return reconcile.TaskMessageError(err)
	}

	if ok {
		log.Infof("ignoring already existing pubkey %s", k.Fingerprint)
		return nil // already done - duplicate add.
	}

	fd, err := os.OpenFile(p, os.O_APPEND|os.O_WRONLY, 0600)
	if err != nil {
		return reconcile.TaskMessageError(err)
	}
	defer fd.Close()
	defer fd.Sync()

	// enforce a single newline between entries.
	line := strings.TrimSpace(k.Key)
	_, err = fd.WriteString(line + "\n")
	if err != nil {
		return reconcile.TaskMessageErrorf("auth keys write: %v", err)
	}

	return nil
}

func (pt *PubkeyTask) Update(prev, value []byte, version int64, td *reconcile.TaskData) *reconcile.TaskMessage {
	return pt.Create(value, version, td)
}

func (pt *PubkeyTask) Ensure(prev, value []byte, version int64, td *reconcile.TaskData) *reconcile.TaskMessage {
	return pt.Create(value, version, td)
}

func (pt *PubkeyTask) Delete(value []byte, version int64, td *reconcile.TaskData) *reconcile.TaskMessage {

	k := new(portal.PublicKey)
	err := proto.Unmarshal(value, k)
	if err != nil {
		return reconcile.TaskMessageErrorf("proto unmarshal: %v", err)
	}

	p := path.Join(userRoot, pt.user, ".ssh", "authorized_keys")

	err = delLine(p, k.Key)
	if err != nil {
		return reconcile.TaskMessageError(err)
	}

	return nil
}
