package main

/*==============================================================================
 * Model Service
 * =============
 *
 * The Model service is a reconciler that observed updates to experiment
 * revisions. Every experiment has a revision key i.e.
 *
 *     /experiment/battlestar/galactica/rev/000cf5117f4 -> 000cf5117f4...
 *
 * the value points to the hash of the current revision of the experiment on the
 * master branch (multi-branch not supported yet). When a push happens this rev
 * subkey is updated. The revision history is available through etcd key
 * versioning, the current version always points to the head revision.
 *
 * This reconciler works by
 *
 *     1. Observing experiment revision subkeys.
 *     2. Pulling the source associated with each revision.
 *     3. Compiling the model found in that revision.
 *     4. Pushing the compiled XIR into MinIO.
 *     5. Pushing the compilation status into storage (etcd).
 *
 * A status subkey named `model` is kept on the revision subkey i.e.
 *
 *     /experiment/battlestar/galactica/rev/model -> {
 *         Result: Working,
 *         Worker: $HOSTNAME,
 *         Version: version(/experiment/battlestar/galactica/rev),
 *     }
 *
 * This key tracks to what point in the revision history the experiment has been
 * reconciled.
 *
 * When the reconciler is started, no assumptions are made about the current
 * state. Each experiment revision in etcd is checked against it's status
 * subkey. If the current revision is greater than what's tracked in the status
 * object, or if no status subkey exists, the revision is reconciled. Once the
 * reconciler has gotten through it's initial pass and is reacting to key update
 * notifications, no attempts are made to look backwards into the key history,
 * we assume that the revision in the update notification is the only
 * reconciliation that is needed.
 *
 * If the model key is deleted, we drop all assumptions about the reconciliation
 * state and run the history from the begining like on startup. This provides an
 * easy way for admins and tools to force a full reconciliation, just delete the
 * status subkey.
 *
 *============================================================================*/

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"regexp"

	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/plumbing"
	"github.com/minio/minio-go/v7"
	log "github.com/sirupsen/logrus"

	"gitlab.com/mergetb/portal/services/internal"
	//"gitlab.com/mergetb/portal/services/pkg/merror"
	"gitlab.com/mergetb/portal/services/pkg/storage"

	"gitlab.com/mergetb/tech/reconcile"
)

var (
	nameExp          = "([a-zA-Z_]+[a-zA-Z0-9_]+)"
	experimentBucket = storage.PrefixedBucket(&storage.Experiment{})
	branchKey        = regexp.MustCompile(
		"^" + experimentBucket + nameExp + "/" + nameExp + "/branch/" + nameExp + "$",
	)
	tagKey = regexp.MustCompile(
		"^" + experimentBucket + nameExp + "/" + nameExp + "/tag/(.+)$",
	)
	revKey = regexp.MustCompile(
		"^" + experimentBucket + nameExp + "/" + nameExp + "/rev/(.+)$",
	)
)

func init() {
	internal.InitLogging()
	internal.InitReconciler()
}

var Version = ""

func main() {

	log.Infof("portal version: %s", Version)

	err := storage.InitPortalEtcdClient()
	if err != nil {
		log.Fatalf("etcd client init: %v", err)
	}

	err = storage.InitPortalMinIOClient()
	if err != nil {
		log.Fatalf("minio client init: %v", err)
	}

	go runGrpcServer()

	t := storage.ReconcilerConfigModel.ToReconcilerManager(
		&XpTask{},
	)

	t.Run()

}

type XpTask struct {
	Key        string
	Experiment string
	Project    string
	Branch     string
	Tag        string
	Rev        string
}

func (t *XpTask) Parse(key string) bool {

	log.Debugf("parsing key: %s", key)

	if tkns := branchKey.FindAllStringSubmatch(key, -1); len(tkns) > 0 {
		t.Key = key
		t.Project = tkns[0][1]
		t.Experiment = tkns[0][2]
		t.Branch = tkns[0][3]
		log.Debugf("handling branch %s", t.Branch)
		return true
	}

	if tkns := tagKey.FindAllStringSubmatch(key, -1); len(tkns) > 0 {
		t.Key = key
		t.Project = tkns[0][1]
		t.Experiment = tkns[0][2]
		t.Tag = tkns[0][3]
		log.Debugf("handling tag %s", t.Tag)
		return true
	}

	if tkns := revKey.FindAllStringSubmatch(key, -1); len(tkns) > 0 {
		t.Key = key
		t.Project = tkns[0][1]
		t.Experiment = tkns[0][2]
		t.Rev = tkns[0][3]
		log.Debugf("handling rev %s", t.Rev)
		return true
	}

	log.Debugf("not handling key %s", key)
	return false
}

func (t *XpTask) Create(value []byte, version int64, td *reconcile.TaskData) *reconcile.TaskMessage {

	// not a revision, so don't do anything
	if len(t.Rev) == 0 {
		log.Infof("[%s] not compiling model", t.Key)
		return nil
	}

	// 1. Process new revision
	revision := string(value)
	log.Infof("[%s] handling model request %s", t.Key, revision)

	comp_err := t.handleExperimentUpdate(revision)
	if comp_err != nil {
		// not fatal
		log.Infof("exp update error: %v", comp_err)
	}

	// 2. Track compilation status
	exp := storage.NewExperiment(t.Experiment, t.Project)
	err := exp.AddCompilation(revision, comp_err) // note: sets model compilation timestamp
	if err != nil {
		log.Errorf("failed to add revision %s to experiment %s.%s: %v",
			revision, t.Experiment, t.Project, err,
		)
	}

	// 3. if we have a rev key, delete it so that it doesn't clog up the key space
	if len(t.Rev) > 0 {
		_, err := td.TaskValue.Delete(td.EtcdClient())
		if err != nil {
			log.Error(err)
		}
	}

	return nil

}

func (t *XpTask) Update(prev, value []byte, version int64, td *reconcile.TaskData) *reconcile.TaskMessage {

	log.Infof("[%s] received update; treating as create", t.Key)
	return t.Create(value, version, td)

}

func (t *XpTask) Ensure(prev, value []byte, version int64, td *reconcile.TaskData) *reconcile.TaskMessage {

	log.Infof("[%s] received ensure; ignoring", t.Key)
	return reconcile.TaskMessageUndefined()

}

func (t *XpTask) Delete(value []byte, version int64, td *reconcile.TaskData) *reconcile.TaskMessage {

	if len(t.Rev) > 0 {
		// don't do anything on rev delete
		return nil
	}

	bucketName := fmt.Sprintf("xp-%s-%s", t.Project, t.Experiment)

	ch := storage.MinIOClient.ListObjects(
		context.TODO(),
		bucketName,
		minio.ListObjectsOptions{},
	)

	chErr := storage.MinIOClient.RemoveObjects(
		context.TODO(),
		bucketName,
		ch,
		minio.RemoveObjectsOptions{},
	)
	for err := range chErr {
		log.Errorf("[%s] minio delete object: %v", t.Key, err)
	}

	err := storage.MinIOClient.RemoveBucket(
		context.TODO(),
		bucketName,
	)
	if err != nil {
		log.Errorf("[%s] minio delete bucket: %v", t.Key, err)
	}

	return nil

}

func (t *XpTask) handleExperimentUpdate(revision string) error {

	// 0. don't recompile if it already exists
	if checkXirAlreadyExists(t.Project, t.Experiment, revision) {
		return nil
	}

	// 1. Pull source

	dir, err := os.MkdirTemp(os.TempDir(), fmt.Sprintf("xprev.%s.%s*", t.Project, t.Experiment))
	if err != nil {
		return fmt.Errorf("create temporary directory: %v", err)
	}

	cloneOps := &git.CloneOptions{
		URL: fmt.Sprintf("http://git-server:8080/%s/%s", t.Project, t.Experiment),
	}

	if t.Branch != "" {
		cloneOps.ReferenceName = plumbing.NewBranchReferenceName(t.Branch)
		cloneOps.SingleBranch = true
	}

	repo, err := git.PlainClone(dir, false, cloneOps)
	if err != nil {
		// ignore the ref not found error - it's a bug in go-git
		log.Infof("got clone error: %+v", err)
		if !errors.Is(err, plumbing.ErrReferenceNotFound) {
			return fmt.Errorf("reconcile clone repo: %v", err)
		}
		log.Infof("Ignoring ref not found error on clone")
	}

	w, err := repo.Worktree()
	if err != nil {
		return fmt.Errorf("git worktree: %v", err)
	}

	hash, err := repo.ResolveRevision(plumbing.Revision(revision))
	if err != nil {
		return fmt.Errorf("resolve revision: %v", err)
	}

	log.Infof("Checking out revision %s", hash)
	err = w.Checkout(&git.CheckoutOptions{
		Hash: *hash,
	})
	if err != nil {
		return fmt.Errorf("git revision checkout: %v", err)
	}

	// 2. Compile source into XIR
	modelPath := filepath.Join(dir, "model.py")
	out, err := os.ReadFile(modelPath)
	if err != nil {
		return err
	}

	net, err := compileModel(string(out))
	if err != nil {
		return err
	}

	buf, err := net.ToB64Buf()

	if err != nil {
		return fmt.Errorf("Error converting network to base64: %+v", err)
	}

	// 3. Push XIR to MinIO
	model, err := os.ReadFile(modelPath)
	if err != nil {
		return fmt.Errorf("Unable to read model file: %v", err)
	}

	err = storeXir(t.Project, t.Experiment, revision, model, buf)
	if err != nil {
		return fmt.Errorf("Storing XIR: %v", err)
	}

	return nil
}

func checkXirAlreadyExists(project, experiment, revision string) bool {

	bucket := fmt.Sprintf("xp-%s-%s", project, experiment)
	_, err := storage.MinIOClient.StatObject(
		context.TODO(),
		bucket,
		revision,
		minio.GetObjectOptions{},
	)

	return err == nil
}

func storeXir(project, experiment, revision string, model, buf []byte) error {

	bucket := fmt.Sprintf("xp-%s-%s", project, experiment)

	found, err := storage.MinIOClient.BucketExists(context.TODO(), bucket)
	if err != nil {
		return fmt.Errorf("minio check bucket: %v", err)
	}
	if !found {
		err := storage.MinIOClient.MakeBucket(
			context.TODO(), bucket, minio.MakeBucketOptions{})
		if err != nil {
			return fmt.Errorf("minio make bucket: %v", err)
		}
	}

	_, err = storage.MinIOClient.PutObject(
		context.TODO(),
		bucket,
		revision, // object name as hash
		bytes.NewReader(buf),
		int64(len(buf)),
		minio.PutObjectOptions{},
	)
	if err != nil {
		return fmt.Errorf("minio put: %v", err)
	}

	// store the model.py with this revision for later reference.
	_, err = storage.MinIOClient.PutObject(
		context.TODO(),
		bucket,
		revision+"-model",
		bytes.NewReader(model),
		int64(len(model)),
		minio.PutObjectOptions{},
	)
	if err != nil {
		return fmt.Errorf("minio put: %v", err)
	}

	return nil

}
