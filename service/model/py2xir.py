import sys
from sys import stdin, stderr, exit
import os
import mergexp
import base64
import traceback

# grab the well-known module-level global that has the topology.
from mergexp import __xp

# execute the given python model script.
try:
    exec(stdin.read())
except Exception as e:
    messages = []

    tbe = traceback.TracebackException.from_exception(e)

    # as exec(stdin.read()) is also on the stack, we skip it,
    # as it's not very useful for a user
    if len(tbe.stack.format()) >= 2:
        messages.append(''.join(tbe.stack.format()[1:]).strip())

    messages.append(''.join(traceback.format_exception_only(e)).strip())

    ans = []
    for m in messages:
        # remove/rename these messages
        m = m.replace('File "<string>",', 'File "model.py",')
        m = m.replace(', in <module>', '')

        if m:
            ans.append(m)

    print('\n'.join(ans), file=stderr)
    exit(10)

# make sure the __xp instance is OK.
if not isinstance(__xp['topo'], mergexp.Network):
    print("bad mergexp network %s"%(type(__xp['topo'])), file=stderr)
    print("netwk %s"%(__xp['topo']), file=stderr)
    exit(20)

topo = __xp['topo']

# base64 encode it so it's predictably printable and readable
print(base64.b64encode(topo.xir().SerializeToString()).decode())
exit(0)
