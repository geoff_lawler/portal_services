package main

import (
	"context"
	"errors"
	"fmt"
	"net"
	"os"
	"path/filepath"
	"time"

	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/config"
	"github.com/go-git/go-git/v5/plumbing"
	gpo "github.com/go-git/go-git/v5/plumbing/object"
	"github.com/go-git/go-git/v5/plumbing/transport"
	http "github.com/go-git/go-git/v5/plumbing/transport/http"

	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	portal "gitlab.com/mergetb/api/portal/v1/go"
	id "gitlab.com/mergetb/portal/services/pkg/identity"
	me "gitlab.com/mergetb/portal/services/pkg/merror"
	"google.golang.org/protobuf/encoding/protojson"
)

type srv struct{}

func runGrpcServer() {

	log.Infof("Starting model GRPC service")

	grpcServer := grpc.NewServer()
	portal.RegisterModelServer(grpcServer, &srv{})

	l, err := net.Listen("tcp", "0.0.0.0:6000")
	if err != nil {
		log.Fatalf("failed to listen: %#v", err)
	}

	log.Info("Listening on tcp://0.0.0.0:6000")
	grpcServer.Serve(l)
}

func (m *srv) Compile(
	ctx context.Context, rq *portal.CompileRequest,
) (*portal.CompileResponse, error) {

	resp := &portal.CompileResponse{
		Success: false,
	}

	net, err := compileModel(rq.Model)
	if err != nil {

		var merr *me.MergeError
		if errors.As(err, &merr) {
			if errors.Is(err, me.ErrMxCompileError) {
				return &portal.CompileResponse{
					Success: false,
					Errors:  []string{merr.Detail},
				}, nil
			}

			return nil, me.ToGRPCError(merr)
		}

		return nil, err
	}

	buf, err := protojson.Marshal(net)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	resp.Success = true
	resp.Network = string(buf)

	return resp, nil
}

// NB: This function checks out the entire repository so could take a loong time.
// The better way to do this (TODO, of course) is to have live repos live on disk
// and track with the experimetor's usage. These would likely live in the git service
// and be accessed via a custom GRPC interface to the git service.
//
// There is also support for cloning into memory, but that has it's own troublesome
// things.
func (m *srv) Push(
	ctx context.Context, rq *portal.PushRequest,
) (*portal.PushResponse, error) {

	log.Debugf("push req: %+v", rq)

	if rq.Branch == "" {
		rq.Branch = "master"
	}

	if rq.Branch != "master" {
		return nil, status.Error(codes.InvalidArgument, "push to non-master branch not yet implmented")
	}

	token, tokenType, err := id.AccessTokenFromGrpcContext(ctx)
	if err != nil {
		return nil, status.Errorf(codes.Unauthenticated, "request lacks authentication information")
	}

	log.Debugf("Got a %s token: %s", tokenType, token)

	var auth transport.AuthMethod
	if tokenType == id.Bearer {
		auth = &http.BasicAuth{
			Username: token,
			Password: "",
		}
	} else {
		auth = &http.TokenAuth{
			Token: token,
		}
	}

	pid, eid := rq.Project, rq.Experiment
	url := fmt.Sprintf("http://git-server:8080/%s/%s", pid, eid)
	empty := false

	// Checkout repo
	dir, err := os.MkdirTemp(os.TempDir(), fmt.Sprintf("xprev.%s.%s*", pid, eid))
	if err != nil {
		return nil, status.Errorf(codes.Internal, "create temporary directory: %v", err)
	}
	defer os.RemoveAll(dir)

	cloneOps := &git.CloneOptions{
		URL:           fmt.Sprintf("http://git-server:8080/%s/%s", pid, eid),
		ReferenceName: plumbing.NewBranchReferenceName(rq.Branch),
		SingleBranch:  true,
	}

	repo, err := git.PlainClone(dir, false, cloneOps)
	if err != nil {
		// older versions of the git server will return a repository empty error.
		// newer verson reports that the branch/ref is missing
		if err != transport.ErrEmptyRemoteRepository {
			if errors.Is(err, git.NoMatchingRefSpecError{}) {
				// always create the master branch if it doesn't exist
				if rq.Branch != "master" {
					return nil, status.Errorf(
						codes.FailedPrecondition,
						"Unable to push to non existent branch %s", rq.Branch,
					)
				}
			} else {
				return nil, status.Errorf(codes.Internal, "clone repo: %v", err)
			}
		}
		// handle an empty repo by init'ing a local repo. Bug in go-git will
		// error out and not create the repo when cloning an empty repo. So
		// create our own empty repo here with a remote that points to the exp
		// repo. Not the best, but it works.
		//
		// My rating of ease of use, documentation, and expected reaction
		// in go-git is one star.
		//
		repo, err = git.PlainInit(dir, false)
		if err != nil {
			return nil, status.Errorf(codes.Internal, "git plain init: %+v", err)
		}

		_, err = repo.CreateRemote(&config.RemoteConfig{
			Name: "origin",
			URLs: []string{url},
		})
		if err != nil {
			return nil, status.Errorf(codes.Internal, "git create remote:  %+v", err)
		}

		empty = true
	}

	w, err := repo.Worktree()
	if err != nil {
		return nil, status.Errorf(codes.Internal, "git worktree: %v", err)
	}

	if empty != true {
		err = w.Checkout(&git.CheckoutOptions{})
		if err != nil {
			return nil, status.Errorf(codes.Internal, "git plain checkout: %v", err)
		}
	}

	// write model, commit change, and push.
	file := "model.py"
	err = os.WriteFile(filepath.Join(dir, file), []byte(rq.Model), 0644)

	_, err = w.Add(file)
	if err != nil {
		return nil, status.Errorf(codes.Internal, "git add: %+v", err)
	}

	sig := &gpo.Signature{
		Name:  "mergebot",
		Email: "ops@mergetb.net",
		When:  time.Now(),
	}

	rev, err := w.Commit("merge model auto-commit", &git.CommitOptions{
		All:    true,
		Author: sig,
	})
	if err != nil {
		return nil, status.Errorf(codes.Internal, "git commit: %+v", err)
	}

	// once pushed, this reconciler will pick it up as a change and do the compile thing.
	err = repo.Push(&git.PushOptions{
		Auth: auth,
	})
	if err != nil {
		return nil, status.Errorf(codes.Internal, "git push: %+v", err)
	}

	if rq.Tag != "" {
		// create the tag and push it.
		ref := createRef(repo, w, "tag", rq.Tag)
		err = repo.Push(&git.PushOptions{
			Auth: auth,
			RefSpecs: []config.RefSpec{
				config.RefSpec(
					// This refspec is refs/tags/[name]:refs/tags/[name]
					// see https://git-scm.com/book/en/v2/Git-Internals-The-Refspec
					fmt.Sprintf("%s:%s", ref.Name(), ref.Name()),
				)},
		})
		if err != nil {
			return nil, status.Errorf(codes.Internal, "git push tag: %+v", err)
		}
	}

	return &portal.PushResponse{Revision: rev.String()}, nil
}

func createRef(repo *git.Repository, w *git.Worktree, kind, name string) *plumbing.Reference {

	var refName plumbing.ReferenceName

	b, err := repo.Branch(name)
	if err != nil {
		if err == git.ErrBranchNotFound {
			if kind == "tag" {
				refName = plumbing.NewTagReferenceName(name)
			} else {
				refName = plumbing.NewBranchReferenceName(name)
			}
			log.Infof("Creating new ref %s", refName)
		} else {
			log.Fatalf("repo branch: %s", err.Error())
		}
	} else {
		refName = b.Merge
	}

	head, err := repo.Head()
	if err != nil {
		if err != plumbing.ErrReferenceNotFound {
			log.Fatalf("repo head: %s", err.Error())
		}
		head = plumbing.NewSymbolicReference(plumbing.HEAD, refName)
	}

	ref := plumbing.NewHashReference(refName, head.Hash())

	err = repo.Storer.SetReference(ref)
	if err != nil {
		log.Fatalf("repo set ref: %s", err.Error())
	}

	return ref
}
