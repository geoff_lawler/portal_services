package main

import (
	"context"
	"fmt"
	"net/http"

	"github.com/grpc-ecosystem/go-grpc-middleware/v2/interceptors"
	log "github.com/sirupsen/logrus"
	id "gitlab.com/mergetb/portal/services/pkg/identity"
	ident "gitlab.com/mergetb/portal/services/pkg/identity"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type KratosAuthHandler struct {
	next http.Handler
}

func checkAuthKratos(h http.Handler) http.Handler {

	return &KratosAuthHandler{next: h}

}

func needsAuth(r *http.Request) bool {

	// List of URLs that do not require authorization:
	// * POST /register - request an account, cannot be authorized as there is no account
	// * POST /login - cannot be authorized as user is logging in
	// * POST /organization/membership - user requesting membership in in an org before user
	//    account is authorized. It is then up to the organization creators to approve
	//    the request and init/activate the user portal account.
	// * GET /organization - for listing orgs. We have this here an non-activated users
	//	  need to list orgs to request access to them. Same for user categories and location
	//    possibilities.
	//   GET /configurations/user and /configurations/entitytype - read public portal
	//       configuration about user config (list of institutions, countries, US States),
	//       user categories, and project/org categories and subcategories.
	switch r.Method {
	case "GET":
		switch r.URL.Path {
		case "/organization", "/configurations/user", "/configurations/entitytype":
			return false
		}
	case "POST":
		switch r.URL.Path {
		case "/register", "/login", "/organization/membership":
			return false
		}
	}

	return true
}

func (h *KratosAuthHandler) check(w http.ResponseWriter, r *http.Request) error {

	if r.Method == http.MethodOptions {
		return nil
	}

	if !needsAuth(r) {
		return nil
	}

	c, err := r.Cookie("ory_kratos_session")
	if err != nil {
		log.Warnf("error getting kratos session cookie: %v", err)
		w.WriteHeader(401) // unauthorized
		return err
	}

	// We've got the cookie, so check the session.
	cli := ident.KratosPublicCli()
	s, resp, err := cli.FrontendAPI.ToSession(context.Background()).Cookie(c.String()).Execute()
	if err != nil {
		log.Debugf("Error `Ory.ToSession``: %v\n", err)
		log.Debugf("Full HTTP response: %v\n", resp)
		w.WriteHeader(401) // unauthorized
		return fmt.Errorf("Unable to get user auth session")
	}

	log.Debugf("valid session: %v", s)

	// valid session. apiserver will confirm user against policy.

	return nil
}

func (h *KratosAuthHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	err := h.check(w, r)
	if err != nil {
		log.Errorf("auth check failed: %v", err)
		return
	}

	log.Info("auth check ok")

	h.next.ServeHTTP(w, r)

}

//
// Handle a disabled API. The strategy here is to read kratos id and user information and only allow
// authenticated admin access. To read kratos ID and user data, we need an authenticated kratos session.
// This means we need to allow Login/Logout calls to always be allowed. On successful login, subsequent
// API calls will have a token which allows us to get the kratos session, traits, and read the username
// and thus read the User data from etcd.
//
// So we do that then check for admin on the traits (portal ops) or Admin in the User data (portal user
// with Admin access.)
//

// allButLoginAPI - Allow all but Identity.Login/Logout calls.
func allButLoginAPI(ctx context.Context, callMeta interceptors.CallMeta) bool {
	// TODO: instead of hardcoded strings here, find correct type in grpc grenerated code that maps to
	// these strings. Doesn't seem to bwe a way to do this though. :shrug:
	return !(callMeta.Service == "portal.v1.Identity" && (callMeta.Method == "Login" || callMeta.Method == "Logout"))
}

// disabledApiAuth - if the API is disabled, only allow portal ops and users with admin access enabled.
// Otherwise deny all access with Permission Denied.
func disabledApiAuth(ctx context.Context) (context.Context, error) {

	if disabled_api {
		// We allow login and logout API to still be active. This lets us get traits and user data
		// given the grpc context as Login() sets the kratos session on valid authentication.
		user, traits, err := ident.GetUserAndTraits(ctx)
		if err != nil {
			return nil, status.Errorf(codes.PermissionDenied, "Error reading user and ID data: %v", err)
		}

		if !(traits.Username == id.Admin || user.Admin == true) {
			return nil, status.Errorf(codes.PermissionDenied, "The portal is currently unavailable.")
		}

	}

	return ctx, nil
}
