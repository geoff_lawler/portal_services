package main

import (
	"context"
	"sort"
	"strings"

	"github.com/maruel/natural"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	portal "gitlab.com/mergetb/api/portal/v1/go"
	id "gitlab.com/mergetb/portal/services/pkg/identity"
	"gitlab.com/mergetb/portal/services/pkg/policy"
	"gitlab.com/mergetb/portal/services/pkg/storage"
)

func (x *xps) Alloc(
	ctx context.Context, rq *portal.AllocationRequest,
) (*portal.AllocationResponse, error) {

	// Not clear if this is even needed.
	// TODO(ry)
	return nil, status.Error(codes.Unimplemented, "not implemented")
}

func (x *xps) Free(
	ctx context.Context, rq *portal.FreeRequest,
) (*portal.FreeResponse, error) {

	// Not clear if this is even needed.
	// TODO(ry)
	return nil, status.Error(codes.Unimplemented, "not implemented")
}

func (x *xps) Fetch(
	ctx context.Context, msg *portal.FetchAllocationTableRequest,
) (*portal.FetchAllocationTableResponse, error) {

	// Not clear if this is even needed.
	// TODO(ry)
	return nil, status.Error(codes.Unimplemented, "not implemented")
}

// Resource Pools API
func (x *xps) GetPool(
	ctx context.Context, rq *portal.GetPoolRequest,
) (*portal.GetPoolResponse, error) {

	caller, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, err
	}

	err = policy.ReadPool(caller, rq.Name)
	if err != nil {
		return nil, status.Error(codes.PermissionDenied, err.Error())
	}

	ps, err := storage.GetPools()
	if err != nil {
		return nil, status.Error(codes.Internal, "get pools")
	}

	for _, p := range ps {
		if p.Pool.Name == rq.Name {
			sortResources(p)
			return &portal.GetPoolResponse{Pool: p.Pool}, err
		}
	}

	return nil, status.Errorf(codes.NotFound, "pool not found %s", rq.Name)
}

func (x *xps) GetPools(
	ctx context.Context, rq *portal.GetPoolsRequest,
) (*portal.GetPoolsResponse, error) {

	caller, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, err
	}

	err = policy.ReadPools(caller)
	if err != nil {
		return nil, status.Error(codes.PermissionDenied, err.Error())
	}

	ps, err := storage.GetPools()
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	resp := &portal.GetPoolsResponse{}

	for _, p := range ps {

		err = policy.ReadPool(caller, p.Name)
		if err != nil {
			continue
		}

		sortResources(p)
		resp.Pools = append(resp.Pools, p.Pool)
	}

	return resp, nil
}

func (x *xps) CreatePool(
	ctx context.Context, rq *portal.CreatePoolRequest,
) (*portal.CreatePoolResponse, error) {

	caller, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, err
	}

	err = policy.CreatePool(caller)
	if err != nil {
		return nil, status.Error(codes.PermissionDenied, err.Error())
	}

	// We only allow a project to be in one pool at a time. If the project needs resources
	// from multiple pools, then a new pool should be created.
	for _, proj := range rq.Projects {
		if pp := storage.GetProjectPool(proj); pp != nil {
			if pp.Name != "default" {
				return nil, status.Errorf(codes.FailedPrecondition, "Project %s exists in pool %s", proj, pp.Name)
			}
		}
	}

	if len(rq.Facilities) > 1 {
		return nil, status.Errorf(codes.FailedPrecondition, "Only one facility per pool allowed")
	}

	pool := storage.NewPool(rq.Name)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	for _, f := range rq.Facilities {
		if !facilityExists(f) {
			return nil, status.Errorf(codes.NotFound, "facility does not exist: %s", f)
		}
	}

	for _, p := range rq.Projects {
		if !projectExists(p) {
			return nil, status.Errorf(codes.NotFound, "project does not exist: %s", p)
		}
	}

	for _, o := range rq.Organizations {
		if !organizationExists(o) {
			return nil, status.Errorf(codes.NotFound, "organization does not exist: %s", o)
		}
	}

	pool.Description = rq.Description
	pool.Projects = rq.Projects
	pool.Organizations = rq.Organizations
	pool.Facilities = make(map[string]*portal.Pool_Resources)
	pool.Creator = caller.Username

	for _, f := range rq.Facilities {
		pool.Facilities[f] = &portal.Pool_Resources{}
	}

	_, err = pool.Create()
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	return &portal.CreatePoolResponse{}, nil
}

func (x *xps) DeletePool(
	ctx context.Context, rq *portal.DeletePoolRequest,
) (*portal.DeletePoolResponse, error) {

	caller, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, err
	}

	err = policy.DeletePool(caller, rq.Name)
	if err != nil {
		return nil, status.Error(codes.PermissionDenied, err.Error())
	}

	pool := readPool(rq.Name)
	if pool == nil {
		return nil, status.Errorf(codes.NotFound, "pool %s does not exist", rq.Name)
	}

	_, err = pool.Delete()
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	return &portal.DeletePoolResponse{}, nil
}

func (x *xps) AddProject(
	ctx context.Context, rq *portal.AddProjectToPoolRequest,
) (*portal.AddProjectToPoolResponse, error) {

	caller, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, err
	}

	err = policy.UpdatePoolProject(caller, rq.Name, rq.Project)
	if err != nil {
		return nil, status.Error(codes.PermissionDenied, err.Error())
	}

	if !projectExists(rq.Project) {
		return nil, status.Errorf(codes.NotFound, "project %s does not exist", rq.Project)
	}

	pool := readPool(rq.Name)
	if pool == nil {
		return nil, status.Errorf(codes.NotFound, "pool %s does not exist", rq.Name)
	}

	if pp := storage.GetProjectPool(rq.Project); pp != nil {
		if pp.Name != "default" {
			return nil, status.Errorf(codes.FailedPrecondition, "Project %s exists in pool %s", rq.Project, pp.Name)
		}
	}

	_, err = pool.AddProject(rq.Project)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	return &portal.AddProjectToPoolResponse{}, nil
}

func (x *xps) RemoveProject(
	ctx context.Context, rq *portal.RemoveProjectFromPoolRequest,
) (*portal.RemoveProjectFromPoolResponse, error) {

	caller, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, err
	}

	err = policy.UpdatePoolProject(caller, rq.Name, rq.Project)
	if err != nil {
		return nil, status.Error(codes.PermissionDenied, err.Error())
	}

	pool := readPool(rq.Name)
	if pool == nil {
		return nil, status.Errorf(codes.NotFound, "pool %s does not exist", rq.Name)
	}

	_, err = pool.RemoveProject(rq.Project)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	return &portal.RemoveProjectFromPoolResponse{}, nil
}

func (x *xps) AddOrganization(
	ctx context.Context, rq *portal.AddOrganizationToPoolRequest,
) (*portal.AddOrganizationToPoolResponse, error) {

	caller, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	err = policy.UpdatePoolOrganization(caller, rq.Name, rq.Organization)
	if err != nil {
		return nil, status.Error(codes.PermissionDenied, err.Error())
	}

	if !organizationExists(rq.Organization) {
		return nil, status.Errorf(codes.NotFound, "organization %s does not exist", rq.Organization)
	}

	pool := readPool(rq.Name)
	if pool == nil {
		return nil, status.Errorf(codes.NotFound, "pool %s does not exist", rq.Name)
	}

	if op := storage.GetOrganizationPool(rq.Organization); op != nil {
		if op.Name != "default" {
			return nil, status.Errorf(codes.FailedPrecondition, "Organization %s exists in pool %s", rq.Organization, op.Name)
		}
	}

	_, err = pool.AddOrganization(rq.Organization)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	return &portal.AddOrganizationToPoolResponse{}, nil
}

func (x *xps) RemoveOrganization(
	ctx context.Context, rq *portal.RemoveOrganizationFromPoolRequest,
) (*portal.RemoveOrganizationFromPoolResponse, error) {

	caller, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	err = policy.UpdatePoolOrganization(caller, rq.Name, rq.Organization)
	if err != nil {
		return nil, status.Error(codes.PermissionDenied, err.Error())
	}

	pool := readPool(rq.Name)
	if pool == nil {
		return nil, status.Errorf(codes.NotFound, "pool %s does not exist", rq.Name)
	}

	_, err = pool.RemoveOrganization(rq.Organization)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	return &portal.RemoveOrganizationFromPoolResponse{}, nil
}

func (x *xps) AddFacility(
	ctx context.Context, rq *portal.AddFacilityToPoolRequest,
) (*portal.AddFacilityToPoolResponse, error) {

	caller, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, err
	}

	err = policy.UpdatePoolFacility(caller, rq.Name, rq.Facility)
	if err != nil {
		return nil, status.Error(codes.PermissionDenied, err.Error())
	}

	pool := readPool(rq.Name)
	if pool == nil {
		return nil, status.Errorf(codes.NotFound, "pool %s does not exist", rq.Name)
	}

	if !facilityExists(rq.Facility) {
		return nil, status.Errorf(codes.NotFound, "facility %s does not exist", rq.Facility)
	}

	if _, ok := pool.Facilities[rq.Facility]; ok {
		return nil, status.Error(codes.AlreadyExists, "Facility already exists in pool")
	}

	//
	// We currently enforce one facility per pool. This is because the portal doesn't support
	// multi-facility realizations. If we allowed more than one facility here, then the realizations
	// for projects in this pool would get resources from more than one facility.
	//
	// Once the portal supports this, we can remove this restriction.
	//
	if len(pool.Facilities) > 0 {
		return nil, status.Errorf(codes.FailedPrecondition, "Only one facility per pool allowed")
	}

	// confirm the resources do in fact exist.
	missing := missingResources(rq.Facility, rq.Resources)
	if len(missing) > 0 {
		return nil, status.Errorf(codes.NotFound, "Resources do not exist: %s", strings.Join(missing, " "))
	}

	_, err = pool.AddFacility(rq.Facility, rq.Resources)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	return &portal.AddFacilityToPoolResponse{}, nil
}

func (x *xps) RemoveFacility(
	ctx context.Context, rq *portal.RemoveFacilityFromPoolRequest,
) (*portal.RemoveFacilityFromPoolResponse, error) {

	caller, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, err
	}

	err = policy.UpdatePoolFacility(caller, rq.Name, rq.Facility)
	if err != nil {
		return nil, status.Error(codes.PermissionDenied, err.Error())
	}

	pool := readPool(rq.Name)
	if pool == nil {
		return nil, status.Errorf(codes.NotFound, "pool %s does not exist", rq.Name)
	}

	if _, ok := pool.Facilities[rq.Facility]; !ok {
		return nil, status.Error(codes.NotFound, "Facility is not in the pool")
	}

	_, err = pool.RemoveFacility(rq.Facility)
	if err != nil {
		return nil, status.Error(codes.Internal, "remove facility: "+err.Error())
	}

	return &portal.RemoveFacilityFromPoolResponse{}, nil
}

func (x *xps) UpdatePoolResources(
	ctx context.Context, rq *portal.UpdatePoolResourcesRequest,
) (*portal.UpdatePoolResourcesResponse, error) {

	caller, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, err
	}

	err = policy.UpdatePoolFacility(caller, rq.Pool, rq.Facility)
	if err != nil {
		return nil, status.Error(codes.PermissionDenied, err.Error())
	}

	pool := readPool(rq.Pool)
	if pool == nil {
		return nil, status.Errorf(codes.NotFound, "pool %s does not exist", rq.Pool)
	}

	if _, ok := pool.Facilities[rq.Facility]; !ok {
		return nil, status.Error(codes.NotFound, "facility is not in the pool")
	}

	// confirm the resources do in fact exist.
	missing := missingResources(rq.Facility, rq.Resources)
	if len(missing) > 0 {
		return nil, status.Errorf(codes.NotFound, "Resources do not exist: %s", strings.Join(missing, " "))
	}

	_, err = pool.UpdateResources(rq.Facility, rq.Resources, rq.Patchstrategy)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	// reread to get updated resources.
	err = pool.Read()
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	return &portal.UpdatePoolResourcesResponse{
		Pool:      rq.Pool,
		Facility:  rq.Facility,
		Resources: pool.Facilities[rq.Facility].Resources,
	}, nil
}

func soExists(o storage.Object) bool {

	err := o.Read()
	if err != nil {
		return false
	}

	if o.GetVersion() == 0 {
		return false
	}

	return true
}

func facilityExists(name string) bool {
	o := storage.NewFacility(name)
	return soExists(o)
}

func projectExists(name string) bool {
	o := storage.NewProject(name)
	return soExists(o)
}

func organizationExists(name string) bool {
	o := storage.NewOrganization(name)
	return soExists(o)
}

func readPool(name string) *storage.Pool {

	o := storage.NewPool(name)
	err := o.Read()
	if err != nil {
		return nil
	}

	if o.GetVersion() == 0 {
		return nil
	}

	return o
}

func missingResources(facility string, resources []string) []string {

	result := []string{}

	model, err := storage.ReadFacilityModel(facility)
	if err != nil {
		return result
	}

	for _, r := range resources {
		found := false
		for _, mr := range model.Resources {
			if mr.Id == r {
				found = true
				break
			}
		}
		if !found {
			result = append(result, r)
		}
	}

	return result
}

func sortResources(p *storage.Pool) {

	for _, res := range p.Facilities {
		sort.Sort(natural.StringSlice(res.Resources))
	}
}
