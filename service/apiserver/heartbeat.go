package main

import (
	"time"

	log "github.com/sirupsen/logrus"

	"gitlab.com/mergetb/tech/reconcile/pkg/heartbeat"

	"gitlab.com/mergetb/portal/services/pkg/storage"
)

// Send heartbeats to reconcilers by periodically writing a key to etcd
// Use a periodic interval from config
func runHeartbeater() {

	interval := storage.HeartbeatIntervalFromConfig()
	if interval == 0 {
		log.Warn("disabling reconciler heartbeat service. Interval 0 (or not set) in portal config")
		return
	}

	log.Infof("starting reconciler heartbeat service with interval of %d seconds", interval)

	for {
		err := heartbeat.SendEtcdHeartbeat(storage.EtcdClient, interval)
		if err != nil {
			log.Errorf("failed to sent reconciler heartbeat: %+v", err)
		} else {
			log.Tracef("sent etcd heartbeat with interval of %d seconds", interval)
		}

		time.Sleep(time.Duration(interval) * time.Second)
	}
}
