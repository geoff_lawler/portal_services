package main

import (
	"context"

	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	portal "gitlab.com/mergetb/api/portal/v1/go"

	"gitlab.com/mergetb/portal/services/internal"
	id "gitlab.com/mergetb/portal/services/pkg/identity"
	"gitlab.com/mergetb/portal/services/pkg/policy"
	"gitlab.com/mergetb/portal/services/pkg/storage"
)

func (x *xps) AddWgIfConfig(
	ctx context.Context, rq *portal.AddWgIfConfigRequest,
) (*portal.AddWgIfConfigResponse, error) {

	l := log.WithFields(log.Fields{
		"request": rq,
	})
	l.Info("received AddWgIfConfig request")

	// TODO this is called by faciliry operators. Update roles and policy framework
	// to allow this check
	/*
		caller, err := id.GRPCCaller(ctx)
		if err != nil {
			l.Error(err)
			return nil, status.Error(codes.Internal, "GRPC caller")
		}

		mzid, err := internal.MzidFromString(rq.Enclaveid)
		if err != nil {
			l.Error(err)
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}

		// Seems correct to use materialize policy
		err = policy.Materialize(caller, mzid.Pid, mzid.Eid, mzid.Rid)
		if err != nil {
			l.Error(err)
			return nil, status.Error(codes.PermissionDenied, "forbidden")
		}
	*/

	wgif := storage.NewWgIfRequest(rq.Enclaveid, rq.Config.Key)
	err := wgif.Read()
	if err != nil {
		l.Error(err)
		return nil, status.Error(codes.Internal, err.Error())
	}

	wgif.Gateway = rq.Gateway
	wgif.Config.Endpoint = rq.Config.Endpoint
	wgif.Config.Accessaddr = rq.Config.Accessaddr

	// IS RECONCILED
	if wgif.Ver != 0 {
		// we already have this key in this enclave
		l.Warn("pubkey/enclave already exists")
		_, err = wgif.Update()
	} else {
		_, err = wgif.Create()
	}

	if err != nil {
		l.Error(err)
		return nil, status.Error(codes.Internal, err.Error())
	}

	return &portal.AddWgIfConfigResponse{}, nil
}

func (x *xps) DelWgIfConfig(
	ctx context.Context, rq *portal.DelWgIfConfigRequest,
) (*portal.DelWgIfConfigResponse, error) {

	l := log.WithFields(log.Fields{
		"request": rq,
	})
	l.Info("received DelWgIfConfig request")

	// TODO this is called by faciliry operators. Update roles and policy framework
	// to allow this check
	/*
		caller, err := id.GRPCCaller(ctx)
		if err != nil {
			return nil, status.Error(codes.Internal, "GRPC caller")
		}

		mzid, err := internal.MzidFromString(rq.Enclaveid)
		if err != nil {
			l.Error(err)
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}

		// Seems correct to use materialize policy...
		err = policy.Materialize(caller, mzid.Pid, mzid.Eid, mzid.Rid)
		if err != nil {
			return nil, status.Error(codes.PermissionDenied, "forbidden")
		}
	*/

	wgif := storage.NewWgIfRequest(rq.Enclaveid, rq.Key)
	err := wgif.Read()
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	if wgif.Ver == 0 {
		l.Warn("pubkey/enclave does not exist; nothing to do")
	} else {
		_, err = wgif.Delete()
		if err != nil {
			return nil, status.Error(codes.Internal, err.Error())
		}
	}

	return &portal.DelWgIfConfigResponse{}, nil
}

func (x *xps) GetWgIfConfig(
	ctx context.Context, rq *portal.GetWgIfConfigRequest,
) (*portal.GetWgIfConfigResponse, error) {

	return nil, status.Error(codes.Unimplemented, "not implemented")
}

func (x *xps) GetWgEnclave(
	ctx context.Context, rq *portal.GetWgEnclaveRequest,
) (*portal.GetWgEnclaveResponse, error) {

	log.Infof("get wg enclave %s", rq.Enclaveid)

	caller, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, err
	}

	mzid, err := internal.MzidFromString(rq.Enclaveid)
	if err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	// Seems correct to use materialize policy...
	err = policy.Materialize(caller, mzid.Pid, mzid.Eid, mzid.Rid)
	if err != nil {
		return nil, status.Error(codes.PermissionDenied, "forbidden")
	}

	enc := storage.NewWgEnclave(rq.Enclaveid)
	err = enc.Read()
	if err != nil {
		return nil, status.Errorf(codes.Internal, err.Error())
	}

	if enc.Ver == 0 {
		return nil, status.Error(codes.NotFound, "enclave does not exist")
	}

	return &portal.GetWgEnclaveResponse{Enclave: enc.WgEnclave}, nil
}
