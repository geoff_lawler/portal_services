package main

import (
	"context"
	"errors"
	"fmt"
	"sort"
	"time"

	"github.com/maruel/natural"
	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	portal "gitlab.com/mergetb/api/portal/v1/go"
	id "gitlab.com/mergetb/portal/services/pkg/identity"
	"gitlab.com/mergetb/portal/services/pkg/merror"
	"gitlab.com/mergetb/portal/services/pkg/policy"
	"gitlab.com/mergetb/portal/services/pkg/realize"
	"gitlab.com/mergetb/portal/services/pkg/storage"
)

func (x *xps) GetRealizations(
	ctx context.Context, rq *portal.GetRealizationsRequest,
) (*portal.GetRealizationsResponse, error) {

	// grab the caller info

	caller, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, err
	}

	u := storage.NewUser(caller.Username)
	err = u.Read()
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	resp := new(portal.GetRealizationsResponse)

	// read in all experiments from all projects.
	// We then use policy to filter out projects
	// the caller cannot read.
	projects, err := storage.ListProjects()
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	for _, proj := range projects {

		if rq.Filter == portal.FilterMode_ByUser {
			// Only show rls relevant to the user.
			if member, ok := proj.Members[caller.Username]; ok {
				if member.State != portal.Member_Active {
					// is a user but is not active.
					continue
				}
			} else {
				// user not a member
				continue
			}
		}

		err = policy.ReadProject(caller, proj.Name)
		if err != nil {
			log.Infof("Skipping read project. User read denied.")
			continue
		}

		for _, e := range proj.Experiments {

			err = policy.ReadExperiment(caller, proj.Name, e)
			if err != nil {
				log.Infof("skipping read exp. user access denied")
				continue
			}

			exp := storage.NewExperiment(e, proj.Name)
			err := exp.Read()
			if err != nil {
				return nil, status.Error(codes.Internal, err.Error())
			}

			for revid, model := range exp.Models {

				log.Debugf("EXPERIMENT %s REVISION %s has %d realizations", exp.Name, revid, len(model.Realizations))

				for _, rlz := range model.Realizations {

					err = policy.ReadRealization(caller, exp.Project, exp.Name, rlz)
					if err != nil {
						log.Infof("skipping read rlz. user access denied")
						continue
					}

					// we found it, so just append it
					result, err := storage.ReadRealizationResult(exp.Project, exp.Name, rlz)
					if err == nil {
						resp.Results = append(resp.Results, result)
						continue
					}

					// not an in progress error, so log it and continue
					if !errors.Is(err, merror.ErrRealizeInProgress) {
						log.Error(err)
						continue
					}

					// we didn't find the result, but if the request exists,
					// add an inprogress result for it
					rq := storage.NewRealizeRequest(rlz, exp.Name, exp.Project)
					err = rq.Read()
					if err != nil {
						log.Error(err)
						continue
					}

					missing, err := rq.ToInProgressRealizationResult()
					if err != nil {
						log.Error(err)
						continue
					}

					resp.Results = append(resp.Results, missing)
				}
			}
		}
	}

	// sort results.
	sort.Slice(resp.Results, func(i, j int) bool {
		rName := func(r *portal.Realization) string {
			return fmt.Sprintf("%s.%s.%s", r.Id, r.Eid, r.Pid)
		}
		return natural.Less(rName(resp.Results[i].Realization), rName(resp.Results[j].Realization))
	})

	if rq.Summary {
		for _, res := range resp.Results {
			res.Summary = storage.ToRealizationSummary(res.Realization)
			res.Realization = nil
		}
	}

	return resp, nil
}

func (x *xps) GetRealization(
	ctx context.Context, rq *portal.GetRealizationRequest,
) (*portal.GetRealizationResponse, error) {

	caller, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, err
	}

	err = policy.ReadRealization(caller, rq.Project, rq.Experiment, rq.Realization)
	if err != nil {
		return nil, status.Error(codes.PermissionDenied, err.Error())
	}

	orig_rq := storage.NewRealizeRequest(rq.Realization, rq.Experiment, rq.Project)

	result, err := storage.ReadRealizationResult(rq.Project, rq.Experiment, rq.Realization)
	if err != nil {
		if !errors.Is(err, merror.ErrRealizeInProgress) {
			return nil, merror.ToGRPCError(err)
		}

		// if it's in progress, that's fine, just get the data from the rq
		err := orig_rq.Read()
		if err != nil {
			return nil, merror.ToGRPCError(err)
		}

		result, err = orig_rq.ToInProgressRealizationResult()
		if err != nil {
			return nil, merror.ToGRPCError(err)
		}

	}

	ans := &portal.GetRealizationResponse{
		Result: result,
	}

	if rq.StatusMS != 0 {
		tf, err := orig_rq.GetGoal(time.Duration(rq.StatusMS) * time.Millisecond)
		if err != nil {
			return nil, merror.ToGRPCError(err)
		}

		ans.Status = portal.NewTaskTreeFromReconcileForest(tf)
	}

	return ans, nil

}

func (x *xps) Realize(
	ctx context.Context, rq *portal.RealizeRequest,
) (*portal.RealizeResponse, error) {

	log.Debugf("Realize request %s.%s.%s", rq.Realization, rq.Experiment, rq.Project)

	caller, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, err
	}

	err = policy.CreateRealization(caller, rq.Project, rq.Experiment, rq.Realization)
	if err != nil {
		return nil, status.Error(codes.PermissionDenied, err.Error())
	}

	// Enforce the allowed mtz name.
	if err := storage.IsValidMaterializationName(rq.Project, rq.Experiment, rq.Realization); err != nil {
		return nil, status.Error(codes.InvalidArgument, fmt.Sprintf("Invalid realization name: %s", err.Error()))
	}

	// bail early if no facility has been commissioned
	fcs, err := storage.ListFacilities()
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	if len(fcs) == 0 {
		return nil, status.Error(codes.Internal, "no facilities have been commissioned")
	}

	// preflight
	//
	// We used to check whether a {project, experiment, rlz} name is
	// valid or not using a regex, but we now enforce the validity of it
	// in the Merge API. Otherwise, we'd have to ensure that the regex
	// is consistent between the API and apiserver.

	// We only accept one key into the realization: tag, branch, or revision.
	rev := ""
	if rq.Branch != "" && rq.Revision == "" && rq.Tag == "" {

		rev, err = readRev("branch", rq.Branch, rq.Project, rq.Experiment)
		if err != nil {
			return nil, status.Errorf(codes.Internal, err.Error())
		}

	} else if rq.Branch == "" && rq.Revision == "" && rq.Tag != "" {

		rev, err = readRev("tag", rq.Tag, rq.Project, rq.Experiment)
		if err != nil {
			return nil, status.Errorf(codes.Internal, err.Error())
		}

	} else if rq.Branch == "" && rq.Revision != "" && rq.Tag == "" {

		rev = rq.Revision

	} else {
		return nil, status.Errorf(
			codes.InvalidArgument,
			"Realization takes tag, revision, or branch. Only one can be specfied.",
		)
	}

	// read experiment to ensure that revision exists and is compiled successfully
	exp := storage.NewExperiment(rq.Experiment, rq.Project)
	err = exp.Read()
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	model, _, err := exp.ReadExperimentModel(rev)
	if err != nil {
		return nil, merror.ToGRPCError(merror.NotFoundError(
			fmt.Sprintf("%s.%s revision", rq.Experiment, rq.Project),
			rev,
		))
	}

	if model.Compiled == false {
		return nil, merror.ToGRPCError(merror.MxCompileError(
			fmt.Sprintf("%s.%s contains revision %s, but it failed to compile",
				rq.Experiment, rq.Project, rev,
			),
		))
	}

	if rq.Creator == "" {
		rq.Creator = caller.Username
	}

	rq.Revision = rev

	r := &storage.RealizeRequest{RealizeRequest: rq}
	err = r.Read()
	if err != nil {
		return nil, status.Errorf(codes.Internal, err.Error())
	}

	if r.GetVersion() != 0 {
		return nil, status.Error(codes.AlreadyExists, "realization already exists")
	}

	// IS RECONCILED
	_, err = r.Create()
	if err != nil {
		return nil, merror.ToGRPCError(err)
	}

	return &portal.RealizeResponse{}, nil

}

func (x *xps) Relinquish(
	ctx context.Context, rq *portal.RelinquishRequest,
) (*portal.RelinquishResponse, error) {

	caller, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, err
	}

	err = policy.DeleteRealization(caller, rq.Project, rq.Experiment, rq.Realization)
	if err != nil {
		return nil, status.Error(codes.PermissionDenied, err.Error())
	}

	r := storage.NewRealizeRequest(
		rq.Realization,
		rq.Experiment,
		rq.Project,
	)
	_, err = r.Delete()
	if err != nil {
		return nil, merror.ToGRPCError(err)
	}

	return &portal.RelinquishResponse{}, nil

}

func (x *xps) GetResources(
	ctx context.Context, rq *portal.GetResourcesRequest,
) (*portal.GetResourcesResponse, error) {

	caller, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, err
	}

	res, err := realize.GetResources()
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	// Now filter resources by the pools the user can access.
	userResources, err := getUserResources(caller)
	if err != nil {
		return nil, status.Errorf(codes.Internal, err.Error())
	}

	response := &portal.GetResourcesResponse{}

	// this is not efficent.
	for _, candidate := range res {
		if resources, ok := userResources[candidate.Resource.Facility]; ok {
			for _, resource := range resources {
				if candidate.Resource.Id == resource {
					response.Resources = append(response.Resources, candidate)
				}
			}
		}
	}

	return response, nil

}

func (x *xps) UpdateRealization(
	ctx context.Context, rq *portal.UpdateRealizationRequest,
) (*portal.UpdateRealizationResponse, error) {

	caller, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, err
	}

	err = policy.CreateRealization(caller, rq.Project, rq.Experiment, rq.Realization)
	if err != nil {
		return nil, status.Error(codes.PermissionDenied, err.Error())
	}

	// we only support updating the expiry of the realization at the moment,
	if rq.Duration.When == portal.ReservationDuration_given && rq.Duration.Duration == "" {
		// Not set, ignore update request
		return &portal.UpdateRealizationResponse{}, nil
	}

	rr, err := storage.ReadRealizationResult(rq.Project, rq.Experiment, rq.Realization)
	if err != nil {
		return nil, merror.ToGRPCError(err)
	}

	resp := &portal.UpdateRealizationResponse{}

	expires, err := storage.ParseReservationDuration(time.Now(), rq.Duration)
	if err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	rr.Realization.Expires = expires
	resp.Expires = expires

	err = storage.WriteRealizationResult(rr)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	return resp, nil
}

// tag and branch entries in etcd hold the latest revision of the given tag or branch.
func readRev(kind, name, pid, eid string) (string, error) {

	key := fmt.Sprintf("/experiments/%s/%s/%s/%s", pid, eid, kind, name)

	resp, err := storage.EtcdClient.Get(context.TODO(), key)
	if err != nil {
		return "", fmt.Errorf("read %s %s failed: %v", kind, name, err)
	}

	if len(resp.Kvs) != 1 {
		return "", fmt.Errorf("bad data in %s %s read", kind, name)
	}

	return string(resp.Kvs[0].Value), nil
}

func getUserResources(user *id.IdentityTraits) (map[string][]string, error) {

	log.Debugf("finding user accessible resources for %s", user.Username)

	projects, err := storage.ListProjects()
	if err != nil {
		return nil, err
	}

	// poor man's set
	resources := make(map[string]map[string]bool)

	for _, project := range projects {

		err = policy.ReadProject(user, project.Name)
		if err != nil {
			log.Debugf("user does not have read access to project %s", project.Name)
			continue
		}

		if pool := storage.GetProjectPool(project.Name); pool != nil {
			for f, fr := range pool.Facilities {
				if len(resources[f]) == 0 {
					resources[f] = make(map[string]bool)
				}
				for _, node := range fr.Resources {
					resources[f][node] = true
				}
			}
		}
	}

	result := make(map[string][]string)
	for k, v := range resources {
		for node := range v {
			result[k] = append(result[k], node)
		}
	}

	log.Debugf("user resources: %v", result)

	return result, nil
}
