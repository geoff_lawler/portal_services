package main

import (
	"context"
	"fmt"
	"io"

	"github.com/minio/minio-go/v7"
	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	portal "gitlab.com/mergetb/api/portal/v1/go"
	_ "gitlab.com/mergetb/portal/services/pkg/commission"
	id "gitlab.com/mergetb/portal/services/pkg/identity"
	"gitlab.com/mergetb/portal/services/pkg/policy"
	"gitlab.com/mergetb/portal/services/pkg/storage"
	xir "gitlab.com/mergetb/xir/v0.3/go"
)

func (x *xps) RegisterFacility(
	ctx context.Context, rq *portal.RegisterFacilityRequest,
) (*portal.RegisterFacilityResponse, error) {

	var rbs storage.RbStack

	caller, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, err
	}

	err = policy.CreateFacility(caller)
	if err != nil {
		return nil, status.Error(codes.PermissionDenied, err.Error())
	}

	if rq.Facility == nil {
		return nil, status.Error(codes.InvalidArgument, "facility object required")
	}

	f := storage.NewFacility(rq.Facility.Name)
	err = f.Read()
	if err != nil {
		return nil, status.Error(codes.Internal, "facility read")
	}

	if f.Ver != 0 {
		return nil, status.Error(codes.AlreadyExists, "facility already exists")
	}

	f.Facility = rq.Facility
	rb, err := f.Create()
	if err != nil {
		return nil, fmt.Errorf("internal error: %v", err)
	}
	rbs = append(rbs, rb)

	if rq.Model != nil {

		err = storage.CreateFacilityModel(rq.Facility.Name, rq.Model)
		if err != nil {
			rbs.Unwind()
			return nil, status.Error(codes.Internal, err.Error())
		}
	}

	return &portal.RegisterFacilityResponse{}, nil

}

func (x *xps) GetFacilities(
	ctx context.Context, rq *portal.GetFacilitiesRequest,
) (*portal.GetFacilitiesResponse, error) {

	caller, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, err
	}

	// let policy decide what a user can see.
	facs, err := storage.ListFacilities()
	if err != nil {
		return nil, status.Error(codes.Internal, "list facilities")
	}

	result := new(portal.GetFacilitiesResponse)
	for _, f := range facs {

		err = policy.ReadFacility(caller, f.Name)
		if err != nil {
			continue
		}

		result.Facilities = append(result.Facilities, f.Facility)
	}

	if len(result.Facilities) == 0 {
		return nil, status.Error(codes.PermissionDenied, "access to no facilities")
	}

	return result, nil
}

func (x *xps) GetFacility(
	ctx context.Context, rq *portal.GetFacilityRequest,
) (*portal.GetFacilityResponse, error) {

	caller, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, err
	}

	err = policy.ReadFacility(caller, rq.Name)
	if err != nil {
		return nil, status.Error(codes.PermissionDenied, "forbidden")
	}

	f := storage.NewFacility(rq.Name)
	err = f.Read()
	if err != nil {
		return nil, status.Error(codes.Internal, "read facility")
	}

	var model *xir.Facility
	if rq.WithModel {

		model, err = storage.ReadFacilityModel(rq.Name)
		if err != nil {
			return nil, err
		}

	}

	return &portal.GetFacilityResponse{Facility: f.Facility, Model: model}, nil
}

func (x *xps) UpdateFacility(
	ctx context.Context, rq *portal.UpdateFacilityRequest,
) (*portal.UpdateFacilityResponse, error) {

	// Support for updating the meta-data about a facility. This is the information
	// found in etcd in the portal.Facility data structure. This is handled
	// just by calling the pkg.storage.Facility Update() function.

	// We do support a very limited range of facility model updates currently:
	//
	// 1. Alloc Modes for resources.
	//		This is used to update Alloc/NoAlloc to make nodes allocable or not.
	// 2. Roles for resources.
	//      This is (can be) used to update active emu nodes.

	//
	// More model update complicated scenarios will require running reconcilation
	// over model changes. For example, updating or voiding existing realizations when a node
	// is removed from the model. This is future work.
	//

	caller, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, err
	}

	err = policy.UpdateFacility(caller, rq.Name)
	if err != nil {
		return nil, status.Error(codes.PermissionDenied, "forbidden")
	}

	f := storage.NewFacility(rq.Name)
	err = f.Read()
	if err != nil {
		return nil, status.Errorf(codes.Internal, "internal read facilty db err: %v", err)
	}

	if f.GetVersion() == 0 {
		return nil, status.Errorf(codes.NotFound, "no such facility %s", f.Name)
	}

	f.UpdateRequest = rq
	_, err = f.Update()
	if err != nil {
		return nil, status.Errorf(codes.Internal, "Error updating facility: %v", err)
	}

	if rq.Model != nil {
		err = storage.UpdateFacilityModel(rq)
		if err != nil {
			return nil, status.Errorf(codes.Internal, "Error updating facility model: %v", err)
		}
	}

	return &portal.UpdateFacilityResponse{}, nil
}

// Resourse status
func (x *xps) DeactivateResources(
	ctx context.Context, rq *portal.DeactivateResourcesRequest,
) (*portal.DeactivateResourcesResponse, error) {

	// We cheat here and just invoke the facility update with a model
	// that has the resources set to remove no_alloc.
	ureq := &portal.UpdateFacilityRequest{
		Name:  rq.Facility,
		Model: &xir.Facility{},
		ModelPatchStrategy: &portal.PatchStrategy{
			Strategy: portal.PatchStrategy_expand,
		},
	}

	// build a list of resources that are no-alloc and add that to the model via an update.
	for _, n := range rq.Resources {
		ureq.Model.Resources = append(ureq.Model.Resources, &xir.Resource{
			Id:    n,
			Alloc: []xir.AllocMode{xir.AllocMode_NoAlloc},
		})
	}

	_, err := x.UpdateFacility(ctx, ureq)

	if err != nil {
		return nil, err
	}

	return &portal.DeactivateResourcesResponse{}, nil
}

func (x *xps) ActivateResources(
	ctx context.Context, rq *portal.ActivateResourcesRequest,
) (*portal.ActivateResourcesResponse, error) {

	// See above (DeactivateResources) re: cheating.
	ureq := &portal.UpdateFacilityRequest{
		Name:  rq.Facility,
		Model: &xir.Facility{},
		ModelPatchStrategy: &portal.PatchStrategy{
			Strategy: portal.PatchStrategy_subtract, // remove any no allocs we find.
		},
	}

	// build a list of resources that are no-alloc and add that to the model via an update.
	for _, n := range rq.Resources {
		ureq.Model.Resources = append(ureq.Model.Resources, &xir.Resource{
			Id:    n,
			Alloc: []xir.AllocMode{xir.AllocMode_NoAlloc},
		})
	}

	_, err := x.UpdateFacility(ctx, ureq)

	if err != nil {
		return nil, err
	}

	return &portal.ActivateResourcesResponse{}, nil
}

func (x *xps) DeleteFacility(
	ctx context.Context, rq *portal.DeleteFacilityRequest,
) (*portal.DeleteFacilityResponse, error) {

	var rbs storage.RbStack

	caller, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, err
	}

	err = policy.DeleteFacility(caller, rq.Name)
	if err != nil {
		return nil, status.Error(codes.PermissionDenied, "forbidden")
	}

	bucket := fmt.Sprintf("facility-%s", rq.Name)

	bucketFound, err := storage.MinIOClient.BucketExists(context.TODO(), bucket)
	if err != nil {
		log.Errorf("minio: bucket exists check: %s %v", bucket, err)
		return nil, fmt.Errorf("model save error")
	}

	if bucketFound {

		// read the model and release block pool allocations

		modelFound := true
		_, err = storage.MinIOClient.StatObject(
			context.TODO(),
			bucket,
			"model",
			minio.StatObjectOptions{},
		)
		if err != nil {
			errResponse := minio.ToErrorResponse(err)
			if errResponse.Code == "NoSuchKey" {
				modelFound = false
			} else {
				log.Errorf("stat model failed: %v", err)
				return nil, fmt.Errorf("stat model failed")
			}
		}

		if modelFound {

			bp, err := storage.FetchBlockPool()
			if err != nil {
				log.Errorf("failed to fetch block pool: %v", err)
				return nil, fmt.Errorf("block pool fetch failed")
			}

			obj, err := storage.MinIOClient.GetObject(
				context.TODO(),
				bucket,
				"model",
				minio.GetObjectOptions{},
			)
			if err != nil {
				log.Errorf("failed to get facility model for %s: %v", bucket, err)
				return nil, fmt.Errorf("facility model get failed")
			}
			defer obj.Close()

			buf, err := io.ReadAll(obj)
			if err != nil {
				log.Errorf("failed to read facility model for %s: %v", bucket, err)
				return nil, fmt.Errorf("Facility model read failed")
			}

			model, err := xir.FacilityFromB64String(string(buf))
			if err != nil {
				log.Errorf("failed to parse facility model for %s: %v", bucket, err)
				return nil, fmt.Errorf("Facility parse failed")
			}

			for _, r := range model.Resources {
				if r.HasRoles(xir.Role_InfraSwitch, xir.Role_Leaf) {
					if r.LeafConfig != nil {
						for _, b := range r.LeafConfig.ServiceAddressBlocks.List {
							bp.Blocks = append(bp.Blocks, b)
						}
						for _, b := range r.LeafConfig.TenantAddressBlocks.List {
							bp.Blocks = append(bp.Blocks, b)
						}
						for _, bs := range r.LeafConfig.InfrapodAddressBlocks {
							for _, b := range bs.List {
								bp.Blocks = append(bp.Blocks, b)
							}
						}
					}
				}
			}

			rb, err := storage.WriteBlockPool(bp)
			if err != nil {
				return nil, fmt.Errorf("block pool write failed: %v", err)
			}
			rbs = append(rbs, rb)

			// remove the model

			objs := storage.MinIOClient.ListObjects(
				context.TODO(),
				bucket,
				minio.ListObjectsOptions{
					Recursive: true,
				})

			errs := storage.MinIOClient.RemoveObjects(
				context.TODO(),
				bucket,
				objs,
				minio.RemoveObjectsOptions{})

			failed := false
			for err := range errs {
				rbs.Unwind()
				failed = true
				log.Errorf("delete object from bucket %s: %v", bucket, err)
			}
			if failed {
				return nil, fmt.Errorf("bucket clear failed")
			}

			err = storage.MinIOClient.RemoveBucket(context.TODO(), bucket)
			if err != nil {
				rbs.Unwind()
				log.Errorf("failed to delete bucket %s: %v", bucket, err)
				return nil, fmt.Errorf("bucket delete error")
			}

		}

	}

	// remove the metadata

	f := storage.NewFacility(rq.Name)
	rb, err := f.Delete()
	if err != nil {
		return nil, fmt.Errorf("internal error: %v", err)
	}
	rbs = append(rbs, rb)

	return &portal.DeleteFacilityResponse{}, nil

}

func (x *xps) ListBlockPools(
	ctx context.Context, rq *portal.ListBlockPoolsRequest,
) (*portal.ListBlockPoolsResponse, error) {

	return nil, nil

}

func (x *xps) InitializeHarbor(
	ctx context.Context, rq *portal.InitHarborRequest,
) (*portal.InitHarborResponse, error) {

	caller, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, err
	}

	err = policy.UpdateFacility(caller, rq.Facility)
	if err != nil {
		return nil, status.Error(codes.PermissionDenied, "forbidden")
	}

	// preflight

	_, err = storage.MinIOClient.StatObject(
		context.TODO(),
		fmt.Sprintf("facility-%s", rq.Facility),
		"model",
		minio.StatObjectOptions{},
	)
	if err != nil {
		log.Errorf("init harbor: stat model: %v", err)
		return nil, fmt.Errorf(
			"error getting facility model fot %s: %v", rq.Facility, err)
	}

	// IS RECONCILED
	ihr := storage.InitHarborRequest{rq}
	_, err = ihr.Create()
	if err != nil {
		log.Errorf("create init harbor request for %s: %v", rq.Facility, err)
		return nil, status.Error(codes.Internal, "failed to create init harbor request")
	}

	return &portal.InitHarborResponse{}, nil

}

func (x *xps) DeinitializeHarbor(
	ctx context.Context, rq *portal.DeinitHarborRequest,
) (*portal.DeinitHarborResponse, error) {

	caller, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, err
	}

	err = policy.UpdateFacility(caller, rq.Facility)
	if err != nil {
		return nil, status.Error(codes.PermissionDenied, "forbidden")
	}

	// preflight

	_, err = storage.MinIOClient.StatObject(
		context.TODO(),
		fmt.Sprintf("facility-%s", rq.Facility),
		"model",
		minio.StatObjectOptions{},
	)
	if err != nil {
		log.Errorf("deinit harbor: stat model: %v", err)
		return nil, fmt.Errorf(
			"error getting facility model fot %s: %v", rq.Facility, err)
	}

	// IS RECONCILED
	ihr := storage.InitHarborRequest{&portal.InitHarborRequest{
		Facility: rq.Facility,
	}}
	_, err = ihr.Delete()
	if err != nil {
		log.Errorf("delete init harbor request for %s: %v", rq.Facility, err)
		return nil, status.Error(codes.Internal, "failed to create deinit harbor request")
	}

	return &portal.DeinitHarborResponse{}, nil

}
