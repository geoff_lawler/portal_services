package main

import (
	"context"
	"fmt"
	"os"
	"sort"
	"strings"
	"time"

	"golang.org/x/crypto/ssh"
	"golang.org/x/exp/maps"

	"github.com/maruel/natural"
	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"

	portal "gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/portal/services/internal"
	id "gitlab.com/mergetb/portal/services/pkg/identity"
	"gitlab.com/mergetb/portal/services/pkg/merror"
	"gitlab.com/mergetb/portal/services/pkg/policy"
	"gitlab.com/mergetb/portal/services/pkg/storage"
)

// User =======================================================================

func (x *xps) ActivateUser(
	ctx context.Context, rq *portal.ActivateUserRequest,
) (*portal.ActivateUserResponse, error) {

	err := modUserState(ctx, rq.Username, portal.UserState_Active)
	if err != nil {
		return nil, err
	}

	return &portal.ActivateUserResponse{}, nil

}

func (x *xps) FreezeUser(
	ctx context.Context, rq *portal.FreezeUserRequest,
) (*portal.FreezeUserResponse, error) {

	err := modUserState(ctx, rq.Username, portal.UserState_Frozen)
	if err != nil {
		return nil, err
	}

	return &portal.FreezeUserResponse{}, nil

}

func (x *xps) UpdateUser(
	ctx context.Context, rq *portal.UpdateUserRequest,
) (*portal.UpdateUserResponse, error) {

	caller, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, err
	}

	err = policy.UpdateUser(caller, rq.Username)
	if err != nil {
		return nil, merror.ToGRPCError(err)
	}

	if rq.State != nil {
		switch rq.State.Value {
		case portal.UserState_NotSet:

		case portal.UserState_Frozen:
			err = policy.DeactivateUser(caller, rq.Username)
			if err != nil {
				return nil, merror.ToGRPCError(err)
			}

		case portal.UserState_Active:
			err = policy.ActivateUser(caller, rq.Username)
			if err != nil {
				return nil, merror.ToGRPCError(err)
			}

		default:
			return nil, status.Error(codes.InvalidArgument, "invalid user state")
		}
	}

	u := storage.NewUser(rq.Username)
	err = u.Read()
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	if u.Ver == 0 {
		return nil, status.Error(codes.NotFound, err.Error())
	}

	if rq.ToggleAdmin {
		if !caller.Admin {
			return nil, status.Errorf(
				codes.PermissionDenied,
				"%s is not authorized to toggle admin capability",
				caller.Username,
			)
		}
	}

	u.UpdateRequest = rq

	_, err = u.Update()
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	return &portal.UpdateUserResponse{}, nil

}

func (x *xps) InitUser(
	ctx context.Context, rq *portal.InitUserRequest,
) (*portal.InitUserResponse, error) {

	err := initUser(ctx, rq.Username)
	if err != nil {
		return nil, err
	}

	return &portal.InitUserResponse{}, nil

}

// initUser assigns gid/uid and project to the user, if allowed by policy
func initUser(ctx context.Context, username string) error {
	caller, err := id.GRPCCaller(ctx)
	if err != nil {
		return err
	}

	if err = policy.InitUser(caller, username); err != nil {
		// not allowed by policy
		return merror.ToGRPCError(err)
	}

	// confirm the ID actually exists.
	conn, cli, err := identityClient()
	if err != nil {
		return status.Error(codes.Internal, err.Error())
	}
	defer conn.Close()

	_, err = cli.GetIdentity(ctx, &portal.GetIdentityRequest{Username: username})
	if err != nil {
		log.Error(err)
		return status.Errorf(codes.NotFound, "User %s is not registered", username)
	}

	// Create the User - this allocates uid/gid and creates the personal project.
	// IS RECONCILED
	u := storage.NewUser(username)
	_, err = u.Create()
	if err != nil {
		return status.Error(codes.Internal, "create user: "+err.Error())
	}

	return nil

}

func (x *xps) GetUsers(
	ctx context.Context, rq *portal.GetUsersRequest,
) (*portal.GetUsersResponse, error) {

	caller, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, err
	}

	users, err := storage.ListUsers()
	if err != nil {
		return nil, status.Error(codes.PermissionDenied, err.Error())
	}

	// apply policy on a per-user basis. add to list if policy allows.
	resp := new(portal.GetUsersResponse)
	for _, u := range users {
		err = policy.ReadUser(caller, u.Username)
		if err == nil {
			resp.Users = append(resp.Users, u.User)
		}
	}

	sort.Slice(resp.Users, func(i, j int) bool {
		return natural.Less(resp.Users[i].Username, resp.Users[j].Username)
	})

	return resp, nil

}

func (x *xps) GetUser(
	ctx context.Context, rq *portal.GetUserRequest,
) (*portal.GetUserResponse, error) {

	caller, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, err
	}

	err = policy.ReadUser(caller, rq.Username)
	if err != nil {
		return nil, merror.ToGRPCError(err)
	}

	u := storage.NewUser(rq.Username)
	err = u.Read()
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	if u.Ver == 0 {
		return nil, status.Error(codes.NotFound, "user not found")
	}

	resp := new(portal.GetUserResponse)
	resp.User = u.User

	if rq.StatusMS != 0 {
		tf, err := u.GetGoal(time.Duration(rq.StatusMS) * time.Millisecond)

		if err != nil {
			return nil, status.Error(codes.Internal, err.Error())
		}

		resp.Status = portal.NewTaskTreeFromReconcileForest(tf)
	}

	return resp, nil
}

// This function will modify a user's state as indicated in the 'state' variable.
func modUserState(ctx context.Context, username string, state portal.UserState) error {

	caller, err := id.GRPCCaller(ctx)
	if err != nil {
		return err
	}

	if state == portal.UserState_Active {
		err = policy.ActivateUser(caller, username)
		if err != nil {
			return merror.ToGRPCError(err)
		}
	} else {
		err = policy.DeactivateUser(caller, username)
		if err != nil {
			return merror.ToGRPCError(err)
		}
	}

	u := storage.NewUser(username)
	err = u.Read()
	if err != nil {
		return status.Error(codes.Internal, err.Error())
	}

	// to get into this state, the account initial registration data must have been submitted;
	// so, version cannot be 0
	if u.Ver == 0 {
		return status.Error(codes.NotFound, "user not found")
	}

	if state == portal.UserState_Active {
		// ensure that accounts being activated are first initialized
		if u.Uid == 0 || u.Gid == 0 {
			err = initUser(ctx, username)
			if err != nil {
				return err
			}
		}
	}

	u.UpdateRequest = &portal.UpdateUserRequest{
		State: &portal.UserStateUpdate{
			Value: state,
		},
	}

	_, err = u.Update()
	if err != nil {
		return status.Error(codes.Internal, err.Error())
	}

	return nil

}

func (x *xps) DeleteUser(
	ctx context.Context, rq *portal.DeleteUserRequest,
) (*portal.DeleteUserResponse, error) {

	caller, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, err
	}

	_, err = id.GetIdForUser(rq.User)
	if err != nil {
		log.Warnf("No ID found for user %s", rq.User)
	}

	err = policy.DeleteUser(caller, rq.User)
	if err != nil {
		return nil, merror.ToGRPCError(err)
	}

	u := storage.NewUser(rq.User)
	_, err = u.Delete()
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	return &portal.DeleteUserResponse{}, nil

}

// Public Keys ................................................................

func (x *xps) GetUserPublicKeys(
	ctx context.Context,
	rq *portal.GetUserPublicKeysRequest,
) (*portal.GetUserPublicKeysResponse, error) {

	caller, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, err
	}

	err = policy.ReadUser(caller, rq.User)
	if err != nil {
		return nil, merror.ToGRPCError(err)
	}

	keys, err := storage.ListUserPublicKeys(rq.User)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	resp := &portal.GetUserPublicKeysResponse{}

	for _, k := range keys {
		resp.Keys = append(resp.Keys, k.PublicKey)
	}

	return resp, nil
}

func (x *xps) AddUserPublicKey(
	ctx context.Context,
	rq *portal.AddUserPublicKeyRequest,
) (*portal.AddUserPublicKeyResponse, error) {

	caller, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, err
	}

	err = policy.UpdateUser(caller, rq.User)
	if err != nil {
		return nil, merror.ToGRPCError(err)
	}

	p, _, _, _, err := ssh.ParseAuthorizedKey([]byte(rq.Key))
	if err != nil {
		return nil, status.Errorf(codes.InvalidArgument, "unable to parse ssh pubkey: %v", err)
	}

	// IS RECONCILED
	pubkey := storage.NewPublicKey(rq.User, ssh.FingerprintSHA256(p))
	pubkey.PublicKey.Key = rq.Key
	if _, err := pubkey.Create(); err != nil {
		return nil, status.Errorf(codes.Internal, "create pubkey: %v", err)
	}

	return &portal.AddUserPublicKeyResponse{}, nil
}

func (x *xps) DeleteUserPublicKey(
	ctx context.Context,
	rq *portal.DeleteUserPublicKeyRequest,
) (*portal.DeleteUserPublicKeyResponse, error) {

	caller, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, err
	}

	err = policy.UpdateUser(caller, rq.User)
	if err != nil {
		return nil, merror.ToGRPCError(err)
	}

	k := storage.NewPublicKey(rq.User, rq.Fingerprint)
	_, err = k.Delete()
	if err != nil {
		return nil, status.Errorf(codes.Internal, "delete pubkey: %v", err)
	}

	return &portal.DeleteUserPublicKeyResponse{}, nil
}

func (x *xps) DeleteUserPublicKeys(
	ctx context.Context,
	rq *portal.DeleteUserPublicKeysRequest,
) (*portal.DeleteUserPublicKeysResponse, error) {

	caller, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, err
	}

	err = policy.UpdateUser(caller, rq.User)
	if err != nil {
		return nil, merror.ToGRPCError(err)
	}

	keys, err := storage.ListUserPublicKeys(rq.User)
	if err != nil {
		return nil, status.Errorf(codes.Internal, "list user pubkeys: %v", err)
	}
	for _, k := range keys {
		_, err := k.Delete()
		if err != nil {
			return nil, status.Errorf(codes.Internal, "delete user pubkey: %v", err)
		}
	}

	return &portal.DeleteUserPublicKeysResponse{}, nil
}

// Merge generated SSH keys and certifcates.
func (x *xps) GetUserSSHKeys(
	ctx context.Context,
	rq *portal.GetUserSSHKeysRequest,
) (*portal.GetUserSSHKeysResponse, error) {

	log.Debug("GetUserSSHKeys")

	caller, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, err
	}

	err = policy.ReadUser(caller, rq.Username)
	if err != nil {
		return nil, merror.ToGRPCError(err)
	}

	kp := storage.NewSSHUserKeyPair(rq.Username)
	err = kp.Read()
	if err != nil {
		return nil, status.Errorf(codes.Internal, "keys read: %s", err)
	}

	if kp.GetVersion() == 0 {
		return nil, status.Error(codes.NotFound, "keys not found")
	}

	r := &portal.GetUserSSHKeysResponse{
		Keys: &portal.SSHKeyPair{
			Public:  kp.Public,
			Private: kp.Private,
		},
	}

	return r, nil
}

func (x *xps) GetUserSSHCert(
	ctx context.Context,
	rq *portal.GetUserSSHCertRequest,
) (*portal.GetUserSSHCertResponse, error) {

	log.Debug("GetUserSSHCert")

	caller, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, err
	}

	err = policy.ReadUser(caller, rq.Username)
	if err != nil {
		return nil, merror.ToGRPCError(err)
	}

	c := storage.NewSSHUserCert(rq.Username)
	err = c.Read()
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	if c.GetVersion() == 0 {
		return nil, status.Error(codes.NotFound, "cert not found")
	}

	r := &portal.GetUserSSHCertResponse{Cert: c.SSHCert}

	return r, nil
}

// Project ====================================================================

func (x *xps) CreateProject(
	ctx context.Context, rq *portal.CreateProjectRequest,
) (*portal.CreateProjectResponse, error) {

	caller, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, err
	}

	p := storage.NewProject(rq.Project.Name)
	err = p.Read()
	if err != nil {
		return nil, status.Errorf(codes.Internal, err.Error())
	}

	if p.GetVersion() != 0 {
		return nil, status.Error(codes.AlreadyExists, "project already exists")
	}

	// validate policy for project creation
	err = policy.CreateProject(caller, rq.Project.Name)
	if err != nil {
		return nil, merror.ToGRPCError(err)
	}

	// validate policy for adding project to organizations
	if rq.Project.Organization != "" {
		err = policy.AddOrganizationProject(caller, rq.Project.Organization)
		if err != nil {
			return nil, merror.ToGRPCError(err)
		}

		p.Organization = rq.Project.Organization

		if rq.Project.OrgMembership == nil {
			// default is member/active
			p.OrgMembership = &portal.Member{
				Role:  portal.Member_Member,
				State: portal.Member_Active,
			}
		} else {
			p.OrgMembership = rq.Project.OrgMembership
		}
	}

	// users can be added without validation
	for name := range rq.Project.Members {
		rq.Project.Members[name].State = portal.Member_Active
	}

	// fill project data for storage.
	p.Description = rq.Project.Description
	p.AccessMode = rq.Project.AccessMode
	p.Members = rq.Project.Members

	if p.Members == nil {
		p.Members = make(map[string]*portal.Member)
	}

	// add calling user as creator
	p.Members[rq.User] = &portal.Member{
		Role:  portal.Member_Creator,
		State: portal.Member_Active,
	}

	pc, err := storage.GetPortalConfig()
	if err != nil {
		return nil, status.Errorf(codes.Internal, "Unable to read portal configuration: %s", err.Error())
	}

	if rq.Project.Category != "" {
		if _, ok := pc.Config.EntityTypes[rq.Project.Category]; !ok {
			keys := maps.Keys(pc.Config.EntityTypes)
			return nil, status.Errorf(
				codes.InvalidArgument,
				"Project category must be one of %s", strings.Join(keys, ", "),
			)
		}
		p.Category = rq.Project.Category
	}

	if rq.Project.Subcategory != "" {
		if rq.Project.Category == "" {
			return nil, status.Error(codes.InvalidArgument, "Cannot have a subcategory without a category")
		}

		vals := pc.Config.EntityTypes[rq.Project.Category]

		// if there is a subcategory, check it.
		if len(vals) > 0 {
			if rq.Project.Subcategory == "" {
				return nil, status.Errorf(
					codes.InvalidArgument,
					"Project subcategory must exist if category exists",
				)
			}
		} else {
			// if there is no subcategories, this should not be set.
			return nil, status.Errorf(
				codes.InvalidArgument, "Category %s does not have a subcategory", rq.Project.Category,
			)
		}

		p.Subcategory = rq.Project.Subcategory
	}

	// IS RECONCILED
	_, err = p.Create()
	if err != nil {
		return nil, status.Errorf(codes.Internal, err.Error())
	}

	return &portal.CreateProjectResponse{}, nil
}

func (x *xps) GetProjects(
	ctx context.Context, rq *portal.GetProjectsRequest,
) (*portal.GetProjectsResponse, error) {

	caller, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, err
	}

	result := new(portal.GetProjectsResponse)

	if rq.Filter == portal.FilterMode_ByAll {
		// let policy decide what the user can see

		projs, err := storage.ListProjects()
		if err != nil {
			return nil, status.Error(codes.Internal, "list projects")
		}

		for _, p := range projs {

			err = policy.ReadProject(caller, p.Name)
			if err != nil {
				continue
			}

			result.Projects = append(result.Projects, p.Project)
		}
	} else if rq.Filter == portal.FilterMode_ByUser {
		// just return the projects in the user's projects field.
		u := storage.NewUser(caller.Username)
		err := u.Read()
		if err != nil {
			return nil, status.Errorf(codes.NotFound, "no such user: %s", caller.Username)
		}

		for pid := range u.Projects {
			p := storage.NewProject(pid)
			err := p.Read()
			if err != nil {
				log.Errorf("No such project '%s' in user's project list", pid)
				continue
			}

			result.Projects = append(result.Projects, p.Project)
		}

	} else {
		return nil, status.Errorf(codes.InvalidArgument, "Bad API filter: %s", rq.Filter)
	}

	sort.Slice(result.Projects, func(i, j int) bool {
		return natural.Less(result.Projects[i].Name, result.Projects[j].Name)
	})

	return result, nil
}

func (x *xps) GetProject(
	ctx context.Context, rq *portal.GetProjectRequest,
) (*portal.GetProjectResponse, error) {

	caller, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, err
	}

	p := storage.NewProject(rq.Name)
	err = p.Read()
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}
	if p.GetVersion() == 0 {
		return nil, status.Error(codes.NotFound, "project does not exist")
	}

	err = policy.ReadProject(caller, rq.Name)
	if err != nil {
		return nil, merror.ToGRPCError(err)
	}

	ans := &portal.GetProjectResponse{
		Project: p.Project,
	}

	if rq.StatusMS != 0 {
		tf, err := p.GetGoal(time.Duration(rq.StatusMS) * time.Millisecond)
		if err != nil {
			return nil, merror.ToGRPCError(err)
		}

		ans.Status = portal.NewTaskTreeFromReconcileForest(tf)
	}

	return ans, nil
}

func (x *xps) UpdateProject(
	ctx context.Context, rq *portal.UpdateProjectRequest,
) (*portal.UpdateProjectResponse, error) {

	caller, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, err
	}

	// validate policy for project update
	err = policy.UpdateProject(caller, rq.Name)
	if err != nil {
		return nil, merror.ToGRPCError(err)
	}

	// validate policy for adding project to member organizations
	if rq.Organization != nil {
		for name := range rq.Organization.Set {
			err = policy.AddOrganizationProject(caller, name)
			if err != nil {
				return nil, merror.ToGRPCError(err)
			}
		}
	}

	// users can be added without validation
	if rq.Members != nil {
		for name := range rq.Members.Set {
			rq.Members.Set[name].State = portal.Member_Active
		}
	}

	p := storage.NewProject(rq.Name)
	err = p.Read()
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	if p.Ver == 0 {
		return nil, status.Error(codes.NotFound, "project not found")
	}

	p.UpdateRequest = rq

	// IS RECONCILED
	_, err = p.Update()
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	return &portal.UpdateProjectResponse{}, nil
}

func (x *xps) DeleteProject(
	ctx context.Context, rq *portal.DeleteProjectRequest,
) (*portal.DeleteProjectResponse, error) {

	caller, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, err
	}

	p := storage.NewProject(rq.Name)
	err = p.Read()
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}
	if p.GetVersion() == 0 {
		return nil, status.Error(codes.NotFound, "project does not exist")
	}

	err = policy.DeleteProject(caller, rq.Name)
	if err != nil {
		return nil, merror.ToGRPCError(err)
	}

	_, err = p.Delete()
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	return &portal.DeleteProjectResponse{}, nil
}

func (x *xps) GetProjectMembers(
	ctx context.Context, rq *portal.GetProjectMembersRequest,
) (*portal.GetProjectMembersResponse, error) {

	caller, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, err
	}

	err = policy.ReadProject(caller, rq.Name)
	if err != nil {
		return nil, merror.ToGRPCError(err)
	}

	p := storage.NewProject(rq.Name)
	err = p.Read()
	if err != nil {
		return nil, status.Error(codes.NotFound, "project not found")
	}

	members := []*portal.Member{}

	for _, m := range p.Members {
		members = append(members, m)
	}

	return &portal.GetProjectMembersResponse{Members: members}, nil
}

func (x *xps) GetProjectMember(
	ctx context.Context, rq *portal.GetProjectMemberRequest,
) (*portal.GetProjectMemberResponse, error) {

	caller, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, err
	}

	err = policy.ReadProject(caller, rq.Project)
	if err != nil {
		return nil, merror.ToGRPCError(err)
	}

	_, m, err := getProjectMember(rq.Project, rq.Member)
	if err != nil {
		return nil, err
	}

	if m != nil {
		return &portal.GetProjectMemberResponse{Member: m}, nil
	}

	return nil, status.Error(codes.NotFound, "project member not found")
}

func (x *xps) AddProjectMember(
	ctx context.Context, rq *portal.AddProjectMemberRequest,
) (*portal.AddProjectMemberResponse, error) {

	caller, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, err
	}

	err = policy.UpdateProject(caller, rq.Project)
	if err != nil {
		return nil, merror.ToGRPCError(err)
	}

	p, m, err := getProjectMember(rq.Project, rq.Username)
	if err != nil {
		return nil, err
	}

	if m != nil {
		return nil, status.Error(codes.AlreadyExists, "user is already a project member")
	}

	// project members are always active
	rq.Member.State = portal.Member_Active

	// ok now we can add the member
	mu := &portal.MembershipUpdate{}
	mu.Set = make(map[string]*portal.Member)
	mu.Set[rq.Username] = rq.Member
	p.UpdateRequest = &portal.UpdateProjectRequest{Members: mu}

	// IS RECONCILED
	_, err = p.Update()
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	return &portal.AddProjectMemberResponse{}, nil
}

func (x *xps) UpdateProjectMember(
	ctx context.Context, rq *portal.UpdateProjectMemberRequest,
) (*portal.UpdateProjectMemberResponse, error) {

	caller, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, err
	}

	err = policy.UpdateProject(caller, rq.Project)
	if err != nil {
		return nil, merror.ToGRPCError(err)
	}

	p, _, err := getProjectMember(rq.Project, rq.Username)
	if err != nil {
		return nil, err
	}

	// project members are always active
	rq.Member.State = portal.Member_Active

	mu := &portal.MembershipUpdate{}
	mu.Set = make(map[string]*portal.Member)
	mu.Set[rq.Username] = rq.Member
	p.UpdateRequest = &portal.UpdateProjectRequest{Members: mu}

	// IS RECONCILED
	_, err = p.Update()
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	return &portal.UpdateProjectMemberResponse{}, nil
}

func (x *xps) DeleteProjectMember(
	ctx context.Context, rq *portal.DeleteProjectMemberRequest,
) (*portal.DeleteProjectMemberResponse, error) {

	caller, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, err
	}

	// check policy for modifying project and user objects; passing either
	// one is sufficient

	entityPassed := (policy.UpdateProject(caller, rq.Project) == nil)
	memberPassed := (policy.UpdateUser(caller, rq.Member) == nil)

	if !entityPassed && !memberPassed {
		return nil, status.Error(codes.PermissionDenied, err.Error())
	}

	p, m, err := getProjectMember(rq.Project, rq.Member)
	if err != nil {
		return nil, err
	}
	if m == nil {
		return nil, status.Error(codes.NotFound, "user is not a project member")
	}

	// ok now we can del the member
	mu := &portal.MembershipUpdate{}
	mu.Remove = append(mu.Remove, rq.Member)
	p.UpdateRequest = &portal.UpdateProjectRequest{Members: mu}

	// IS RECONCILED
	_, err = p.Update()
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	return &portal.DeleteProjectMemberResponse{}, nil
}

// Experiment ====================================================================

func (x *xps) CreateExperiment(
	ctx context.Context, rq *portal.CreateExperimentRequest,
) (*portal.CreateExperimentResponse, error) {

	log.Debugf("create experiment: %+v", rq.Experiment)

	caller, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, err
	}

	if rq.Experiment == nil {
		return nil, status.Error(codes.InvalidArgument, "experiment descriptor required")
	}

	err = policy.CreateExperiment(caller, rq.Experiment.Project, rq.Experiment.Name)
	if err != nil {
		log.Errorf("create experiment permission denied: %v", err)
		return nil, merror.ToGRPCError(err)
	}

	exp := &storage.Experiment{Experiment: rq.Experiment}

	if rq.Experiment.Creator == "" {
		exp.Creator = caller.Username
	} else {
		exp.Creator = rq.Experiment.Creator
	}

	// ex: api.foo.bar.example.com:1234
	dots := strings.Split(api_endpoint, ":")
	tkns := strings.Split(dots[0], ".")
	repo := fmt.Sprintf("git.%s", strings.Join(tkns[1:], "."))
	exp.Repository = fmt.Sprintf("https://%s/%s/%s", repo, rq.Experiment.Project, rq.Experiment.Name)

	// IS RECONCILED
	_, err = exp.Create()
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	return &portal.CreateExperimentResponse{}, nil
}

func (x *xps) GetExperiments(
	ctx context.Context, rq *portal.GetExperimentsRequest,
) (*portal.GetExperimentsResponse, error) {

	caller, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, err
	}

	experiments := []*storage.Experiment{}

	if rq.Filter == portal.FilterMode_ByAll {
		// let policy decide what can be seen.
		projects, err := storage.ListProjects()
		if err != nil {
			return nil, status.Error(codes.Internal, "list projects")
		}

		for _, p := range projects {

			if err = policy.ReadProject(caller, p.Name); err != nil {
				continue
			}

			for _, e := range p.Experiments {

				if err = policy.ReadExperiment(caller, p.Name, e); err != nil {
					continue
				}
				experiments = append(experiments, storage.NewExperiment(e, p.Name))
			}
		}
	} else if rq.Filter == portal.FilterMode_ByUser {
		// return the experiments from the projects the user is in.
		u := storage.NewUser(caller.Username)
		err := u.Read()
		if err != nil {
			return nil, status.Errorf(codes.NotFound, "no such user: %s", caller.Username)
		}

		for pid := range u.Projects {
			p := storage.NewProject(pid)
			err := p.Read()
			if err != nil {
				log.Errorf("No such project '%s' in user's project list", pid)
				continue
			}

			for _, e := range p.Experiments {
				experiments = append(experiments, storage.NewExperiment(e, pid))
			}
		}
	} else {
		return nil, status.Errorf(codes.InvalidArgument, "Bad API filter: %s", rq.Filter)
	}

	err = storage.ReadExperiments(experiments)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	result := new(portal.GetExperimentsResponse)
	for _, e := range experiments {
		result.Experiments = append(result.Experiments, e.Experiment)
	}

	sort.Slice(result.Experiments, func(i, j int) bool {
		return natural.Less(
			fmt.Sprintf("%s.%s", result.Experiments[i].Name, result.Experiments[i].Project),
			fmt.Sprintf("%s.%s", result.Experiments[j].Name, result.Experiments[j].Project),
		)
	})

	return result, nil
}

func (x *xps) GetProjectExperiments(
	ctx context.Context, rq *portal.GetProjectExperimentsRequest,
) (*portal.GetProjectExperimentsResponse, error) {

	caller, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, err
	}

	err = policy.ReadProject(caller, rq.Project)
	if err != nil {
		return nil, merror.ToGRPCError(err)
	}

	project := storage.NewProject(rq.Project)
	err = project.Read()
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	experiments := []*storage.Experiment{}
	for _, e := range project.Experiments {
		experiments = append(experiments, storage.NewExperiment(e, project.Name))
	}

	err = storage.ReadExperiments(experiments)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	result := new(portal.GetProjectExperimentsResponse)
	for _, e := range experiments {
		result.Experiments = append(result.Experiments, e.Experiment)
	}

	return result, nil
}

func (x *xps) GetExperiment(
	ctx context.Context, rq *portal.GetExperimentRequest,
) (*portal.GetExperimentResponse, error) {

	caller, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, err
	}

	err = policy.ReadExperiment(caller, rq.Project, rq.Experiment)
	if err != nil {
		return nil, merror.ToGRPCError(err)
	}

	e := storage.NewExperiment(rq.Experiment, rq.Project)
	err = e.Read()
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	if e.Ver == 0 {
		return nil, status.Errorf(codes.NotFound, "no such experiment %s.%s", rq.Experiment, rq.Project)
	}

	ans := &portal.GetExperimentResponse{
		Experiment: e.Experiment,
	}

	if rq.WithModels {
		ans.Models, err = e.ReadExperimentModels()
		if err != nil {
			return nil, err
		}
	}

	if rq.StatusMS != 0 {
		tf, in_progress, err := e.GetGoal(time.Duration(rq.StatusMS) * time.Millisecond)
		if err != nil {
			return nil, err
		}

		if e.Experiment.Models == nil {
			e.Experiment.Models = make(map[string]*portal.ExperimentModel)
		}

		ans.Status = portal.NewTaskTreeFromReconcileForest(tf)
		for k, m := range in_progress {
			ans.Experiment.Models[k] = m
		}
	}

	return ans, nil

}

func (x *xps) UpdateExperiment(
	ctx context.Context, rq *portal.UpdateExperimentRequest,
) (*portal.UpdateExperimentResponse, error) {

	caller, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, err
	}

	err = policy.UpdateExperiment(caller, rq.Project, rq.Name)
	if err != nil {
		return nil, merror.ToGRPCError(err)
	}

	// Previously, we used a regex to enforce valid experiment and
	// project names. Now the validity of names is enforced in the Merge
	// API.
	e := storage.NewExperiment(rq.Name, rq.Project)
	err = e.Read()
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	if e.Ver == 0 {
		return nil, status.Errorf(codes.NotFound, "no such experiment %s.%s", rq.Name, rq.Project)
	}

	e.UpdateRequest = rq

	_, err = e.Update()
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	return &portal.UpdateExperimentResponse{}, nil
}

func (x *xps) DeleteExperiment(
	ctx context.Context, rq *portal.DeleteExperimentRequest,
) (*portal.DeleteExperimentResponse, error) {

	log.Debugf("deleting %s.%s", rq.Experiment, rq.Project)

	caller, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, err
	}

	err = policy.DeleteExperiment(caller, rq.Project, rq.Experiment)
	if err != nil {
		log.Errorf("delete experiment permission denied: %v", err)
		return nil, merror.ToGRPCError(err)
	}

	exp := storage.NewExperiment(rq.Experiment, rq.Project)
	err = exp.Read()
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	if exp.Ver == 0 {
		return nil, status.Errorf(codes.NotFound, "no such experiment %s.%s", rq.Experiment, rq.Project)
	}

	_, err = exp.Delete()
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	return &portal.DeleteExperimentResponse{}, nil
}

func (x *xps) GetRevision(
	ctx context.Context, rq *portal.GetRevisionRequest,
) (*portal.GetRevisionResponse, error) {

	caller, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, err
	}

	// if you can read the experiment, you can read the revisions.
	err = policy.ReadExperiment(caller, rq.Project, rq.Experiment)
	if err != nil {
		return nil, merror.ToGRPCError(err)
	}

	e := storage.NewExperiment(rq.Experiment, rq.Project)
	err = e.Read()
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	if e.Ver == 0 {
		return nil, status.Errorf(codes.NotFound, "no such experiment %s.%s", rq.Experiment, rq.Project)
	}

	xpnet, model, err := e.ReadExperimentModel(rq.Revision)
	if err != nil {
		return nil, err
	}

	var enc []byte

	switch rq.Encoding {

	case portal.GetRevisionRequest_XIRB64:
		enc, err = xpnet.Model.ToB64Buf()
		if err != nil {
			return nil, status.Error(codes.Internal, err.Error())
		}

	case portal.GetRevisionRequest_DOT:
		g, err := xpnet.Model.ToNodeGraph()
		if err != nil {
			return nil, status.Error(codes.Internal, err.Error())
		}

		enc, err = g.ToDot()
		if err != nil {
			return nil, status.Error(codes.Internal, err.Error())
		}
	}

	return &portal.GetRevisionResponse{Model: xpnet, Encoding: string(enc), ModelFile: model}, nil

}

// Organizations =====================================================================

func (x *xps) ActivateOrganization(
	ctx context.Context, rq *portal.ActivateOrganizationRequest,
) (*portal.ActivateOrganizationResponse, error) {

	err := modOrganizationState(ctx, rq.Organization, portal.UserState_Active)
	if err != nil {
		return nil, err
	}

	return &portal.ActivateOrganizationResponse{}, nil

}

func (x *xps) FreezeOrganization(
	ctx context.Context, rq *portal.FreezeOrganizationRequest,
) (*portal.FreezeOrganizationResponse, error) {

	err := modOrganizationState(ctx, rq.Organization, portal.UserState_Frozen)
	if err != nil {
		return nil, err
	}

	return &portal.FreezeOrganizationResponse{}, nil

}

// GetOrganizations
//
// Note that this call is publically accessible. So anyone can call it
// and so the usual policy methods with respect to the caller are not
// applied. We still apply a narrow version of the Read policy to the
// organizations themselves so protected or private orgs may be hidden.
// We do this by setting the caller to Any:Any.
func (x *xps) GetOrganizations(
	ctx context.Context, rq *portal.GetOrganizationsRequest,
) (*portal.GetOrganizationsResponse, error) {

	caller, err := id.GRPCCaller(ctx)
	if err != nil {
		// This is OK - the caller may not be a portal user yet.
		// Set the caller to nil and the policy pkg will sort it out.
		caller = nil

		// There is no user, give them everything. It's likely they just did not
		// set the value in the request. This will happen on REST API access as this
		// is a GET method so there is no request body.
		if rq.Filter == portal.FilterMode_ByUser {
			rq.Filter = portal.FilterMode_ByAll
		}
	}

	result := new(portal.GetOrganizationsResponse)

	if rq.Filter == portal.FilterMode_ByAll {
		// let policy decide what the user can see
		orgs, err := storage.ListOrganizations()
		if err != nil {
			return nil, status.Error(codes.Internal, "list organizations")
		}

		for _, o := range orgs {
			err = policy.ReadOrganization(caller, o.Name)
			if err != nil {
				continue
			}

			result.Organizations = append(result.Organizations, o.Organization)
		}
	} else if rq.Filter == portal.FilterMode_ByUser {
		// just return the organizations in the user's organizations field.
		u := storage.NewUser(caller.Username)
		err := u.Read()
		if err != nil {
			return nil, status.Errorf(codes.NotFound, "no such user: %s", caller.Username)
		}

		for oid := range u.Organizations {
			o := storage.NewOrganization(oid)
			err := o.Read()
			if err != nil {
				log.Errorf("No such organization '%s' in user's organization list", oid)
				continue
			}

			result.Organizations = append(result.Organizations, o.Organization)
		}

	} else {
		return nil, status.Errorf(codes.InvalidArgument, "Bad API filter: %s", rq.Filter)
	}

	sort.Slice(result.Organizations, func(i, j int) bool {
		return natural.Less(result.Organizations[i].Name, result.Organizations[j].Name)
	})

	return result, nil
}

func (x *xps) GetOrganization(
	ctx context.Context, rq *portal.GetOrganizationRequest,
) (*portal.GetOrganizationResponse, error) {

	caller, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, status.Error(codes.PermissionDenied, err.Error())
	}

	o := storage.NewOrganization(rq.Name)
	err = o.Read()
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}
	if o.GetVersion() == 0 {
		return nil, status.Error(codes.NotFound, "organization does not exist")
	}

	err = policy.ReadOrganization(caller, rq.Name)
	if err != nil {
		return nil, merror.ToGRPCError(err)
	}

	ans := &portal.GetOrganizationResponse{
		Organization: o.Organization,
	}

	if rq.StatusMS != 0 {
		tf, err := o.GetGoal(time.Duration(rq.StatusMS) * time.Millisecond)
		if err != nil {
			return nil, merror.ToGRPCError(err)
		}

		ans.Status = portal.NewTaskTreeFromReconcileForest(tf)
	}

	return ans, nil
}

func (x *xps) CreateOrganization(
	ctx context.Context, rq *portal.CreateOrganizationRequest,
) (*portal.CreateOrganizationResponse, error) {

	log.Debugf("CreateOrganization: %+v", rq)

	caller, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, status.Error(codes.PermissionDenied, err.Error())
	}

	o := storage.NewOrganization(rq.Organization.Name)
	err = o.Read()
	if err != nil {
		return nil, status.Errorf(codes.Internal, err.Error())
	}

	if o.GetVersion() != 0 {
		return nil, status.Error(codes.AlreadyExists, "organization already exists")
	}

	err = policy.CreateOrganization(caller, rq.Organization.Name, rq.Organization.AccessMode)
	if err != nil {
		return nil, merror.ToGRPCError(err)
	}

	// fill organization data for storage.
	o.Description = rq.Organization.Description
	o.AccessMode = rq.Organization.AccessMode
	o.Members = map[string]*portal.Member{
		rq.User: {
			Role:  portal.Member_Creator,
			State: portal.Member_Active,
		},
	}

	// organizations always start in Pending state
	// no projects or additional members until the organization is approved and becomes Active
	o.State = portal.UserState_Pending

	if rq.Organization.Category != "" {
		o.Category = rq.Organization.Category
	}

	if rq.Organization.Subcategory != "" {
		if rq.Organization.Category == "" {
			return nil, status.Error(codes.InvalidArgument, "Cannot have a subcategory without a category")
		}
		o.Subcategory = rq.Organization.Subcategory
	}

	// IS RECONCILED
	_, err = o.Create()
	if err != nil {
		return nil, status.Errorf(codes.Internal, err.Error())
	}

	return &portal.CreateOrganizationResponse{}, nil

}

func (x *xps) UpdateOrganization(
	ctx context.Context, rq *portal.UpdateOrganizationRequest,
) (*portal.UpdateOrganizationResponse, error) {

	caller, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, status.Error(codes.PermissionDenied, err.Error())
	}

	// validate policy for organization update
	err = policy.UpdateOrganization(caller, rq.Name)
	if err != nil {
		return nil, merror.ToGRPCError(err)
	}

	o := storage.NewOrganization(rq.Name)
	err = o.Read()
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}
	if o.Ver == 0 {
		return nil, status.Error(codes.NotFound, "organization does not exist")
	}

	// validate policy for adding projects to this organization
	if rq.Projects != nil {
		for name := range rq.Projects.Set {
			err = policy.UpdateProject(caller, name)
			if err != nil {
				return nil, merror.ToGRPCError(err)
			}
		}
	}

	// validate policy for adding users to this organization
	if rq.Members != nil {
		for name := range rq.Members.Set {
			state := portal.Member_Active

			// fallback to EntityRequested if user hasn't confirmed
			err = policy.UpdateUser(caller, name)
			if err != nil {
				state = portal.Member_EntityRequested
			}

			rq.Members.Set[name].State = state
		}
	}

	o.UpdateRequest = rq

	// IS RECONCILED
	_, err = o.Update()
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	return &portal.UpdateOrganizationResponse{}, nil
}

func (x *xps) DeleteOrganization(
	ctx context.Context, rq *portal.DeleteOrganizationRequest,
) (*portal.DeleteOrganizationResponse, error) {

	caller, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, status.Error(codes.PermissionDenied, err.Error())
	}

	o := storage.NewOrganization(rq.Name)
	err = o.Read()
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}
	if o.Ver == 0 {
		return nil, status.Error(codes.NotFound, "organization does not exist")
	}

	err = policy.DeleteOrganization(caller, rq.Name)
	if err != nil {
		return nil, merror.ToGRPCError(err)
	}

	_, err = o.Delete()
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	return &portal.DeleteOrganizationResponse{}, nil
}

func (x *xps) GetOrganizationMembers(
	ctx context.Context, rq *portal.GetOrganizationMembersRequest,
) (*portal.GetOrganizationMembersResponse, error) {

	caller, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, status.Error(codes.PermissionDenied, err.Error())
	}

	err = policy.ReadOrganization(caller, rq.Organization)
	if err != nil {
		return nil, merror.ToGRPCError(err)
	}

	o := storage.NewOrganization(rq.Organization)
	err = o.Read()
	if err != nil {
		return nil, status.Error(codes.NotFound, "organization not found")
	}
	if o.Ver == 0 {
		return nil, status.Error(codes.NotFound, "organization does not exist")
	}

	members := []*portal.Member{}

	for _, m := range o.Members {
		members = append(members, m)
	}

	return &portal.GetOrganizationMembersResponse{Members: members}, nil
}

func (x *xps) GetOrganizationMember(
	ctx context.Context, rq *portal.GetOrganizationMemberRequest,
) (*portal.GetOrganizationMemberResponse, error) {

	caller, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, status.Error(codes.PermissionDenied, err.Error())
	}

	err = policy.ReadOrganization(caller, rq.Organization)
	if err != nil {
		return nil, merror.ToGRPCError(err)
	}

	_, m, err := getOrganizationMember(rq.Organization, rq.Username)
	if err != nil {
		return nil, err
	}

	if m != nil {
		return &portal.GetOrganizationMemberResponse{Member: m}, nil
	}

	return nil, status.Error(codes.NotFound, "organization member not found")
}

func (x *xps) RequestOrganizationMembership(
	ctx context.Context, rq *portal.RequestOrganizationMembershipRequest,
) (*portal.RequestOrganizationMembershipResponse, error) {

	var state portal.Member_State

	caller, err := id.GRPCCallerAllowInactive(ctx)
	if err != nil {
		return nil, status.Error(codes.PermissionDenied, err.Error())
	}

	if rq.Kind != portal.MembershipType_UserMember {
		return nil, status.Error(codes.InvalidArgument, "only user membership can be requested this way")
	}

	// check policy for modifying organization and user objects
	entityPassed := (policy.UpdateOrganization(caller, rq.Organization) == nil)

	// special case that circumvents policy: caller is the candidate member
	memberPassed := ((caller.Username == rq.Id) || (policy.UpdateUser(caller, rq.Id) == nil))

	// determine member state based on policy checks
	if entityPassed {
		if memberPassed {
			state = portal.Member_Active
			if err = activateUserAsOrganization(ctx, caller, rq.Id); err != nil {
				return nil, status.Errorf(codes.Internal, "failed to create user account: %+v", err)
			}
		} else {
			state = portal.Member_EntityRequested
		}
	} else {
		if memberPassed {
			state = portal.Member_MemberRequested
		} else {
			return nil, status.Error(codes.PermissionDenied, "insufficient permission to add organization user")
		}
	}

	// set the state
	if rq.Member == nil {
		rq.Member = &portal.Member{
			Role: portal.Member_Member,
		}
	}
	rq.Member.State = state

	// determine current membership status for this user
	o, m, err := getOrganizationMember(rq.Organization, rq.Id)
	if err != nil {
		return nil, err
	}
	if m != nil {
		return nil, status.Errorf(codes.AlreadyExists, "user is already an organization member")
	}

	// ok now we can add the member
	mu := &portal.MembershipUpdate{
		Set: map[string]*portal.Member{
			rq.Id: rq.Member,
		},
	}
	o.UpdateRequest = &portal.UpdateOrganizationRequest{Members: mu}

	// IS RECONCILED
	_, err = o.Update()
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	return &portal.RequestOrganizationMembershipResponse{Member: rq.Member}, nil
}

func (x *xps) ConfirmOrganizationMembership(
	ctx context.Context, rq *portal.ConfirmOrganizationMembershipRequest,
) (*portal.ConfirmOrganizationMembershipResponse, error) {

	caller, err := id.GRPCCallerAllowInactive(ctx)
	if err != nil {
		return nil, status.Error(codes.PermissionDenied, err.Error())
	}

	if rq.Kind != portal.MembershipType_UserMember {
		return nil, status.Error(codes.InvalidArgument, "only user membership can be confirmed this way")
	}

	// check policy for modifying organization and user objects
	entityPassed := (policy.UpdateOrganization(caller, rq.Organization) == nil)

	// special case that circumvents policy: caller is the candidate member
	memberPassed := ((caller.Username == rq.Id) || (policy.UpdateUser(caller, rq.Id) == nil))

	// determine current membership status for this user
	o, m, err := getOrganizationMember(rq.Organization, rq.Id)
	if err != nil {
		return nil, err
	}
	if m == nil {
		return nil, status.Error(codes.NotFound, "user is not an organization member")
	}

	switch m.State {
	case portal.Member_Active:
		return nil, status.Error(codes.AlreadyExists, "user is already an Active member")

	case portal.Member_MemberRequested:
		if !entityPassed {
			return nil, status.Error(codes.PermissionDenied, "insufficient permission to confirm user membership")
		}

	case portal.Member_EntityRequested:
		if !memberPassed {
			return nil, status.Error(codes.PermissionDenied, "insufficient permission to confirm user membership")
		}
	default:
		return nil, status.Error(codes.Internal, err.Error())
	}

	// Create/activate the user if it does not exist. This may happen when a newly registered, but not created User
	// requests membership in this Org. It is then up to the Org user to init & activate the user.
	if err = activateUserAsOrganization(ctx, caller, rq.Id); err != nil {
		return nil, status.Errorf(codes.Internal, "failed to create user account: %+v", err)
	}

	// ok now we can add the member to the organization
	m.State = portal.Member_Active
	mu := &portal.MembershipUpdate{
		Set: map[string]*portal.Member{
			rq.Id: m,
		},
	}
	o.UpdateRequest = &portal.UpdateOrganizationRequest{Members: mu}

	// IS RECONCILED
	_, err = o.Update()
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	return &portal.ConfirmOrganizationMembershipResponse{Member: m}, nil
}

func (x *xps) UpdateOrganizationMember(
	ctx context.Context, rq *portal.UpdateOrganizationMemberRequest,
) (*portal.UpdateOrganizationMemberResponse, error) {

	caller, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, status.Error(codes.PermissionDenied, err.Error())
	}

	err = policy.UpdateOrganization(caller, rq.Organization)
	if err != nil {
		return nil, merror.ToGRPCError(err)
	}

	o, m, err := getOrganizationMember(rq.Organization, rq.Username)
	if err != nil {
		return nil, err
	}
	if m == nil {
		return nil, status.Error(codes.NotFound, "user is not an organization member")
	}

	// disallow setting membership state in this function
	rq.Member.State = m.State

	mu := &portal.MembershipUpdate{}
	mu.Set = make(map[string]*portal.Member)
	mu.Set[rq.Username] = rq.Member
	o.UpdateRequest = &portal.UpdateOrganizationRequest{Members: mu}

	// IS RECONCILED
	_, err = o.Update()
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	return &portal.UpdateOrganizationMemberResponse{Member: rq.Member}, nil
}

func (x *xps) DeleteOrganizationMember(
	ctx context.Context, rq *portal.DeleteOrganizationMemberRequest,
) (*portal.DeleteOrganizationMemberResponse, error) {

	caller, err := id.GRPCCallerAllowInactive(ctx)
	if err != nil {
		return nil, status.Error(codes.PermissionDenied, err.Error())
	}

	// check policy for modifying organization and user objects
	entityPassed := (policy.UpdateOrganization(caller, rq.Organization) == nil)
	memberPassed := ((caller.Username == rq.Username) || (policy.UpdateUser(caller, rq.Username) == nil))

	// a user can be removed from an organization if either the UpdateUser or UpdateOrganization
	// policies pass
	if !entityPassed && !memberPassed {
		return nil, status.Error(
			codes.PermissionDenied,
			fmt.Sprintf(
				"Permission denied: entity auth: %t, user auth: %t", entityPassed, memberPassed,
			),
		)
	}

	o, m, err := getOrganizationMember(rq.Organization, rq.Username)
	if err != nil {
		return nil, err
	}
	if m == nil {
		return nil, status.Error(codes.NotFound, "user is not an organization member")
	}

	mu := &portal.MembershipUpdate{}
	mu.Remove = append(mu.Remove, rq.Username)
	o.UpdateRequest = &portal.UpdateOrganizationRequest{Members: mu}

	// IS RECONCILED
	_, err = o.Update()
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	return &portal.DeleteOrganizationMemberResponse{}, nil
}

func (x *xps) GetOrganizationProjects(
	ctx context.Context, rq *portal.GetOrganizationProjectsRequest,
) (*portal.GetOrganizationProjectsResponse, error) {

	_, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, status.Error(codes.PermissionDenied, err.Error())
	}

	return &portal.GetOrganizationProjectsResponse{}, nil

}

func (x *xps) GetOrganizationProject(
	ctx context.Context, rq *portal.GetOrganizationProjectRequest,
) (*portal.GetOrganizationProjectResponse, error) {

	_, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, status.Error(codes.PermissionDenied, err.Error())
	}

	return &portal.GetOrganizationProjectResponse{}, nil

}

func (x *xps) AddOrganizationProject(
	ctx context.Context, rq *portal.AddOrganizationProjectRequest,
) (*portal.AddOrganizationProjectResponse, error) {

	_, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, status.Error(codes.PermissionDenied, err.Error())
	}

	return &portal.AddOrganizationProjectResponse{}, nil

}

func (x *xps) UpdateOrganizationProject(
	ctx context.Context, rq *portal.UpdateOrganizationProjectRequest,
) (*portal.UpdateOrganizationProjectResponse, error) {

	_, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, status.Error(codes.PermissionDenied, err.Error())
	}

	return &portal.UpdateOrganizationProjectResponse{}, nil

}

func (x *xps) DeleteOrganizationProject(
	ctx context.Context, rq *portal.DeleteOrganizationProjectRequest,
) (*portal.DeleteOrganizationProjectResponse, error) {

	caller, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, status.Error(codes.PermissionDenied, err.Error())
	}

	if err = policy.UpdateOrganization(caller, rq.Organization); err != nil {
		return nil, merror.ToGRPCError(err)
	}

	o := storage.NewOrganization(rq.Organization)
	err = o.Read()
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	o.UpdateRequest = &portal.UpdateOrganizationRequest{
		Projects: &portal.MembershipUpdate{
			Remove: []string{rq.Project},
		},
	}

	// IS RECONCILED
	_, err = o.Update()
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	return &portal.DeleteOrganizationProjectResponse{}, nil

}

// Policy =====================================================================

func (x *xps) GetPolicy(
	ctx context.Context, rq *portal.GetPolicyRequest,
) (*portal.GetPolicyResponse, error) {

	caller, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, err
	}

	err = policy.ReadPolicy(caller)
	if err != nil {
		return nil, merror.ToGRPCError(err)
	}

	p, err := os.ReadFile(policy.PolicyFile())
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	return &portal.GetPolicyResponse{Policy: string(p)}, nil

}

// User traits and configuration ==========================================
func (x *xps) GetUserConfigurations(
	ctx context.Context, rq *portal.GetUserConfigurationsRequest,
) (*portal.GetUserConfigurationsResponse, error) {

	// Public API endpoint - do not check user token or policy.

	pc, err := storage.GetPortalConfig()
	if err != nil {
		return nil, status.Errorf(codes.Internal, "Error reading portal configuraation: %v", err)
	}

	return &portal.GetUserConfigurationsResponse{
		Institutions: pc.Config.Institutions,
		Countries:    countries,
		Usstates:     usstates,
		Categories:   pc.Config.Categories,
	}, nil
}

func (x *xps) UpdateUserConfigurations(
	ctx context.Context, rq *portal.UpdateUserConfigurationsRequest,
) (*portal.UpdateUserConfigurationsResponse, error) {

	caller, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, err
	}

	err = policy.UpdatePortal(caller)
	if err != nil {
		return nil, merror.ToGRPCError(err)
	}

	pc, err := storage.GetPortalConfig()
	if err != nil {
		return nil, status.Errorf(codes.Internal, "Error reading portal configuraation: %v", err)
	}

	// Should lock around this.....
	update := false
	if len(rq.Institutions) != 0 {
		insts := internal.ApplyPatch(rq.Institutions, pc.Config.Institutions, rq.Patchstrategy)
		sort.Slice(insts, func(i, j int) bool {
			return natural.Less(insts[i], insts[j])
		})
		pc.Config.Institutions = insts
		update = true
	}

	if len(rq.Categories) != 0 {
		cats := internal.ApplyPatch(rq.Categories, pc.Config.Categories, rq.Patchstrategy)
		sort.Slice(cats, func(i, j int) bool {
			return natural.Less(cats[i], cats[j])
		})
		pc.Config.Categories = cats
		update = true
	}

	if update {
		pc.Update()
	}

	return &portal.UpdateUserConfigurationsResponse{}, nil
}

func (x *xps) GetEntityTypeConfigurations(
	ctx context.Context, rq *portal.GetEntityTypeConfigurationsRequest,
) (*portal.GetEntityTypeConfigurationsResponse, error) {

	// Public API endpoint - do not check user token or policy.

	pc, err := storage.GetPortalConfig()
	if err != nil {
		return nil, status.Errorf(codes.Internal, "Error reading portal configuraation: %v", err)
	}

	resp := &portal.GetEntityTypeConfigurationsResponse{
		Types: []*portal.EntityType{},
	}

	for k, vals := range pc.Config.EntityTypes {
		resp.Types = append(resp.Types, &portal.EntityType{
			Etype:    k,
			Subtypes: vals,
		})
	}

	return resp, nil
}

func (x *xps) UpdateEntityTypeConfigurations(
	ctx context.Context, rq *portal.UpdateEntityTypeConfigurationsRequest,
) (*portal.UpdateEntityTypeConfigurationsResponse, error) {

	caller, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, err
	}

	err = policy.UpdatePortal(caller)
	if err != nil {
		return nil, merror.ToGRPCError(err)
	}

	pc, err := storage.GetPortalConfig()
	if err != nil {
		return nil, status.Errorf(codes.Internal, "Error reading portal configuraation: %v", err)
	}

	log.Infof("rq: %+v", rq)

	// Should lock around this.....
	if len(rq.Types) != 0 {

		if pc.Config.EntityTypes == nil {
			pc.Config.EntityTypes = make(map[string][]string)
		}

		// We only support remove and replace at the types level.
		for _, t := range rq.Types {
			switch rq.Patchstrategy.Strategy {
			case portal.PatchStrategy_remove:
				delete(pc.Config.EntityTypes, t.Etype)
			case portal.PatchStrategy_replace:
				sort.Slice(t.Subtypes, func(i, j int) bool {
					return natural.Less(t.Subtypes[i], t.Subtypes[j])
				})
				pc.Config.EntityTypes[t.Etype] = t.Subtypes
			case portal.PatchStrategy_expand, portal.PatchStrategy_subtract:
				return nil, status.Error(
					codes.InvalidArgument,
					"expand and subtract are not supported when updating entity types",
				)
			}
		}

		pc.Update()
	}

	return &portal.UpdateEntityTypeConfigurationsResponse{}, nil
}

// Model ======================================================================

func (x *xps) Compile(
	ctx context.Context, rq *portal.CompileRequest,
) (*portal.CompileResponse, error) {

	// We call the model service directly here as this is not something
	// that fits the reconciler architecture at all. We want a real time
	// response to the compilation. Also the model compilation requires
	// calling out to an external python process - and we do not want to
	// need to have python and the external script installed on whatever
	// machine happens to be running this apiserver.

	_, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, err
	}

	resp := &portal.CompileResponse{}

	Model(func(cli portal.ModelClient) error {
		// Simply pass the call through to the model service.
		resp, err = cli.Compile(ctx, rq)
		return nil
	})

	return resp, err
}

func (x *xps) Push(
	ctx context.Context, rq *portal.PushRequest,
) (*portal.PushResponse, error) {

	_, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, err
	}

	resp := &portal.PushResponse{}

	// The model needs the user token to authorize the git push, so
	// rebuild the token metadata and pass it along.
	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		return nil, fmt.Errorf("missing call metadata")
	}

	vals := []string{}
	for _, s := range []string{"authorization", "grpcgateway-cookie"} {
		if v, ok := md[s]; ok {
			vals = append(vals, s)
			vals = append(vals, v...)
		}
	}

	pushCtx := context.TODO()
	pushCtx = metadata.AppendToOutgoingContext(pushCtx, vals...)

	Model(func(cli portal.ModelClient) error {
		// Simply pass the call through to the model service.
		resp, err = cli.Push(pushCtx, rq)
		return nil
	})

	return resp, err
}

// Health =====================================================================

func (x *xps) Health(
	ctx context.Context, rq *portal.HealthRequest,
) (*portal.HealthResponse, error) {

	// we're here so we're ok more or less.
	return &portal.HealthResponse{Status: "ok"}, nil
}

// Helpers ====================================================================

func Model(f func(portal.ModelClient) error) error {

	conn, wkc, err := ModelClient()
	if err != nil {
		return err
	}
	defer conn.Close()

	return f(wkc)
}

func ModelClient() (*grpc.ClientConn, portal.ModelClient, error) {

	conn, err := grpc.Dial(
		fmt.Sprintf("%s:%d", "model", 6000),
		grpc.WithInsecure(),
	)
	if err != nil {
		return nil, nil, fmt.Errorf("grpc dial: %v", err)
	}

	return conn, portal.NewModelClient(conn), nil
}

func modOrganizationState(
	ctx context.Context, organization string, state portal.UserState) error {

	caller, err := id.GRPCCaller(ctx)
	if err != nil {
		return status.Error(codes.PermissionDenied, err.Error())
	}

	if state == portal.UserState_Active {
		err = policy.ActivateOrganization(caller, organization)
		if err != nil {
			return status.Error(codes.PermissionDenied, "forbidden")
		}
	} else {
		err = policy.DeactivateOrganization(caller, organization)
		if err != nil {
			return status.Error(codes.PermissionDenied, "forbidden")
		}
	}

	o := storage.NewOrganization(organization)
	err = o.Read()
	if err != nil {
		return fmt.Errorf("internal error: %v", err)
	}

	if o.Ver == 0 {
		return status.Error(codes.NotFound, "organization not found")
	}

	o.UpdateRequest = &portal.UpdateOrganizationRequest{
		State: &portal.UserStateUpdate{
			Value: state,
		},
	}

	// IS RECONCILED
	_, err = o.Update()
	if err != nil {
		return status.Error(codes.Internal, err.Error())
	}

	return nil

}

func getProjectMember(pid, uid string) (*storage.Project, *portal.Member, error) {

	p := storage.NewProject(pid)
	err := p.Read()
	if err != nil {
		return nil, nil, status.Error(codes.Internal, err.Error())
	}

	if p.Ver == 0 {
		return nil, nil, status.Error(codes.NotFound, "project '"+pid+"' does not exist")
	}

	u := storage.NewUser(uid)
	err = u.Read()
	if err != nil {
		return nil, nil, status.Error(codes.Internal, err.Error())
	}

	if u.Ver == 0 {
		return p, nil, status.Error(codes.NotFound, "user '"+uid+"' does not exist")
	}

	m, ok := p.Members[uid]
	if !ok {
		return p, nil, nil
	}

	return p, m, nil
}

func getProjectMemberOrganization(pid, oid string) (*storage.Project, *portal.Member, error) {

	p := storage.NewProject(pid)
	err := p.Read()
	if err != nil {
		return nil, nil, status.Error(codes.Internal, err.Error())
	}

	if p.Ver == 0 {
		return nil, nil, status.Error(codes.NotFound, "project '"+pid+"' does not exist")
	}

	o := storage.NewOrganization(oid)
	err = o.Read()
	if err != nil {
		return nil, nil, status.Error(codes.Internal, err.Error())
	}

	if o.Ver == 0 {
		return p, nil, status.Error(codes.NotFound, "user '"+oid+"' does not exist")
	}

	if p.Organization == oid && p.OrgMembership != nil {
		return p, p.OrgMembership, nil
	}

	return p, nil, nil
}

func getOrganizationMember(oid, uid string) (*storage.Organization, *portal.Member, error) {

	o := storage.NewOrganization(oid)
	err := o.Read()
	if err != nil {
		return nil, nil, status.Error(codes.Internal, err.Error())
	}

	if o.Ver == 0 {
		return nil, nil, status.Error(codes.NotFound, "organization '"+oid+"' does not exist")
	}

	// Note portal User data may not exist.

	m, ok := o.Members[uid]
	if !ok {
		return o, nil, nil
	}

	return o, m, nil
}

func getOrganizationMemberProject(oid, pid string) (*storage.Organization, *portal.Member, error) {

	o := storage.NewOrganization(oid)
	err := o.Read()
	if err != nil {
		return nil, nil, status.Error(codes.Internal, err.Error())
	}

	if o.Ver == 0 {
		return nil, nil, status.Error(codes.NotFound, "organization '"+oid+"' does not exist")
	}

	p := storage.NewProject(pid)
	err = p.Read()
	if err != nil {
		return nil, nil, status.Error(codes.Internal, err.Error())
	}

	if p.Ver == 0 {
		return o, nil, status.Error(codes.NotFound, "project '"+pid+"' does not exist")
	}

	m, ok := o.Projects[pid]
	if !ok {
		return o, nil, nil
	}

	return o, m, nil
}

// activateUserAsOrganization: Create/activate a user, based on their confirmed membership
// in the organization.  The error returned if any can be passed back to the api handler directly.
// It will either be an GRPC error or a mergeerror.
func activateUserAsOrganization(ctx context.Context, caller *id.IdentityTraits, username string) error {

	u := storage.NewUser(username)
	err := u.Read()
	if err != nil {
		return err
	}

	// if user is either not-inited or not activated, init and activate now
	if u.Ver == 0 || u.State != portal.UserState_Active {
		err = modUserState(ctx, username, portal.UserState_Active)
		if err != nil {
			return err
		}
	}

	return nil
}
