package main

import (
	"regexp"

	log "github.com/sirupsen/logrus"
	"gitlab.com/mergetb/portal/services/pkg/cred"
	"gitlab.com/mergetb/portal/services/pkg/storage"
	"gitlab.com/mergetb/tech/reconcile"
)

// hostInit - if there are not creds (keys or certs), create them.
func hostInit(principals []string, name string) error {

	log.Infof("handling host init %s", name)

	host := principals[0]
	skp := storage.NewSSHHostKeyPair(name)
	err := skp.Read()
	if err != nil {
		log.Errorf("key pair read: %v", err)
		return err
	}

	if skp.GetVersion() == 0 || skp.Public == "" {
		log.Infof("creating new ssh keypair for %s (%s)", host, name)

		pub, priv, err := cred.GenerateSSHKeyPair(skp.User)
		if err != nil {
			log.Errorf("[%s] ssh key gen", skp.User)
			return err
		}

		skp.SSHKeyPair.Public = pub
		skp.SSHKeyPair.Private = priv

		// IS RECONCILED
		_, err = skp.Create()

	} else {
		log.Debugf("Keypair exists, not creating new ones.")
	}

	// now that we have keys, make a cert if needed.
	hc := storage.NewSSHHostCert(name)
	err = hc.Read()
	if err != nil {
		log.Errorf("[%s] cert read", principals[0])
	}

	if hc.GetVersion() == 0 || hc.Cert == "" {

		log.Infof("generating host cert for %s", principals[0])

		c, err := generateHostSSHCert(principals, name)
		if err != nil {
			return err
		}

		// IS RECONCILED
		// Write the cert to etcd for other reconcilers
		hc.Cert = c
		_, err = hc.Create()
		if err != nil {
			return err
		}
	} else {
		log.Debugf("Certificate exists, not creating new one.")
	}

	return nil
}

// Merge Reconciler for XDC keys
var (
	xdcBucket = storage.PrefixedBucket(&storage.XDC{})
	xdcName   = "([a-zA-Z_]+[a-zA-Z0-9_]+)"
	xdcKey    = regexp.MustCompile(`^` + xdcBucket + xdcName + `\.` + xdcName + `$`)
)

type XdcTask struct {
	XDC     string
	Project string
}

func (xt *XdcTask) Parse(key string) bool {

	tkns := xdcKey.FindAllStringSubmatch(key, -1)
	if len(tkns) > 0 {
		xt.XDC = tkns[0][1]
		xt.Project = tkns[0][2]

		return true
	}

	return false
}

func (xt *XdcTask) Create(value []byte, version int64, td *reconcile.TaskData) *reconcile.TaskMessage {

	// svc and pod name are principals.
	// Note the first entry will be the etc key under which the keys and certs are stored.
	// which also maps to where they are written in the mergefs.
	name := storage.XdcId(xt.XDC, xt.Project)
	ps := []string{
		name,
		xt.XDC + "-" + xt.Project, // svc anme
	}

	err := hostInit(ps, name)
	if err != nil {
		return reconcile.TaskMessageError(err)
	}

	return nil
}

func (xt *XdcTask) Update(prev, value []byte, version int64, td *reconcile.TaskData) *reconcile.TaskMessage {
	log.Debugf("got xdc update. ignoring.")
	return nil
}

func (xt *XdcTask) Ensure(prev, value []byte, version int64, td *reconcile.TaskData) *reconcile.TaskMessage {
	log.Debugf("got xdctask ensure.")
	return xt.Create(value, version, td)
}

func (xt *XdcTask) Delete(value []byte, version int64, td *reconcile.TaskData) *reconcile.TaskMessage {

	name := storage.XdcId(xt.XDC, xt.Project)
	log.Debugf("got xdc delete for %s", name)

	skp := storage.NewSSHHostKeyPair(name)
	err := skp.Read()
	if err != nil {
		log.Warnf("error reading keypair for %s", name)
	} else {
		if skp.GetVersion() > 0 {
			log.Infof("deleting key pair for %s", name)
			skp.Delete()
		}
	}

	hc := storage.NewSSHHostCert(name)
	err = hc.Read()
	if err != nil {
		log.Warnf("error reading cert for %s", name)
	} else {
		if hc.GetVersion() > 0 {
			log.Infof("deleting cert for %s", name)
			hc.Delete()
		}
	}

	return nil
}
