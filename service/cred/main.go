package main

import (
	"flag"
	"os"

	log "github.com/sirupsen/logrus"
	"gitlab.com/mergetb/portal/services/internal"
	"gitlab.com/mergetb/portal/services/pkg/storage"
)

var (
	caEndpoint string
	caCertsDir string
	caProvPw   string
	caProvName string
	xdcNs      string
)

func init() {
	internal.InitLogging()
	internal.InitReconciler()

	x, ok := os.LookupEnv("XDC_NAMESPACE")
	if !ok {
		log.Fatal("XDC_NAMESPACE must be defined in the environment")
	}
	xdcNs = x

	x, ok = os.LookupEnv("CERT_PROV_PW")
	if !ok {
		log.Fatal("CERT_PROV_PW must be defined in the environment")
	}
	caProvPw = x
}

var Version = ""

func main() {

	log.Infof("portal version: %s", Version)

	flag.StringVar(&caEndpoint, "ca-endpoint", "https://step-ca", "API endpoint for the SSH key/cert issuer")
	flag.StringVar(&caCertsDir, "ca-certs-dir", "/etc/step-ca/data/certs", "Location of CA certs in the filesystem")
	flag.StringVar(&caProvName, "cert-provider", "merge@mergetb.example.net", "Name of the SSH Cert provider in the CA")

	flag.Parse()

	log.Infof("Starting credentials management service")

	err := storage.InitPortalEtcdClient()
	if err != nil {
		log.Fatalf("storage etcd init: %+v", err)
	}

	// keep an eye out for new hosts that need credentials.
	go runPodWatch()

	// poll for soon-to-expire certs
	go certWatch()

	runReconciler()
}
