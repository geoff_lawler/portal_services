package main

import (
	"os"
	"path"
	"regexp"

	log "github.com/sirupsen/logrus"
	portal "gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/portal/services/pkg/cred"
	"gitlab.com/mergetb/portal/services/pkg/storage"
	"gitlab.com/mergetb/tech/reconcile"
	"google.golang.org/protobuf/proto"
)

var (
	nameExp          = "([a-zA-Z_]+[a-zA-Z0-9_]+)"
	userBucket       = storage.PrefixedBucket(&storage.User{})
	userStatusBucket = storage.PrefixedBucket(&storage.UserStatus{})
	userKey          = regexp.MustCompile("^" + userBucket + nameExp + "$")
	userStatusKey    = regexp.MustCompile("^" + userStatusBucket + nameExp + "$")
)

type UserTask struct {
	Username string
}

func NewUserTask() *UserTask {
	return &UserTask{}
}

func (ut *UserTask) Parse(k string) bool {

	tkns := userKey.FindAllStringSubmatch(k, -1)

	if len(tkns) > 0 {
		ut.Username = tkns[0][1]

		log.Debugf("got cred user task for %+v", ut.Username)
		return true
	}

	return false
}

func (ut *UserTask) Create(value []byte, version int64, td *reconcile.TaskData) *reconcile.TaskMessage {

	log.Debugf("reconciling cred create for user: %s", ut.Username)

	u := new(portal.User)
	err := proto.Unmarshal(value, u)
	if err != nil {
		return reconcile.TaskMessageError(err)
	}

	if u.State != portal.UserState_Active {
		return reconcile.TaskMessageWarningf("Skipping cred create user action for not-active user account: %s", u.Username)
	}

	skp := storage.NewSSHUserKeyPair(ut.Username)
	err = skp.Read()
	if err != nil {
		return reconcile.TaskMessageError(err)
	}

	if skp.Public != "" {
		log.Infof("key for %s already exists", skp.User)
		return nil
	}

	log.Infof("creating ssh keypair for %s", skp.User)
	pub, priv, err := cred.GenerateSSHKeyPair(skp.User)
	if err != nil {
		log.Errorf("[%s] ssh key gen: %+v", skp.User, err)
		return reconcile.TaskMessageError(err)
	}

	skp.SSHKeyPair.Public = pub
	skp.SSHKeyPair.Private = priv

	// IS RECONCILED
	td.AddWrittenKey(skp.Key())
	_, err = skp.Create()

	return reconcile.CheckErrorToMessage(err)
}

func (ut *UserTask) Delete(value []byte, ver int64, td *reconcile.TaskData) *reconcile.TaskMessage {

	log.Infof("usertask delete value: %s", value)
	u := new(portal.User)
	err := proto.Unmarshal(value, u)
	if err != nil {
		log.Infof("in delete err user read: %s", err.Error())
	} else {
		log.Infof("delete user: %+v", u)
	}

	skp := storage.NewSSHUserKeyPair(ut.Username)
	_, err = skp.Delete()

	return reconcile.CheckErrorToMessage(err)
}

func (ut *UserTask) Update(prev, value []byte, ver int64, td *reconcile.TaskData) *reconcile.TaskMessage {

	// We update -> create as users can be created, but not init/activated. The Update will be triggered
	// once the user is init/activated as data is added to the User data structure at that time.
	return ut.Create(value, ver, td)
}

func (ut *UserTask) Ensure(prev, value []byte, ver int64, td *reconcile.TaskData) *reconcile.TaskMessage {

	return ut.Create(value, ver, td)
}

//
// User Status - create/delete certs on user login/logout.
//

type UserStatusTask struct {
	Username string
}

func (t *UserStatusTask) Parse(k string) bool {

	tkns := userStatusKey.FindAllStringSubmatch(k, -1)
	if len(tkns) > 0 {
		t.Username = tkns[0][1]
		return true
	}

	return false
}

func (t *UserStatusTask) Create(value []byte, ver int64, td *reconcile.TaskData) *reconcile.TaskMessage {

	log.Infof("creating user ssh cert")

	// generate user SSH certs on logged in status
	us := new(portal.UserStatus)
	err := proto.Unmarshal(value, us)
	if err != nil {
		return reconcile.TaskMessageError(err)
	}

	if us.Loggedin != true {
		log.Info("user already logged in")
		return nil
	}

	user := storage.NewUser(us.Username)
	err = user.Read()
	if err != nil {
		return reconcile.TaskMessageErrorf("user read: %+v", err)
	}

	if user.State == portal.UserState_Active {
		log.Infof("generating ssh cert for %s", us.Username)

		cert, err := generateUserSSHCert(us.Username)
		if err != nil {
			return reconcile.TaskMessageErrorf("gen user cert: %+v", err)
		}

		// add the CA Host Pub key.
		caKey, err := os.ReadFile(path.Join(caCertsDir, "ssh_host_ca_key.pub"))
		if err != nil {
			return reconcile.TaskMessageErrorf("read CA host key: %+v", err)
		}

		sc := storage.NewSSHUserCert(us.Username)
		sc.Cert = cert
		sc.CAHostPubKey = string(caKey)

		// IS RECONCILED
		td.AddWrittenKey(sc.Key())
		_, err = sc.Create()
		if err != nil {
			return reconcile.TaskMessageErrorf("storage cert create: %+v", err)
		}
	}

	return nil
}

func (ut *UserStatusTask) Update(prev, value []byte, ver int64, td *reconcile.TaskData) *reconcile.TaskMessage {

	// NOOP for now. We could generate new keys I guess?
	return nil
}

func (ut *UserStatusTask) Ensure(prev, value []byte, ver int64, td *reconcile.TaskData) *reconcile.TaskMessage {

	// NOOP for now.
	return reconcile.TaskMessageUndefined()
}

func (t *UserStatusTask) Delete(value []byte, ver int64, td *reconcile.TaskData) *reconcile.TaskMessage {

	log.Infof("running user status delete for %s", t.Username)

	us := new(portal.UserStatus)
	err := proto.Unmarshal(value, us)
	if err != nil {
		return reconcile.TaskMessageErrorf("user status unmarshal: %+v", err)
	}

	// attempt to delete user ssh cert
	c := storage.NewSSHUserCert(us.Username)
	err = c.Read()
	if err != nil {
		return reconcile.TaskMessageError(err)
	}

	_, err = c.Delete()
	if err != nil {
		return reconcile.TaskMessageError(err)
	}

	return nil
}
