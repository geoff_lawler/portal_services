package main

import (
	"gitlab.com/mergetb/portal/services/pkg/storage"
)

// ***************************************************************************
// TODO:
// * Delete user SSH certs (if they exist) when a user state is set to
//     non-active.
// * Implement reconciler status during actions.
// * Check the validity of the host and user certs from teh CA and set a
//   etcd lease for that amount of time in order to generate a new cert before
//   experation.
// ***************************************************************************

func runReconciler() {

	t := storage.ReconcilerConfigCredentials.ToReconcilerManager(
		&UserTask{},
		&UserStatusTask{},
		&XdcTask{},
	)

	t.Run()
}
