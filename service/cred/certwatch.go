package main

import (
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/mergetb/portal/services/pkg/storage"
	"golang.org/x/crypto/ssh"
)

const (
	expWithin    = 1 * time.Hour
	pollDuration = 30 * time.Minute
)

func certWatch() {

	defer log.Warn("cert watch poll loop exited")

	for {

		certs, err := storage.GetSSHCerts()
		if err != nil {
			log.Errorf("Error getting certs: %v", err)
		} else {

			// check all host certs for expiration
			log.Debugf("reading all certs")

			for _, c := range certs {

				if c.IsHost {

					err := handleRenew(expWithin, c)
					if err != nil {
						log.Errorf("Error checking host cert expiration %v", err)
						continue
					}
				}
			}
		}
		time.Sleep(pollDuration)
	}
}

func handleRenew(exp time.Duration, cert *storage.SSHCert) error {

	log.Tracef("cert: %v\n", cert.Cert)

	data, _, _, _, err := ssh.ParseAuthorizedKey([]byte(cert.Cert))
	if err != nil {
		return err
	}

	c := data.(*ssh.Certificate)

	host := c.ValidPrincipals[0]
	before := time.Unix(int64(c.ValidBefore), 0)
	t := time.Now().Add(exp)

	if t.After(before) {

		log.Infof("Host cert for %s to soon expire. Regenerating.", host)

		hc := storage.NewSSHHostCert(host)
		err = hc.Read()
		if err != nil {
			return err
		}

		newCert, err := generateHostSSHCert(c.ValidPrincipals, host)
		if err != nil {
			return err
		}

		hc.Cert = newCert
		// IS RECONCILED
		_, err = hc.Create()
		if err != nil {
			return err
		}
	}

	return nil
}
