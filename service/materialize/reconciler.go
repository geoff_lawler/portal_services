package main

import (
	"regexp"

	log "github.com/sirupsen/logrus"
	"google.golang.org/protobuf/proto"

	portal "gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/portal/services/pkg/storage"
	"gitlab.com/mergetb/tech/reconcile"

	"gitlab.com/mergetb/portal/services/internal"
)

var (
	nameExp     = "([a-zA-Z_]+[a-zA-Z0-9_]+)"
	mtzRqBucket = storage.PrefixedBucket(&storage.MaterializeRequest{})
	mtzRqKey    = regexp.MustCompile(
		"^" + mtzRqBucket + nameExp + "/" + nameExp + "/" + nameExp + "$",
	)
)

type MtzTask struct {
	Rid string
	Eid string
	Pid string
}

func runReconciler() {

	t := storage.ReconcilerConfigMaterialize.ToReconcilerManager(
		&MtzTask{},
	)

	t.Run()
}

func (mt *MtzTask) Mzid() string {
	return internal.MzidToString(&internal.Mzid{
		Rid: mt.Rid,
		Eid: mt.Eid,
		Pid: mt.Pid,
	})
}

func (mt *MtzTask) Parse(key string) bool {

	parts := mtzRqKey.FindAllStringSubmatch(key, -1)
	if len(parts) == 0 {
		return false
	}

	mt.Pid = parts[0][1]
	mt.Eid = parts[0][2]
	mt.Rid = parts[0][3]

	log.Debugf("parsed MtzTask %v from key: %s", mt, key)

	return true
}

func (mt *MtzTask) Create(value []byte, version int64, td *reconcile.TaskData) *reconcile.TaskMessage {

	log.Info("creating materialization")

	rq := new(portal.MaterializeRequest)
	err := proto.Unmarshal(value, rq)
	if err != nil {
		return reconcile.TaskMessageError(err)
	}

	err = handleMaterialize(rq)
	if err != nil {
		return reconcile.TaskMessageError(err)
	}

	return nil
}

func (mt *MtzTask) Update(prev, value []byte, version int64, td *reconcile.TaskData) *reconcile.TaskMessage {

	log.Infof("received update for mztask: doing nothing")
	return nil
}

func (mt *MtzTask) Ensure(prev, value []byte, version int64, td *reconcile.TaskData) *reconcile.TaskMessage {

	log.Infof("received ensure for mztask: doing nothing")
	return reconcile.TaskMessageUndefined()
}

func (mt *MtzTask) Delete(value []byte, version int64, td *reconcile.TaskData) *reconcile.TaskMessage {

	rq := new(portal.MaterializeRequest) // We reuse the materialize request
	err := proto.Unmarshal(value, rq)
	if err != nil {
		return reconcile.TaskMessageError(err)
	}

	err = handleDematerialize(rq)
	if err != nil {
		return reconcile.TaskMessageError(err)
	}

	return nil
}
