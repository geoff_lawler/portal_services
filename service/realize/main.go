package main

import (
	"bytes"
	"context"
	"fmt"
	"regexp"
	"time"

	"github.com/minio/minio-go/v7"
	log "github.com/sirupsen/logrus"
	"google.golang.org/protobuf/proto"

	portal "gitlab.com/mergetb/api/portal/v1/go"
	xir "gitlab.com/mergetb/xir/v0.3/go"

	"gitlab.com/mergetb/portal/services/internal"
	"gitlab.com/mergetb/portal/services/pkg/realize"
	"gitlab.com/mergetb/portal/services/pkg/storage"

	"gitlab.com/mergetb/tech/reconcile"
)

func init() {
	internal.InitLogging()
	internal.InitReconciler()
}

var (
	Version = ""

	nameExp = "([a-z]+[a-z0-9]{0,62})"

	// /realizations/battlestar/galactica/raptor
	rlzRqBucket = storage.PrefixedBucket(&storage.RealizeRequest{})
	rlzRqKey    = regexp.MustCompile(
		"^" + rlzRqBucket + nameExp + "/" + nameExp + "/" + nameExp + "$",
	)

	// /realizations/battlestar/galactica/raptor/realize
	rlzKey = regexp.MustCompile(
		"^" + rlzRqBucket + nameExp + "/" + nameExp + "/" + nameExp + "/realize$",
	)

	// How often to check for expired realizations.
	defaultRealizationTimeoutLoopWait = time.Minute * 5 // 5 minutes beween timeout checks.
)

func main() {

	log.Infof("portal version: %s", Version)

	err := storage.InitPortalEtcdClient()
	if err != nil {
		log.Fatalf("etcd client init: %v", err)
	}

	err = storage.InitPortalMinIOClient()
	if err != nil {
		log.Fatalf("minio client init: %v", err)
	}

	go handleReservationTimeout()

	t := storage.ReconcilerConfigRealize.ToReconcilerManager(
		&RlzTask{},
	)

	t.Run()

}

type RlzTask struct {
	Key         string
	Project     string
	Experiment  string
	Realization string
}

func (t *RlzTask) Parse(key string) bool {

	tkns := rlzRqKey.FindAllStringSubmatch(key, -1)
	if len(tkns) > 0 {
		t.Key = key
		return true
	}

	return false

}

func (t *RlzTask) Create(value []byte, version int64, td *reconcile.TaskData) *reconcile.TaskMessage {

	log.Infof("[%s] handling realization request", t.Key)

	rzrq := new(portal.RealizeRequest)
	err := proto.Unmarshal(value, rzrq)
	if err != nil {
		return reconcile.TaskMessageErrorf("realize request unmarshal: %v", err)
	}

	// less efficient than reading it directly,
	// but reading the model consistently is more important
	exp := storage.NewExperiment(rzrq.Experiment, rzrq.Project)
	err = exp.Read()
	if err != nil {
		return reconcile.TaskMessageErrorf("unable to read experiment %s.%s: %v", rzrq.Experiment, rzrq.Project, err)
	}

	model, _, err := exp.ReadExperimentModel(rzrq.Revision)
	if err != nil {
		return reconcile.TaskMessageErrorf("unable to read model for experiment%s.%s: %v", rzrq.Experiment, rzrq.Project, err)
	}
	xpnet := model.Model

	bucket := fmt.Sprintf("xp-%s-%s", rzrq.Project, rzrq.Experiment)
	if xpnet == nil {
		return reconcile.TaskMessageErrorf("cannot realize %s/%s: nil xpnet", bucket, rzrq.Revision)
	}

	if xpnet.Nodes == nil || len(xpnet.Nodes) == 0 {
		return reconcile.TaskMessageErrorf("cannot realize %s/%s: no nodes in xpnet", bucket, rzrq.Revision)
	}

	return reconcile.CheckErrorToMessage(dorealize(t.Key, version, xpnet, rzrq))
}

func (t *RlzTask) Update(prev, value []byte, version int64, td *reconcile.TaskData) *reconcile.TaskMessage {

	// TODO read new duration timeout and update time on realization if needed.
	log.Infof("[%s] received update; treating as create", t.Key)
	return t.Create(value, version, td)

}

func (t *RlzTask) Ensure(prev, value []byte, version int64, td *reconcile.TaskData) *reconcile.TaskMessage {

	log.Infof("[%s] received ensure; ignoring", t.Key)
	return reconcile.TaskMessageUndefined()

}

func (t *RlzTask) Delete(value []byte, version int64, td *reconcile.TaskData) *reconcile.TaskMessage {

	log.Infof("[%s] handling relinquish", t.Key)

	rq := new(portal.RealizeRequest)
	err := proto.Unmarshal(value, rq)
	if err != nil {
		return reconcile.TaskMessageErrorf("realize request unmarshal for %s: %v", t.Key, err)
	}

	mzid := fmt.Sprintf("%s.%s.%s", rq.Realization, rq.Experiment, rq.Project)

	// update resource allocation table

	current, err := realize.ReadTable()
	if err != nil {
		return reconcile.TaskMessageErrorf("failed to read allocation table: %v", err)
	}

	updated, err := realize.Dealloc(current, mzid)
	if err != nil {
		return reconcile.TaskMessageErrorf("failed to deallocate resources for %s: %v", mzid, err)
	}

	err = realize.WriteTable(updated)
	if err != nil {
		return reconcile.TaskMessageErrorf("failed to write allocation table update for %s: %v", mzid, err)
	}

	// update vlan allocation table

	bucket := fmt.Sprintf("realize-%s-%s-%s", rq.Project, rq.Experiment, rq.Realization)

	res, err := storage.ReadRealizationResult(rq.Project, rq.Experiment, rq.Realization)
	if err != nil {
		return reconcile.TaskMessageErrorf("[%s] read realization result: %v", mzid, err)
	}

	// we didn't allocate vsets if the diagnostics had an error
	if !res.Diagnostics.Error() {
		err = realize.Relinquish(res.Realization)
		if err != nil {
			return reconcile.TaskMessageErrorf("failed to relinquish rlz: %v", err)
		}
	}

	// clear emulation endpoint count from etcd

	err = storage.DeleteEndpoints(res.Realization)
	if err != nil {
		log.Errorf("remove emulation endpoints: %v", err)
	}

	// clean infrapod count from the infraserver
	err = storage.DeleteInfrapodCount(res.Realization)
	if err != nil {
		log.Errorf("remove infrapod count: %v", err)
	}

	// remove realization from minio

	err = storage.MinIOClient.RemoveObject(
		context.TODO(),
		bucket,
		"diagnostics",
		minio.RemoveObjectOptions{},
	)
	if err != nil {
		log.Warnf("minio diagnostics remove: %v", err)
	}

	err = storage.MinIOClient.RemoveObject(
		context.TODO(),
		bucket,
		"realization",
		minio.RemoveObjectOptions{},
	)
	if err != nil {
		log.Warnf("minio realization remove: %v", err)
	}

	err = storage.MinIOClient.RemoveBucket(context.TODO(), bucket)
	if err != nil {
		log.Warnf("minio bucket remove: %v", err)
	}

	return nil

}

func dorealize(key string, version int64, xpnet *xir.Network, rq *portal.RealizeRequest) error {

	a, err := realize.ReadTable()
	if err != nil {
		return fmt.Errorf("read alloc table: %v", err)
	}

	tbx, err := realize.BuildResourceInternet()
	if err != nil {
		return fmt.Errorf("build resource internet: %v", err)
	}

	mzid := fmt.Sprintf("%s.%s.%s", rq.Realization, rq.Experiment, rq.Project)

	resourcePool, err := GetResourcePool(rq.Project)
	if err != nil {
		return fmt.Errorf("No resource pool available for realization: %s", err.Error())
	}

	log.Infof("Using resource pool %s", resourcePool.Name)

	rlzError := false

	rlz, diags, err := realize.Realize(
		tbx.Device("the-internet"),
		tbx,
		xpnet.Lift(),
		a,
		resourcePool,
		realize.RealizeParameters{
			Mzid:    mzid,
			Hash:    rq.Revision,
			Creator: rq.Creator,
		},
		xpnet.Parameters,
	)
	if err != nil {
		rlzError = true
		log.Warnf("realize: %v", err)
	}
	if diags.Error() {
		rlzError = true
		log.Warnf("realize: error in the diagnostics")
	}

	// free realization if anything goes awry
	defer func() {
		if err != nil {
			err2 := realize.Relinquish(rlz)
			if err2 != nil {
				log.Warnf("realize: failed to relinquish: %v", err2)
			}
		}
	}()

	dbuf, err := proto.Marshal(&portal.DiagnosticList{Value: diags})
	if err != nil {
		return fmt.Errorf("marshal diags: %v", err)
	}

	expires, err := storage.ParseReservationDuration(time.Now(), rq.GetDuration())
	if err != nil {
		return err
	}

	rlz.Expires = expires

	log.Infof("realization expires: %s", rlz.Expires.AsTime())
	log.Infof("realization duration: %s", rlz.Expires.AsTime().Sub(time.Now()))

	// on an rlzError, this is likely to fail (like if rlz is nil)
	rbuf, err := proto.Marshal(rlz)
	if err != nil {
		if rlzError {
			rbuf = nil
		} else {
			return fmt.Errorf("marshal realization: %v", err)
		}
	}

	//TODO this needs a rollback, and more generally the allocation table should
	//     be moved into the storage library

	if !rlzError {
		err = realize.WriteTable(a)
		if err != nil {
			return fmt.Errorf("allocation table write: %v", err)
		}
	}

	// if this realization uses network emulation, record the number of endpoints on the emu server
	// this is used by the realization engine to load balance across emulation servers
	err = storage.RecordEndpoints(rlz)
	if err != nil {
		log.Errorf("record emulation endpoints: %v", err)
		// not a failure per-se, but could impact future realizations...
	}

	// store the number of infrapods used so that the realization server can load balance across multiple infraservers
	err = storage.StoreInfrapodCount(rlz)
	if err != nil {
		log.Errorf("store infrapod count: %v", err)
	}

	bucket := fmt.Sprintf("realize-%s-%s-%s", rq.Project, rq.Experiment, rq.Realization)

	found, err := storage.MinIOClient.BucketExists(context.TODO(), bucket)
	if err != nil {
		return fmt.Errorf("minio check bucket: %v", err)
	}
	if !found {
		err := storage.MinIOClient.MakeBucket(
			context.TODO(), bucket, minio.MakeBucketOptions{})
		if err != nil {
			return fmt.Errorf("minio make bucket: %v", err)
		}
	}

	_, err = storage.MinIOClient.PutObject(
		context.TODO(),
		bucket,
		"diagnostics",
		bytes.NewReader(dbuf),
		int64(len(dbuf)),
		minio.PutObjectOptions{},
	)
	if err != nil {
		return fmt.Errorf("minio diagnostics put: %v", err)
	}

	_, err = storage.MinIOClient.PutObject(
		context.TODO(),
		bucket,
		"realization",
		bytes.NewReader(rbuf),
		int64(len(rbuf)),
		minio.PutObjectOptions{},
	)
	if err != nil {
		return fmt.Errorf("minio realization put: %v", err)
	}

	return nil

}

func GetResourcePool(project string) (*portal.Pool, error) {

	pools, err := storage.GetPools()
	if err != nil {
		return nil, err
	}

	for _, pool := range pools {
		for _, proj := range pool.Projects {
			if project == proj {
				return pool.Pool, nil
			}
		}
	}

	// if project is in no pool, it uses the default.
	pool := storage.NewPool("default") // "default" should be parameterized
	err = pool.Read()
	if err != nil {
		return nil, err
	}

	return pool.Pool, nil
}

func handleReservationTimeout() {

	log.Infof("Started reservation timeout loop. Watching for expired realizations.")

	for {

		log.Debugf("Starting reservation timeout check.")

		rrs, err := storage.GetAllRealizationRequests()
		if err != nil {
			log.Errorf("Error reading realization requests: %s", err.Error())
		}

		for _, r := range rrs {

			rzid := fmt.Sprintf("%s.%s.%s", r.Realization, r.Experiment, r.Project)

			rlz, err := storage.ReadRealizationResult(r.Project, r.Experiment, r.Realization)
			if err != nil {
				log.Warnf("Unable to read realization in expiration-check loop: %s", rzid)
				continue
			}

			// exp.IsZero() does not work for some reason so check for zeros. nil Expires is for backwards compatibility.
			if rlz.Realization.Expires == nil || (rlz.Realization.Expires.Seconds == 0 && rlz.Realization.Expires.Nanos == 0) {
				log.Debugf("Ignoring realization that does not expire: %s", rzid)
				continue
			}

			now := time.Now()
			exp := rlz.Realization.Expires.AsTime()

			if exp.Before(now) {
				// It's in the past, kill it.
				log.Infof("Found expired realization: %s. Deleting it.", rzid)

				_, err = r.Delete()
				if err != nil {
					log.Errorf("Error deleting expired realization: %s: %s", rzid, err.Error())
					// Not sure what to do here. We will likely hit this in the next loop as well.
					continue
				}
			}

			// Do we want a between-realizations-expire-check sleep here as well? Will reading all the realization data
			// from MinIO be a tax on the system?
		}

		log.Debugf("Completed reservation timeout check. Sleeping for %s", defaultRealizationTimeoutLoopWait)

		// Wait - then loop again.
		time.Sleep(defaultRealizationTimeoutLoopWait)
	}
}
