package main

import (
	"context"
	"fmt"

	log "github.com/sirupsen/logrus"
	"github.com/teris-io/shortid"
	portal "gitlab.com/mergetb/api/portal/v1/go"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func (x *ids) ListIdentities(
	ctx context.Context, rq *portal.ListIdentityRequest,
) (*portal.ListIdentityResponse, error) {

	log.Info("List Identities")

	ids, err := readIdentities()
	if err != nil {
		return nil, internalError("read ids: %v", err)
	}

	resp := new(portal.ListIdentityResponse)
	resp.Identities = ids

	return resp, nil

}

func (x *ids) GetIdentity(
	ctx context.Context, rq *portal.GetIdentityRequest,
) (*portal.GetIdentityResponse, error) {

	log.Info("Get Identity")

	ids, err := readIdentities()
	if err != nil {
		return nil, internalError("read ids: %v", err)
	}

	resp := new(portal.GetIdentityResponse)

	for _, id := range ids {
		if id.Username == rq.Username {
			resp.Identity = id
			return resp, nil
		}
	}

	return nil, status.Errorf(codes.NotFound, "Username %s not found", rq.Username)
}

func (x *ids) Register(
	ctx context.Context, rq *portal.RegisterRequest,
) (*portal.RegisterResponse, error) {

	log.Info("Register")

	// returns merge error in grpc error so just pass it along.
	_, err := newIdentity(
		rq.Username, rq.Email, rq.Password,
	)

	if err != nil {
		return nil, err
	}

	return &portal.RegisterResponse{}, nil
}

func (x *ids) Unregister(
	ctx context.Context, rq *portal.UnregisterRequest,
) (*portal.UnregisterResponse, error) {

	log.Info("Unregister")

	err := delIdentity(rq.Username)
	if err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &portal.UnregisterResponse{}, nil

}

func (x *ids) Login(
	ctx context.Context, rq *portal.LoginRequest,
) (*portal.LoginResponse, error) {

	log.Info("Login")

	result, err := loginapi(rq.Username, rq.Password)
	if err != nil {
		log.Infof("loginapi error: %+v", err)
		return nil, err
	}
	if result == nil {
		return nil, status.Error(codes.PermissionDenied, err.Error())
	}

	return &portal.LoginResponse{
		Token: result.SessionToken,
	}, nil

}

func (x *ids) Logout(
	ctx context.Context, rq *portal.LogoutRequest,
) (*portal.LogoutResponse, error) {

	log.Info("Logout")

	// TODO: implement id service logout and call it here.

	return &portal.LogoutResponse{}, nil
}

func internalError(format string, args ...interface{}) error {

	id, serr := shortid.Generate()
	if serr != nil {
		log.Errorf("failed to create error id: %v", serr)
	}

	log.WithFields(log.Fields{"ref": id}).Errorf(format, args...)

	return fmt.Errorf("internal error ref: %s", id)

}
