package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"

	log "github.com/sirupsen/logrus"
	portal "gitlab.com/mergetb/api/portal/v1/go"

	ory "github.com/ory/kratos-client-go"
	ident "gitlab.com/mergetb/portal/services/pkg/identity"
	"gitlab.com/mergetb/portal/services/pkg/merror"
	"gitlab.com/mergetb/portal/services/pkg/storage"
)

type LoginResultAPI struct {
	SessionToken string      `json:"session_token"`
	Session      ory.Session `json:"session"`
}

func readIdentities() ([]*portal.IdentityInfo, error) {

	pageSize := int64(512)
	var pageToken string
	var cnt int

	cli := ident.KratosAdminCli()
	pids := []*portal.IdentityInfo{}

	// ugly
	for cnt = 0; cnt < 10; cnt++ { // I don't trust Ory to give us the next token, so put a cap of 10 loops on this.
		ids, resp, err := cli.IdentityAPI.ListIdentities(context.Background()).PageSize(pageSize).PageToken(pageToken).Execute()
		if err != nil {
			log.Errorf("Read Identities: %v", err)
			if resp.StatusCode != 200 {
				return nil, KratosRespError(resp)
			}
			return nil, fmt.Errorf("Kratos error: %v", err)
		}

		log.Infof("read %d IDs this page", len(ids))

		for _, id := range ids {
			userId := &ident.Identity{}
			userId.Traits.Traits = make(map[string]string)
			err := userId.FromOryID(&id)
			if err != nil {
				log.Errorf("Error conveting Ory Identity Traits")
				continue
			}

			// log.Debugf("reading user for id %s", userId.Traits.Username)

			// Read admin status from User data.
			admin := false
			user := storage.NewUser(userId.Traits.Username)
			err = user.Read()
			if err == nil {
				admin = user.Admin
			}

			pids = append(pids, &portal.IdentityInfo{
				Email:    userId.Traits.Email,
				Username: userId.Traits.Username,
				Admin:    admin,
				Traits:   userId.Traits.Traits,
			})

		}

		pageToken, err = ident.GetNextPageToken(resp)
		if err != nil {
			return nil, KratosRespError(resp)
		}

		if pageToken == "" {
			break
		}

	}

	log.Infof("read %d IDs total", len(pids))

	return pids, nil
}

func newIdentity(uid, email, pw string) (string, error) {

	cli := ident.KratosPublicCli()

	ts := map[string]interface{}{
		"email":    email,
		"username": uid,
	}

	flow, resp, err := cli.FrontendAPI.CreateNativeRegistrationFlow(context.Background()).Execute()
	if err != nil {
		log.Errorf("kratos error: %+v", err)
		log.Errorf("kratos resp: %+v", resp)
		return "", err
	}
	if resp.StatusCode != 200 {
		err := KratosRespError(resp)
		log.Errorf("create reg flow error: %+v", err)
		return "", err
	}

	// I can't stand this style of invoking things. So stupid and error-prone and not documented.
	reg, resp, err := cli.FrontendAPI.UpdateRegistrationFlow(
		context.Background(),
	).Flow(
		flow.Id,
	).UpdateRegistrationFlowBody(
		ory.UpdateRegistrationFlowBody{
			UpdateRegistrationFlowWithPasswordMethod: ory.NewUpdateRegistrationFlowWithPasswordMethod(
				"password", pw, ts,
			),
		},
	).Execute()
	// TODO check error here. The error returned seems to match the resp.StatusCode though,
	// so I don't know if it needed.
	if resp.StatusCode != 200 {
		err := KratosRespError(resp)
		log.Errorf("create reg flow error: %+v", err)
		return "", err
	}

	log.Debugf("Got registration: %+v", reg)

	return "", nil
}

func delIdentity(uid string) error {

	pageSize := int64(512)
	var pageToken string
	var cnt int

	var idid string

	cli := ident.KratosAdminCli()
	for cnt = 0; cnt < 10; cnt++ { // I don't trust Ory to give us the next token, so put a cap of 10 loops on this.
		ids, resp, err := cli.IdentityAPI.ListIdentities(context.Background()).PageSize(pageSize).PageToken(pageToken).Execute()
		if err != nil {
			e := KratosRespError(resp)
			log.Errorf("error message: %s", e)
			return e
		}

		for _, id := range ids {
			traits, ok := id.Traits.(map[string]interface{})
			if ok {
				if _, ok := traits["username"]; ok {
					username, ok := traits["username"].(string)
					if ok {
						if username == uid {
							idid = id.Id
							break
						}
					}
				}
			}
		}

		pageToken, err = ident.GetNextPageToken(resp)
		if err != nil {
			return KratosRespError(resp)
		}

		if pageToken == "" {
			break
		}
	}

	if idid != "" {
		resp, err := cli.IdentityAPI.DeleteIdentity(context.Background(), idid).Execute()
		if err != nil {
			log.Errorf("DeleteIdentity error: %+v", err)
			e := KratosRespError(resp)
			log.Errorf("error message: %s", e)
			return e
		}
	} else {
		return fmt.Errorf("ID %s not found", uid)
	}

	log.Infof("Deleted identity ID: %s (%s)", uid, idid)

	return nil
}

func loginapi(uid, pw string) (*LoginResultAPI, error) {

	cli := ident.KratosPublicCli()

	// init the flow
	flow, r, err := cli.FrontendAPI.CreateNativeLoginFlow(context.Background()).Execute()
	if err != nil {
		log.Errorf("Error FrontendAPI.CreateNativeLoginFlow: %v\n", err)
		log.Errorf("Full HTTP response: %v\n", r)
		return nil, fmt.Errorf("init login flow: %s", err)
	}

	b := ory.UpdateLoginFlowBody{
		UpdateLoginFlowWithPasswordMethod: &ory.UpdateLoginFlowWithPasswordMethod{
			Method:     "password",
			Password:   pw,
			Identifier: uid,
		},
	}

	login, resp, err := cli.FrontendAPI.UpdateLoginFlow(context.Background()).Flow(flow.Id).UpdateLoginFlowBody(b).Execute()

	if err != nil {
		log.Errorf("Login Flow: %v", err)
		if resp.StatusCode != 200 {
			return nil, KratosRespError(resp)
		}
		return nil, fmt.Errorf("Kratos error: %v", err)
	}

	log.Debugf("got session token: %+v", login.SessionToken)

	return &LoginResultAPI{SessionToken: *login.SessionToken, Session: login.Session}, nil
}

func KratosRespError(resp *http.Response) error {

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return fmt.Errorf("no error body to parse")
	}

	log.Debugf("body: %s", string(body))

	// These structs must exist in the kratos code. TODO: find them and use them. I wish you luck.
	type OryUiErrMessage struct {
		Id   int    `json:"id"`
		Type string `json:"type"`
		Text string `json:"text"`
	}
	type OryUiNode struct {
		// lots of other things
		Messages []OryUiErrMessage `json:"messages"`
	}

	type OryUiMessages struct {
		Ui struct {
			Nodes []OryUiNode `json:"nodes"`
		}
	}

	ms := OryUiMessages{}
	err = json.Unmarshal(body, &ms)
	if err != nil {
		log.Errorf("kratos response unmarhsal error: %s", err)
		return fmt.Errorf("unparsable error")
	}

	log.Debugf("parsed messages: %+v", ms)

	// Look through the nodes for messages.
	messages := []string{}
	for _, node := range ms.Ui.Nodes {
		for _, message := range node.Messages {
			if message.Text != "" {
				messages = append(messages, fmt.Sprintf("%s (%s/%d)", message.Text, message.Type, message.Id))
			}
		}
	}
	return merror.ToGRPCError(merror.IdentityError(messages))

}

func findKratosConflict(uid, email string) (string, error) {

	log.Debugf("Looking for account id/email conflict for %s/%s", uid, email)

	ids, err := readIdentities()
	if err != nil {
		return "", err
	}

	for _, id := range ids {
		if id.Email == email {
			return fmt.Sprintf("An identity with the email %s already exists", email), nil
		}

		if id.Username == uid {
			return fmt.Sprintf("An identity with the username %s already exists", uid), nil
		}
	}

	return "", nil
}
