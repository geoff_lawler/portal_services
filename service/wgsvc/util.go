package main

import (
	"context"
	"errors"
	"fmt"
	"net"
	"regexp"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"

	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	"gitlab.com/mergetb/api/facility/v1/go"
	portal "gitlab.com/mergetb/api/portal/v1/go"

	"gitlab.com/mergetb/portal/services/internal"
	"gitlab.com/mergetb/portal/services/pkg/connect"
	"gitlab.com/mergetb/portal/services/pkg/merror"
	"gitlab.com/mergetb/portal/services/pkg/realize"
	"gitlab.com/mergetb/portal/services/pkg/storage"
)

const (
	addrMin uint64 = 1
	addrMax uint64 = 254
)

// Take an enclave ID and return a list of sites that are part of the realization.
// Read the realization rather than the materialization, as the latter may not
// exist in storage by the time this reconciler is invoked
func enclaveToSiteList(enclaveid string) ([]string, error) {
	mzid, err := internal.MzidFromString(enclaveid)
	if err != nil {
		return nil, err
	}

	rlz, err := storage.ReadRealizationResult(
		mzid.Pid,
		mzid.Eid,
		mzid.Rid,
	)
	if err != nil {
		return nil, err
	}

	return realize.SiteList(rlz.Realization), nil
}

// assertGatewayAddrs ensures we've allocated an address for all sites in the enclave
func assertGatewayAddrs(enc *storage.WgEnclave) error {
	l := log.WithFields(log.Fields{
		"enclaveid": enc.Enclaveid,
	})

	// if updated, we'll need to save enc
	var updated bool = false

	if len(enc.GatewayIps) == 0 {
		enc.GatewayIps = make(map[string]string)
		// mtz object is potentially large; read once, and subsequently grab sites from GatewayIps
		sites, err := enclaveToSiteList(enc.Enclaveid)
		if err != nil {
			return fmt.Errorf("read site list: %w", err)
		}
		for _, site := range sites {
			// empty strings for ips not assigned yet
			enc.GatewayIps[site] = ""
		}
		updated = true
	}

	// allocate access addr for each site in the enclave and request gateway ifx creation
	var rec_err error
	for site, ipstr := range enc.GatewayIps {
		if ipstr != "" {
			continue // already done with
		}
		ip, err := nextAddress(enc.Enclaveid)
		if err != nil {
			rec_err = fmt.Errorf("nextAddress for site %s failed: %w", site, err)
			break
		}

		ipstr = ip.String()

		l.WithFields(log.Fields{
			"site":   site,
			"wgaddr": ipstr,
		}).Info("allocated gateway addr for site")

		ctx, cancel := context.WithTimeout(context.Background(), time.Second*60)
		err = createGwIfSite(enc, site, ipstr, ctx)
		cancel()

		if err != nil {
			xerr := freeAddress(enc.Enclaveid, ip)
			if xerr != nil {
				l.Errorf("cannot free ip %s: %v", ip, xerr)
			}
			rec_err = fmt.Errorf("createGwIfSite for site %s failed: %w", site, err)
			// potentially could log the error and continue to process other sites
			break
		} else {
			enc.GatewayIps[site] = ipstr
			updated = true
		}
	}

	if updated {
		_, err := enc.Update()
		if err != nil {
			return fmt.Errorf("update wg enclave: %+v", err)
		}
	}
	return rec_err
}

func waitForEnclaveSite(enc *storage.WgEnclave, site string, ctx context.Context) error {
	l := log.WithFields(log.Fields{
		"enclaveid": enc.Enclaveid,
		"site":      site,
	})

	mzid, err := internal.MzidFromString(enc.Enclaveid)
	if err != nil {
		return err
	}

	sleep_time := 3 * time.Second

	for {
		select {
		case <-ctx.Done():
			return ctx.Err()
		default:
			// check to see if the mz exists in the first place
			_, err := storage.ReadMaterialization(mzid.Pid, mzid.Eid, mzid.Rid)
			if err != nil {
				if errors.Is(err, merror.ErrMaterializeInProgress) {
					l.Infof("waiting for materialization to exist ...")
					time.Sleep(sleep_time)
					continue

				} else {
					return fmt.Errorf("waitForEnclaveSite: mtz doesn't exist: %w", err)
				}
			}

			// check to see if the facility has reported it
			var status *portal.TaskSummary
			err = connect.FacilityClient(
				site,
				func(cli facility.FacilityClient) error {
					st, err := cli.GetMaterialization(
						context.TODO(),
						&facility.GetMaterializationRequest{
							Pid:     mzid.Pid,
							Eid:     mzid.Eid,
							Rid:     mzid.Rid,
							Summary: true,
						},
					)
					if err == nil {
						status = st.Summary
					}
					return err
				},
			)
			if err == nil {
				l.Debugf("site reported status: %+v", status)
				return nil
			}

			l.Infof("waiting for site to report mtz status ...")
			time.Sleep(sleep_time)
		}
	}
}

func createGwIfSite(enc *storage.WgEnclave, site, wgAddr string, ctx context.Context) error {
	l := log.WithFields(log.Fields{
		"enclaveid":  enc.Enclaveid,
		"site":       site,
		"accessaddr": wgAddr,
	})

	l.Debug("waiting for site to report materialization status")
	err := waitForEnclaveSite(enc, site, ctx)
	if err != nil {
		l.Errorf("wait for site: %+v", err)
		return err
	}

	l.Debug("sending CreateWgInterfaceRequest to site")
	err = connect.FacilityWGClient(
		site,
		func(cli facility.WireguardClient) error {
			_, err := cli.CreateWgInterface(
				context.TODO(),
				&facility.CreateWgInterfaceRequest{
					Enclaveid:  enc.Enclaveid,
					Accessaddr: wgAddr,
				},
			)
			return err
		},
	)
	if err != nil {
		l.Errorf("send CreateWgInterface to site: %+v", err)
		return err
	}

	l.Info("successfully sent CreateWgInterfaceRequest to site")
	return nil
}

func deleteGwIfSite(enc *storage.WgEnclave, site string, ctx context.Context) error {
	l := log.WithFields(log.Fields{
		"enclaveid": enc.Enclaveid,
		"site":      site,
	})

	l.Debug("sending DeleteWgInterfaceRequest to site")
	err := connect.FacilityWGClient(
		site,
		func(cli facility.WireguardClient) error {
			_, err := cli.DeleteWgInterface(
				context.TODO(),
				&facility.DeleteWgInterfaceRequest{
					Enclaveid: enc.Enclaveid,
				},
			)
			return err
		},
	)
	if err != nil {
		l.Errorf("send DeleteWgInterface to site: %+v", err)
		return err
	}

	l.Info("successfully sent DeleteWgInterface request to site")
	return nil
}

func deleteGwIfs(enc *storage.WgEnclave) {

	l := log.WithFields(log.Fields{
		"enclaveid": enc.Enclaveid,
	})

	for site, wgaddr := range enc.GatewayIps {
		// Dematerialize tells the site to remove wgifs implicitly -- no need to send here
		//deleteGwIfSite(enc, site, context.Background())

		// free the address
		ip := net.ParseIP(wgaddr)
		if ip == nil {
			err := fmt.Errorf("addr is not a valid IP address: %s", wgaddr)
			l.Error(err)
			continue
		}

		err := freeAddress(enc.Enclaveid, ip)
		if err != nil {
			l.Error(err)
		}

		l.WithFields(log.Fields{
			"site":   site,
			"wgaddr": wgaddr,
		}).Info("freed gateway addr for site")
	}
}

func readOrCreateEnclave(enclaveid string) (*storage.WgEnclave, error) {
	enc := storage.NewWgEnclave(enclaveid)
	err := enc.Read()
	if err != nil {
		return nil, fmt.Errorf("read wg enclave: %+v", err)
	}

	// IS RECONCILED
	// create on version 0
	if enc.Ver == 0 {
		// create the enclave
		_, err = enc.Create()
		if err != nil {
			return nil, fmt.Errorf("create wg enclave: %+v", err)
		}

		log.Debugf("created wg enclave %s", enclaveid)
	}

	if enc.ClientIps == nil {
		enc.ClientIps = make(map[string]string)
	}

	return enc, nil
}

func addrCounter(name string) *storage.Counter {
	// 254 values, which is inclusive of .1 through .254
	return &storage.Counter{
		CountSet: internal.CountSet{
			Name:   "enclave-addrs/" + name,
			Size:   addrMax,
			Offset: addrMin,
		},
	}
}

func nextAddress(enclave string) (net.IP, error) {
	c := addrCounter(enclave)
	err := c.Read()
	if err != nil {
		return nil, fmt.Errorf("storage counter/address read: %+v", err)
	}

	b, err := c.Next()
	if err != nil {
		return nil, fmt.Errorf("storage counter/address next: %+v", err)
	}

	if b < addrMin || b > addrMax {
		return nil, fmt.Errorf("corrupted counter/address. %d is outside of valid range [%d,%d]",
			b, addrMin, addrMax,
		)
	}

	// TODO: pull this from dynamic merge configuration.
	i := net.IPv4(192, 168, 254, byte(b))
	return i, nil
}

func freeAddress(enclave string, ip net.IP) error {
	c := addrCounter(enclave)

	err := c.Read()
	if err != nil {
		return fmt.Errorf("storage counter/address read: %+v", err)
	}

	ip4 := ip.To4()
	if ip4 == nil {
		return fmt.Errorf("ip address %s is not an IPv4 address", ip.String())
	}

	_ = c.Free(uint64(ip4[3]))
	_, err = c.Update()
	if err != nil {
		return fmt.Errorf("storage counter/address update: %+v", err)
	}

	return nil
}

func freeAddressString(enclave, ipstr string) error {
	ip := net.ParseIP(ipstr)
	if ip == nil {
		return fmt.Errorf("%s is not a valid IP address", ipstr)
	}

	return freeAddress(enclave, ip)
}

func containerName2WgdData(name string) (string, string, error) {
	// from the containername, get the pod name. from the pod name, we can get the containerdid
	// and the containerdid is what wgd needs to find the container on the node side.
	// in merge/portal the container name is the pod name.
	l := log.WithFields(log.Fields{
		"name": name,
	})
	l.Debug("containerName2WgdData")

	// We only support names of the form xdc.pid
	tokens := regexp.MustCompile("[.]").Split(name, 2)
	if len(tokens) != 2 {
		return "", "", fmt.Errorf("bad container name format")
	}

	xid, pid := tokens[0], tokens[1]
	labSel := fmt.Sprintf("proj=%s, name=%s", pid, xid)
	l = l.WithField("k8sLabelSelector", labSel)

	pods, err := k8c.CoreV1().Pods(xdcNs).List(
		context.TODO(),
		v1.ListOptions{
			LabelSelector: labSel,
		},
	)

	if err != nil {
		l.Errorf("pod not found: %+v", err)
		return "", "", err
	}

	if len(pods.Items) != 1 {
		l.Errorf("wrong number of pods found: %d", len(pods.Items))
		return "", "", fmt.Errorf("wrong number of pods found")
	}
	pod := pods.Items[0]

	hostEP := pod.Status.HostIP + ":" + wgdPort

	// format is like "containerd://bb213810335cfe6f1494681..."
	contIDUrl := pod.Status.ContainerStatuses[0].ContainerID
	spl := strings.Split(contIDUrl, "//")

	if len(spl) != 2 {
		l.Errorf("incorrect format for container ID: %s", contIDUrl)
		return "", "", fmt.Errorf("incorrect format for container id: %s", contIDUrl)
	}

	contID := spl[1]

	l = l.WithFields(log.Fields{
		"nodeIP":      hostEP,
		"containerID": contID,
	})

	l.Trace("found container data")
	return hostEP, contID, nil
}
