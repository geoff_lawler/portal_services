package main

import (
	"context"
	"fmt"
	"net"
	"os"
	"regexp"
	"sync"

	log "github.com/sirupsen/logrus"
	"google.golang.org/protobuf/proto"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"

	portal "gitlab.com/mergetb/api/portal/v1/go"
	wgd "gitlab.com/mergetb/api/wgd/v1/go"
	xdcd "gitlab.com/mergetb/api/xdcd/v1/go"
	"gitlab.com/mergetb/portal/services/pkg/podwatch"
	"gitlab.com/mergetb/portal/services/pkg/storage"
	"gitlab.com/mergetb/tech/reconcile"
)

var (
	k8c *kubernetes.Clientset

	nameExp     = "([a-zA-Z_]+[a-zA-Z0-9_]+)"
	xdcWgBucket = storage.PrefixedBucket(&storage.XdcWgClient{})
	xdcWgKey    = regexp.MustCompile(`^` + xdcWgBucket + nameExp + `\.` + nameExp + `$`)

	mutex sync.Mutex
)

// A /24 means we have 254 addresses per materialization. Probably sufficient, but might
// need to be extended at some point.
const (
	wgAllowedIPs string = "192.168.254.0/24"
	nameserver   string = "172.30.0.1"
)

type XdcTask struct {
	XDC     string
	Project string
}

func init() {
	x, ok := os.LookupEnv("XDC_NAMESPACE")
	if !ok {
		log.Fatal("XDC_NAMESPACE must be defined in the environment")
	}
	xdcNs = x
	log.Debugf("XDC_NAMESPACE=%s", xdcNs)

	config, err := rest.InClusterConfig()
	if err != nil {
		log.WithFields(log.Fields{
			"err": err,
		}).Fatal("failed to fetch kubeconfig")
	}

	k8c, err = kubernetes.NewForConfig(config)
	if err != nil {
		log.WithFields(log.Fields{
			"err": err,
		}).Fatal("failed to create k8s client")
	}
}

func (xt *XdcTask) Parse(key string) bool {

	tkns := xdcWgKey.FindAllStringSubmatch(key, -1)
	if len(tkns) > 0 {
		xt.XDC = tkns[0][1]
		xt.Project = tkns[0][2]
		return true
	}

	return false
}

// doPut assumes much:
//  1. this is a container in a merge portal,
//  2. k8s is managing the container,
//  3. containerd is the container interface,
//  4. a wgd instance is running on the container host and the endpoint is listening
//     on port wgdPort.
//
// This is not at all even close to a general "make me a wgif on my host" solution.
// Most likely, it doesn't even work well in a merge portal.
//
// TODO: support multi-site wg enclaves. This will mean parsing the address space
// somehow then attaching to each site.
func (xt *XdcTask) doPut(value []byte, version int64, td *reconcile.TaskData) error {
	l := log.WithFields(log.Fields{
		"xdc":     xt.XDC,
		"project": xt.Project,
	})

	// sanity check, reconcile package can be weird
	if value == nil {
		err := fmt.Errorf("cannot handle reconcile request: no value to reconcile")
		l.Error(err)
		return err
	}

	req := new(portal.AttachXDCRequest)
	err := proto.Unmarshal(value, req)
	if err != nil {
		l.Error(err)
		return err
	}

	encId := req.Realization + "." + req.Experiment + "." + req.Project

	l = l.WithField("enclaveid", encId)

	// This is the core logic to perform the XDC attachment
	err = doAttach(req.Xdc, req.Project, encId)
	if err != nil {
		return err
	}

	// Update the XDC data to reflect that the XDC is now attached.
	// Should this be in the XDC reconciler? Maybe. Putting it here though
	// means we know the attach worked. We'd need to read the status + attach request
	// key in the XDC reconciler otherwise.
	xdc := storage.NewXDC("", xt.XDC, xt.Project)
	if err = xdc.Read(); err != nil {
		l.Errorf("XDC read error: %+v", err)
	} else {
		l.Debugf("setting XDC materialization to %s", encId)

		_, err = xdc.SetMaterialization(encId)
		if err != nil {
			l.Errorf("XDC write error: %+v", err)
		}
	}

	// We put the attached mtz name in both etcd and as a label on the pod. This is
	// so we can handle the case of the XDC getting restarted - we use the etcd data
	// to rettach the XDC. We use the label to mean the XDC is attached.
	err = podwatch.AddPodLabel(xt.XDC, xt.Project, xdcNs, attachedLabel, encId)
	if err != nil {
		l.Errorf("Error labeling node as attached: %s", err.Error())
	}

	return nil
}

func (xt *XdcTask) doDelete(req *portal.AttachXDCRequest) error {
	encId := req.Realization + "." + req.Experiment + "." + req.Project
	return doDetach(req.Xdc, req.Project, encId)
}

func (xt *XdcTask) Create(value []byte, version int64, td *reconcile.TaskData) *reconcile.TaskMessage {
	log.Debugf("received reconcile Create request for xdc: %s.%s", xt.XDC, xt.Project)

	err := xt.doPut(value, version, td)
	if err != nil {
		return reconcile.TaskMessageError(err)
	}

	return nil
}

func (xt *XdcTask) Update(prev, value []byte, version int64, td *reconcile.TaskData) *reconcile.TaskMessage {
	log.Debugf("received reconcile Update request for xdc: %s.%s", xt.XDC, xt.Project)

	err := xt.doPut(value, version, td)
	if err != nil {
		return reconcile.TaskMessageError(err)
	}

	return nil
}

func (xt *XdcTask) Ensure(prev, value []byte, version int64, td *reconcile.TaskData) *reconcile.TaskMessage {
	log.Debugf("received reconcile Ensure request for xdc: %s.%s", xt.XDC, xt.Project)

	err := xt.doPut(value, version, td)
	if err != nil {
		return reconcile.TaskMessageError(err)
	}

	return nil

}

func (xt *XdcTask) Delete(value []byte, version int64, td *reconcile.TaskData) *reconcile.TaskMessage {
	log.Debugf("received reconcile Delete requuest for xdc: %s.%s", xt.XDC, xt.Project)

	// sanity check, reconcile package can be weird
	if value == nil {
		err := fmt.Errorf("cannot handle reconcile delete request: no value to reconcile")
		log.Error(err)
		return reconcile.TaskMessageError(err)
	}

	req := new(portal.AttachXDCRequest)
	err := proto.Unmarshal(value, req)
	if err != nil {
		log.Error(err)
		return reconcile.TaskMessageError(err)
	}

	err = xt.doDelete(req)
	if err != nil {
		return reconcile.TaskMessageError(err)
	}

	return nil
}

func wgdCreateInterface(wgdEp, cid, encId string, ifx *portal.WgIfConfig, peers []*portal.WgIfConfig) (string, error) {
	pubKey := ""
	err := Wgd(
		wgdEp,
		func(cli wgd.WgdClient) error {
			resp, err := cli.CreateContainerInterface(
				context.TODO(),
				&wgd.CreateContainerInterfaceRequest{
					Enclaveid:   encId,
					Accessaddr:  ifx.Accessaddr,
					Containerid: cid,
					Peers:       peers,
				},
			)
			if err == nil {
				pubKey = resp.Key
			}
			return err
		},
	)
	if err != nil {
		return "", fmt.Errorf("delete container interface: %+v", err)
	}

	return pubKey, nil
}

func wgdGetInterface(wgdEp, cid string) (*wgd.GetContainerInterfaceResponse, error) {
	var resp *wgd.GetContainerInterfaceResponse = nil
	err := Wgd(
		wgdEp,
		func(cli wgd.WgdClient) error {
			r, err := cli.GetContainerInterface(
				context.Background(),
				&wgd.GetContainerInterfaceRequest{
					Containerid: cid,
				},
			)
			if err == nil {
				resp = r
			}
			return err
		},
	)
	if err != nil {
		return nil, fmt.Errorf("get container interface: %+v", err)
	}

	return resp, nil
}

func wgdDeleteInterface(wgdEp, cid string) (string, error) {
	pubKey := ""
	err := Wgd(
		wgdEp,
		func(cli wgd.WgdClient) error {
			resp, err := cli.DeleteContainerInterface(
				context.TODO(),
				&wgd.DeleteContainerInterfaceRequest{
					Containerid: cid,
				},
			)
			if err == nil {
				pubKey = resp.Key
			}
			return err
		},
	)
	if err != nil {
		return "", fmt.Errorf("delete container interface: %+v", err)
	}

	return pubKey, nil
}

func wgdAddPeers(wgdEp, cid string, peers []*portal.WgIfConfig) error {
	err := Wgd(
		wgdEp,
		func(cli wgd.WgdClient) error {
			_, err := cli.AddWgPeers(
				context.TODO(),
				&wgd.AddWgPeersRequest{
					Containerid: cid,
					Peers:       peers,
				},
			)
			return err
		},
	)
	if err != nil {
		return fmt.Errorf("add wg peers: %+v", err)
	}

	return nil
}

func xdcdConfigureTunnel(svcName, encId, nameserver string) error {
	err := Xdcd(
		svcName,
		func(cli xdcd.XdcdClient) error {
			_, err := cli.ConfigureTunnel(
				context.Background(),
				&xdcd.ConfigureTunnelRequest{
					Nameserver: nameserver,
					Rid:        encId,
				},
			)
			return err
		},
	)
	if err != nil {
		return fmt.Errorf("xdcd configure tunnel: %+v", err)
	}

	return nil
}

func xdcdClearTunnel(svcName string) error {
	err := Xdcd(
		svcName,
		func(cli xdcd.XdcdClient) error {
			_, err := cli.ClearTunnelData(
				context.Background(),
				&xdcd.ClearTunnelDataRequest{},
			)
			return err
		},
	)
	if err != nil {
		return fmt.Errorf("xdcd clear tunnel: %+v", err)
	}

	return nil
}

// This function configures the ifx on the XDC
// If another interface is already configured, it will be deleted
func xdcConfigureInterface(enc *storage.WgEnclave, xdcFqdn string, ifx *portal.WgIfConfig) (string, error) {
	l := log.WithFields(log.Fields{
		"enclaveid": enc.Enclaveid,
		"xdcFqdn":   xdcFqdn,
		"ifx":       ifx,
	})
	l.Trace("xdcConfigureInterface")

	wgdEp, cid, err := containerName2WgdData(xdcFqdn)
	if err != nil {
		return "", fmt.Errorf("containerName2WgdData: %v", err)
	}
	l.Tracef("found endpoint:%s cid:%s", wgdEp, cid)

	// See if interface already exists
	resp, err := wgdGetInterface(wgdEp, cid)
	if err != nil {
		l.Error(err)
		return "", err
	}

	haveIfx := false
	if resp.Containerid != "" {
		if resp.Accessaddr == ifx.Accessaddr && resp.Key != "" && resp.Key == ifx.Key {
			l.Info("interface already configured in XDC")
			haveIfx = true
		} else {
			// teardown if we wanted a key that does not match
			l.Warnf("found existing interface in XDC that is not fully configured. Tearing down and rebuilding")
			_, err := wgdDeleteInterface(wgdEp, cid)
			if err != nil {
				l.Errorf("could not delete wgd interface: %+v", err)
				return "", nil
			}
		}
	} else {
		l.Debug("no interface found in XDC")
	}

	// Create interface and populate with desired peers
	peers := []*portal.WgIfConfig{}
	for _, gp := range enc.Gateways {
		peers = append(peers, gp)
	}

	var pubKey string

	// If the ifx exists, just add peers. Else, create ifx and add peers
	if haveIfx {
		err = wgdAddPeers(wgdEp, cid, peers)
		if err != nil {
			l.Error(err)
			return "", err
		}
	} else {
		pubKey, err = wgdCreateInterface(wgdEp, cid, enc.Enclaveid, ifx, peers)
		if err != nil {
			l.Error(err)
			return "", err
		}
	}

	if pubKey != "" {
		l.WithField("pubKey", pubKey).Info("configured interface in XDC")
	}

	return pubKey, nil
}

// doAttach does address management and calls out to the external services
// to do the actual attach,
//
// NOTE: xdcName here is *just* the xdc name not the xdc.project format.
func doAttach(xdcName, project, encId string) error {
	var wgAddr string
	var allocatedAddr net.IP
	var allocated bool = false

	xdcFqdn := xdcName + "." + project
	l := log.WithFields(log.Fields{
		"enclaveid": encId,
		"xdcFqdn":   xdcFqdn,
	})

	retfn := func(err error, encId string, allocated bool, addr net.IP) error {
		if err != nil && allocated {
			freeAddress(encId, addr)
		}
		return err
	}

	// this function can be called concurrently by reconciler and podwatch; protect it thus
	mutex.Lock()
	defer mutex.Unlock()

	l.Info("handling XDC attachment request")

	enc, err := readOrCreateEnclave(encId)
	if err != nil {
		l.Error(err)
		return err
	}

	// First, determine whether we've allocated an address for this XDC or not
	for xfqdn, xAddr := range enc.ClientIps {
		if xfqdn == xdcFqdn {
			wgAddr = xAddr
			l = l.WithField("wgAddr", wgAddr)
			l.Info("found existing address for XDC")
			break
		}
	}

	if wgAddr == "" {
		l.Debug("Could not find existing client config for XDC")

		// allocate an address.
		allocatedAddr, err = nextAddress(encId)
		if err != nil {
			return fmt.Errorf("nextaddr: %v", err)
		}
		wgAddr = allocatedAddr.String()
		allocated = true

		l = l.WithField("wgAddr", wgAddr)
		l.Info("allocated address for XDC")
	}

	// If we have a client in the enclave, grab it; otherwise fall back to a new generaton
	var client *portal.WgIfConfig = nil
	client_exists := false
	for _, cli := range enc.Clients {
		if cli.Endpoint == xdcFqdn {
			client = cli
			client_exists = true
			break
		}
	}

	if !client_exists {
		client = &portal.WgIfConfig{
			Endpoint:   xdcFqdn,
			Accessaddr: wgAddr,
			Allowedips: []string{wgAllowedIPs},
		}
	} else {
		if client.Accessaddr != wgAddr {
			err = fmt.Errorf("BUG: incongruence between ClientIps and Client.Config.Accessaddr in enclave")
			l.Error(err)
			return retfn(err, encId, allocated, allocatedAddr)
		}
	}

	l.WithField("client", client)

	// Ensure that the interface is up and configured correctly
	var newPubkey string
	newPubkey, err = xdcConfigureInterface(enc, xdcFqdn, client)
	if err != nil {
		return retfn(err, encId, allocated, allocatedAddr)
	}

	// Now tell the xdcd to update the xdc configuration to route experiment
	// traffic to the tunnel.
	err = xdcdConfigureTunnel(
		xdcName+"-"+project, // k8s svc name is of the form xdc-project
		encId,
		nameserver, // TODO: make dynamic. based on site? from a cm?
	)
	if err != nil {
		l.Warn(err)
		//return err
	}

	// We may have generated a new pubkey when an old key was already registered for the ifx.
	// If so, delete the old one now
	if client_exists && newPubkey != "" {
		l.Warn("generated new pubkey for client. Deleting old now")

		wgif := storage.NewWgIfRequest(enc.Enclaveid, client.Key)
		_, err = wgif.Delete()
		if err != nil {
			l.Errorf("wgif delete: %+v", err)
		}
	}

	// If we generated a new pubkey, add a new if req for it. This will reconcile into the new key
	// getting associated to the enclave and distributed to the remote sites
	if newPubkey != "" {
		client.Key = newPubkey

		l.Info("creating new wgif request")
		wgif := storage.NewWgIfRequest(enc.Enclaveid, newPubkey)
		wgif.Config = client
		wgif.Gateway = false

		// IS RECONCILED
		_, err = wgif.Create()
		if err != nil {
			return retfn(
				fmt.Errorf("wgif create: %+v", err),
				encId,
				allocated,
				allocatedAddr,
			)
		}
	} else {
		l.Debug("no new pubkey created; not creating new wgif request")
	}

	// Save the allocated address in the enclave
	if allocated {
		enc.ClientIps[xdcFqdn] = wgAddr

		// IS RECONCILED
		_, err = enc.Update()
		if err != nil {
			return retfn(err, encId, allocated, allocatedAddr)
		}
	}

	return nil
}

func doDetach(xdcName, project, encId string) error {
	xdcFqdn := xdcName + "." + project
	l := log.WithFields(log.Fields{
		"xdcFqdn":   xdcFqdn,
		"enclaveid": encId,
	})
	l.Info("handling XDC detachment request")

	// Using the enclave, find the client data for the attachment
	enc := storage.NewWgEnclave(encId)
	err := enc.Read()
	if err != nil {
		l.Errorf("could not read enclave data: %+v", err)
		return err
	}

	// Get address from the IP map
	wgAddr := ""
	for xfqdn, xAddr := range enc.ClientIps {
		if xfqdn == xdcFqdn {
			wgAddr = xAddr
			break
		}
	}

	// free client address
	if wgAddr == "" {
		l.Debug("no allocated address found for XDC")
	} else {
		err = freeAddressString(encId, wgAddr)
		if err != nil {
			l.Errorf("could not free accessaddr: %+v", err)
		}
		l.WithField("wgaddr", wgAddr).Info("freed client addr for XDC")

		// remove from the map
		delete(enc.ClientIps, xdcFqdn)

		// IS RECONCILED
		_, err := enc.Update()
		if err != nil {
			l.Errorf("could not update enclave data: %+v", err)
			return err
		}
	}

	// find and issue delete for key
	var client *portal.WgIfConfig = nil
	for _, c := range enc.Clients {
		if c.Endpoint == xdcFqdn {
			client = c
			break
		}
	}

	if client == nil {
		l.Debug("no XDC WG client in enclave")
	} else {
		// invoke the if delete in this reconciler
		l.Debugf("deleting client key %s from storage", client.Key)
		delReq := storage.NewWgIfRequest(encId, client.Key)
		_, err = delReq.Delete()
		if err != nil {
			l.Errorf("could not delete client key: %+v", err)
		}
	}

	// At this point the XDC may be gone. If it is, just do wgsvc data clean up.
	xdc := storage.NewXDC("", xdcName, project)
	err = xdc.Read()
	if err != nil {
		l.Warnf("unable to read XDC data: %+v", err)
	}

	if xdc.GetVersion() == 0 {
		l.Infof("XDC is not present")
		return nil
	}

	// XDC is present; call out to wgd to remove the interface
	wgdEp, cid, err := containerName2WgdData(xdcFqdn)
	if err != nil {
		l.Errorf("containerName2WgdData: %+v", err)
	} else {
		_, err = wgdDeleteInterface(wgdEp, cid)
		if err != nil {
			l.Errorf("could not delete wgd interface: %+v", err)
		}
	}

	// update the XDC attachment field and clear the tunnel data of the XDC
	_, err = xdc.SetMaterialization("")
	if err != nil {
		l.Errorf("XDC write error: %+v", err)
	}

	if wgAddr != "" {
		err = xdcdClearTunnel(xdcName + "-" + project) // k8s svc name is of the form xdc-project
		if err != nil {
			l.Error(err)
		}

		err = podwatch.RemovePodLabel(xdcName, project, xdcNs, attachedLabel)
		if err != nil {
			l.Errorf("error labeling node as detached: %+v", err)
		}

	}

	l.Info("XDC detached")
	return nil
}
