package main

import (
	"errors"
	"fmt"
	"regexp"
	"strings"

	log "github.com/sirupsen/logrus"
	portal "gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/portal/services/pkg/merror"
	"gitlab.com/mergetb/portal/services/pkg/storage"
	"gitlab.com/mergetb/tech/reconcile"
)

var (
	mtzNameExp  = "([a-zA-Z_]+[a-zA-Z0-9_]+)"
	mtzRqBucket = storage.PrefixedBucket(&storage.MaterializeRequest{})
	mtzRqKey    = regexp.MustCompile(
		"^" + mtzRqBucket + mtzNameExp + "/" + mtzNameExp + "/" + mtzNameExp + "$",
	)
)

type MtzTask struct {
	Rid     string
	Eid     string
	Pid     string
	Etcdkey string
}

func (m *MtzTask) mzid() string {
	return fmt.Sprintf("%s.%s.%s", m.Rid, m.Eid, m.Pid)
}

func (m *MtzTask) Parse(k string) bool {
	parts := mtzRqKey.FindAllStringSubmatch(k, -1)
	if len(parts) > 0 {
		m.Pid = parts[0][1]
		m.Eid = parts[0][2]
		m.Rid = parts[0][3]
		m.Etcdkey = k
		return true
	}
	return false
}

func (m *MtzTask) handlePut() error {
	mid := m.mzid()

	l := log.WithFields(log.Fields{
		"enclaveid": mid,
	})

	enc, err := readOrCreateEnclave(mid)
	if err != nil {
		return fmt.Errorf("wg enc read: %+v", err)
	}

	l.Info("wgsvc/mtz: allocating gateway IPs for sites")

	err = assertGatewayAddrs(enc)
	if err != nil {
		return err
	}

	return nil
}

func (m *MtzTask) doPut(value []byte, ver int64, td *reconcile.TaskData) *reconcile.TaskMessage {
	// check to see if the mz exists in the first place
	_, err := storage.ReadMaterialization(m.Pid, m.Eid, m.Rid)
	if err != nil && !errors.Is(err, merror.ErrMaterializeInProgress) {
		return reconcile.TaskMessageWarningf("wgsvc doPut: mtz doesn't exist: %+v , nothing to do", err)
	}

	return reconcile.CheckErrorToMessage(m.handlePut())
}

func (m *MtzTask) doDelete() error {
	mid := m.mzid()

	l := log.WithFields(log.Fields{
		"enclaveid": mid,
	})

	enc, err := readOrCreateEnclave(mid)
	if err != nil {
		err = fmt.Errorf("wg enc read: %+v", err)
		return err
	}

	l.Info("wgsvc/mtz: clearing gateway IPs and freeing addresses")

	// Tell sites to delete gateway ifs
	deleteGwIfs(enc)

	enc.GatewayIps = make(map[string]string)

	// IS RECONCILED
	_, err = enc.Update()
	if err != nil {
		return err
	}

	// Gateways, Clients, GatewayIPs .... all needs to go NOW. Not async, now. Because as soon as we
	// get kicked with the same mtz again, we would run the risk of hitting old state

	l.Info("wgsvc/mtz: detaching XDC attachment requests for enclave")

	// Detach all clients
	for key, cli := range enc.Clients {
		tkns := strings.Split(cli.Endpoint, ".")
		xid, pid := tkns[0], tkns[1]

		l2 := l.WithFields(log.Fields{
			"xdcFqdn": cli.Endpoint,
		})

		// synchronously perform the detachment
		// (note that this removes the wgif request in storage)
		err = doDetach(xid, pid, enc.Enclaveid)
		if err != nil {
			l2.Warnf("XDC detach error: %+v", err)
		}

		// synchronously remove the wgif for the XDC
		wgtask := &WgIfTask{
			Enclaveid: enc.Enclaveid,
			PublicKey: key,
		}
		err = wgtask.doDelete()
		if err != nil {
			l2.Warnf("XDC wgif remove error: %+v", err)
		}

		// delete the attachment request from storage
		c := storage.NewXdcWgClient(&portal.AttachXDCRequest{Project: pid, Xdc: xid})
		err = c.Read()
		if err != nil {
			l2.Warnf("no xdc attach request found: %+v; ignoring", err)
			continue
		}

		_, err := c.Delete()
		if err != nil {
			l2.Warnf("could not delete xdc attach request: %+v", err)
		}
	}

	l.Info("wgscv/mtz: removing gateway WG ifx requests for enclave")

	// Delete all gateway keys
	for key, gw := range enc.Gateways {
		l2 := l.WithFields(log.Fields{
			"site": gw.Endpoint,
		})

		// synchronously remove the wgif for the XDC
		wgtask := &WgIfTask{
			Enclaveid: enc.Enclaveid,
			PublicKey: key,
		}
		err = wgtask.doDelete()
		if err != nil {
			l2.Warnf("Gateway wgif remove error: %+v", err)
		}

		// remove the request in storage
		wgif := storage.NewWgIfRequest(mid, key)
		err = wgif.Read()
		if err != nil {
			l2.Warnf("no ggateway wgif request found: %+v; ignoring", err)
			continue
		}

		_, err = wgif.Delete()
		if err != nil {
			l2.Warnf("could not delete wgif key: %+v", err)
		}
	}

	// Delete any ifreqs that aren't in the enclave yet
	err = storage.DeleteEnclaveWgIfreqs(enc.Enclaveid)
	if err != nil {
		l.Warn(err)
	}

	return nil
}

func (m *MtzTask) Create(value []byte, ver int64, td *reconcile.TaskData) *reconcile.TaskMessage {
	log.Infof("received reconcile Create request for mzid=%s", m.mzid())
	return m.doPut(value, ver, td)
}

func (m *MtzTask) Update(prev, value []byte, ver int64, td *reconcile.TaskData) *reconcile.TaskMessage {
	log.Infof("received reconcile Update request for mzid=%s", m.mzid())
	return m.doPut(value, ver, td)
}

func (m *MtzTask) Ensure(prev, value []byte, ver int64, td *reconcile.TaskData) *reconcile.TaskMessage {
	log.Infof("received reconcile Ensure request for mzid=%s", m.mzid())
	return m.doPut(value, ver, td)
}

func (m *MtzTask) Delete(value []byte, ver int64, td *reconcile.TaskData) *reconcile.TaskMessage {
	log.Infof("received reconcile Delete request for mzid=%s", m.mzid())

	err := m.doDelete()
	if err != nil {
		log.Error(err)
		return reconcile.TaskMessageError(err)
	}

	return nil
}
