package main

import (
	"fmt"

	log "github.com/sirupsen/logrus"
	wgd "gitlab.com/mergetb/api/wgd/v1/go"
	xdcd "gitlab.com/mergetb/api/xdcd/v1/go"
	"gitlab.com/mergetb/portal/services/internal"
	"google.golang.org/grpc"
)

func WgdClient(ep string) (*grpc.ClientConn, wgd.WgdClient, error) {
	l := log.WithFields(log.Fields{
		"endpoint": ep,
	})
	l.Trace("wgd grpc connect")

	conn, err := grpc.Dial(
		ep,
		grpc.WithInsecure(), // local portal service. no security needed?
		internal.GRPCMaxMessage,
	)
	if err != nil {
		l = l.WithField("err", err)
		l.Error("bad connect")
		return nil, nil, fmt.Errorf("error connecting to wgd: %+v", err)
	}

	return conn, wgd.NewWgdClient(conn), nil
}

func Wgd(ep string, f func(wgd.WgdClient) error) error {

	conn, cli, err := WgdClient(ep)
	if err != nil {
		return err
	}
	defer conn.Close()

	return f(cli)
}

func XdcdClient(svc string) (*grpc.ClientConn, xdcd.XdcdClient, error) {
	l := log.WithFields(log.Fields{
		"svc": svc,
	})
	l.Tracef("xdcd grpc connect")

	// TODO make this "xdc" and port configurable. The "xdc" is the namespace/project
	// that the xdcs run in.
	conn, err := grpc.Dial(svc+"."+xdcNs+":6000", grpc.WithInsecure()) // local service insecure ok?

	if err != nil {
		l = l.WithField("err", err)
		l.Error("bad connect")
		return nil, nil, fmt.Errorf("error connecting to xdcd: %+v", err)
	}

	return conn, xdcd.NewXdcdClient(conn), nil
}

func Xdcd(ep string, f func(xdcd.XdcdClient) error) error {

	conn, cli, err := XdcdClient(ep)
	if err != nil {
		return err
	}
	defer conn.Close()

	return f(cli)
}
