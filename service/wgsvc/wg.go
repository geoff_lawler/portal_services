package main

import (
	"context"
	"fmt"
	"net"
	"regexp"

	log "github.com/sirupsen/logrus"
	facility "gitlab.com/mergetb/api/facility/v1/go"
	portal "gitlab.com/mergetb/api/portal/v1/go"
	wgd "gitlab.com/mergetb/api/wgd/v1/go"
	"gitlab.com/mergetb/tech/reconcile"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/proto"

	"gitlab.com/mergetb/portal/services/pkg/connect"
	"gitlab.com/mergetb/portal/services/pkg/storage"
)

var (
	// ex: /wgif/rlz.exp.project/aBZeAL6DZyQ+NJavRobRrU9wrBa0LgABQyHIST5TLFI=

	wgifBucket = storage.PrefixedBucket(&storage.WgIfRequest{})
	wgifKey    = regexp.MustCompile("^" + wgifBucket + "([^/]+)/(.+)$")

	gatewayAllowedIPs = []string{"192.168.254.0/24", "172.30.0.0/16"}

	forceKeyRedistribution = false
)

type WgIfTask struct {
	Enclaveid string
	PublicKey string
	Etcdkey   string
}

func (w *WgIfTask) Parse(k string) bool {
	tkns := wgifKey.FindAllStringSubmatch(k, -1)
	if len(tkns) > 0 {
		w.Enclaveid = tkns[0][1]
		w.PublicKey = tkns[0][2]
		w.Etcdkey = k

		return true
	}

	return false
}

// Handle a new interface request for a client (for now, this is just XDCs)
//  1. Collect all gateways (sites) in the enclave
//  2. For each gateway, call out to the site to configure the new peer
func (w *WgIfTask) handleClientRequest(req *portal.AddWgIfConfigRequest) error {
	l := log.WithFields(log.Fields{
		"enclaveid": w.Enclaveid,
		"publickey": w.PublicKey,
		"endpoint":  req.Config.Endpoint,
	})

	// Read the wg enclave from storage. This is created at materialization time
	we, err := readOrCreateEnclave(w.Enclaveid)
	if err != nil {
		l.Error(err)
		return err
	}

	// If another key exists for this client, delete it
	enclaveHasKey := false
	for _, ifx := range we.Clients {
		if ifx.Endpoint == req.Config.Endpoint {
			if ifx.Key != w.PublicKey {
				l.WithField(
					"oldkey", ifx.Key,
				).Warn("found existing key for client while processing new key request. Will delete old key")

				wgif := storage.NewWgIfRequest(w.Enclaveid, ifx.Key)
				_, err = wgif.Delete()
				if err != nil {
					l.Error(err)
					return err
				}
			} else {
				enclaveHasKey = true
			}
		}
	}

	// If we already track this key in the enclave, then we have a decision to make about whether to
	// advertise it to all remote peers (sites).
	//
	// On the one hand, sending it doesn't hurt anything. The site can just observe that it already
	// has the peer configured, or blindly reconfigure it. Furthermore, if the site has a bug or
	// loses data somehow, redistributiung allows us to reconstruct tunnels that might other wise be
	// lost without an explicit re-key (e.g., detach/attach)
	//
	// On the other hand, this could lead to lots of needless RPCs in the common case and likely
	// slow down the wgsvc on service restarts when there are many XDCs running.
	//
	// So, we make this configurable via an env variable. It is disabled by default
	if enclaveHasKey {
		if forceKeyRedistribution {
			l.Warn("Forced key redistribution enabled. Processing AddWgIfConfigRequest even though client pubkey has already been advertised.")
		} else {
			l.Info("Forced key redistribution disabled. Skipping AddWgIfConfigRequest as client pubkey has already been advertised. If you wish to re-advertise all keys, set WG_FORCE_KEY_REDISTRIBUTION=1 in the wgsvc environment")
			return nil
		}
	} else {
		l.Info("processing new AddWgIfConfigRequest")
	}

	// Add the new client to each gateway
	var errs []error
	for site := range we.GatewayIps {
		l2 := l.WithField("site", site)

		// allowed IPs on this remote peer is just the XDC accessaddr

		cfg := *req.Config
		cfg.Allowedips = []string{fmt.Sprintf("%s/32", req.Config.Accessaddr)}

		l2.Debug("making facility RPC to add XDC peer to site WG ifx")
		err = connect.FacilityWGClient(
			site,
			func(cli facility.WireguardClient) error {
				_, err := cli.AddWgPeers(
					context.TODO(),
					&facility.AddWgPeersRequest{
						Enclaveid: we.Enclaveid,
						Configs: []*portal.WgIfConfig{
							&cfg,
						},
					},
				)
				return err
			},
		)

		if err != nil {
			// Don't stop just because one failure. Let others know regardless.
			err := fmt.Errorf("error adding peer to site: %+v", err)
			l2.Error(err)
			errs = append(errs, err)
			continue
		}
	}

	if len(errs) > 0 {
		return fmt.Errorf("%+v", errs)
	}

	// Add the if to the enclave for efficient lookup
	// This will nop if the client/pubkey already exists in the enclave
	we.AddIf = req

	// IS RECONCILED
	_, err = we.Update()
	if err != nil {
		return err
	}

	return nil
}

func gatewayEndpointEquals(endpointa, endpointb string) (bool, error) {
	hosta, _, err := net.SplitHostPort(endpointa)
	if err != nil {
		return false, err
	}

	hostb, _, err := net.SplitHostPort(endpointb)
	if err != nil {
		return false, err
	}

	return hosta == hostb, nil
}

// Handle a new interface request for a gateway (aka, site)
//  1. Collect all clients (XDCs) connected to the enclave
//  2. For each client, add the gateway config as a new peer
func (w *WgIfTask) handleGatewayRequest(req *portal.AddWgIfConfigRequest) error {
	l := log.WithFields(log.Fields{
		"enclaveid": w.Enclaveid,
		"publickey": w.PublicKey,
		"endpoint":  req.Config.Endpoint,
	})

	// Force gateway allowed IP "for now"
	req.Config.Allowedips = gatewayAllowedIPs

	// Read the wg enclave from storage. This is created at materialization time
	we, err := readOrCreateEnclave(w.Enclaveid)
	if err != nil {
		return err
	}

	// If another key exists for this gateway, delete it
	for _, ifx := range we.Gateways {
		equals, err := gatewayEndpointEquals(ifx.Endpoint, req.Config.Endpoint)
		if err != nil {
			l.Warn(err)
			continue
		}

		if equals && ifx.Key != w.PublicKey {
			l.WithField(
				"oldkey", ifx.Key,
			).Warn("found existing key for gateway while processing new key request. Will delete old key")

			wgif := storage.NewWgIfRequest(w.Enclaveid, ifx.Key)
			_, err = wgif.Delete()
			if err != nil {
				l.Error(err)
				return err
			}
		}
	}

	// Add the if to the enclave for efficient lookup
	we.AddIf = req

	// IS RECONCILED
	_, err = we.Update()
	if err != nil {
		return err
	}

	// Add the new gateway to each client.
	// Errors could occur if the XDC is in a state, or there is trouble reaching wgd.
	var errs []error
	for _, c := range we.Clients {
		l := log.WithFields(log.Fields{
			"enclaveid": w.Enclaveid,
			"publickey": w.PublicKey,
			"xdc":       c.Endpoint,
		})
		l.Debug("adding peer (gateway) to XDC")

		// Get the host on which the XDC is running from its name. Use this to contact the
		// wgd running on that node.
		wgdEp, cid, err := containerName2WgdData(c.Endpoint) // Endpoint is the XDC name.
		if err != nil {
			err := fmt.Errorf("error querying container ID for XDC: %+v", err)
			l.Error(err)
			errs = append(errs, err)
			continue
		}

		// Tell wgd to add the peer to its wg ifx
		err = wgdAddPeers(wgdEp, cid, []*portal.WgIfConfig{req.Config})
		if err != nil {
			// Don't stop just because one failure. Let others know regardless.
			err := fmt.Errorf("error adding peer to XDC: %+v", err)
			l.Error(err)
			errs = append(errs, err)
			continue
		}

		l.Info("successfully added peer (gateway) to XDC")
	}

	if len(errs) > 0 {
		return fmt.Errorf("%+v", errs)
	}

	return nil
}

func (w *WgIfTask) handleRequest(req *portal.AddWgIfConfigRequest) error {
	if req.Gateway {
		return w.handleGatewayRequest(req)
	} else {
		return w.handleClientRequest(req)
	}
}

func (w *WgIfTask) doDelete() error {
	l := log.WithFields(log.Fields{
		"enclaveid": w.Enclaveid,
		"publickey": w.PublicKey,
	})
	l.Debug("deleting wg if")

	we := storage.NewWgEnclave(w.Enclaveid)
	err := we.Read()
	if err != nil {
		l.Error(err)
		return err
	}

	if we.Ver == 0 {
		err := fmt.Errorf("wg enclave not found")
		l.Error(err)
		return err
	}

	gateway := false
	client := false
	var cfg *portal.WgIfConfig

	// find the client or gateway that issued this key
	for _, c := range we.Clients {
		if c.Key == w.PublicKey {
			client = true
			cfg = c
			break
		}
	}

	if !client {
		for _, g := range we.Gateways {
			if g.Key == w.PublicKey {
				gateway = true
				cfg = g
				break
			}
		}
	}

	if !client && !gateway {
		l.Debug("no enclave client or gateway tracks this key")
		return nil
	}

	// Errors may occur below if the materialization has been blown away
	// Warn and continue
	if gateway {
		// delete gateway if from all clients (XDCs)
		for _, c := range we.Clients {
			l2 := l.WithField("xdc", c.Endpoint)

			// Get the host on which the XDC is running from its name. Use this to contact the
			// wgd running on that node.
			wgdEp, cid, err := containerName2WgdData(c.Endpoint) // Endpoint is the XDC name.
			if err != nil {                                      // likely the XDC is not there anymore.
				l2.Warnf("Error getting container name from endpoint: %+v", err)
				continue
			}

			err = Wgd(
				wgdEp,
				func(cli wgd.WgdClient) error {
					_, err := cli.DeleteWgPeers(
						context.TODO(),
						&wgd.DeleteWgPeersRequest{
							Containerid: cid,
							Peers: []*portal.WgIfConfig{
								cfg,
							},
						},
					)
					return err
				},
			)
			if err != nil {
				// Don't stop just because one failure. Let others know regardless.
				l2.Warnf("could not delete WG peer in XDC: %+v", err)
			} else {
				l2.Info("successfully deleted peer (gateway) from XDC")
			}
		}

	} else {
		// delete client from all gateways
		for site := range we.GatewayIps {
			l2 := l.WithField("site", site)

			err = connect.FacilityWGClient(
				site,
				func(cli facility.WireguardClient) error {
					_, err := cli.DelWgPeer(
						context.TODO(),
						&facility.DelWgPeerRequest{
							Enclaveid: we.Enclaveid,
							Config:    cfg,
						},
					)
					return err
				},
			)

			// Don't stop just because one failure. Let others know regardless.
			if err != nil {
				// parse the error; ignore not found as this could be due to a full de-mtz
				if gerr, ok := status.FromError(err); ok {
					switch gerr.Code() {
					case codes.NotFound:
						// ok if not found
						l2.Debug("WG peer not found error: ignoring")
						err = nil
					default:
						l2.Errorf("Error telling site to delete WG peer: %+v", gerr.Message())
					}
				} else {
					l2.Warnf("Error telling site to delete WG peer: %+v", err)
				}
			}

			if err == nil {
				l2.Info("successfully sent DelWgPeer request to site")
			}
		}
	}

	// lastly, remove the cfg from the enclave
	req := &portal.AddWgIfConfigRequest{
		Enclaveid: w.Enclaveid,
		Config:    cfg,
		Gateway:   gateway,
	}

	we.DelIf = req

	// IS RECONCILED
	_, err = we.Update()
	if err != nil {
		l.Error(err)
	}

	return err
}

func (w *WgIfTask) doCreate(value []byte, version int64, td *reconcile.TaskData) *reconcile.TaskMessage {
	l := log.WithFields(log.Fields{
		"enclaveid": w.Enclaveid,
		"pubkey":    w.PublicKey,
	})

	// sanity check, reconcile package can be weird
	if value == nil {
		return reconcile.TaskMessageErrorf("cannot handle reconcile request: nil value")
	}

	req := new(portal.AddWgIfConfigRequest)
	err := proto.Unmarshal(value, req)
	if err != nil {
		return reconcile.TaskMessageError(err)
	}

	if req.Config == nil || req.Config.Endpoint == "" {
		return reconcile.TaskMessageErrorf("cannot handle reconcile request: no config or missing endpoint in config. req:%+v", req)
	}

	err = w.handleRequest(req)
	if err != nil {
		return reconcile.TaskMessageError(err)
	}

	l.Debugf("successfully handled reconcile request for wg if: %s/%s", w.Enclaveid, w.PublicKey)
	return nil
}

func (w *WgIfTask) Create(value []byte, version int64, td *reconcile.TaskData) *reconcile.TaskMessage {
	l := log.WithFields(log.Fields{
		"enclaveid": w.Enclaveid,
		"pubkey":    w.PublicKey,
	})
	l.Info("wgif reconcile create")

	return w.doCreate(value, version, td)
}

func (w *WgIfTask) Update(prev, value []byte, ver int64, td *reconcile.TaskData) *reconcile.TaskMessage {
	l := log.WithFields(log.Fields{
		"enclaveid": w.Enclaveid,
		"pubkey":    w.PublicKey,
	})
	l.Info("wgif reconcile update")

	return w.doCreate(value, ver, td)
}

func (w *WgIfTask) Ensure(prev, value []byte, ver int64, td *reconcile.TaskData) *reconcile.TaskMessage {
	l := log.WithFields(log.Fields{
		"enclaveid": w.Enclaveid,
		"pubkey":    w.PublicKey,
	})
	l.Info("wgif reconcile ensure")

	return w.doCreate(value, ver, td)
}

func (w *WgIfTask) Delete(value []byte, ver int64, td *reconcile.TaskData) *reconcile.TaskMessage {
	l := log.WithFields(log.Fields{
		"enclaveid": w.Enclaveid,
		"pubkey":    w.PublicKey,
	})
	l.Info("wgif reconcile delete")

	err := w.doDelete()
	if err != nil {
		l.Error(err)
	}

	return nil
}

func runWgifStartup() {
	log.Debug("running wgif startup routine")

	mtzs, err := storage.ListMaterializeRequests()
	if err != nil {
		log.Fatalf("failed to list mtz requests: %+v", err)
	}

	enclaves, err := storage.ListWgEnclaves()
	if err != nil {
		log.Fatalf("failed to list WG enclaves: %+v", err)
	}

	ifreqs, err := storage.ListWgIfreqs()
	if err != nil {
		log.Fatalf("failed to list WG ifreqs: %+v", err)
	}

	ifxToDelete := []*WgIfTask{}

	// assert gateways for all mtz requests
	for _, mtz := range mtzs {
		mtzTask := &MtzTask{
			Rid: mtz.Realization,
			Eid: mtz.Experiment,
			Pid: mtz.Project,
		}

		mtzTask.handlePut()
	}

	// Iterate through the enclave and verify that every key it tracks still has an associated
	// ifreq key in storage. The latter could have been deleted while we were offline
	for _, enc := range enclaves {
		l := log.WithFields(log.Fields{
			"enclaveid": enc.Enclaveid,
		})

		reqs, ok := ifreqs[enc.Enclaveid]
		if !ok {
			// no keys for this enclave in any ifreq; ok
			l.Debugf("no ifreqs in storage for enclave")
			continue
		}

		// walk through clients and verify that all keys they track still have an associated
		// ifreq in storage
		for pk := range enc.Clients {
			l2 := l.WithFields(log.Fields{
				"publickey": pk,
			})

			found := false
			for _, req := range reqs {
				if pk == req.Config.Key {
					found = true
					break
				}
			}

			if !found {
				l2.Warn("found client key with no matching ifreq; deleting the key from the enclave")
				ifxToDelete = append(ifxToDelete, &WgIfTask{
					Enclaveid: enc.Enclaveid,
					PublicKey: pk,
				})
			}
		}

		// likewise for gateways
		for pk := range enc.Gateways {
			l2 := l.WithFields(log.Fields{
				"publickey": pk,
			})

			found := false
			for _, req := range reqs {
				if pk == req.Config.Key {
					found = true
					break
				}
			}

			if !found {
				l2.Warn("found gateway key with no matching ifreq; deleting the key from the enclave")
				ifxToDelete = append(ifxToDelete, &WgIfTask{
					Enclaveid: enc.Enclaveid,
					PublicKey: pk,
				})
			}
		}
	}

	for _, ifx := range ifxToDelete {
		ifx.doDelete()
	}
}
