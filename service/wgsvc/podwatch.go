package main

import (
	"regexp"

	log "github.com/sirupsen/logrus"
	v1 "k8s.io/api/core/v1"

	"gitlab.com/mergetb/portal/services/pkg/podwatch"
	"gitlab.com/mergetb/portal/services/pkg/storage"
	"gitlab.com/mergetb/tech/reconcile"
)

var (
	attachedLabel = "attached"

	podPath     = "wgsvc"
	podBucket   = storage.PrefixedBucket(&storage.Pod{Path: podPath})
	podKey      = regexp.MustCompile("^" + podBucket + "(.+)$")
	workerCount = 4
)

func runPodWatch() {

	podwatch.InitPodWatch()

	pw := &podwatch.PodWatcher{
		OnPodAdd:       onPodAdd,
		OnPodUpdate:    onPodUpdate,
		OnPodDelete:    onPodDelete,
		WorkerPoolSize: workerCount,
		Namespace:      xdcNs,
	}

	pw.Watch()
}

func onPodAdd(pod *v1.Pod) {
	handlePod(pod, true)
}

func onPodDelete(pod *v1.Pod) {
	handlePod(pod, false)
}

func onPodUpdate(prevPod, newPod *v1.Pod) {

	handlePod(newPod, true)
}

// bridge this to a reconciler by writing keys
// this is important for task status to be done correctly
func handlePod(pod *v1.Pod, put bool) {

	t, err := podwatch.PodType(pod)
	if err != nil {
		log.Errorf("podtype: %v", err)
		return
	}

	if t != podwatch.XDC {
		log.Debugf("Ignoring new non-XDC (ssh-jump) pod")
		return
	}

	p := storage.NewPod(podPath, pod)

	if put {
		log.Infof("writing task: %s", p.Key())

		p.Ver = -1
		_, err = p.Update()
		if err != nil {
			log.Errorf("pod update: %v", err)
		}
	} else {
		log.Infof("deleting task: %s", p.Key())

		p.Ver = -1
		_, err = p.Delete()
		if err != nil {
			log.Errorf("pod delete: %v", err)
		}
	}

}

type WgPodWatch struct {
	Name string
}

func (w *WgPodWatch) Parse(k string) bool {
	tkns := podKey.FindAllStringSubmatch(k, -1)
	if len(tkns) > 0 {
		w.Name = tkns[0][1]

		return true
	}

	return false
}

func (w *WgPodWatch) Create(value []byte, version int64, td *reconcile.TaskData) *reconcile.TaskMessage {
	return w.handle(value, td)
}

func (w *WgPodWatch) Update(prev, value []byte, version int64, td *reconcile.TaskData) *reconcile.TaskMessage {
	return w.handle(value, td)
}

func (w *WgPodWatch) Ensure(prev, value []byte, version int64, td *reconcile.TaskData) *reconcile.TaskMessage {
	return w.handle(value, td)
}

func (w *WgPodWatch) Delete(value []byte, version int64, td *reconcile.TaskData) *reconcile.TaskMessage {
	// NOOP
	return nil
}

func (w *WgPodWatch) handle(value []byte, td *reconcile.TaskData) *reconcile.TaskMessage {
	if value == nil {
		return reconcile.TaskMessageErrorf("cannot handle podwatch request: nil value")
	}

	pod := new(v1.Pod)
	err := pod.Unmarshal(value)
	if err != nil {
		return reconcile.TaskMessageError(err)
	}

	name, proj, err := podwatch.PodName(pod)
	if err != nil {
		return reconcile.TaskMessageErrorf("podname: %v", err)
	}

	// Is the XDC already attached?
	xdc := storage.NewXDC("", name, proj)
	err = xdc.Read()
	if err != nil {
		return reconcile.TaskMessageErrorf("xdc read: %v", err)
	}

	if xdc.Materialization == "" {
		return reconcile.TaskMessageInfof("Ignoring new XDC as it was not previously attached")
	}

	// XDC is attached although the pod may not be. So reattach it.
	mtzLabel, ok := pod.Labels[attachedLabel]
	if ok && mtzLabel == xdc.Materialization {
		return reconcile.TaskMessageInfof("XDC pod %s.%s is already attached according to pod label", name, proj)
	}

	log.Infof("Reattaching XDC %s-%s to %s", xdc.Name, xdc.Project, xdc.Materialization)
	// log.Infof("[reattach] - pod status: %v", pod.Status)

	encId := xdc.Materialization

	//err = doAttach(xdc.Name, xdc.Project, encId, true) // attach with existing enclave data.
	err = doAttach(xdc.Name, xdc.Project, encId) // attach with existing enclave data.
	if err != nil {
		return reconcile.TaskMessageErrorf("attach error: %v", err)
	}

	// Finally add a label so we know we're attached. This is not really great, but it should work.
	err = podwatch.AddLabel(pod, attachedLabel, xdc.Materialization)
	if err != nil {
		return reconcile.TaskMessageErrorf("labling XDC as attached: %s", err)
	}

	return nil
}
