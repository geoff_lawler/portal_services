package main

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/mergetb/portal/services/pkg/storage"
)

func users() {
	u, err := storage.ListUsers()
	if err != nil {
		log.WithFields(log.Fields{"err": err}).Error("get users")
		return
	}
	numUsers.Set(float64(len(u)))
}
