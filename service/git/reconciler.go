package main

import (
	"fmt"
	"os"
	"regexp"

	"github.com/go-git/go-git/v5"
	log "github.com/sirupsen/logrus"
	"google.golang.org/protobuf/proto"

	portal "gitlab.com/mergetb/api/portal/v1/go"

	"gitlab.com/mergetb/portal/services/pkg/storage"

	"gitlab.com/mergetb/tech/reconcile"
)

var (
	nameExp  = "([a-zA-Z_]+[a-zA-Z0-9_]+)"
	xpBucket = storage.PrefixedBucket(&storage.Experiment{})
	xpKey    = regexp.MustCompile(
		"^" + xpBucket + nameExp + "/" + nameExp + "$",
	)
)

/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 * TODO
 *	- Ensure repos with no experiment get deleted on startup (runPollCheck)
 *!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/

func runReconciler() {

	err := storage.InitPortalEtcdClient()
	if err != nil {
		log.Fatalf("etcd client init: %v", err)
	}

	t := storage.ReconcilerConfigGit.ToReconcilerManager(
		&GitTask{},
	)

	t.Run()

}

type GitTask struct {
	Name       string
	Key        string
	Project    string
	Experiment string
	Repopath   string
}

func (t *GitTask) Parse(key string) bool {

	tkns := xpKey.FindAllStringSubmatch(key, -1)
	if len(tkns) == 0 {
		return false
	}

	t.Name = "git"
	t.Key = key
	t.Project = tkns[0][1]
	t.Experiment = tkns[0][2]
	t.Repopath = fmt.Sprintf("/var/git/%s/%s.git", t.Project, t.Experiment)

	return true

}

func (t *GitTask) Create(value []byte, version int64, td *reconcile.TaskData) *reconcile.TaskMessage {

	return reconcile.CheckErrorToMessage(handleExperimentUpdate(t, value, version))

}

func (t *GitTask) Update(prev, value []byte, version int64, td *reconcile.TaskData) *reconcile.TaskMessage {

	log.Infof("[%s] received update; treating as create", t.Key)
	return t.Create(value, version, td)

}

func (t *GitTask) Ensure(prev, value []byte, version int64, td *reconcile.TaskData) *reconcile.TaskMessage {

	log.Infof("[%s] received ensure; ignoring", t.Key)
	return reconcile.TaskMessageUndefined()

}

func (t *GitTask) Delete(value []byte, version int64, td *reconcile.TaskData) *reconcile.TaskMessage {

	log.Infof("[%s] handling experiment delete", t.Key)

	err := os.RemoveAll(t.Repopath)
	if err != nil {
		return reconcile.TaskMessageErrorf("remove repo error: %v", err)
	}

	return nil

}

type StatusTask struct {
	Name       string
	XpKey      string
	Key        string
	Project    string
	Experiment string
	Repopath   string
}

func handleExperimentUpdate(t *GitTask, value []byte, version int64) error {

	log.Infof("[%s] handling experiment update", t.Key)

	exp := new(portal.Experiment)
	err := proto.Unmarshal(value, exp)
	if err != nil {
		return fmt.Errorf("failed to unmarshal value: %v", err)
	}

	if exp.Project != t.Project || exp.Name != t.Experiment {
		return fmt.Errorf("[%s] key experiment/project (%s/%s) does not match value (%s/%s)",
			t.Key,
			t.Experiment, t.Project,
			exp.Name, exp.Project,
		)
	}

	fileinfo, err := os.Stat(t.Repopath)
	if err != nil {
		if !os.IsNotExist(err) {
			return fmt.Errorf("stat %s: %v", t.Repopath)
		}

		// if we cannot stat the path because it does not exist create the repo
		log.Infof("[%s] repo path for %s does not exist", t.Key, t.Repopath)
		err = initRepo(t.Key, t.Repopath, version)
		if err == nil {
			return nil
		}
	}

	// remove any files in the way
	if !fileinfo.Mode().IsDir() {
		err = os.RemoveAll(t.Repopath)
		if err != nil {
			return fmt.Errorf("remove file at repo path: %v", err)
		}

		log.Infof("[%s] path %s exists but is not a directory, clobbering", t.Key, t.Repopath)
		return initRepo(t.Key, t.Repopath, version)
	}

	// if the directory exists, test if it has the repo
	_, err = git.PlainOpen(t.Repopath)
	if err != nil {

		// if the directory does not contain a repository delete it and
		// initialize a new bare git repo

		log.Infof("[%s] path %s exists but is not a repo, clobbering", t.Key, t.Repopath)

		err = os.RemoveAll(t.Repopath)
		if err != nil {
			return fmt.Errorf("remove file at repo path: %v", err)
		}

		return initRepo(t.Key, t.Repopath, version)
	}

	log.Infof("[%s] repo exists, nothing to do", t.Key)

	return nil

}

func initRepo(key, path string, version int64) error {

	log.Infof("initializing repo at %s", path)

	repo, err := git.PlainInit(path, true)
	if err != nil {
		return fmt.Errorf("failed to init git repo: %v", err)
	}

	cfg, err := repo.Config()
	if err != nil {
		return fmt.Errorf("failed to get repo config: %v", err)
	}

	//TODO contribute this as a supported part of the config upstream
	cfg.Raw.Section("receive").AddOption("sharedrepository", "1")
	cfg.Raw.Section("core").AddOption("hooksPath", "/usr/bin/git-hooks")

	err = repo.SetConfig(cfg)
	if err != nil {
		return fmt.Errorf("failed to set repo config: %v", err)
	}

	return nil

}
