package main

import (
	"encoding/base64"
	"net/http"
	"net/http/cgi"
	"net/url"
	"strings"

	log "github.com/sirupsen/logrus"

	"gitlab.com/mergetb/portal/services/internal"
	id "gitlab.com/mergetb/portal/services/pkg/identity"
	"gitlab.com/mergetb/portal/services/pkg/policy"
	"gitlab.com/mergetb/portal/services/pkg/storage"
)

func init() {
	internal.InitLogging()
	internal.InitReconciler()
}

var (
	Version string
)

func main() {

	log.Infof("portal version: %s", Version)

	log.Info("Merge git server starting")

	err := storage.InitPortalEtcdClient()
	if err != nil {
		log.Fatalf("storage client init: %v", err)
	}

	go runReconciler()

	runcgi()

}

func runcgi() {

	/*
		log.Fatal(
			http.ListenAndServeTLS(
				":443",
				"/mgs/.cert/merge-git-server.pem",
				"/mgs/.cert/merge-git-server-key.pem",
				&AuthChecker{},
			),
		)
	*/

	log.Fatal(
		http.ListenAndServe(
			":8080",
			&AuthChecker{},
		),
	)

}

type AuthChecker struct{}

func (h *AuthChecker) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	log.Debugf("REQUEST: %s %s", r.Method, r.URL)
	log.Debugf("HEADERS:")
	for k, v := range r.Header {
		log.Debugf("  %s: %+v", k, v)
	}

	next := &cgi.Handler{
		Path: "/usr/libexec/git-core/git-http-backend",
		Dir:  "/var/git",
		Env: []string{
			"GIT_HTTP_EXPORT_ALL=1",
			"GIT_PROJECT_ROOT=/var/git",
		},
	}

	var project, experiment string
	parts := strings.Split(r.URL.Path, "/")
	if len(parts) < 3 { // 3 because of leading slash
		log.Errorf("expected /<project>/<experiment>/..., got: %s", r.URL.Path)
		w.WriteHeader(http.StatusNotFound)
	}
	project = parts[1]
	experiment = parts[2]

	// if there is no authorization header and this is a modifying operation
	// (i.e. git push) send back a 401 so the git client will send creds if
	// avail
	if isWrite(r.URL) {

		log.Debugf("parsing req for auth token: %+v", r)
		token, tokenType, err := id.AccessTokenFromHTTPRequest(r)

		log.Debugf("Got %s token: %s", tokenType, token)

		if token == "" {
			// the expectation is that clients are doing a
			//
			//   git clone https://<token>@git.mergetb.net/<project>/<experiment>
			//
			// and the way to get git to pass the <token> in the HTTP Authorization
			// header is to respond with a 401: WWW-Authenticate: Basic. Git does
			// not recognize 401: WWW-Authenticate: Bearer afaict.

			log.Infof("Found no auth token in request. Sending back request for auth.")

			w.Header().Add("WWW-Authenticate", "Basic")
			w.WriteHeader(http.StatusUnauthorized)

			//TODO remove this and just return with 401?
			next.ServeHTTP(w, r)
			return
		}

		// token is base64 encoded. Ory doesn't like that.
		if tokenType == id.Basic {
			tkn, err := base64.StdEncoding.DecodeString(token)
			if err != nil {
				log.Errorf("token base64 decode error: %v", err)
				w.WriteHeader(http.StatusUnauthorized)
				return
			}
			// trim the trailing `:`
			log.Debugf("pre trim token: %s", tkn)
			token = string(tkn[:len(tkn)-1])
		}
		if tokenType == id.Bearer {
			// grr. The cookie is stuffed into the Bearer for some reason.
			tokenType = id.Cookie
		}

		// get user info from kratos based on token
		s, err := id.SessionFromToken(token, tokenType)
		if err != nil {
			log.Errorf("auth session error: %s", err)
			w.WriteHeader(401) // or 5XX?
			return
		}

		ident, err := id.OrySessionToId(s)
		if err != nil {
			log.Errorf("no user id in auth session: %s", err)
			w.WriteHeader(401) // I guess?
			return
		}

		// check policy for this write
		err = policy.UpdateExperiment(&ident.Traits, project, experiment)
		if err != nil {
			log.Errorf("policy check failed: %v", err)
			w.WriteHeader(http.StatusForbidden)
			return
		}
	}

	// TODO check if anonymous read is allowed via policy

	// the REMOTE_USER variable tells git the client has been authenticated
	//
	//   https://github.com/git/git/blob/v2.29.2/http-backend.c#L274
	//
	next.Env = append(next.Env, "REMOTE_USER=bjalbo")
	next.ServeHTTP(w, r)
}

func checkToken(token string, isCookie bool) (string, int) {

	t := "token"
	if isCookie {
		t = "cookie"
	}

	if !isCookie {
		tkn, err := base64.StdEncoding.DecodeString(token)
		if err != nil {
			log.Errorf("token base64 decode error: %v", err)
			return "", http.StatusUnauthorized
		}
		// trim the trailing `:`
		log.Debugf("pre trim token: %s", tkn)
		token = string(tkn[:len(tkn)-1])
	}

	log.Debugf("checking auth of %s %s", t, token)

	// All of this needs to be redone to use ory go bindings now that they support
	// a cli API.
	log.Warn("cli auth currently borked. Needs to be updated")
	return "", http.StatusUnauthorized
}

func isWrite(u *url.URL) bool {

	if u.Query().Get("service") == "git-receive-pack" {
		return true
	}

	if strings.HasSuffix(u.Path, "/git-receive-pack") {
		return true
	}

	return false

}
