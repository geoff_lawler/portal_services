package main

import (
	"bufio"
	"context"
	"fmt"
	"os"
	"strings"

	log "github.com/sirupsen/logrus"
	"gitlab.com/mergetb/portal/services/pkg/storage"
)

//
// See https://git-scm.com/docs/git-http-backend for explanation of where PATH_INFO
// comes from.
//

func main() {

	err := storage.InitPortalEtcdClient()
	if err != nil {
		log.Fatalf("storage client init: %v", err)
	}

	// TODO ensure the below is a durable location or one under fluentd
	f, err := os.OpenFile("/var/log/git-post-receive.log", os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0644)
	if err != nil {
		log.Fatal(err)
	}
	log.SetOutput(f)

	scanner := bufio.NewScanner(os.Stdin)
	scanner.Scan()
	input := scanner.Text()
	parts := strings.Fields(input)
	if len(parts) != 3 {
		log.Fatalf("expected <oldrev> <newrev> <ref>, got %s", input)
	}
	rev := parts[1]
	ref := parts[2]

	log.Info(input)

	path := os.Getenv("PATH_INFO")
	if path == "" {
		log.Infof("%+v", os.Environ())
		log.Fatal("no PATH_INFO in env")
	}

	parts = strings.Split(path, "/")
	if len(parts) < 3 {
		log.Fatalf("expected path /<proj>/<exp>/..., found %s", path)
	}
	proj := parts[1]
	exp := parts[2]

	key := fmt.Sprintf("/experiments/%s/%s/rev/%s", proj, exp, rev)
	log.Infof("notifying %s at commit %s", key, rev)

	// We always create the rev entry.
	_, err = storage.EtcdClient.Put(
		context.TODO(),
		key,
		rev,
	)
	if err != nil {
		log.Fatalf("failed to put revision %s -> %s: %v", key, rev, err)
	}

	// Add the tag or branch as well. Tags look like "refs/tags/TAG". Branches
	// look like /refs/heads/BRANCH
	parts = strings.Split(ref, "/")
	if len(parts) == 3 {

		if parts[1] == "tags" {

			key := fmt.Sprintf("/experiments/%s/%s/tag/%s", proj, exp, parts[2])
			_, err = storage.EtcdClient.Put(
				context.TODO(),
				key,
				rev,
			)
			if err != nil {
				log.Fatalf("failed to put tag %s -> %s: %v", key, rev, err)
			}

		} else if parts[1] == "heads" {

			key := fmt.Sprintf("/experiments/%s/%s/branch/%s", proj, exp, parts[2])
			_, err = storage.EtcdClient.Put(
				context.TODO(),
				key,
				rev,
			)
			if err != nil {
				log.Fatalf("failed to put branch %s -> %s: %v", key, rev, err)
			}
		}
	}
}
