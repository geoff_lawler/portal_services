#!/bin/bash

curl -L \
    https://dl.min.io/client/mc/release/linux-amd64/mc.RELEASE.2021-03-23T05-46-11Z \
    -o /tmp/mc

chmod +x /tmp/mc
mv /tmp/mc /usr/local/bin/mc
