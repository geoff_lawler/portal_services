package main

import (
	"encoding/json"
	"fmt"
	"os/exec"
	"path/filepath"
	"strings"

	log "github.com/sirupsen/logrus"
)

// cid2NSName takes a container ID and returns the netns NAME
func cid2NSName(cid string) (string, error) {
	//
	// Cycle through a few ways to get the network namespace from the container id.
	//
	if _, err := exec.LookPath("crictl"); err == nil {
		return criCtl2Ns(cid)
	} else if _, err := exec.LookPath("ctr"); err == nil {
		return ctr2Ns(cid)
	}

	return "", fmt.Errorf("No way to get netns from container id.")
}

func ctr2Ns(cid string) (string, error) {

	// example cmd: ctr -n k8s.io c info $CID
	// This will return much json about the container. Parse out the
	// namespace from that.

	log.Info("converting containerdid into namespace. id: " + cid)
	out, err := RunOutput("ctr -n k8s.io c info " + cid)
	if err != nil {
		log.Info("error running ctr")
		return "", err
	}

	// Output is JSON, so build a type that matches the JSON
	// we want. Use that to parse out the data.
	type netNamespaceJSON struct {
		Spec struct {
			Linux struct {
				Namespaces []struct {
					Type string
					Path string
				}
			}
		}
	}

	var nnj netNamespaceJSON
	err = json.Unmarshal(out, &nnj)
	if err != nil {
		log.Info("json unmarshal")
		return "", err
	}

	for _, ns := range nnj.Spec.Linux.Namespaces {
		if ns.Type == "network" {

			tkns := strings.Split(ns.Path, string(filepath.Separator))
			if len(tkns) != 5 { // 5 as there's an "" token at the start.
				return "", fmt.Errorf("do not know how to parse ns path")
			}

			pid := tkns[2]
			out, err = RunOutput("ip netns identify " + pid)
			if err != nil {
				return "", err
			}

			return string(out), nil
		}
	}

	return "", fmt.Errorf("unable to find netns via ctr")
}

func criCtl2Ns(cid string) (string, error) {

	out, err := RunOutput("crictl inspect " + cid)
	if err != nil {
		return "", err
	}

	type nsJson struct {
		Info struct {
			RuntimeSpec struct {
				Linux struct {
					Namespaces []struct {
						Type string
						Path string
					}
				}
			}
		}
	}

	var nsj nsJson
	err = json.Unmarshal(out, &nsj)
	if err != nil {
		return "", err
	}

	// crictl namespace.Type == "network" is of the form
	// "path": "/var/run/netns/289f51e9-35b0-4f8e-8ceb-429d894892c6"
	// so we can just grab the netns directly
	for _, ns := range nsj.Info.RuntimeSpec.Linux.Namespaces {

		if ns.Type == "network" {

			tkns := strings.Split(ns.Path, "/")
			if len(tkns) == 5 {
				return tkns[4], nil
			}
		}
	}

	return "", fmt.Errorf("unable to find netns via crictl")
}
