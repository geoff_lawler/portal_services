package main

import (
	"fmt"
	"net"
	"strconv"
)

// WireguardEndpoint is just a small wrapper around net.UDPAddr fopr parsing.
type WgEndpoint net.UDPAddr

func WgEndpointParse(endpoint string) (*WgEndpoint, error) {
	host, port, err := net.SplitHostPort(endpoint)
	if err != nil {
		return nil, fmt.Errorf("bad endpoint: %s", endpoint)
	}

	ip := net.ParseIP(host)
	if ip == nil {
		// may be a net name?
		ipaddr, err := net.ResolveIPAddr("ip", host)
		if err != nil {
			return nil, fmt.Errorf("bad ip address. Host: %s", host)
		}
		ip = ipaddr.IP
	}

	intport, err := strconv.Atoi(port)
	if err != nil {
		return nil, fmt.Errorf("bad port")
	}
	return &WgEndpoint{Port: intport, IP: ip}, nil
}

func (wgep *WgEndpoint) String() string {
	u := net.UDPAddr{
		IP:   wgep.IP,
		Port: wgep.Port,
	}
	return u.String()
}
