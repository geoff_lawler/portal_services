package main

import (
	"bytes"
	"fmt"
	"io"
	"os/exec"
	"regexp"
	"strings"

	log "github.com/sirupsen/logrus"
)

// RunCommand ...
func RunCommand(c string) error {
	log.Debugf("Running command: %s\n", c)
	tokens := strings.Split(singleSpace(c), " ")
	cmd := exec.Command(tokens[0], tokens[1:]...)
	out, err := cmd.CombinedOutput()
	if err != nil {
		log.WithFields(log.Fields{
			"err":     err,
			"cmd":     c,
			"out/err": string(out),
		}).Error("error running cmd")
		log.Errorf(fmt.Sprint(err) + ": " + string(out))
		return err
	}
	return nil
}

// RunOutput - run a command and get the output. Assumes a single line of output.
func RunOutput(c string) ([]byte, error) {
	log.Debugf("Running output command: %s\n", c)
	tokens := strings.Split(singleSpace(c), " ")
	cmd := exec.Command(tokens[0], tokens[1:]...)
	out, err := cmd.Output()
	if err != nil {
		log.WithFields(log.Fields{
			"err": err,
			"cmd": c,
		}).Warn("error running cmd")
		return nil, err
	}
	log.Tracef("output: %s\n", out)
	return bytes.Trim(out, "\n"), nil
}

// InCommandOutput - put a []byte on stdin when running command c and
// return the output of the command. Assumes a single line of output.
func InCommandOutput(in []byte, c string) ([]byte, error) {
	log.Debugf("Running input/output command: %s\n", c)
	tokens := strings.Split(singleSpace(c), " ")
	cmd := exec.Command(tokens[0], tokens[1:]...)
	stdin, err := cmd.StdinPipe()
	if err != nil {
		return nil, err
	}

	go func() {
		defer stdin.Close()
		// not the best way to do this. is there a byte writer for stdin?
		io.WriteString(stdin, string(in[:]))
	}()

	out, err := cmd.Output()
	if err != nil {
		log.WithError(err).Warn("running cmd with stdin")
		return nil, err
	}
	log.Tracef("output: %s\n", out)
	return bytes.Trim(out, "\n"), nil
}

func singleSpace(c string) string {
	singleSpace := regexp.MustCompile(`\s+`)
	return singleSpace.ReplaceAllString(c, " ")
}
