This utility allows you to read and write Portal protobufs directly from and to 
an Portal etcd instance. Copy the built file to an etcd container and run it there.

Supported types are found in the `keyToObj` function and are: Project, Experiment,
User, XDC, Organization. Note that editing these directly is not generally useful as 
the data is usually in a few places. (Like list of experiments in a project is in 
the Project data and in /experiments/ etcd entries. Editing a single instance 
of a type will likely lead to inconsistencies in the data across etcd entries.

That being said this is useful for surgical strikes on data which cannot be easily
changed in other ways. Description entries on most everything for instance. Or 
deleting/adding data to a single type instance to remove data inconsistencies. For
instance an experimen is deleted, but the experiment is still in the project experiment
list for some reason. 

Reading data is always ok.

Help:
```
A utility to view and edit Portal protobufs in a Merge etcd database

Usage:
  mrgdb [command]

Available Commands:
  completion  Generate the autocompletion script for the specified shell
  help        Help about any command
  show        show merge database entries
  update      update merge database entries

Flags:
  -h, --help              help for mrgdb
  -l, --loglevel string   Level to log at. One of panic, fatal, error, warning, info, debug, trace (default "info")

Use "mrgdb [command] --help" for more information about a command.
```

Example of reading and writing a value:
```
$ kubectl cp build/mrgdb etcd-0:/tmp/
$ kubectl exec -it etcd-0 -- /tmp/mrgdb show key /projects/glawler |tee proj.json
{
  "name": "glawler",
  "members": {
    "glawler": {
      "role": "Creator",
      "state": "Active"
    }
  },
  "experiments": [
    "ingress",
    "phobos",
    "case",
    "crash"
  ],
  "gid": 1001,
  "ver": "4"
}
$ vim proj.json   # add a descripton
$ cat proj.json 
{
  "name": "glawler",
  "members": {
    "glawler": {
      "role": "Creator",
      "state": "Active"
    }
  },
  "description": "This is glawler's project",
  "experiments": [
    "ingress",
    "phobos",
    "case",
    "crash"
  ],
  "gid": 1001,
  "ver": "4"
}
$ kubectl cp proj.json etcd-0:/tmp/
$ kubectl exec -it etcd-0 -- /tmp/mrgdb update key /projects/glawler /tmp/proj.json
$ kubectl exec -it etcd-0 -- /tmp/mrgdb show key /projects/glawler
{
  "name": "glawler",
  "description": "This is glawler's project",
  "members": {
    "glawler": {
      "role": "Creator",
      "state": "Active"
    }
  },
  "experiments": [
    "ingress",
    "phobos",
    "case",
    "crash"
  ],
  "gid": 1001,
  "ver": "4"
}
```

The new description is added. `mrgdb` will refuse to update an object if the JSON 
does not match the protbuf definitions given in the Merge API here
https://gitlab.com/mergetb/api/-/tree/main/portal/v1/go.

glhf
