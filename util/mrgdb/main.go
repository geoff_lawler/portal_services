package main

import (
	"context"
	"fmt"
	"os"
	"strings"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	portal "gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/portal/services/pkg/storage"
	clientv3 "go.etcd.io/etcd/client/v3"
	"google.golang.org/protobuf/encoding/protojson"
	"google.golang.org/protobuf/proto"
)

var (
	version  = ""
	logLevel string
)

var root = &cobra.Command{
	Use:     "mrgdb",
	Short:   "A utility to view and edit Portal protobufs in a Merge etcd database",
	Version: version,
	PersistentPreRun: func(cmd *cobra.Command, args []string) {
		lvl, err := log.ParseLevel(logLevel)
		if err != nil {
			log.Fatalf("bad log level: %s", err)
		}
		log.SetLevel(lvl)
	},
}

var update = &cobra.Command{
	Use:   "update",
	Short: "update merge database entries",
}

var show = &cobra.Command{
	Use:   "show",
	Short: "show merge database entries",
}

func init() {

	err := storage.InitPortalEtcdClient()
	if err != nil {
		log.Fatalf("storage client init: %v", err)
	}
}

func main() {

	log.SetFormatter(&log.TextFormatter{
		DisableTimestamp: true,
	})

	cobra.EnablePrefixMatching = true

	root.PersistentFlags().StringVarP(
		&logLevel, "loglevel", "l", "info", "Level to log at. One of "+strings.Join(logLevels(), ", "),
	)

	updateCmds()
	showCmds()

	root.AddCommand(update)
	root.AddCommand(show)

	root.Execute()
}

func updateCmds() {

	key := &cobra.Command{
		Use:   "key [key] [path to json data]",
		Short: "update the data at the given etcd key. file must be valid JSON data (pref. read via mrgdb show key ...).",
		Args:  cobra.ExactArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			updateKey(args[0], args[1])
		},
	}

	update.AddCommand(key)
}

func showCmds() {

	s := &cobra.Command{
		Use:   "key [full etcd key]",
		Short: "show the data in JSON at the given etcd key",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			showKey(args[0])
		},
	}

	show.AddCommand(s)
}

func updateKey(key, file string) {

	kvc := clientv3.NewKV(storage.EtcdClient)
	resp, err := kvc.Get(context.TODO(), key)
	if err != nil {
		log.Fatalf("get key: %v", err)
		os.Exit(1)
	}

	if len(resp.Kvs) == 0 {
		log.Fatalf("no data found for key %s", key)
	}

	// TODO: support reading from stdin for piping: cat file | mrgdb update key
	jbuf, err := os.ReadFile(file)
	if err != nil {
		log.Fatalf("read file: %v", err)
	}

	obj, err := keyToObj(key)
	if err != nil {
		log.Fatalf(err.Error())
	}

	m, ok := obj.Value().(proto.Message)
	if !ok {
		log.Fatalf("object is not a protobuf")
	}

	// JSON to protobuf
	err = protojson.Unmarshal(jbuf, m)
	if err != nil {
		log.Fatalf("unmarshal JSON -> protobuf error: %v", err)
	}

	// protobuf to byte[]
	buf, err := proto.Marshal(m)
	if err != nil {
		log.Fatalf("marshal protobuf -> []byte error: %v", err)
	}

	_, err = kvc.Put(context.TODO(), key, string(buf))
	if err != nil {
		log.Fatalf("etcd put error: %v", err)
	}
}

func showKey(key string) {

	kvc := clientv3.NewKV(storage.EtcdClient)
	resp, err := kvc.Get(context.TODO(), key)
	if err != nil {
		log.Fatalf("get key: %v", err)
		os.Exit(1)
	}

	if len(resp.Kvs) == 0 {
		log.Fatalf("no data found for key %s", key)
	}

	for _, kv := range resp.Kvs {

		obj, err := keyToObj(key)
		if err != nil {
			log.Fatalf(err.Error())
		}

		m, ok := obj.Value().(proto.Message)
		if !ok {
			log.Fatalf("object is not a protobuf")
		}

		err = proto.Unmarshal(kv.Value, m)
		if err != nil {
			log.Fatalf("unmarshal error: %v", err)
		}

		fmt.Println(protojson.Format(m))
	}
}

func keyToObj(key string) (storage.ObjectIO, error) {

	if strings.HasPrefix(key, "/projects/") {
		log.Debugf("project key found: %s", key)
		return &storage.Project{Project: new(portal.Project)}, nil
	} else if strings.HasPrefix(key, "/experiments/") {
		log.Debugf("experiment key found: %s", key)
		return &storage.Experiment{Experiment: new(portal.Experiment)}, nil
	} else if strings.HasPrefix(key, "/users/") {
		log.Debugf("users key found: %s", key)
		return &storage.User{User: new(portal.User)}, nil
	} else if strings.HasPrefix(key, "/xdcs/") {
		log.Debugf("xdc key found: %s", key)
		return &storage.XDC{XDCStorage: new(portal.XDCStorage)}, nil
	} else if strings.HasPrefix(key, "/organizations/") {
		log.Debugf("organization key found: %s", key)
		return &storage.Organization{Organization: new(portal.Organization)}, nil
	} else if strings.HasPrefix(key, "/realizations/") {
		log.Debugf("realization request key found: %s", key)
		return &storage.RealizeRequest{RealizeRequest: new(portal.RealizeRequest)}, nil
	}

	return nil, fmt.Errorf("Do not know how to parse %s", key)
}

func logLevels() []string {
	r := []string{}
	for _, l := range log.AllLevels {
		r = append(r, l.String())
	}
	return r
}
