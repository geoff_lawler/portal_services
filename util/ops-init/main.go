package main

import (
	"context"
	"fmt"
	"os"
	"strings"

	log "github.com/sirupsen/logrus"
	portal "gitlab.com/mergetb/api/portal/v1/go"
	"google.golang.org/grpc"
)

func main() {

	if len(os.Args) != 4 {
		log.Fatal("usage: ops-init <username> <email> <password>")
	}

	log.Println("initializing ops user")

	conn, err := grpc.Dial(
		fmt.Sprintf("%s:%d", "identity", 6000),
		grpc.WithInsecure(),
	)
	if err != nil {
		log.Fatalf("grpc dial: %v", err)
	}
	defer conn.Close()

	cli := portal.NewIdentityClient(conn)

	_, err = cli.Register(context.TODO(), &portal.RegisterRequest{
		Username: os.Args[1],
		Email:    os.Args[2],
		Password: os.Args[3],
		Admin:    true,
		Traits: map[string]string{
			"affiliation": "USC/ISI",
			"position":    "Portal Admin",
		},
	})

	if err != nil {
		// TODO I think Kratos will return a 409 if the user already exists, so
		// we should propagate that back from the identity service in a
		// structured way and detect here instead of string grepping
		//
		//     https://www.ory.sh/kratos/docs/reference/api/#create-an-identity
		//
		if !strings.Contains(err.Error(), "exists already") {
			log.Fatal(err)
		}
	}

}
