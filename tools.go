// +build tools

package tools

import (
	_ "github.com/go-openapi/swag"
	_ "google.golang.org/protobuf/cmd/protoc-gen-go"
	_ "github.com/goware/modvendor"
	_ "github.com/grpc-ecosystem/grpc-gateway/v2/protoc-gen-grpc-gateway"
	_ "github.com/grpc-ecosystem/grpc-gateway/v2/protoc-gen-openapiv2"
	_ "github.com/mwitkow/go-proto-validators"
	//_ "google.golang.org/grpc/cmd/protoc-gen-go-grpc"
)
