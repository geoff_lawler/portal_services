package internal

import (
	"os"
	"strings"

	log "github.com/sirupsen/logrus"
)

var (
	MonitorEtcdServer bool = true
)

func InitLogging() {

	if value, ok := os.LookupEnv("LOGFORMAT"); ok {
		switch strings.ToLower(value) {
		case "json":
			log.SetFormatter(&log.JSONFormatter{})
		case "text":
			log.SetFormatter(&log.TextFormatter{})
		}
	}

	// Support for setting log level via environment
	// default is at info level
	if value, ok := os.LookupEnv("LOGLEVEL"); ok {
		lvl, err := log.ParseLevel(value)
		if err != nil {
			log.SetLevel(log.InfoLevel)
			log.Errorf("bad LOGLEVEL env var: %s. ignoring", value)
		} else {
			log.Infof("setting log level to %s", value)
			log.SetLevel(lvl)
		}
	}
}

func initHeartbeatMonitor() {
	// individual reconcilers can disable etcd heartbeat monitoring by setting this value to 1
	value, ok := os.LookupEnv("DISABLE_ETCD_HEARTBEAT_MONITOR")
	if ok && value == "1" {
		log.Info("detected DISABLE_ETCD_HEARTBEAT_MONITOR=1. Disabling etcd heartbeat monitoring")
		MonitorEtcdServer = false
	}
}

func InitReconciler() {
	initHeartbeatMonitor()
}
