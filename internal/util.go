package internal

import (
	"fmt"
	"strings"
)

type Mzid struct {
	Rid string
	Eid string
	Pid string
}

func MzidFromString(mzid string) (*Mzid, error) {

	parts := strings.Split(mzid, ".")
	if len(parts) != 3 {
		return nil, fmt.Errorf("malformed mzid: %s", mzid)
	}

	return &Mzid{
		Rid: parts[0],
		Eid: parts[1],
		Pid: parts[2],
	}, nil

}

func MzidToString(m *Mzid) string {
	return fmt.Sprintf("%s.%s.%s", m.Rid, m.Eid, m.Pid)
}
